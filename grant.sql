﻿IF NOT EXISTS (SELECT name FROM sys.server_principals WHERE name = 'IIS APPPOOL\nomanaged')
BEGIN
    CREATE LOGIN [IIS APPPOOL\nomanaged] 
      FROM WINDOWS WITH DEFAULT_DATABASE=[master], 
      DEFAULT_LANGUAGE=[us_english]
END
GO
CREATE USER [cpdUser] 
  FOR LOGIN [IIS APPPOOL\nomanaged]
GO
EXEC sp_addrolemember 'db_owner', 'cpdUser'
GO