using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using MarketPlace.Entities.Models;

namespace MarketPlace.Entities.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160318064309_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MarketPlace.Entities.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<int?>("CompanyId");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Auction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ClosingTime");

                    b.Property<bool>("IsOpen");

                    b.Property<DateTime>("OpeningTime");

                    b.Property<int>("OrderId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Auctions");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.AuctionProposal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Accepted");

                    b.Property<int>("AuctionId");

                    b.Property<decimal>("Price");

                    b.Property<DateTime>("ProposalTime");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AuctionРroposals");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.CallBack", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CallerName");

                    b.Property<string>("Comments");

                    b.Property<string>("ConvenientTime");

                    b.Property<DateTime>("Created");

                    b.Property<string>("PhoneNumber");

                    b.Property<DateTime>("Updated");

                    b.Property<string>("UserId");

                    b.Property<DateTime?>("WhenCall");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "CallBacks");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ClientProfile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<int>("Balance");

                    b.Property<int?>("ConsultantProfileId");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("LastName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("MiddleName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ClientProfiles");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("GuaranteeTerm")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.Property<int>("PrepaymentTermPercentage");

                    b.Property<int>("Rating");

                    b.Property<string>("SupplyTerm")
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Companies");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantBusyPeriod", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comments");

                    b.Property<int>("ConsultantId");

                    b.Property<int?>("ConsultantVisitId");

                    b.Property<DateTime>("From");

                    b.Property<DateTime>("To");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ConsultantBusyPeriods");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantProfile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateOfEmployment");

                    b.Property<DateTime?>("DateOfRemoval");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("LastName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("MiddleName")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("Rating");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ConsultantProfiles");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantVisit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<DateTime?>("CancelledDateTime");

                    b.Property<int?>("ConsultantBusyPeriodId");

                    b.Property<DateTime>("Create");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Phone");

                    b.Property<string>("UserId");

                    b.Property<int>("VisitRating");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ConsultantVisits");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Feedback", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FeedbackComment");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Feedbacks");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.File", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileComment");

                    b.Property<string>("FileName");

                    b.Property<string>("FilePath");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Log", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LogTypeId");

                    b.Property<string>("Message")
                        .HasAnnotation("MaxLength", 1024);

                    b.Property<DateTime>("Time");

                    b.Property<string>("Url")
                        .HasAnnotation("MaxLength", 1024);

                    b.Property<string>("UserId")
                        .HasAnnotation("MaxLength", 450);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Logs");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.LogType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "LogTypes");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Message", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("IsNew");

                    b.Property<int>("LinkedTo");

                    b.Property<DateTime>("ReadAt");

                    b.Property<string>("RecieverId");

                    b.Property<string>("SenderId");

                    b.Property<string>("Text")
                        .HasAnnotation("MaxLength", 128);

                    b.Property<string>("Title")
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Messages");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int>("ConsultantProfileId");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 128);

                    b.Property<string>("Notes")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<DateTime>("OrderDate");

                    b.Property<int?>("OrderExecutionRatingId");

                    b.Property<int>("OrderStateId");

                    b.Property<int?>("TenderId");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Orders");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.OrderExecutionRating", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyWinnerRating");

                    b.Property<int>("ConsultantRating");

                    b.Property<int>("OrderId");

                    b.Property<int>("ServiceRating");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "OrderExecutionRatings");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.OrderState", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "OrderStates");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 128);

                    b.Property<int>("OrderId");

                    b.Property<int>("ProductCategoryId");

                    b.Property<int>("Quantity");

                    b.Property<string>("SpecificationData")
                        .HasAnnotation("Relational:ColumnType", "ntext");

                    b.Property<int>("ViewOrder");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "Products");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductCategories");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductDoor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Height");

                    b.Property<bool>("Peephole");

                    b.Property<int>("ProductId");

                    b.Property<int>("Width");

                    b.HasKey("Id");

                    b.HasIndex("ProductId")
                        .IsUnique();

                    b.HasAnnotation("Relational:TableName", "ProductDoors");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductFile", b =>
                {
                    b.Property<int>("FileId");

                    b.Property<int>("ProductId");

                    b.HasKey("FileId", "ProductId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductTenderProposal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 2048);

                    b.Property<decimal>("Price");

                    b.Property<int>("ProductId");

                    b.Property<int>("TenderProposalId");

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductTenderProposals");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindow", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("CleaningDismantling");

                    b.Property<bool>("Delivery");

                    b.Property<int>("Height");

                    b.Property<bool>("Installation");

                    b.Property<bool>("Measurement");

                    b.Property<int>("ProductId");

                    b.Property<int>("ProductWindowBrandId");

                    b.Property<int>("ProductWindowFindingId");

                    b.Property<int>("ProductWindowLaminatingId");

                    b.Property<int>("ProductWindowSystemId");

                    b.Property<int>("ProductWindowTintingId");

                    b.Property<int>("ProductWindowsillId");

                    b.Property<int>("Width");

                    b.HasKey("Id");

                    b.HasIndex("ProductId")
                        .IsUnique();

                    b.HasAnnotation("Relational:TableName", "ProductWindows");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowBrand", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Image");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowBrands");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowFinding", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowFindings");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowGlazing", b =>
                {
                    b.Property<int>("ProductWindowGlazingTypeId");

                    b.Property<int>("ProductWindowId");

                    b.HasKey("ProductWindowGlazingTypeId", "ProductWindowId");

                    b.HasAnnotation("Relational:TableName", "ProductWindowGlazings");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowGlazingType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowGlazingTypes");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowLaminating", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowLaminatings");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowsill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowsills");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowSystem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("ProductWindowBrandId");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowSystems");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowTinting", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 128);

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "ProductWindowTintings");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Tender", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ClosingTime");

                    b.Property<bool>("IsOpen");

                    b.Property<DateTime>("OpeningTime");

                    b.Property<int>("OrderId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.TenderProposal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("GuaranteeTerm")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<bool>("IsWinner");

                    b.Property<string>("Notes")
                        .HasAnnotation("MaxLength", 200);

                    b.Property<int>("PrepaymentTermPercentage");

                    b.Property<decimal>("Price");

                    b.Property<string>("SupplyTerm")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int>("TenderId");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ApplicationUser", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Auction", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Order")
                        .WithMany()
                        .HasForeignKey("OrderId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.AuctionProposal", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Auction")
                        .WithMany()
                        .HasForeignKey("AuctionId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.CallBack", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ClientProfile", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ConsultantProfile")
                        .WithMany()
                        .HasForeignKey("ConsultantProfileId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantBusyPeriod", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ConsultantProfile")
                        .WithMany()
                        .HasForeignKey("ConsultantId");

                    b.HasOne("MarketPlace.Entities.Models.ConsultantVisit")
                        .WithOne()
                        .HasForeignKey("MarketPlace.Entities.Models.ConsultantBusyPeriod", "ConsultantVisitId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantProfile", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ConsultantVisit", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Feedback", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Log", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.LogType")
                        .WithMany()
                        .HasForeignKey("LogTypeId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Message", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("RecieverId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("SenderId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Order", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ConsultantProfile")
                        .WithMany()
                        .HasForeignKey("ConsultantProfileId");

                    b.HasOne("MarketPlace.Entities.Models.OrderExecutionRating")
                        .WithMany()
                        .HasForeignKey("OrderExecutionRatingId");

                    b.HasOne("MarketPlace.Entities.Models.OrderState")
                        .WithMany()
                        .HasForeignKey("OrderStateId");

                    b.HasOne("MarketPlace.Entities.Models.Tender")
                        .WithOne()
                        .HasForeignKey("MarketPlace.Entities.Models.Order", "TenderId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.Product", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Order")
                        .WithMany()
                        .HasForeignKey("OrderId");

                    b.HasOne("MarketPlace.Entities.Models.ProductCategory")
                        .WithMany()
                        .HasForeignKey("ProductCategoryId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductDoor", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductFile", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.File")
                        .WithMany()
                        .HasForeignKey("FileId");

                    b.HasOne("MarketPlace.Entities.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductTenderProposal", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("MarketPlace.Entities.Models.TenderProposal")
                        .WithMany()
                        .HasForeignKey("TenderProposalId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindow", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowBrand")
                        .WithMany()
                        .HasForeignKey("ProductWindowBrandId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowFinding")
                        .WithMany()
                        .HasForeignKey("ProductWindowFindingId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowLaminating")
                        .WithMany()
                        .HasForeignKey("ProductWindowLaminatingId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowSystem")
                        .WithMany()
                        .HasForeignKey("ProductWindowSystemId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowTinting")
                        .WithMany()
                        .HasForeignKey("ProductWindowTintingId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindowsill")
                        .WithMany()
                        .HasForeignKey("ProductWindowsillId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowGlazing", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ProductWindowGlazingType")
                        .WithMany()
                        .HasForeignKey("ProductWindowGlazingTypeId");

                    b.HasOne("MarketPlace.Entities.Models.ProductWindow")
                        .WithMany()
                        .HasForeignKey("ProductWindowId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.ProductWindowSystem", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ProductWindowBrand")
                        .WithMany()
                        .HasForeignKey("ProductWindowBrandId");
                });

            modelBuilder.Entity("MarketPlace.Entities.Models.TenderProposal", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.Company")
                        .WithMany()
                        .HasForeignKey("CompanyId");

                    b.HasOne("MarketPlace.Entities.Models.Tender")
                        .WithMany()
                        .HasForeignKey("TenderId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("MarketPlace.Entities.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
