using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace MarketPlace.Entities.Migrations
{
    public partial class Initial_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Auction_Order_OrderId", table: "Auctions");
            migrationBuilder.DropForeignKey(name: "FK_AuctionProposal_Auction_AuctionId", table: "AuctionРroposals");
            migrationBuilder.DropForeignKey(name: "FK_ConsultantBusyPeriod_ConsultantProfile_ConsultantId", table: "ConsultantBusyPeriods");
            migrationBuilder.DropForeignKey(name: "FK_Log_LogType_LogTypeId", table: "Logs");
            migrationBuilder.DropForeignKey(name: "FK_Order_ConsultantProfile_ConsultantProfileId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Order_OrderState_OrderStateId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Order_ApplicationUser_UserId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Product_Order_OrderId", table: "Products");
            migrationBuilder.DropForeignKey(name: "FK_Product_ProductCategory_ProductCategoryId", table: "Products");
            migrationBuilder.DropForeignKey(name: "FK_ProductDoor_Product_ProductId", table: "ProductDoors");
            migrationBuilder.DropForeignKey(name: "FK_ProductTenderProposal_Product_ProductId", table: "ProductTenderProposals");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_Product_ProductId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId", table: "ProductWindowGlazings");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowGlazing_ProductWindow_ProductWindowId", table: "ProductWindowGlazings");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId", table: "ProductWindowSystems");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_Company_CompanyId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_Tender_TenderId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_ApplicationUser_UserId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddColumn<string>(
                name: "SpecificationData",
                table: "ProductTenderProposals",
                type: "ntext",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "CallBacks",
                nullable: true);
            migrationBuilder.AddColumn<string>(
                name: "Target",
                table: "CallBacks",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_Auction_Order_OrderId",
                table: "Auctions",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_AuctionProposal_Auction_AuctionId",
                table: "AuctionРroposals",
                column: "AuctionId",
                principalTable: "Auctions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ConsultantBusyPeriod_ConsultantProfile_ConsultantId",
                table: "ConsultantBusyPeriods",
                column: "ConsultantId",
                principalTable: "ConsultantProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Log_LogType_LogTypeId",
                table: "Logs",
                column: "LogTypeId",
                principalTable: "LogTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_ConsultantProfile_ConsultantProfileId",
                table: "Orders",
                column: "ConsultantProfileId",
                principalTable: "ConsultantProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_OrderState_OrderStateId",
                table: "Orders",
                column: "OrderStateId",
                principalTable: "OrderStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_ApplicationUser_UserId",
                table: "Orders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Order_OrderId",
                table: "Products",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_ProductCategory_ProductCategoryId",
                table: "Products",
                column: "ProductCategoryId",
                principalTable: "ProductCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductDoor_Product_ProductId",
                table: "ProductDoors",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductTenderProposal_Product_ProductId",
                table: "ProductTenderProposals",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_Product_ProductId",
                table: "ProductWindows",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId",
                table: "ProductWindows",
                column: "ProductWindowBrandId",
                principalTable: "ProductWindowBrands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId",
                table: "ProductWindows",
                column: "ProductWindowFindingId",
                principalTable: "ProductWindowFindings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId",
                table: "ProductWindows",
                column: "ProductWindowLaminatingId",
                principalTable: "ProductWindowLaminatings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId",
                table: "ProductWindowGlazings",
                column: "ProductWindowGlazingTypeId",
                principalTable: "ProductWindowGlazingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowGlazing_ProductWindow_ProductWindowId",
                table: "ProductWindowGlazings",
                column: "ProductWindowId",
                principalTable: "ProductWindows",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId",
                table: "ProductWindowSystems",
                column: "ProductWindowBrandId",
                principalTable: "ProductWindowBrands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_Company_CompanyId",
                table: "TenderProposal",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_Tender_TenderId",
                table: "TenderProposal",
                column: "TenderId",
                principalTable: "Tender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_ApplicationUser_UserId",
                table: "TenderProposal",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Auction_Order_OrderId", table: "Auctions");
            migrationBuilder.DropForeignKey(name: "FK_AuctionProposal_Auction_AuctionId", table: "AuctionРroposals");
            migrationBuilder.DropForeignKey(name: "FK_ConsultantBusyPeriod_ConsultantProfile_ConsultantId", table: "ConsultantBusyPeriods");
            migrationBuilder.DropForeignKey(name: "FK_Log_LogType_LogTypeId", table: "Logs");
            migrationBuilder.DropForeignKey(name: "FK_Order_ConsultantProfile_ConsultantProfileId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Order_OrderState_OrderStateId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Order_ApplicationUser_UserId", table: "Orders");
            migrationBuilder.DropForeignKey(name: "FK_Product_Order_OrderId", table: "Products");
            migrationBuilder.DropForeignKey(name: "FK_Product_ProductCategory_ProductCategoryId", table: "Products");
            migrationBuilder.DropForeignKey(name: "FK_ProductDoor_Product_ProductId", table: "ProductDoors");
            migrationBuilder.DropForeignKey(name: "FK_ProductTenderProposal_Product_ProductId", table: "ProductTenderProposals");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_Product_ProductId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId", table: "ProductWindows");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId", table: "ProductWindowGlazings");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowGlazing_ProductWindow_ProductWindowId", table: "ProductWindowGlazings");
            migrationBuilder.DropForeignKey(name: "FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId", table: "ProductWindowSystems");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_Company_CompanyId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_Tender_TenderId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_TenderProposal_ApplicationUser_UserId", table: "TenderProposal");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "SpecificationData", table: "ProductTenderProposals");
            migrationBuilder.DropColumn(name: "Email", table: "CallBacks");
            migrationBuilder.DropColumn(name: "Target", table: "CallBacks");
            migrationBuilder.AddForeignKey(
                name: "FK_Auction_Order_OrderId",
                table: "Auctions",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AuctionProposal_Auction_AuctionId",
                table: "AuctionРroposals",
                column: "AuctionId",
                principalTable: "Auctions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ConsultantBusyPeriod_ConsultantProfile_ConsultantId",
                table: "ConsultantBusyPeriods",
                column: "ConsultantId",
                principalTable: "ConsultantProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Log_LogType_LogTypeId",
                table: "Logs",
                column: "LogTypeId",
                principalTable: "LogTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_ConsultantProfile_ConsultantProfileId",
                table: "Orders",
                column: "ConsultantProfileId",
                principalTable: "ConsultantProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_OrderState_OrderStateId",
                table: "Orders",
                column: "OrderStateId",
                principalTable: "OrderStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Order_ApplicationUser_UserId",
                table: "Orders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_Order_OrderId",
                table: "Products",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Product_ProductCategory_ProductCategoryId",
                table: "Products",
                column: "ProductCategoryId",
                principalTable: "ProductCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductDoor_Product_ProductId",
                table: "ProductDoors",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductTenderProposal_Product_ProductId",
                table: "ProductTenderProposals",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_Product_ProductId",
                table: "ProductWindows",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId",
                table: "ProductWindows",
                column: "ProductWindowBrandId",
                principalTable: "ProductWindowBrands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId",
                table: "ProductWindows",
                column: "ProductWindowFindingId",
                principalTable: "ProductWindowFindings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId",
                table: "ProductWindows",
                column: "ProductWindowLaminatingId",
                principalTable: "ProductWindowLaminatings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId",
                table: "ProductWindowGlazings",
                column: "ProductWindowGlazingTypeId",
                principalTable: "ProductWindowGlazingTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowGlazing_ProductWindow_ProductWindowId",
                table: "ProductWindowGlazings",
                column: "ProductWindowId",
                principalTable: "ProductWindows",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId",
                table: "ProductWindowSystems",
                column: "ProductWindowBrandId",
                principalTable: "ProductWindowBrands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_Company_CompanyId",
                table: "TenderProposal",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_Tender_TenderId",
                table: "TenderProposal",
                column: "TenderId",
                principalTable: "Tender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_TenderProposal_ApplicationUser_UserId",
                table: "TenderProposal",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
