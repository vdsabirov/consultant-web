﻿
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Metadata.Internal;
using System;
using System.Linq;

namespace MarketPlace.Entities.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderState> OrderStates { get; set; }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }

        public DbSet<ProductDoor> ProductDoors { get; set; }
        public DbSet<ProductWindow> ProductWindows { get; set; }

        public DbSet<Log> Logs { get; set; }
        public DbSet<LogType> LogTypes { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Auction> Auctions { get; set; }
        public DbSet<AuctionProposal> AuctionРroposals { get; set; }

        public DbSet<ProductWindowBrand> ProductWindowBrands { get; set; }
        public DbSet<ProductWindowFinding> ProductWindowFindings { get; set; }
        public DbSet<ProductWindowGlazingType> ProductWindowGlazingTypes { get; set; }
        public DbSet<ProductWindowLaminating> ProductWindowLaminatings { get; set; }
        public DbSet<ProductWindowsill> ProductWindowsills { get; set; }
        public DbSet<ProductWindowSystem> ProductWindowSystems { get; set; }
        public DbSet<ProductWindowTinting> ProductWindowTintings { get; set; }
        public DbSet<ProductWindowGlazing> ProductWindowGlazings { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<ConsultantProfile> ConsultantProfiles { get; set; }

        public DbSet<ClientProfile> ClientProfiles { get; set; }

        public DbSet<ConsultantVisit> ConsultantVisits { get; set; }

        public DbSet<CallBack> CallBacks { get; set; }

        public DbSet<ConsultantBusyPeriod> ConsultantBusyPeriods { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<Tender> Tenders { get; set; }

        public DbSet<TenderProposal> TenderProposals { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<ProductFile> ProductFiles { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Order>().ToTable("Orders");
            

            builder.Entity<OrderState>().ToTable("OrderStates");

            builder.Entity<Product>().ToTable("Products");
            builder.Entity<Product>().
                Property(s => s.SpecificationData)
                .HasColumnType("ntext");
            builder.Entity<Product>()
                .HasOne(o => o.Order)
                .WithMany(p => p.Products)
                .OnDelete(DeleteBehavior.Cascade);//Restrict);

            builder.Entity<ProductCategory>().ToTable("ProductCategories");

            builder.Entity<ProductDoor>().ToTable("ProductDoors");
            builder.Entity<ProductDoor>().HasIndex(p => p.ProductId).IsUnique();

            builder.Entity<ProductWindow>().ToTable("ProductWindows");
            builder.Entity<ProductWindow>().HasIndex(p => p.ProductId).IsUnique();
            builder.Entity<ProductWindow>()
                .HasOne(p => p.ProductWindowSystem)
                .WithMany(w => w.ProductWindows)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<ProductWindow>()
                .HasOne(p => p.ProductWindowsill)
                .WithMany(w => w.ProductWindows)
                .OnDelete(DeleteBehavior.Restrict);
            builder.Entity<ProductWindow>()
                .HasOne(p => p.ProductWindowTinting)
                .WithMany(w => w.ProductWindows)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AuctionProposal>()
                .HasOne(u => u.User)
                .WithMany(a => a.AuctionProposals)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<ProductFile>()
                .HasKey(pf => new { pf.FileId, pf.ProductId });
            builder.Entity<ProductFile>()
                .HasOne(f => f.File)
                .WithMany(pf => pf.ProductFiles)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(f => f.FileId);
            builder.Entity<ProductFile>()
                .HasOne(p => p.Product)
                .WithMany(pf => pf.ProductFiles)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(p => p.ProductId);

            builder.Entity<Log>().ToTable("Logs");
            builder.Entity<LogType>().ToTable("LogTypes");

            builder.Entity<Company>().ToTable("Companies");

            builder.Entity<Auction>().ToTable("Auctions");

            builder.Entity<AuctionProposal>().ToTable("AuctionРroposals");

            builder.Entity<ProductWindowBrand>().ToTable("ProductWindowBrands");
            builder.Entity<ProductWindowFinding>().ToTable("ProductWindowFindings");
            builder.Entity<ProductWindowGlazingType>().ToTable("ProductWindowGlazingTypes");
            builder.Entity<ProductWindowGlazing>().ToTable("ProductWindowGlazings");
            builder.Entity<ProductWindowLaminating>().ToTable("ProductWindowLaminatings");
            builder.Entity<ProductWindowsill>().ToTable("ProductWindowsills");
            builder.Entity<ProductWindowSystem>().ToTable("ProductWindowSystems");
            builder.Entity<ProductWindowTinting>().ToTable("ProductWindowTintings");

            builder.Entity<ProductWindowGlazing>().HasKey(f => new { f.ProductWindowGlazingTypeId, f.ProductWindowId});

            builder.Entity<Message>()
                .HasOne(m => m.Reciever)
                .WithMany(a => a.ReceivedMessages);

            builder.Entity<Message>()
                .HasOne(m => m.Sender)
                .WithMany(a => a.SentMessages);

            builder.Entity<Message>().ToTable("Messages");

            builder.Entity<Feedback>().ToTable("Feedbacks");

            builder.Entity<ConsultantProfile>().ToTable("ConsultantProfiles");

            builder.Entity<ClientProfile>().ToTable("ClientProfiles");

            builder.Entity<ConsultantVisit>().ToTable("ConsultantVisits");

            builder.Entity<CallBack>().ToTable("CallBacks");

            builder.Entity<ConsultantBusyPeriod>().ToTable("ConsultantBusyPeriods");

            builder.Entity<OrderExecutionRating>().ToTable("OrderExecutionRatings");

            builder.Entity<ProductTenderProposal>().
                Property(s => s.SpecificationData)
                .HasColumnType("ntext");

            builder.Entity<ProductTenderProposal>()
                .HasOne(u => u.TenderProposal)
                .WithMany(a => a.ProductTenderProposals)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<ProductTenderProposal>().ToTable("ProductTenderProposals");

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public string GetCurrentUserId(IHttpContextAccessor actionContext, IErrorService errorService)
        {
            try
            {
                var user = Users.FirstOrDefault(f => f.Email.ToLower().Equals(actionContext.HttpContext.User.Identity.Name.ToLower()));
                if (user == null)
                    throw new Exception(MarketPlace.Infrastructure.Constants.Messages.MessagesErrorUserNotFound);
                return user.Id;
            }
            catch (Exception ex)
            {
                errorService.Handle(ex);
            }
            return new Guid().ToString();
        }
    }
}
