﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class ConsultantVisit
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; } 
        public virtual ApplicationUser User { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        [DefaultValue(-1)]
        public int VisitRating { get; set; }

        public DateTime Date { get; set; } //Date/time of the visit
        public DateTime Create { get; set; } // date/time when the visit is created

        public int? ConsultantBusyPeriodId { get; set; } 

        public virtual ConsultantBusyPeriod ConsultantBusyPeriod { get; set; }

        public DateTime? CancelledDateTime { get; set; }

    }
}
