using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class AuctionProposal
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int AuctionId { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public DateTime ProposalTime { get; set; }

        [Required]
        public bool Accepted { get; set; }

        public virtual Auction Auction { get; set; }

        [Required]
        [MaxLength(450)]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}