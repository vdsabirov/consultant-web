﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }

        public string SenderId { get; set; }

        public virtual ApplicationUser Sender { get; set; }

        public string RecieverId { get; set; }

        public virtual ApplicationUser Reciever { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(128)]
        public string Text { get; set; }
        public int LinkedTo { get; set; }

        [DefaultValue(true)]
        public bool IsNew { get; set; }
        public DateTime ReadAt { get; set; }
    }
}
