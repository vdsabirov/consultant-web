﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace  MarketPlace.Entities.Models
{
    public class ProductCategory
    {
        public ProductCategory()
        {
            Products = new HashSet<Product>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Категория")]
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}