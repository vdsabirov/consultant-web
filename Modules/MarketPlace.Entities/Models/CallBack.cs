﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class CallBack
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        [MaxLength(50)]
        public string ConvenientTime { get; set; } //СonvenientTime

        public DateTime? WhenCall { get; set; }

        [MaxLength(1024)]
        public string Comments { get; set; }

        [MaxLength(50)]
        public string CallerName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //откуда пришел запрос
        [MaxLength(200)]
        public string Target { get; set; }

        //public int? ConsultantId { get; set; }

        //public virtual ConsultantProfile Consultant { get; set; }

        //public int? ConsultantVisitId { get; set; }

        //public virtual ConsultantVisit ConsultantVisit { get; set; }

    }
}
