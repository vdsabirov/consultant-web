﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class Company
    {
        public Company()
        {
            Users = new HashSet<ApplicationUser>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано название компании")]
        [MinLength(3, ErrorMessage = "Название компании должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Name { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }

        [MaxLength(50)]
        public string SupplyTerm { get; set; }

        [MaxLength(50)]
        public string GuaranteeTerm { get; set; }

        public int PrepaymentTermPercentage{ get; set; }

        public int Rating { get; set; }


    }
}