﻿using System.ComponentModel.DataAnnotations;

namespace  MarketPlace.Entities.Models
{
    public class ProductDoor
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Высота")]
        [Range(1, 1000)]
        [Required]
        public int Height { get; set; }

        [Display(Name = "Ширина")]
        [Range(1, 1000)]
        [Required]
        public int Width { get; set; }

        [Display(Name = "Глазок")]
        [Required]
        public bool Peephole { get; set; }

        [Required]
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
