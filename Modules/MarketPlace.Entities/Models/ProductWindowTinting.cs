﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class ProductWindowTinting
    {
        public ProductWindowTinting()
        {
            ProductWindows = new HashSet<ProductWindow>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано Название")]
        [MinLength(3, ErrorMessage = "Название должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Title { get; set; }

        public string Description { get; set; }

        public virtual ICollection<ProductWindow> ProductWindows { get; set; }
    }
}