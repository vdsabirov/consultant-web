using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class AuctionEventType
    {
        public AuctionEventType()
        {
            AuctionEvents = new HashSet<AuctionEvent>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        public virtual ICollection<AuctionEvent> AuctionEvents { get; set; }
    }
}