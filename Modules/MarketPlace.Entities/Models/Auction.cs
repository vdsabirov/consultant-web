using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class Auction
    {
        public Auction()
        {
            //AuctionEvents = new HashSet<AuctionEvent>();
            AuctionProposals = new HashSet<AuctionProposal>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public int OrderId { get; set; }
        [Required]
        public bool IsOpen { get; set; }
        [Required]
        public DateTime OpeningTime { get; set; }
        [Required]
        public DateTime ClosingTime { get; set; }

        public virtual Order Order { get; set; }

        public virtual ICollection<AuctionProposal> AuctionProposals { get; set; }
    }
}