﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace  MarketPlace.Entities.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

       //не используется
        [MaxLength(50)]
        public string Code { get; set; }

        //не используется
        [Required]
        public int ViewOrder { get; set; }

        [Required]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        [Required]
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }

        //не используется
        [Range(1, 1000)]
        [DefaultValue(1)]
        public int Quantity { get; set; }

        //использую для хранения json
        //[MaxLength(1024)]
        //public string Specification { get; set; }

        public string SpecificationData { get; set; }


        public virtual ICollection<ProductFile> ProductFiles { get; set; }


        public virtual ICollection<ProductTenderProposal> ProductTenderProposals { get; set; }

    }
}
