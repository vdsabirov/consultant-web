﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class Feedback
    {
        [Key]
        public int Id { get; set; }

        public string FeedbackComment { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

    }
}