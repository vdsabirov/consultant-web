﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(1024)]
        public string Message { get; set; }

        [Required]
        public int LogTypeId { get; set; }
        [Required]
        public LogType LogType { get; set; }

        [MaxLength(450)]
        public string UserId { get; set; }

        [MaxLength(1024)]
        public string Url { get; set; }

        [Required]
        public DateTime Time { get; set; }
    }
}