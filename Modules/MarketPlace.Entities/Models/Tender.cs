﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class Tender
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int OrderId { get; set; }
        [Required]
        public bool IsOpen { get; set; }
        [Required]
        public DateTime OpeningTime { get; set; }
        [Required]
        public DateTime ClosingTime { get; set; }

        public virtual Order Order { get; set; }

        public virtual ICollection<TenderProposal> TenderProposals { get; set; }
    }
}
