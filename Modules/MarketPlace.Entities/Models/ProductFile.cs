﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class ProductFile
    {
        //[Key]
        //public int Id { get; set; }

        [Required]
        public int ProductId { get; set; }
        public Product Product { get; set; }


        [Required]
        public int FileId { get; set; }
        public File File { get; set; }
    }
}
