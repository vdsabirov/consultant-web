using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Entities.Models
{
    public class Order
    {
        public Order()
        {
            Products = new HashSet<Product>();
            OrderStateId = (int) OrdersStates.Forming;
            OrderDate = DateTime.Today;
        }

        [Key]
        public int Id { get; set; }

        //изпользую для отображения статуса
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public int OrderStateId { get; set; }

        public virtual OrderState OrderState { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        [Required]
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [Required]
        public int ConsultantProfileId { get; set; }

        public virtual ConsultantProfile ConsultantProfile { get; set;}

        [MaxLength(200)]
        public string Notes { get; set; }

        public int? TenderId { get; set; }

        public virtual Tender Tender { get; set; }

        public int? OrderExecutionRatingId { get; set; }

        public virtual OrderExecutionRating OrderExecutionRating { get; set; }
    }
}