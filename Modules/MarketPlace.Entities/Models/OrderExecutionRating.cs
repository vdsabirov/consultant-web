﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class OrderExecutionRating
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int OrderId { get; set; }

        public int ServiceRating { get; set; }

        public int CompanyWinnerRating { get; set; }

        public int ConsultantRating { get; set; }

    }
}
