﻿using System.ComponentModel.DataAnnotations;

namespace  MarketPlace.Entities.Models
{
    public class OrderState
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Статус")]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
