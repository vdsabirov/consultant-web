﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class ProductTenderProposal
    {
        [Key]
        public int Id { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        public int TenderProposalId { get; set; }

        public virtual TenderProposal TenderProposal { get; set; }

        public decimal Price { get; set; }

        [MaxLength(2048)]
        public string Description { get; set; }

        public string SpecificationData { get; set; }

    }
}
