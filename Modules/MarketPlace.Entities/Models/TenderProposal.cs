﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class TenderProposal
    {
        [Key]
        public int Id { get; set; }

        public int TenderId { get; set; }
        
        public virtual Tender Tender { get; set; }

        [Required]
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [Required]
        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

        [Required]
        public decimal Price { get; set; }

        [StringLength(3500)]
        public string Notes { get; set; }

        [MaxLength(50)]
        public string SupplyTerm { get; set; }

        [MaxLength(50)]
        public string GuaranteeTerm { get; set; }

        public int PrepaymentTermPercentage { get; set; }

        public bool IsWinner { get; set; }

        public virtual ICollection<ProductTenderProposal> ProductTenderProposals { get; set; }
    }
}
