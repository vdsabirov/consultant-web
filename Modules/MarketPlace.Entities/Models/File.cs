﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class File
    {
        [Key]
        public int Id { get; set; }
        public string FileComment { get; set; }
        public string FileName { get; set; } //name of the file
        public string FilePath { get; set; } //relative path to the file (from root), including name of the file
        public virtual ICollection<ProductFile> ProductFiles { get; set; }
    }
}
