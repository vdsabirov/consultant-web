﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace  MarketPlace.Entities.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не указано имя пользователя")]
        [MinLength(3, ErrorMessage = "Имя пользователя должно содержать не менее 3 символов")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        //[Display(Name = "Отчество")]
        //[MaxLength(50)]
        //public string MiddleName { get; set; }

        //[Display(Name = "Фамилия")]
        //[Required(ErrorMessage = "Не указана фамилия пользователя")]
        //[MinLength(3, ErrorMessage = "Фамилия пользователя должна содержать не менее 3 символов")]
        //[MaxLength(50)]
        //public string LastName { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        internal virtual ICollection<AuctionProposal> AuctionProposals { get; set; }
        internal virtual ICollection<Feedback> Feedbacks { get; set; }
        internal virtual ICollection<ConsultantProfile> ConsultantProfiles { get; set; }
        public virtual ICollection<CallBack> CallBacks { get; set; }

        public virtual ICollection<Message> SentMessages { get; set; }

        public virtual ICollection<Message> ReceivedMessages { get; set; }
    }
}
