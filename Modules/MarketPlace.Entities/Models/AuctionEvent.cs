using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class AuctionEvent
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int AuctionId { get; set; }
        [Required]
        public int AuctionEventTypeId { get; set; }

        [Required]
        [MaxLength(512)]
        public string Message { get; set; }
        [Required]
        public DateTime EventTime { get; set; }

        public virtual AuctionEventType AuctionEventType { get; set; }
        public virtual Auction Auction { get; set; }
    }
}