﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace  MarketPlace.Entities.Models
{
    public class ProductWindow
    {
        public ProductWindow()
        {
            ProductWindowGlazings = new HashSet<ProductWindowGlazing>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Высота")]
        [Range(1, 1000)]
        [Required]
        public int Height { get; set; }

        [Display(Name = "Ширина")]
        [Range(1, 1000)]
        [Required]
        public int Width { get; set; }

        [Required]
        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int ProductWindowBrandId { get; set; }
        public ProductWindowBrand ProductWindowBrand { get; set; }

        public int ProductWindowSystemId { get; set; }
        public ProductWindowSystem ProductWindowSystem { get; set; }


        public int ProductWindowFindingId { get; set; }
        public ProductWindowFinding ProductWindowFinding { get; set; }


        public int ProductWindowTintingId { get; set; }
        public ProductWindowTinting ProductWindowTinting { get; set; }


        public int ProductWindowLaminatingId { get; set; }
        public ProductWindowLaminating ProductWindowLaminating { get; set; }

        public int ProductWindowsillId { get; set; }
        public ProductWindowsill ProductWindowsill { get; set; }

        public virtual ICollection<ProductWindowGlazing> ProductWindowGlazings { get; set; }

        public bool Measurement { get; set; }
        public bool Delivery { get; set; }
        public bool Installation { get; set; }
        public bool CleaningDismantling { get; set; }
    }
}
