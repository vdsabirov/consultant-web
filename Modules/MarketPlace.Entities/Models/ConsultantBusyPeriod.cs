﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Models
{
    public class ConsultantBusyPeriod
    {

        [Key]
        public int Id { get; set; }

        public int ConsultantId { get; set; }

        public virtual ConsultantProfile Consultant { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public string Comments { get; set; }

        public int? ConsultantVisitId { get; set; }

        public virtual ConsultantVisit ConsultantVisit { get; set;}
    }
}
