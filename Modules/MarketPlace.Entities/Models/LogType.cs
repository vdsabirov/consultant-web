﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Entities.Models
{
    public class LogType
    {
        public LogType()
        {
            Logs = new HashSet<Log>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(128)]
        public string Name { get; set; }

        public virtual ICollection<Log> Logs { get; set; }
    }
}