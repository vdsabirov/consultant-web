IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK_HistoryRow] PRIMARY KEY ([MigrationId])
    );

GO

ALTER TABLE [Auctions] DROP CONSTRAINT [FK_Auction_Order_OrderId];

GO

ALTER TABLE [AuctionРroposals] DROP CONSTRAINT [FK_AuctionProposal_Auction_AuctionId];

GO

ALTER TABLE [Logs] DROP CONSTRAINT [FK_Log_LogType_LogTypeId];

GO

ALTER TABLE [Orders] DROP CONSTRAINT [FK_Order_OrderState_OrderStateId];

GO

ALTER TABLE [Orders] DROP CONSTRAINT [FK_Order_ApplicationUser_UserId];

GO

ALTER TABLE [Products] DROP CONSTRAINT [FK_Product_Order_OrderId];

GO

ALTER TABLE [Products] DROP CONSTRAINT [FK_Product_ProductCategory_ProductCategoryId];

GO

ALTER TABLE [ProductDoors] DROP CONSTRAINT [FK_ProductDoor_Product_ProductId];

GO

ALTER TABLE [ProductWindows] DROP CONSTRAINT [FK_ProductWindow_Product_ProductId];

GO

ALTER TABLE [ProductWindows] DROP CONSTRAINT [FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId];

GO

ALTER TABLE [ProductWindows] DROP CONSTRAINT [FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId];

GO

ALTER TABLE [ProductWindows] DROP CONSTRAINT [FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId];

GO

ALTER TABLE [ProductWindowGlazings] DROP CONSTRAINT [FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId];

GO

ALTER TABLE [ProductWindowGlazings] DROP CONSTRAINT [FK_ProductWindowGlazing_ProductWindow_ProductWindowId];

GO

ALTER TABLE [ProductWindowSystems] DROP CONSTRAINT [FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId];

GO

ALTER TABLE [AspNetRoleClaims] DROP CONSTRAINT [FK_IdentityRoleClaim<string>_IdentityRole_RoleId];

GO

ALTER TABLE [AspNetUserClaims] DROP CONSTRAINT [FK_IdentityUserClaim<string>_ApplicationUser_UserId];

GO

ALTER TABLE [AspNetUserLogins] DROP CONSTRAINT [FK_IdentityUserLogin<string>_ApplicationUser_UserId];

GO

ALTER TABLE [AspNetUserRoles] DROP CONSTRAINT [FK_IdentityUserRole<string>_IdentityRole_RoleId];

GO

ALTER TABLE [AspNetUserRoles] DROP CONSTRAINT [FK_IdentityUserRole<string>_ApplicationUser_UserId];

GO

CREATE TABLE [Feedbacks] (
    [Id] int NOT NULL IDENTITY,
    [FeedbackComment] nvarchar(max),
    CONSTRAINT [PK_Feedback] PRIMARY KEY ([Id])
);

GO

ALTER TABLE [Auctions] ADD CONSTRAINT [FK_Auction_Order_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [Orders] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AuctionРroposals] ADD CONSTRAINT [FK_AuctionProposal_Auction_AuctionId] FOREIGN KEY ([AuctionId]) REFERENCES [Auctions] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Logs] ADD CONSTRAINT [FK_Log_LogType_LogTypeId] FOREIGN KEY ([LogTypeId]) REFERENCES [LogTypes] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Orders] ADD CONSTRAINT [FK_Order_OrderState_OrderStateId] FOREIGN KEY ([OrderStateId]) REFERENCES [OrderStates] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Orders] ADD CONSTRAINT [FK_Order_ApplicationUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Products] ADD CONSTRAINT [FK_Product_Order_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [Orders] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [Products] ADD CONSTRAINT [FK_Product_ProductCategory_ProductCategoryId] FOREIGN KEY ([ProductCategoryId]) REFERENCES [ProductCategories] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductDoors] ADD CONSTRAINT [FK_ProductDoor_Product_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [Products] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindows] ADD CONSTRAINT [FK_ProductWindow_Product_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [Products] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindows] ADD CONSTRAINT [FK_ProductWindow_ProductWindowBrand_ProductWindowBrandId] FOREIGN KEY ([ProductWindowBrandId]) REFERENCES [ProductWindowBrands] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindows] ADD CONSTRAINT [FK_ProductWindow_ProductWindowFinding_ProductWindowFindingId] FOREIGN KEY ([ProductWindowFindingId]) REFERENCES [ProductWindowFindings] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindows] ADD CONSTRAINT [FK_ProductWindow_ProductWindowLaminating_ProductWindowLaminatingId] FOREIGN KEY ([ProductWindowLaminatingId]) REFERENCES [ProductWindowLaminatings] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindowGlazings] ADD CONSTRAINT [FK_ProductWindowGlazing_ProductWindowGlazingType_ProductWindowGlazingTypeId] FOREIGN KEY ([ProductWindowGlazingTypeId]) REFERENCES [ProductWindowGlazingTypes] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindowGlazings] ADD CONSTRAINT [FK_ProductWindowGlazing_ProductWindow_ProductWindowId] FOREIGN KEY ([ProductWindowId]) REFERENCES [ProductWindows] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [ProductWindowSystems] ADD CONSTRAINT [FK_ProductWindowSystem_ProductWindowBrand_ProductWindowBrandId] FOREIGN KEY ([ProductWindowBrandId]) REFERENCES [ProductWindowBrands] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AspNetRoleClaims] ADD CONSTRAINT [FK_IdentityRoleClaim<string>_IdentityRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AspNetUserClaims] ADD CONSTRAINT [FK_IdentityUserClaim<string>_ApplicationUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AspNetUserLogins] ADD CONSTRAINT [FK_IdentityUserLogin<string>_ApplicationUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AspNetUserRoles] ADD CONSTRAINT [FK_IdentityUserRole<string>_IdentityRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE;

GO

ALTER TABLE [AspNetUserRoles] ADD CONSTRAINT [FK_IdentityUserRole<string>_ApplicationUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20160211194605_Feedback', N'7.0.0-rc1-16348');

GO

