﻿using System.Collections.Generic;
using System.Linq;

namespace MarketPlace.Entities.Mappers
{
    public class Company
    {
        public static Infrastructure.Models.Company MapTo(Models.Company source)
        {
            return source == null
                ? new Infrastructure.Models.Company()
                : new Infrastructure.Models.Company
                {
                    Id = source.Id,
                    Name = source.Name,
                    MarketPlaceUsers = MapToMarketPlaceUsers(source.Users)
                };
        }

        private static ICollection<Infrastructure.Models.User> MapToMarketPlaceUsers(
            ICollection<Models.ApplicationUser> marketPlaceUsers)
        {
            var result = new List<Infrastructure.Models.User>();
            if (marketPlaceUsers == null || !marketPlaceUsers.Any())
                return result;
            marketPlaceUsers.ToList().ForEach(u => result.Add(User.MapTo(u)));
            return result;
        }
    }
}