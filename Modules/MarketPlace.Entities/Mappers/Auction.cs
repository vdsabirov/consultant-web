﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Entities.Mappers
{
    public class Auction
    {
        public static ICollection<AuctionProposal> MapToAuctionProposals(List<Models.AuctionProposal> proposals)
        {
            if (proposals == null || !proposals.Any()) return new AuctionProposal[0];
            var result = new List<AuctionProposal>();
            proposals.ToList().ForEach(p => result.Add(MapToInfrastructureAuctionProposal(p)));
            return result;
        }

        public static AuctionProposal MapToInfrastructureAuctionProposal(Models.AuctionProposal source)
        {
            return source == null
                ? new AuctionProposal()
                : new AuctionProposal
                {
                    Id = source.Id,
                    Accepted = source.Accepted,
                    AuctionId = source.AuctionId,
                    Price = source.Price,
                    ProposalTime = source.ProposalTime,
                    UserId = source.UserId,
                    Auction = new Infrastructure.Models.Auction
                    {
                        Id = source.Auction?.Id ?? source.AuctionId,
                        ClosingTime = source.Auction?.ClosingTime ?? DateTime.MinValue,
                        OpeningTime = source.Auction?.OpeningTime ?? DateTime.MinValue,
                        IsOpen = source.Auction?.IsOpen ?? false,
                        OrderId = source.Auction?.OrderId ?? -1
                    }
                };
        }

        public static void CopyFrom(Infrastructure.Models.Auction source, Models.Auction destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.ClosingTime = source.ClosingTime;
            destination.OpeningTime = source.OpeningTime;
            destination.IsOpen = source.IsOpen;
            destination.OrderId = source.OrderId;
        }

        public static Infrastructure.Models.Auction MapTo(Models.Auction source)
        {
            return source == null
            ? new Infrastructure.Models.Auction()
            : new Infrastructure.Models.Auction
            {
                Id = source.Id,
                ClosingTime = source.ClosingTime,
                OpeningTime = source.OpeningTime,
                IsOpen = source.IsOpen,
                OrderId = source.OrderId,
                AuctionProposals =
                    source.AuctionProposals != null
                        ? MapToAuctionProposals(source.AuctionProposals.ToList())
                        : new List<AuctionProposal>()
            };
            //return source == null
            //    ? new Infrastructure.Models.Auction()
            //    : new Infrastructure.Models.Auction
            //    {
            //        Id = source.Id,
            //        ClosingTime = source.ClosingTime,
            //        OpeningTime = source.OpeningTime,
            //        IsOpen = source.IsOpen,
            //        OrderId = source.OrderId,
            //        AuctionProposals =
            //            source.AuctionProposals != null
            //                ? MapToAuctionProposals(source.AuctionProposals.ToList())
            //                : new List<AuctionProposal>(),
            //        Order = new Infrastructure.Models.Order
            //        {
            //            Id = source.Order?.Id ?? source.OrderId,
            //            Name = source.Order?.Name ?? string.Empty,
            //            Code = source.Order?.Code ?? string.Empty,
            //            OrderStateId = source.Order?.OrderStateId ?? (int) OrdersStates.Forming,
            //            OrderDate = source.Order?.OrderDate ?? DateTime.MinValue,
            //            Products = source.Order?.Products != null ? Order.MapToProducts(source.Order.Products) : new List<Infrastructure.Models.Product>(),
            //            UserId = source.Order?.UserId ?? string.Empty
            //        }
            //    };
        }

        public static void CopyFromInfrastructureAuctionProposal(AuctionProposal source, Models.AuctionProposal destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.AuctionId= source.AuctionId;
            destination.Accepted = source.Accepted;
            destination.Price = source.Price;
            destination.ProposalTime = source.ProposalTime;
            destination.UserId = source.UserId;
        }
    }
}