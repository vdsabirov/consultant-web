﻿using System.Collections.Generic;
using System.Linq;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Entities.Mappers
{
    public class Order
    {
        public static Infrastructure.Models.Order MapTo(Models.Order source)
        {
            return source == null
                ? new Infrastructure.Models.Order()
                : new Infrastructure.Models.Order
                {
                    Id = source.Id,
                    Name = source.Name,
                    Code = source.Code,
                    OrderStateId = source.OrderStateId,
                    OrderState =
                        new OrderState
                        {
                            Id = source.OrderState?.Id ?? (int) OrdersStates.Forming,
                            Name = source.OrderState?.Name ?? OrdersStates.Forming.ToString()
                        },
                    OrderDate = source.OrderDate,
                    UserId = source.UserId,
                    Products = MapToProducts(source.Products)
                };
        }

        public static void CopyFrom(Infrastructure.Models.Order source, Models.Order destination)
        {
            if(source == null || destination == null) return;
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.Code = source.Code;
            destination.OrderStateId = source.OrderStateId;
            destination.OrderDate = source.OrderDate;
            destination.UserId = source.UserId;
            destination.Products = MapFromProducts(source.Products);
        }

        public static ICollection<Infrastructure.Models.Product> MapToProducts(ICollection<Models.Product> products)
        {
            if (products == null || !products.Any()) return new Infrastructure.Models.Product[0];
            var result = new List<Infrastructure.Models.Product>();
            products.ToList().ForEach(p => result.Add(Product.MapTo(p)));
            return result;
        }

        public static Models.Order MapFrom(Infrastructure.Models.Order source)
        {
            return source == null
                ? new Models.Order()
                : new Models.Order
                {
                    Id = source.Id,
                    Name = source.Name,
                    Code = source.Code,
                    OrderStateId = source.OrderStateId,
                    OrderState =
                        new Models.OrderState
                        {
                            Id = source.OrderState?.Id ?? (int) OrdersStates.Forming,
                            Name = source.OrderState?.Name ?? OrdersStates.Forming.ToString()
                        },
                    OrderDate = source.OrderDate,
                    UserId = source.UserId,
                    Products = MapFromProducts(source.Products)
                };
        }

        private static ICollection<Models.Product> MapFromProducts(ICollection<Infrastructure.Models.Product> products)
        {
            if (products == null || !products.Any()) return new Models.Product[0];
            var result = new List<Models.Product>();
            products.ToList().ForEach(p => result.Add(Product.MapFrom(p)));
            return result;
        }
    }
}