﻿using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;

namespace MarketPlace.Entities.Mappers
{
    public class ProductWindow
    {
        public static void CopyFrom(Infrastructure.Models.ProductWindow source, Models.ProductWindow destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.Height = source.Height;
            destination.Width = source.Width;
            destination.ProductId = source.ProductId;

            destination.ProductWindowBrandId = source.ProductWindowBrandId;
            destination.ProductWindowSystemId = source.ProductWindowSystemId;
            destination.ProductWindowGlazings = GetProductWindowGlazingEntities(source.ProductWindowGlazingTypeId,
                destination.Id);
            destination.ProductWindowFindingId = source.ProductWindowFindingId;
            destination.ProductWindowTintingId = source.ProductWindowTintingId;
            destination.ProductWindowLaminatingId = source.ProductWindowLaminatingId;
            destination.ProductWindowsillId = source.ProductWindowsillId;
            destination.Measurement = source.Measurement;
            destination.Delivery = source.Delivery;
            destination.Installation = source.Installation;
            destination.CleaningDismantling = source.CleaningDismantling;
        }

        private static ICollection<ProductWindowGlazing> GetProductWindowGlazingEntities(
            IEnumerable<int> productWindowGlazingTypes, int productWindowId)
        {
            if (productWindowGlazingTypes == null || !productWindowGlazingTypes.Any())
                return new List<ProductWindowGlazing>();
            var glazingTypes = new List<ProductWindowGlazing>();
            productWindowGlazingTypes.ToList().ForEach(
                f =>
                    glazingTypes.Add(new ProductWindowGlazing
                    {
                        ProductWindowId = productWindowId,
                        ProductWindowGlazingTypeId = f
                    }));
            return glazingTypes;
        }

        private static ICollection<Infrastructure.Models.ProductWindowGlazing> GetProductWindowGlazings(
            List<int> productWindowGlazingTypes, int productWindowId)
        {
            if (productWindowGlazingTypes == null || !productWindowGlazingTypes.Any())
                return new List<Infrastructure.Models.ProductWindowGlazing>();
            var glazingTypes = new List<Infrastructure.Models.ProductWindowGlazing>();
            productWindowGlazingTypes.ForEach(
                f =>
                    glazingTypes.Add(new Infrastructure.Models.ProductWindowGlazing
                    {
                        ProductWindowId = productWindowId,
                        ProductWindowGlazingTypeId = f
                    }));
            return glazingTypes;
        }

        public static Infrastructure.Models.ProductWindow MapTo(Models.ProductWindow source)
        {
            return source == null
                ? new Infrastructure.Models.ProductWindow()
                : new Infrastructure.Models.ProductWindow
                {
                    Id = source.Id,
                    Height = source.Height,
                    Width = source.Width,
                    ProductId = source.ProductId,
                    ProductWindowBrandId = source.ProductWindowBrandId,
                    ProductWindowSystemId = source.ProductWindowSystemId,
                    ProductWindowGlazings =
                        GetProductWindowGlazings(
                            source.ProductWindowGlazings.Select(f => f.ProductWindowGlazingTypeId).ToList(), source.Id),
                    ProductWindowGlazingTypeId = new List<int> (source.ProductWindowGlazings.Select(g => g.ProductWindowGlazingTypeId)),
                    ProductWindowFindingId = source.ProductWindowFindingId,
                    ProductWindowTintingId = source.ProductWindowTintingId,
                    ProductWindowLaminatingId = source.ProductWindowLaminatingId,
                    ProductWindowsillId = source.ProductWindowsillId,
                    Measurement = source.Measurement,
                    Delivery = source.Delivery,
                    Installation = source.Installation,
                    CleaningDismantling = source.CleaningDismantling
                };
        }
    }
}