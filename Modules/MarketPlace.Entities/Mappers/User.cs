﻿namespace MarketPlace.Entities.Mappers
{
    public class User
    {
        public static Infrastructure.Models.User MapTo(Models.ApplicationUser source)
        {
            if (source == null)
               return new Infrastructure.Models.User();

            var u = new Infrastructure.Models.User
            {
                FirstName = source.FirstName,
                //MiddleName = source.MiddleName,
                //LastName = source.LastName,
                Email = source.Email,
                Id = source.Id
            };
            if (source.CompanyId != null)
                u.CompanyId = source.CompanyId;
            return u;
        }
    }
}