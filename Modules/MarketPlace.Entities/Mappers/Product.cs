﻿using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Entities.Mappers
{
    public class Product
    {
        public static Infrastructure.Models.Product MapTo(Models.Product source)
        {
            return source == null
                ? new Infrastructure.Models.Product()
                : new Infrastructure.Models.Product
                {
                    Id = source.Id,
                    Name = source.Name,
                    Code = source.Code,
                    OrderId = source.OrderId,
                    ProductCategoryId = source.ProductCategoryId,
                    ProductCategory =
                        new ProductCategory
                        {
                            Id = source.ProductCategoryId,
                            Title =
                                source.ProductCategory?.Title ??
                                ((ProductsCategories) source.ProductCategoryId).ToString()
                        },
                    Quantity = source.Quantity,
                    SpecificationLink = source.SpecificationData,
                    ViewOrder = source.ViewOrder
                };
        }

        public static Models.Product MapFrom(Infrastructure.Models.Product source)
        {
            return source == null
                ? new Models.Product()
                : new Models.Product
                {
                    Id = source.Id,
                    Name = source.Name,
                    Code = source.Code
                };
        }

        public static void CopyFrom(Infrastructure.Models.Product source, Models.Product destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.Code = source.Code;
            destination.OrderId = source.OrderId;
            destination.ProductCategoryId = source.ProductCategoryId;
            destination.Quantity = source.Quantity;
            destination.SpecificationData = source.SpecificationLink;
            destination.ViewOrder = source.ViewOrder;
        }
    }
}