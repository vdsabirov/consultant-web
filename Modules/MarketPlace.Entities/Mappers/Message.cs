﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Entities.Mappers
{
    public class Message
    {
        public static Infrastructure.Models.Message MapTo(Models.Message source)
        {
            return source == null
                ? new Infrastructure.Models.Message()
                : new Infrastructure.Models.Message
                {
                    Id = source.Id,
                    CreatedAt = source.CreatedAt,
                    SenderId = source.SenderId,
                    RecieverId = source.RecieverId,
                    Title = source.Title,
                    Text = source.Text,
                    LinkedTo = source.LinkedTo,
                    IsNew = source.IsNew,
                    ReadAt = source.ReadAt
                };
        }

        public static void CopyFrom(Infrastructure.Models.Message source, Models.Message destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.CreatedAt = source.CreatedAt;
            destination.SenderId = source.SenderId;
            destination.RecieverId = source.RecieverId;
            destination.Title = source.Title;
            destination.Text = source.Text;
            destination.LinkedTo = source.LinkedTo;
            destination.IsNew = source.IsNew;
            destination.ReadAt = source.ReadAt;
        }

        public static Models.Message MapFrom(Infrastructure.Models.Message source)
        {
            return source == null
                ? new Models.Message()
                : new Models.Message
                {
                    Id = source.Id,
                    CreatedAt = source.CreatedAt,
                    SenderId = source.SenderId,
                    RecieverId = source.RecieverId,
                    Title = source.Title,
                    Text = source.Text,
                    LinkedTo = source.LinkedTo,
                    IsNew = source.IsNew,
                    ReadAt = source.ReadAt
                };
        }
    }
}
