﻿namespace MarketPlace.Entities.Mappers
{
    public class ProductDoor
    {
        public static void CopyFrom(Infrastructure.Models.ProductDoor source, Models.ProductDoor destination)
        {
            if (source == null || destination == null) return;
            destination.Id = source.Id;
            destination.Height = source.Height;
            destination.Width = source.Width;
            destination.ProductId = source.ProductId;
            destination.Peephole = source.Peephole;
        }

        public static Infrastructure.Models.ProductDoor MapTo(Models.ProductDoor source)
        {
            return source == null
                ? new Infrastructure.Models.ProductDoor()
                : new Infrastructure.Models.ProductDoor
                {
                    Id = source.Id,
                    Height = source.Height,
                    Width = source.Width,
                    ProductId = source.ProductId,
                    Peephole = source.Peephole
                };
        }
    }
}