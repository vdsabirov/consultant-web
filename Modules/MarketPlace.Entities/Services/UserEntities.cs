﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.Data.Entity;
using Company = MarketPlace.Infrastructure.Models.Company;
using Microsoft.AspNet.Identity;

namespace MarketPlace.Entities.Services
{
    public class UserEntities : IUserEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public UserEntities(
            IHttpContextAccessor actionContext,
            ApplicationDbContext context
            )
        {
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (context == null) throw new ArgumentNullException(nameof(context));
            _actionContext = actionContext;
            _context = context;
        }

        public string UserId
        {
            get
            {
                try
                {
                    var user =
                        _context.Users.FirstOrDefault(
                            f => f.Email.ToLower().Equals(_actionContext.HttpContext.User.Identity.Name.ToLower()));
                    if (user == null)
                        throw new Exception(Messages.MessagesErrorUserNotFound);
                    return user.Id;
                }
                catch (Exception ex)
                {
                    ErrorService.Handle(ex);
                }
                return new Guid().ToString();
            }
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public List<User> GetUsers()
        {
            var users = new List<User>();
            try
            {
                var currentUserId = UserId;

                var usersIds = _context.Auctions
                    .Join(_context.AuctionРroposals, a => a.Id, p => p.AuctionId, (a, p) => new { Customer = a.Order.UserId, IsAccepted = p.Accepted, Bidder = p.UserId })
                    .Where(a => a.IsAccepted && (a.Customer == currentUserId || a.Bidder == currentUserId))
                    .Select(a => a.Customer == currentUserId ? a.Bidder : a.Customer).Distinct().ToList();
                _context.Users
                    .Join(usersIds, u => u.Id, a => a, (u, a) => u).ToList()
                    .ForEach(f => users.Add(Mappers.User.MapTo(f)));

                //_context.Users
                //    .ToList().ForEach(f => users.Add(Mappers.User.MapTo(f)));
                return users;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return users;
        }

        public User GetCurrentUser()
        {
            try
            {
                var user =
                    _context.Users.Include(c => c.Company).FirstOrDefault(f => f.Id.Equals(UserId));
                if (user == null)
                    return new User();
                var u = Mappers.User.MapTo(user);
                if (user.CompanyId != null)
                {
                    u.CompanyId = user.CompanyId;
                    u.Company = new Company
                    {
                        Id = (int)user.CompanyId,
                        Name = user.Company?.Name ?? string.Empty,
                    };
                };

                return u;
                
                    
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new User();
        }

        public User GetUser(string id)
        {
            var user = _context.Users.Include(c => c.Company).FirstOrDefault(u => u.Id == id);
            if (user == null)
                return null;

            return Mappers.User.MapTo(user);
        }

        public Company GetCompany(int id)
        {
            if (id <= 0)
                return null;

            var company = _context.Companies.FirstOrDefault(c => c.Id == id);
            if (company == null)
                return null;

            return Mappers.Company.MapTo(company);
        }

        public List<Company> GetCompanies()
        {
            var result = new List<Company>();

            _context.Companies.ToList().ForEach(c => Mappers.Company.MapTo(c));

            return result;
        }

    }
}