﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.Data.Entity;
using Company = MarketPlace.Infrastructure.Models.Company;
using Order = MarketPlace.Infrastructure.Models.Order;
using OrderState = MarketPlace.Infrastructure.Models.OrderState;
using Product = MarketPlace.Infrastructure.Models.Product;

namespace MarketPlace.Entities.Services
{
    public class OrderEntities : IOrderEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public OrderEntities(
            IHttpContextAccessor actionContext,
            ApplicationDbContext context
            )
        {
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (context == null) throw new ArgumentNullException(nameof(context));
            _actionContext = actionContext;
            _context = context;
        }

        public string UserId
        {
            get
            {
                try
                {
                    var user =
                        _context.Users.FirstOrDefault(
                            f => f.Email.ToLower().Equals(_actionContext.HttpContext.User.Identity.Name.ToLower()));
                    if (user == null)
                        throw new Exception(Messages.MessagesErrorUserNotFound);
                    return user.Id;
                }
                catch (Exception ex)
                {
                    ErrorService.Handle(ex);
                }
                return new Guid().ToString();
            }
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public List<OrderState> GetOrderStates()
        {
            var orderStates = new List<OrderState>();
            try
            {
                _context.OrderStates.ToList().ForEach(f => orderStates.Add(new OrderState { Id = f.Id, Name = f.Name}));
                return orderStates;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return orderStates;
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public Company GetCompanyByOrderId(int orderId)
        {
            try
            {
                var company =
                    _context.Companies.Join
                        (
                            _context.Orders.Where(o => o.Id == orderId).Join
                                (
                                    _context.Users,
                                    o => o.UserId,
                                    m => m.Id,
                                    (o, m) => new {m.CompanyId}).ToList(),
                            c => c.Id,
                            f => f.CompanyId,
                            (c, f) => c
                        ).First();
                if (company == null)
                    ErrorService.ItemNotFound();
                return Mappers.Company.MapTo(company);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Company();
        }

        public Order GetOrder(int orderId = -1, bool loadProducts = false)
        {
            if (orderId <= 0) return new Order {UserId = UserId, Name = string.Format("Заказ от {0:d}", DateTime.Now), Code = "000"};
            try
            {
                var order = Mappers.Order.MapTo(_context.Orders.Include(o => o.OrderState).FirstOrDefault(f => f.Id == orderId));
                if (loadProducts)
                    order.Products = GetOrderProducts(orderId);
                return order;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Order {UserId = UserId};
        }

        public List<Product> GetOrderProducts(int orderId)
        {
            try
            {
                return Mappers.Order.MapToProducts(_context.Products.Include(p => p.ProductCategory).Where(f => f.OrderId == orderId).ToList()).ToList();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new List<Product>();
        }

        public Order Update(Order order)
        {
            if (order == null) return new Order();

            try
            {
                var entityOrder = _context.Orders.FirstOrDefault(f => f.Id == order.Id);
                if (entityOrder == null)
                {
                    entityOrder = new Models.Order();
                    Mappers.Order.CopyFrom(order, entityOrder);
                    _context.Orders.Add(entityOrder);
                }
                else
                {
                    Mappers.Order.CopyFrom(order, entityOrder);
                }
                _context.SaveChanges();
                return GetOrder(entityOrder.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new Order();
        }

        public void Delete(int orderId)
        {
            try
            {
                var order = _context.Orders.FirstOrDefault(f => f.Id == orderId);
                if (order == null) return;
                _context.Orders.Remove(order);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public Company GetCompanyByUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId)) return new Company();
            try
            {
                var user = _context.Users.FirstOrDefault(f => f.Id.Equals(userId));
                if (user == null || user.CompanyId == null)
                    return new Company();
                var company = _context.Companies.FirstOrDefault(u => u.Id == user.CompanyId);
                if (company == null)
                    return new Company();
                return Mappers.Company.MapTo(company);
                //return Mappers.Company.MapTo(
                //    _context.Users.Where(f => f.Id.Equals(userId)).Join(
                //        _context.Companies, u => u.CompanyId, c => c.Id, (u, c) => c
                //        ).First()
                //    );
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Company();
        }

        public bool IsOrderOwner(int orderId, string userId)
        {
            if (orderId <= 0 || string.IsNullOrEmpty(userId)) return false;
            try
            {
                return _context.Orders.Any(o => o.Id == orderId && o.UserId.Equals(userId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return false;
        }

        public List<Order> GetOrders()
        {
            var orders = new List<Order>();
            try
            {
                _context.Orders.Include(o => o.OrderState).ToList().ForEach(f => orders.Add(Mappers.Order.MapTo(f)));
                return orders;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return orders;
        }

        public List<Order> GetOrdersByCompanyId(int companyId)
        {
            var orders = new List<Order>();
            if (companyId <= 0)
                return orders;
            try
            {
                _context.Orders.Include(o => o.OrderState).Join(
                    _context.Users.Where(f => f.CompanyId == companyId),
                    o => o.UserId,
                    m => m.Id,
                    (o, m) => o
                    ).ToList().ForEach(f => orders.Add(Mappers.Order.MapTo(f)));
                return orders;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return orders;
        }

        public List<Order> GetOrdersByUserId(string userId)
        {
            var orders = new List<Order>();
            try
            {
                _context.Orders.Include(o => o.OrderState).Where(f => f.UserId.Equals(userId))
                    .ToList()
                    .ForEach(f => orders.Add(Mappers.Order.MapTo(f)));
                return orders;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return orders;
        }
    }
}