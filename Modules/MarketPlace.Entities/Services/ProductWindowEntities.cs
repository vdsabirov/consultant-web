﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using ProductWindow = MarketPlace.Infrastructure.Models.ProductWindow;
using ProductWindowBrand = MarketPlace.Infrastructure.Models.ProductWindowBrand;
using ProductWindowFinding = MarketPlace.Infrastructure.Models.ProductWindowFinding;
using ProductWindowGlazingType = MarketPlace.Infrastructure.Models.ProductWindowGlazingType;
using ProductWindowLaminating = MarketPlace.Infrastructure.Models.ProductWindowLaminating;
using ProductWindowsill = MarketPlace.Infrastructure.Models.ProductWindowsill;
using ProductWindowSystem = MarketPlace.Infrastructure.Models.ProductWindowSystem;
using ProductWindowTinting = MarketPlace.Infrastructure.Models.ProductWindowTinting;
using Microsoft.Data.Entity;

namespace MarketPlace.Entities.Services
{
    public class ProductWindowEntities : IProductWindowEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public ProductWindowEntities(
            ApplicationDbContext context,
            IHttpContextAccessor actionContext
            )
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _context = context;
            _actionContext = actionContext;
        }

        public ProductWindow GetProduct(int productWindowId)
        {
            if (productWindowId <= 0)
                return new ProductWindow() { Height = 180, Width = 100, ProductWindowBrandId = 1, ProductWindowFindingId = 1, ProductWindowLaminatingId = 1, ProductWindowSystemId = 1, ProductWindowTintingId = 1};
            try
            {
                return Mappers.ProductWindow.MapTo(_context.ProductWindows.Include(w => w.ProductWindowGlazings).FirstOrDefault(f => f.Id == productWindowId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new ProductWindow {Id = productWindowId};
        }

        public ProductWindow GetProductByProductId(int productId)
        {
            try
            {
                return Mappers.ProductWindow.MapTo(_context.ProductWindows.Include(w => w.ProductWindowGlazings).FirstOrDefault(f => f.ProductId == productId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new ProductWindow {ProductId = productId};
        }

        public ProductWindow Update(ProductWindow product)
        {
            if (product == null) return new ProductWindow();

            try
            {
                var entityProduct = _context.ProductWindows.FirstOrDefault(o => o.Id == product.Id);
                if (entityProduct == null)
                {
                    entityProduct = new Models.ProductWindow();
                    Mappers.ProductWindow.CopyFrom(product, entityProduct);
                    _context.ProductWindows.Add(entityProduct);
                }
                else
                {
                    Mappers.ProductWindow.CopyFrom(product, entityProduct);
                }
                _context.SaveChanges();
                return GetProduct(entityProduct.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return product;
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public void Delete(int productWindowId)
        {
            try
            {
                var productWindow = _context.ProductWindows.FirstOrDefault(f => f.Id == productWindowId);
                if (productWindow == null) return;
                _context.ProductWindows.Remove(productWindow);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public List<ProductWindow> GetProducts()
        {
            var productWindows = new List<ProductWindow>();
            try
            {
                _context.ProductWindows.ToList().ForEach(f => productWindows.Add(Mappers.ProductWindow.MapTo(f)));
                return productWindows;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return productWindows;
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public List<ProductWindowBrand> GetProductWindowBrands()
        {
            var list = new List<ProductWindowBrand>();
            try
            {
                _context.ProductWindowBrands.ToList()
                    .ForEach(f => list.Add(new ProductWindowBrand {Id = f.Id, Image = f.Image, Title = f.Title}));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowFinding> GetProductWindowFindings()
        {
            var list = new List<ProductWindowFinding>();
            try
            {
                _context.ProductWindowFindings.ToList()
                    .ForEach(f => list.Add(new ProductWindowFinding {Id = f.Id, Title = f.Title}));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowGlazingType> GetProductWindowGlazingTypes()
        {
            var list = new List<ProductWindowGlazingType>();
            try
            {
                _context.ProductWindowGlazingTypes.ToList()
                    .ForEach(f => list.Add(new ProductWindowGlazingType {Id = f.Id, Title = f.Title}));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowLaminating> GetProductWindowLaminatings()
        {
            var list = new List<ProductWindowLaminating>();
            try
            {
                _context.ProductWindowLaminatings.ToList()
                    .ForEach(f => list.Add(new ProductWindowLaminating {Id = f.Id, Title = f.Title}));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowSystem> GetProductWindowSystems()
        {
            var list = new List<ProductWindowSystem>();
            try
            {
                _context.ProductWindowSystems.ToList()
                    .ForEach(
                        f =>
                            list.Add(new ProductWindowSystem
                            {
                                Id = f.Id,
                                Title = f.Title,
                                ProductWindowBrandId = f.ProductWindowBrandId,
                                Description = f.Description
                            }));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowTinting> GetProductWindowTintings()
        {
            var list = new List<ProductWindowTinting>();
            try
            {
                _context.ProductWindowTintings.ToList()
                    .ForEach(
                        f =>
                            list.Add(new ProductWindowTinting
                            {
                                Id = f.Id,
                                Title = f.Title,
                                Description = f.Description
                            }));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }

        public List<ProductWindowsill> GetProductWindowills()
        {
            var list = new List<ProductWindowsill>();
            try
            {
                _context.ProductWindowsills.ToList()
                    .ForEach(
                        f =>
                            list.Add(new ProductWindowsill
                            {
                                Id = f.Id,
                                Title = f.Title,
                                Description = f.Description
                            }));
                return list;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return list;
        }
    }
}