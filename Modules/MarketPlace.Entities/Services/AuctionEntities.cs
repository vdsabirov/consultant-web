﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.Data.Entity;
using Auction = MarketPlace.Infrastructure.Models.Auction;
using AuctionProposal = MarketPlace.Infrastructure.Models.AuctionProposal;

namespace MarketPlace.Entities.Services
{
    public class AuctionEntities : IAuctionEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public AuctionEntities(
            IHttpContextAccessor actionContext,
            ApplicationDbContext context
            )
        {
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (context == null) throw new ArgumentNullException(nameof(context));
            _actionContext = actionContext;
            _context = context;
        }

        public string UserId
        {
            get
            {
                try
                {
                    var user =
                        _context.Users.FirstOrDefault(
                            f => f.Email.ToLower().Equals(_actionContext.HttpContext.User.Identity.Name.ToLower()));
                    if (user == null)
                        throw new Exception(Messages.MessagesErrorUserNotFound);
                    return user.Id;
                }
                catch (Exception ex)
                {
                    ErrorService.Handle(ex);
                }
                return new Guid().ToString();
            }
        }

        public Auction GetAuctionByOrderId(int orderId)
        {
            if (orderId <= 0) return new Auction();
            try
            {
                return
                    Mappers.Auction.MapTo(
                        _context.Auctions.Include(o => o.Order)
                            .Include(a => a.AuctionProposals)
                            .FirstOrDefault(f => f.OrderId == orderId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Auction();
        }

        public List<Auction> GetAuctions()
        {
            var auctions = new List<Auction>();
            try
            {
                _context.Auctions.Include(o => o.Order)
                    .Where(f => f.IsOpen && DateTime.Now >= f.OpeningTime && DateTime.Now < f.ClosingTime)
                    .ToList()
                    .ForEach(a => auctions.Add(Mappers.Auction.MapTo(a)));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return auctions;
        }

        public void SetProposal(int auctionId, decimal price)
        {
            try
            {
                _context.AuctionРroposals.Add(new Models.AuctionProposal
                {
                    AuctionId = auctionId,
                    Price = price,
                    UserId = UserId,
                    ProposalTime = DateTime.Now,
                    Accepted = false
                });
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public void SetWinner(int auctionРroposalId)
        {
            try
            {
                var currentUserId = _context.GetCurrentUserId(_actionContext, _errorService);

                var auctionРroposal = _context.AuctionРroposals.FirstOrDefault(f => f.Id == auctionРroposalId);
                if(auctionРroposal == null) return;
                

                var auction = _context.Auctions.Include(o => o.Order).FirstOrDefault(f => f.Id == auctionРroposal.AuctionId);
                if (auction == null || !auction.IsOpen || auction.Order.UserId != currentUserId) return;

                auctionРroposal.Accepted = true;
                auction.IsOpen = false;
                auction.ClosingTime = DateTime.Now;

                var order = _context.Orders.FirstOrDefault(f => f.Id == auction.OrderId);
                if (order == null) return;
                order.OrderStateId = (int) OrdersStates.Performing;

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        Auction IAuctionEntities.GetAuction(int auctionId)
        {
            return GetAuction(auctionId);
        }

        public void Delete(int auctionId)
        {
            try
            {
                var auction = _context.Auctions.FirstOrDefault(f => f.Id == auctionId);
                if (auction == null) return;
                _context.Auctions.Remove(auction);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public List<AuctionProposal> GetAuctionРroposalsByOrderId(int orderId)
        {
            try
            {
                return
                    Mappers.Auction.MapToAuctionProposals(
                        _context.AuctionРroposals.Include(a => a.Auction)
                            .Where(f => f.Auction.OrderId == orderId)
                            .ToList()).ToList();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new List<AuctionProposal>();
        }

        public List<AuctionProposal> GetAuctionРroposalsByAuctionId(int auctionId)
        {
            try
            {
                return
                    Mappers.Auction.MapToAuctionProposals(
                        _context.AuctionРroposals
                            .Where(f => f.AuctionId == auctionId)
                            .ToList()).ToList();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new List<AuctionProposal>();
        }

        public Auction Update(Auction auction)
        {
            if (auction == null) return new Auction();

            try
            {
                var auctionByOrder = _context.Auctions.FirstOrDefault(f => f.OrderId == auction.OrderId);
                if (auctionByOrder != null)
                {
                    ErrorService.AccessDenied();
                    return Mappers.Auction.MapTo(auctionByOrder);
                }

                var entityAuction = _context.Auctions.FirstOrDefault(f => f.Id == auction.Id);
                if (entityAuction == null)
                {
                    entityAuction = new Models.Auction() { OpeningTime = DateTime.Now, ClosingTime = DateTime.Now.AddDays(1)};
                    Mappers.Auction.CopyFrom(auction, entityAuction);
                    _context.Auctions.Add(entityAuction);

                    //Устанавливаем нужный статус у заказа
                    var order = _context.Orders.FirstOrDefault(f => f.Id == auction.OrderId);
                    if (order == null)
                    {
                        ErrorService.ItemNotFound();
                        return new Auction();
                    }
                    order.OrderStateId = (int) OrdersStates.OnTheAuction;
                }
                else
                {
                    Mappers.Auction.CopyFrom(auction, entityAuction);
                }
                _context.SaveChanges();
                return GetAuction(entityAuction.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new Auction();
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public Auction GetAuction(int auctionId)
        {
            if (auctionId <= 0) return new Auction();
            try
            {
                return
                    Mappers.Auction.MapTo(
                        _context.Auctions.Include(a => a.AuctionProposals).FirstOrDefault(f => f.Id == auctionId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Auction();
        }
    }
}