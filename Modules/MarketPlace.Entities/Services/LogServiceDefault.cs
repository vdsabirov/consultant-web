﻿using System;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Log = MarketPlace.Infrastructure.Models.Log;

namespace MarketPlace.Entities.Services
{
    public class LogServiceDefault : ILogService
    {
        private readonly ILogEntities _logEntities;

        public LogServiceDefault(
            ApplicationDbContext context,
            IHttpContextAccessor actionContext
            )
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _logEntities = new LogEntities(context, actionContext);
        }

        public void Write(Log log)
        {
            _logEntities.Write(log);
        }
    }
}