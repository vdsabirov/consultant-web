﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Product = MarketPlace.Infrastructure.Models.Product;
using ProductCategory = MarketPlace.Infrastructure.Models.ProductCategory;

namespace MarketPlace.Entities.Services
{
    public class ProductEntities : IProductEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public ProductEntities(
            ApplicationDbContext context,
            IHttpContextAccessor actionContext
            )
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _context = context;
            _actionContext = actionContext;
        }

        public List<ProductCategory> GetProductCategories()
        {
            try
            {
                return _context.ProductCategories.Select(c => new ProductCategory {Id = c.Id, Title = c.Title}).ToList();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new List<ProductCategory>();
        }

        public string GetProductCategoryName(int productCategoryId)
        {
            try
            {
                var productCategory = _context.ProductCategories.FirstOrDefault(f => f.Id == productCategoryId);
                return productCategory?.Title ?? string.Empty;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return string.Empty;
        }

        public Product Update(Product product)
        {
            if (product == null) return new Product();

            try
            {
                var entityProduct = _context.Products.FirstOrDefault(o => o.Id == product.Id);
                if (entityProduct == null)
                {
                    entityProduct = new Models.Product();
                    Mappers.Product.CopyFrom(product, entityProduct);
                    _context.Products.Add(entityProduct);
                }
                else
                {
                    Mappers.Product.CopyFrom(product, entityProduct);
                }
                _context.SaveChanges();
                product.ObjectId = CreateSubProduct(entityProduct);
                return GetProduct(entityProduct.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return product;
        }

        public Product GetProduct(int productId)
        {
            if (productId <= 0)
                return new Product() { ProductCategoryId = (int)ProductsCategories.Windows, Name = "Простое окно", Code = "000" };
            try
            {
                var product = Mappers.Product.MapTo(_context.Products.FirstOrDefault(f => f.Id == productId));
                SetObjectId(product);
                return product;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Product();
        }

        public void Delete(int productId)
        {
            try
            {
                var product = _context.Products.FirstOrDefault(f => f.Id == productId);
                if (product == null) return;
                _context.Products.Remove(product);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public List<Product> GetProducts()
        {
            var products = new List<Product>();
            try
            {
                _context.Products.ToList().ForEach(f => products.Add(Mappers.Product.MapTo(f)));
                return products;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return products;
        }

        public List<Product> GetProductsByOrderId(int id)
        {
            var products = new List<Product>();
            try
            {
                _context.Products.Where(p => p.OrderId == id).ToList().ForEach(f => products.Add(Mappers.Product.MapTo(f)));
                products.ForEach(f => SetObjectId(f));
                return products;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return products;
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        private int CreateSubProduct(Models.Product product)
        {
            if (product == null) return 0;
            switch ((ProductsCategories) product.ProductCategoryId)
            {
                case ProductsCategories.Windows:
                    var window = CreateProductWindow(product);
                    return window == null ? 0 : window.Id;
                case ProductsCategories.Doors:
                    var door = CreateProductDoor(product);
                    return door == null ? 0 : door.Id;
                default:
                    return 0;
            }
        }

        private Models.ProductDoor CreateProductDoor(Models.Product product)
        {
            if (product == null) return null;

            try
            {
                var entityProductDoor = _context.ProductDoors.FirstOrDefault(o => o.Id == product.Id);
                if (entityProductDoor == null)
                {
                    entityProductDoor = new Models.ProductDoor { ProductId = product.Id };
                    _context.ProductDoors.Add(entityProductDoor);
                }
                else
                {
                    entityProductDoor.ProductId = product.Id;
                }
                _context.SaveChanges();
                return entityProductDoor;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return null;
        }

        private Models.ProductWindow CreateProductWindow(Models.Product product)
        {
            if (product == null) return null;

            try
            {
                var entityProductWindow = _context.ProductWindows.FirstOrDefault(o => o.Id == product.Id);
                if (entityProductWindow == null)
                {
                    entityProductWindow = new Models.ProductWindow
                    {
                        ProductId = product.Id,
                        Height = 180,
                        Width = 100,
                        ProductWindowBrandId = 1,
                        ProductWindowFindingId = 1,
                        ProductWindowLaminatingId = 1,
                        ProductWindowSystemId = 1,
                        ProductWindowTintingId = 1,
                        ProductWindowsillId = 1
                    };
                    _context.ProductWindows.Add(entityProductWindow);
                }
                else
                {
                    entityProductWindow.ProductId = product.Id;
                }
                _context.SaveChanges();
                return entityProductWindow;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return null;
        }


        private void SetObjectId(Product product)
        {
            if (product.ObjectId > 0 || product.Id <= 0)
                return;
            switch ((ProductsCategories)product.ProductCategoryId)
            {
                case ProductsCategories.Windows:
                    var window = _context.ProductWindows.FirstOrDefault(f => f.ProductId == product.Id);
                    if (window != null)
                        product.ObjectId = window.Id;
                    break;
                case ProductsCategories.Doors:
                    var door = _context.ProductDoors.FirstOrDefault(f => f.ProductId == product.Id);
                    if (door != null)
                        product.ObjectId = door.Id;
                    break;
                default:
                    break;
            }
        }
    }
}