﻿using System;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using ProductDoor = MarketPlace.Infrastructure.Models.ProductDoor;

namespace MarketPlace.Entities.Services
{
    public class ProductDoorEntities : IProductDoorEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public ProductDoorEntities(
            ApplicationDbContext context,
            IHttpContextAccessor actionContext
            )
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _context = context;
            _actionContext = actionContext;
        }

        public ProductDoor GetProduct(int productDoorId)
        {
            try
            {
                return Mappers.ProductDoor.MapTo(_context.ProductDoors.FirstOrDefault(f => f.Id == productDoorId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new ProductDoor { Id = productDoorId };
        }

        public ProductDoor GetProductByProductId(int productId)
        {
            try
            {
                return Mappers.ProductDoor.MapTo(_context.ProductDoors.FirstOrDefault(f => f.ProductId == productId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new ProductDoor { ProductId = productId };
        }

        public ProductDoor Update(ProductDoor product)
        {
            if (product == null) return new ProductDoor();

            try
            {
                var entityProduct = _context.ProductDoors.FirstOrDefault(o => o.Id == product.Id);
                if (entityProduct == null)
                {
                    entityProduct = new Models.ProductDoor();
                    Mappers.ProductDoor.CopyFrom(product, entityProduct);
                    _context.ProductDoors.Add(entityProduct);
                }
                else
                {
                    Mappers.ProductDoor.CopyFrom(product, entityProduct);
                }
                _context.SaveChanges();
                return GetProduct(entityProduct.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return product;
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

    }
}