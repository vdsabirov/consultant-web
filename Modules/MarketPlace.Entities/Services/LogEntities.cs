﻿using System;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Log = MarketPlace.Infrastructure.Models.Log;

namespace MarketPlace.Entities.Services
{
    public class LogEntities : ILogEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;

        public LogEntities(
            ApplicationDbContext context,
            IHttpContextAccessor actionContext
            )
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _actionContext = actionContext;
            _context = context;
        }

        public void Write(Log log)
        {
            if (log == null) return;
            try
            {
                if (string.IsNullOrEmpty(log.Message))
                    log.Message = string.Empty; //TODO определить текст

                _context.Logs.Add(new Models.Log
                {
                    LogTypeId = (int) log.LogsType,
                    Message =  log.Message,
                    UserId = UserId,
                    Time = log.Time,
                    Url = log.Url
                });
                _context.SaveChanges();
            }
            catch
            {
                // ignored
            }
        }

        public string UserId
        {
            get
            {
                try
                {
                    var user =
                        _context.Users.FirstOrDefault(
                            f => f.Email.ToLower().Equals(_actionContext.HttpContext.User.Identity.Name.ToLower()));
                    if (user == null)
                        throw new Exception(Messages.MessagesErrorUserNotFound);
                    return user.Id;
                }
                catch
                {
                    // ignored
                }
                return new Guid().ToString();
            }
        }
    }
}