﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using AuctionProposal = MarketPlace.Infrastructure.Models.AuctionProposal;

namespace MarketPlace.Entities.Services
{
    public class AuctionProposalEntities : IAuctionProposalEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public AuctionProposalEntities(
            IHttpContextAccessor actionContext,
            ApplicationDbContext context
            )
        {
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (context == null) throw new ArgumentNullException(nameof(context));
            _actionContext = actionContext;
            _context = context;
        }

        public AuctionProposal GetAuctionРroposal(int auctionProposalId)
        {
            if (auctionProposalId <= 0) return new AuctionProposal() { UserId = _context.GetCurrentUserId(_actionContext, _errorService)};
            try
            {
                return Mappers.Auction.MapToInfrastructureAuctionProposal(_context.AuctionРroposals.FirstOrDefault(f => f.Id == auctionProposalId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new AuctionProposal();
        }

        public List<AuctionProposal> GetAuctionРroposals()
        {
            var auctionProposals = new List<AuctionProposal>();
            try
            {
                var currentUserId = _context.GetCurrentUserId(_actionContext, _errorService);
                _context.AuctionРroposals.Where(a => a.UserId == currentUserId).ToList().ForEach(f => auctionProposals.Add(Mappers.Auction.MapToInfrastructureAuctionProposal(f)));
                return auctionProposals;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return auctionProposals;
        }

        public AuctionProposal Update(AuctionProposal auctionProposal)
        {
            if (auctionProposal == null) return new AuctionProposal();

            var currentUserId = _context.GetCurrentUserId(_actionContext, _errorService);
            if (currentUserId != auctionProposal.UserId)
            {
                _errorService.ItemNotFound();
                return new AuctionProposal();
            }

            try
            {
                var entityAuctionProposal = _context.AuctionРroposals.FirstOrDefault(f => f.AuctionId == auctionProposal.AuctionId && f.UserId == currentUserId);
                if (entityAuctionProposal == null)
                {
                    entityAuctionProposal = new Models.AuctionProposal();
                    Mappers.Auction.CopyFromInfrastructureAuctionProposal(auctionProposal, entityAuctionProposal);
                    _context.AuctionРroposals.Add(entityAuctionProposal);
                }
                else
                {
                    entityAuctionProposal.Price = auctionProposal.Price;
                    entityAuctionProposal.ProposalTime = auctionProposal.ProposalTime;
                    //Mappers.Auction.CopyFromInfrastructureAuctionProposal(auctionProposal, entityAuctionProposal);
                }
                _context.SaveChanges();
                return GetAuctionРroposal(entityAuctionProposal.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new AuctionProposal();
        }

        public void Delete(int auctionProposalId)
        {
            try
            {
                var auctionProposal = _context.AuctionРroposals.FirstOrDefault(f => f.Id == auctionProposalId);
                if (auctionProposal == null) return;
                _context.AuctionРroposals.Remove(auctionProposal);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}