﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using MarketPlace.Entities.Models;
using Message = MarketPlace.Infrastructure.Models.Message;

namespace MarketPlace.Entities.Services
{
    public class MessageEntities : IMessageEntities
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly ApplicationDbContext _context;
        private IErrorService _errorService;

        public MessageEntities( IHttpContextAccessor actionContext,
            ApplicationDbContext context)
        {
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (context == null) throw new ArgumentNullException(nameof(context));
            _actionContext = actionContext;
            _context = context;
        }

        public IErrorService ErrorService
        {
            get
            {
                return _errorService ??
                       (_errorService =
                           new ErrorServiceDefault(new LogServiceDefault(_context, _actionContext), _actionContext));
            }
            set { _errorService = value; }
        }

        public void Delete(int messageId)
        {
            throw new NotImplementedException();
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public Message GetMessage(int messageId = -1)
        {
            var currentUserId = _context.GetCurrentUserId(_actionContext, ErrorService);
            if (messageId <= 0) return new Message {SenderId = currentUserId, CreatedAt = DateTime.Now };
            try
            {
                return Mappers.Message.MapTo(_context.Messages.FirstOrDefault(f => f.Id == messageId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return new Message { SenderId = currentUserId, CreatedAt = DateTime.Now };
        }

        public List<Message> GetMessages()
        {
            var messages = new List<Message>();
            try
            {
                _context.Messages.ToList().ForEach(f => messages.Add(Mappers.Message.MapTo(f)));
                return messages;
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return messages;
        }

        public List<Message> GetMessages(string senderId, string recievierId)
        {
            var messages = new List<Message>();
            try
            {
                var curentUserId = _context.GetCurrentUserId(_actionContext, _errorService);
                if (senderId != curentUserId && recievierId != curentUserId)
                    return messages;
                
                if (senderId == null)
                    _context.Messages.Where(f => f.RecieverId.Equals(recievierId))
                        .ToList()
                        .ForEach(f => messages.Add(Mappers.Message.MapTo(f)));
                if (recievierId == null)
                    _context.Messages.Where(f => f.SenderId.Equals(senderId))
                        .ToList()
                        .ForEach(f => messages.Add(Mappers.Message.MapTo(f)));

                if (senderId != null &&  recievierId != null)
                    _context.Messages.Where(f => f.SenderId.Equals(senderId) && f.RecieverId.Equals(recievierId))
                        .ToList()
                        .ForEach(f => messages.Add(Mappers.Message.MapTo(f)));

                return messages.OrderByDescending(m => m.CreatedAt).ToList();
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return messages;
        }

        public bool IsMessageReciever(int messageId, string userId)
        {
            if (messageId <= 0 || string.IsNullOrEmpty(userId)) return false;
            try
            {
                return _context.Messages.Any(o => o.Id == messageId && o.RecieverId.Equals(userId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return false;
        }

        public bool IsMessageSender(int messageId, string userId)
        {
            if (messageId <= 0 || string.IsNullOrEmpty(userId)) return false;
            try
            {
                return _context.Messages.Any(o => o.Id == messageId && o.SenderId.Equals(userId));
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }
            return false;
        }

        public Message Update(Message message)
        {
            var currentUserId = _context.GetCurrentUserId(_actionContext, ErrorService);
            if (message == null) return new Message { SenderId = currentUserId, CreatedAt = DateTime.Now };

            try
            {
                var entityMessage = _context.Messages.FirstOrDefault(f => f.Id == message.Id);
                if (entityMessage == null)
                {
                    entityMessage = new Models.Message();
                    Mappers.Message.CopyFrom(message, entityMessage);
                    _context.Messages.Add(entityMessage);
                }
                else
                {
                    Mappers.Message.CopyFrom(message, entityMessage);
                }
                _context.SaveChanges();
                return GetMessage(entityMessage.Id);
            }
            catch (Exception ex)
            {
                ErrorService.Handle(ex);
            }

            return new Message { SenderId = currentUserId, CreatedAt = DateTime.Now };
        }
    }
}
