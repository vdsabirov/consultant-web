﻿using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Models;
using Microsoft.Data.Entity;
using Order = MarketPlace.Entities.Models.Order;
using OrderState = MarketPlace.Entities.Models.OrderState;
using Product = MarketPlace.Entities.Models.Product;
using ProductCategory = MarketPlace.Entities.Models.ProductCategory;
using ProductDoor = MarketPlace.Entities.Models.ProductDoor;
using ProductWindow = MarketPlace.Entities.Models.ProductWindow;
using ProductWindowBrand = MarketPlace.Entities.Models.ProductWindowBrand;
using ProductWindowFinding = MarketPlace.Entities.Models.ProductWindowFinding;
using ProductWindowGlazingType = MarketPlace.Entities.Models.ProductWindowGlazingType;
using ProductWindowLaminating = MarketPlace.Entities.Models.ProductWindowLaminating;
using ProductWindowSystem = MarketPlace.Entities.Models.ProductWindowSystem;
using ProductWindowsill = MarketPlace.Entities.Models.ProductWindowsill;
using ProductWindowTinting = MarketPlace.Entities.Models.ProductWindowTinting;

using Newtonsoft.Json.Linq;

namespace MarketPlace.Entities.InitDataStore
{
    public class DataStoreInit
    {
        public static void CreateSampleData( ApplicationDbContext context, string userId)
        {
            using (var transaction = context.Database.BeginTransaction())
            {

                // add orders
                var orders =
                    Enum.GetValues(typeof (OrdersStates))
                        .Cast<OrdersStates>()
                        .ToList()
                        .Select(v => new Order
                        {
                            UserId = userId,
                            Name = $"Заказ {9 - (int) v}",
                            OrderDate = DateTime.Today.AddDays(-((int) v - 1)),
                            Code = $"00{9 - (int) v}",
                            OrderStateId = (int) v,
                            Products = new List<Product>
                            {
                                new Product
                                {
                                    Code = "001",
                                    Name = "Окно",
                                    ProductCategoryId = (int) ProductsCategories.Windows,
                                    ViewOrder = 1,
                                    Quantity = 2

                                }
                            }
                        }).ToList();
                orders.ForEach(m => context.Orders.AddRange(m));
                context.SaveChanges();

                var orderProductWindows = new List<ProductWindow>();
                var orderProductDoors = new List<ProductDoor>();
                foreach (var product in context.Products)
                {
                    if (product.ProductCategoryId == (int)ProductsCategories.Windows)
                    {
                        orderProductWindows.Add(new ProductWindow
                        {
                            Product = product,
                            ProductId = product.Id,
                            Width = 100,
                            Height = 200,
                            ProductWindowBrand = context.ProductWindowBrands.First(),
                            ProductWindowSystem = context.ProductWindowSystems.First(),
                            ProductWindowFinding = context.ProductWindowFindings.First(),
                            ProductWindowTinting = context.ProductWindowTintings.First(),
                            ProductWindowLaminating = context.ProductWindowLaminatings.First(),
                            ProductWindowsill = context.ProductWindowsills.First(),
                        });
                    }
                    if (product.ProductCategoryId == (int)ProductsCategories.Doors)
                    {
                        orderProductDoors.Add(new ProductDoor
                        {
                            Product = product,
                            ProductId = product.Id,
                            Width = 150,
                            Height = 400,
                            Peephole = true
                        });
                    }
                }
                orderProductWindows.ForEach(w => context.ProductWindows.AddRange(w));
                orderProductDoors.ForEach(w => context.ProductDoors.AddRange(w));
                context.SaveChanges();

                transaction.Commit();
            }
        }

        public static void CreateVocablaryData(ApplicationDbContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                //if (context.ProductWindowBrands.Count() == 0)
                //{
                //    // add ProductWindowBrands
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowBrands] ON");
                //    var brands = GetDefaultProductWindowBrands();
                //    brands.ForEach(f => context.ProductWindowBrands.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowBrands] OFF");
                //}

                //if (context.ProductWindowFindings.Count() == 0)
                //{
                //    // add ProductWindowFindings
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowFindings] ON");
                //    var findings = GetDefaultProductWindowFindings();
                //    findings.ForEach(f => context.ProductWindowFindings.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowFindings] OFF");
                //}

                //if (context.ProductWindowGlazingTypes.Count() == 0)
                //{
                //    // add ProductWindowGlazingTypes
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowGlazingTypes] ON");
                //    var productWindowGlazingTypes = GetDefaultProductWindowGlazingTypes();
                //    productWindowGlazingTypes.ForEach(f => context.ProductWindowGlazingTypes.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowGlazingTypes] OFF");
                //}

                //if (context.ProductWindowLaminatings.Count() == 0)
                //{
                //    // add ProductWindowLaminatings
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowLaminatings] ON");
                //    var productWindowLaminatings = GetDefaultProductWindowLaminatings();
                //    productWindowLaminatings.ForEach(f => context.ProductWindowLaminatings.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowLaminatings] OFF");
                //}

                //if (context.ProductWindowSystems.Count() == 0)
                //{
                //    // add ProductWindowSystems
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowSystems] ON");
                //    var productWindowSystems = GetDefaultProductWindowSystems();
                //    productWindowSystems.ForEach(f => f.ProductWindowBrand = context.ProductWindowBrands.First());
                //    productWindowSystems.ForEach(f => context.ProductWindowSystems.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowSystems] OFF");
                //}

                //if (context.ProductWindowTintings.Count() == 0)
                //{
                //    // add ProductWindowTintings
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowTintings] ON");
                //    var productWindowTintings = GetDefaultProductWindowTintings();
                //    productWindowTintings.ForEach(f => context.ProductWindowTintings.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowTintings] OFF");
                //}

                //if (context.ProductWindowsills.Count() == 0)
                //{
                //    // add ProductWindowsills
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowsills] ON");
                //    var productWindowsills = GetDefaultProductWindowsills();
                //    productWindowsills.ForEach(f => context.ProductWindowsills.AddRange(f));
                //    context.SaveChanges();
                //    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductWindowsills] OFF");
                //}

                if (context.ProductCategories.Count() == 0)
                {
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductCategories] ON");
                    // add product categories
                    var productCategories = new List<ProductCategory>
                    {
                        new ProductCategory {Id = 1, Title = "Окна"},
                        new ProductCategory {Id = 2, Title = "Двери"}
                    };
                    productCategories.ForEach(f => context.ProductCategories.AddRange(f));
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[ProductCategories] OFF");
                }

                if (context.OrderStates.Count() == 0)
                {
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[OrderStates] ON");
                    // add orderStates
                    var orderStates = new List<OrderState>
                    {
                        new OrderState {Id = 1, Name = "Формируется"},
                        new OrderState {Id = 2, Name = "Сформирован"},
                        new OrderState {Id = 3, Name = "Отменен"},
                        new OrderState {Id = 4, Name = "На тендере"},
                        new OrderState {Id = 5, Name = "На аукционе"},
                        new OrderState {Id = 6, Name = "Исполняется"},
                        new OrderState {Id = 7, Name = "Выполнен"},
                        new OrderState {Id = 8, Name = "Заархивирован"},
                        new OrderState {Id = 9, Name = "Ожидает оплату"}
                    };
                    orderStates.ForEach(m => context.OrderStates.AddRange(m));
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[OrderStates] OFF");
                }

                if (context.LogTypes.Count() == 0)
                {
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[LogTypes] ON");
                    Enum.GetValues(typeof(LogsTypes))
                        .Cast<LogsTypes>()
                        .ToList()
                        .Select(f => new LogType { Id = (int)f, Name = f.ToString() })
                        .ToList()
                        .ForEach(l => context.LogTypes.Add(l));
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[LogTypes] OFF");
                }

                transaction.Commit();
            }
        }

        public static void CreateCompanies(ApplicationDbContext context)
        {
            var companies = new List<Models.Company>()
                {
                    new Models.Company {Name = "MarketPlace"},
                    new Models.Company {Name = "Northwind"},
                    new Models.Company {Name = "AdventureWorks"}
                };
            companies.ForEach(f => context.Companies.AddRange(f));
            context.SaveChanges();
        }

        private static List<ProductWindowBrand> GetDefaultProductWindowBrands()
        {
            var jBrands = @"[
                {
                  'Id': 1,
                  'Title': 'Профиль REHAU',
                  'Image': ''
                },
                {
                  'Id': 2,
                  'Title': 'Профиль VEKA',
                  'Image': ''
                },
                {
                  'Id': 3,
                  'Title': 'Профиль Salamander',
                  'Image': ''
                },
                {
                  'Id': 4,
                  'Title': 'Профиль KBE',
                  'Image': ''
                },
                {
                  'Id': 5,
                  'Title': 'Профиль WDS',
                  'Image': ''
                }
              ]";

            return  JArray.Parse(jBrands).ToObject<List<ProductWindowBrand>>();
        }

        private static List<ProductWindowFinding> GetDefaultProductWindowFindings()
        {
            var jarray = @" [
                {
                  'Id': 1,
                  'Title': 'Фурнитура Roto'
                },
                {
                  'Id': 2,
                  'Title': 'Фурнитура FUHR'
                },
                {
                  'Id': 3,
                  'Title': 'Фурнитура Siegenia'
                },
                {
                  'Id': 4,
                  'Title': 'Фурнитура Axor'
                }
              ]";

            return JArray.Parse(jarray).ToObject<List<ProductWindowFinding>>();
        }

        private static List<ProductWindowGlazingType> GetDefaultProductWindowGlazingTypes()
        {
            var jarray = @" [
                {
                  'Id': 1,
                  'Title': 'Шумоизоляция'
                },
                {
                  'Id': 2,
                  'Title': 'Теплоизоляция'
                },
                {
                  'Id': 3,
                  'Title': 'Светоизоляция'
                },
                {
                  'Id': 4,
                  'Title': 'Повышенная прочность'
                }
              ]";

            return JArray.Parse(jarray).ToObject<List<ProductWindowGlazingType>>();
        }

        private static List<ProductWindowLaminating> GetDefaultProductWindowLaminatings()
        {
            var jarray = @" [
                {
                  'Id': 1,
                  'Title': 'Под дерево, ольха'
                },
                {
                  'Id': 2,
                  'Title': 'Под дерево, Дуб'
                },
                {
                  'Id': 3,
                  'Title': 'Однотонная, цвет'
                }
              ]";

            return JArray.Parse(jarray).ToObject<List<ProductWindowLaminating>>();
        }

        private static List<ProductWindowSystem> GetDefaultProductWindowSystems()
        {
            var jarray = @" [
                {
                  'Id': 1,
                  'Title': 'SIB-DESIGN (70 ММ / 3 КАМЕРЫ И ТЕРМОБЛОК)',
                  'Description': '<h4>\nСПЕЦИАЛЬНО ДЛЯ СУРОВОГО КЛИМАТА\n</h4>\n\n<p>\nВ основе разработки REHAU Sib-Design лежат составляющие, которые позволяют обеспечить высокие энергоэффективные характеристики окна: это глубина 70 мм, дополнительная изолирующая воздушная камера между армированием и внутренней стенкой профиля «Thermo-Block» и возможность установки стеклопакета толщиной до 41 мм. Результатом является исключительно высокое сопротивление теплопередачи: 0,71 м2°С/Вт.\n</p>\n\n<h4>\nНА ЛЮБОЙ ВКУС\n</h4>\n\n<p>\nС окнами REHAU SIB-Design становятся возможными различные архитектурные изыски.\n</p>\n\n<p>\n15 мм радиусы на видимых лицевых поверхностях придадут окнам из профилей REHAU SIB-Design элегантный внешний вид.\n</p>\n\n<p>\nПрофили REHAU SIB-Design позволяют изготавливать окна различных форм: от прямоугольного до изогнутого или круглого.\n</p>'
                },
                {
                  'Id': 2,
                  'Title': 'DELIGHT-DESIGN (70 ММ / 5 КАМЕР)',
                  'Description': '<h4>DELIGHT-DESIGN: ГАРМОНИЯ СВЕТА И СТИЛЯ</h4>\n\n<p>\nНоу-хау технического решения DELIGHT-Design позволяет сократить раму коробки и створки и впустить в дом на 10% больше света, по сравнению с традиционными системами. Помимо этого, система DELIGHT-Design дает прекрасную возможность для импровизации и удовлетворения Ваших индивидуальных пожеланий по дизайну окна. Дизайнерскую створку отличают благородные пропорции, округлость форм и изящная рельефность декоративного штапика.\n</p>\n\n<h4>ОСНОВНЫЕ СИСТЕМНЫЕ ХАРАКТЕРИСТИКИ:</h4>\n\n<ul>\n<li>Системная глубина / число камер: 70 мм/ 5 камер.</li>\n<li>Исключительно высокое значение теплоизоляции: rопр=0,8м2 с/вт.</li>\n<li>Взломобезопасность: установка усиленных приборов запирания благодаря смещению оси <li>приборного паза 13 мм.</li>\n<li>Воздухо- и водонепроницаемость: надежная защита от сквозняков, пыли и воды благодаря двум контурам уплотнений (нахлест уплотнений по 7/8 мм снаружи / внутри).</li>\n<li>Поверхность: высококачественная, идеально гладкая, удобная для ухода.</li>\n<ul>'
                }
              ]";

            return JArray.Parse(jarray).ToObject<List<ProductWindowSystem>>();
        }

        private static List<ProductWindowTinting> GetDefaultProductWindowTintings()
        {
            var jarray = @" [
                {
                  'Id': 1,
                  'Title': 'Тонировка 10%, серая'
                },
                {
                  'Id': 2,
                  'Title': 'Тонировка 20%, серая'
                },
                {
                  'Id': 3,
                  'Title': 'Тонировка 20%, цветная'
                },
                {
                  'Id': 4,
                  'Title': 'Тонировка металик'
                }
              ]";
            return JArray.Parse(jarray).ToObject<List<ProductWindowTinting>>();
        }

        private static List<ProductWindowsill> GetDefaultProductWindowsills()
        {
            var jarray = @" [
                {
                    'Id': 1,
                    'Title': '10 см'
                },
                {
                    'Id': 2,
                    'Title': '20 см'
                }
              ]";

            return JArray.Parse(jarray).ToObject<List<ProductWindowsill>>();
        }
    }
}