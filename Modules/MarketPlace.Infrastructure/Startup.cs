﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;

namespace MarketPlace.Infrastructure
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        }

        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}