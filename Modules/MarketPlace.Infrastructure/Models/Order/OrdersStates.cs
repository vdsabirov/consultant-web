﻿namespace MarketPlace.Infrastructure.Models
{
    public enum OrdersStates
    {
        Archived = 8, // Заархивирован
        AtTheTender = 4, // На тендере
        Canceled = 3, // Отменен
        Done = 7, // Выполнен
        Formed = 2, // Сформирован
        Forming = 1, // Формируется
        OnTheAuction = 5, // На аукционе
        Performing = 6, // Исполняется
        WattingPayment = 9 //Ожидает оплату 
    }
}