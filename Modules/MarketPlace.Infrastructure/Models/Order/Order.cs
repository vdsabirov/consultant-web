﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MarketPlace.Infrastructure.Models
{
    public class Order
    {
        public Order()
        {
            Products = new HashSet<Product>();
            OrderStateId = (int)OrdersStates.Forming;
            OrderState = new OrderState { Id = (int)OrdersStates.Forming, Name = OrdersStates.Forming.ToString() };
            OrderDate = DateTime.Today;
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано название заказа")]
        [MinLength(3, ErrorMessage = "Название заказа должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Name { get; set; }

        [Display(Name = "Код")]
        [Required(ErrorMessage = "Не указан код заказа")]
        [MaxLength(50)]
        public string Code { get; set; }

        [Display(Name = "Дата")]
        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public int OrderStateId { get; set; }

        public OrderState OrderState { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        [Required]
        [MaxLength(450)]
        public string UserId { get; set; }

        [Display(Name = "Владелец")]
        public string UserName { get; set; }
        [Display(Name = "Компания")]
        public string CompanyName { get; set; }
    }
}