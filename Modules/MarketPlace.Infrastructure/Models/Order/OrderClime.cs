﻿namespace MarketPlace.Infrastructure.Models
{
    public class OrderClime
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CompanyId { get; set; }
    }
}