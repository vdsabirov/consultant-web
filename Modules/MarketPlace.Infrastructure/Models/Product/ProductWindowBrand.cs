﻿using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class ProductWindowBrand
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано Название")]
        [MinLength(3, ErrorMessage = "Название должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Title { get; set; }

        [Display(Name = "Ссылка на файл")]
        public string Image { get; set; }
    }
}