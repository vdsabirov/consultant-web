﻿using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class ProductCategory
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Категория")]
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
    }
}