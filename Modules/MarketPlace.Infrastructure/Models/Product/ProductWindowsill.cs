﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class ProductWindowsill
    {
        public ProductWindowsill()
        {
            ProductWindows = new HashSet<ProductWindow>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано Название")]
        [MinLength(3, ErrorMessage = "Название должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Title { get; set; }

        public string Description { get; set; }

        public int ProductWindowBrandId { get; set; }
        public ProductWindowBrand ProductWindowBrand { get; set; }

        public virtual ICollection<ProductWindow> ProductWindows { get; set; }

    }
}