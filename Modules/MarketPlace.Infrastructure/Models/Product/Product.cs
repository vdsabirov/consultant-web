﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class Product
    {
        public Product()
        {
            Quantity = 1;
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано название продукта")]
        [MinLength(3, ErrorMessage = "Название продукта должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Name { get; set; }

        [Display(Name = "Код")]
        [Required(ErrorMessage = "Не указан код продукта")]
        [MaxLength(50)]
        public string Code { get; set; }

        [Display(Name = "Порядок")]
        [Required]
        public int ViewOrder { get; set; }

        [Required]
        public int OrderId { get; set; }

        public Order Order { get; set; }

        [Required]
        public int ProductCategoryId { get; set; }

        public ProductCategory ProductCategory { get; set; }

        [Required]
        [Range(1, 1000)]
        [Display(Name = "Количество")]
        public int Quantity { get; set; }

        [Display(Name = "Спецификация")]
        [MaxLength(1024)]
        public string SpecificationLink { get; set; }

        [DefaultValue(-1)]
        public int ObjectId { get; set; }
    }

    public enum ProductsCategories
    {
        Doors = 2,
        Windows = 1
    }
}