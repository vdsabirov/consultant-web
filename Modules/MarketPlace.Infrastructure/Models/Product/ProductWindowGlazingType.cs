﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class ProductWindowGlazingType
    {
        public ProductWindowGlazingType()
        {
            ProductWindowGlazings = new HashSet<ProductWindowGlazing>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано Название")]
        [MinLength(3, ErrorMessage = "Название должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Title { get; set; }

        public virtual ICollection<ProductWindowGlazing> ProductWindowGlazings { get; set; }
    }
}