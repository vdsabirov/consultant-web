﻿namespace MarketPlace.Infrastructure.Models
{
    public class ProductWindowGlazing
    {
        public int ProductWindowId { get; set; }
        public ProductWindow ProductWindow { get; set; }

        public int ProductWindowGlazingTypeId { get; set; }
        public ProductWindowGlazingType ProductWindowGlazingType { get; set; }
    }
}