﻿using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class User
    {

        public string Email { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не указано имя пользователя")]
        [MinLength(3, ErrorMessage = "Имя пользователя должно содержать не менее 3 символов")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        //[Display(Name = "Отчество")]
        //[Required(ErrorMessage = "Не указано отчество пользователя")]
        //[MinLength(3, ErrorMessage = "Отчество пользователя должно содержать не менее 3 символов")]
        //[MaxLength(50)]
        //public string MiddleName { get; set; }

        //[Display(Name = "Фамилия")]
        //[Required(ErrorMessage = "Не указана фамилия пользователя")]
        //[MinLength(3, ErrorMessage = "Фамилия пользователя должна содержать не менее 3 символов")]
        //[MaxLength(50)]
        //public string LastName { get; set; }

        [Required]
        [MaxLength(450)]
        public string Id { get; set; }

        public int? CompanyId { get; set; }
        public Company Company { get; set; }

    }
}