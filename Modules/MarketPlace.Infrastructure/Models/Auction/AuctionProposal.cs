using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class AuctionProposal
    {
        public AuctionProposal()
        {
            ProposalTime = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public int AuctionId { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public DateTime ProposalTime { get; set; }

        [DefaultValue(false)]
        public bool Accepted { get; set; }

        public virtual Auction Auction { get; set; }

        [Required]
        [MaxLength(450)]
        public string UserId { get; set; }

        //public ApplicationUser User { get; set; }
    }
}