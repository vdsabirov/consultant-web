﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Infrastructure.Models
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string SenderId { get; set; }
        public string RecieverId { get; set; }

        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(128)]
        public string Text { get; set; }
        public int LinkedTo { get; set; }
        public bool IsNew { get; set; }
        public DateTime ReadAt { get; set; }
    }
}
