﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class Company
    {
        public Company()
        {
            MarketPlaceUsers = new HashSet<User>();
        }

        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Не указано название компании")]
        [MinLength(3, ErrorMessage = "Название компании должно содержать не менее 3 символов")]
        [MaxLength(128)]
        public string Name { get; set; }

        public virtual ICollection<User> MarketPlaceUsers { get; set; }
    }
}