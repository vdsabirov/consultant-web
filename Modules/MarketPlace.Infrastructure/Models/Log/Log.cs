﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Infrastructure.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(1024)]
        [Required]
        public string Message { get; set; }

        [MaxLength(1024)]
        public string Url { get; set; }

        [Required]
        public DateTime Time { get; set; }

        [Required]
        public LogsTypes LogsType { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}