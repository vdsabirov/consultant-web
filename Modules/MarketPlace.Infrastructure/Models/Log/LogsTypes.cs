﻿namespace MarketPlace.Infrastructure.Models
{
    public enum LogsTypes
    {
        Error = 1,
        AccessDenied = 2,
        ItemNotFound = 3
    }
}