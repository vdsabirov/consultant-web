﻿namespace MarketPlace.Infrastructure.Constants
{
    public class Messages
    {
        public const string MessagesAccessDenied = "Отсутствует доступ к запрашиваемому ресурсу";
        public const string MessagesErrorUserNotFound = "Не удалось определить текущего пользователя";
        public const string MessagesErrorUnknown = "Не удалось определить причину ошибки";
        public const string MessagesItemNotFound = "Не удалось определить запрашиваемый объект";
    }
}