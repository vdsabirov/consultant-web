﻿namespace MarketPlace.Infrastructure.Constants
{
    public class Identity
    {
        public const string RolesAdministrators = "Administrators";
        public const string RolesAdministratorsNormalizedName = "Администраторы";

        public const string RolesManagers = "Managers";
        public const string RolesManagersNormalizedName = "Менеджеры";

        public const string RolesConsultants = "Consultants";
        public const string RolesConsultantsNormalizedName = "Консультанты";

        public const string RolesEstimators = "Estimators";
        public const string RolesEstimatorsNormalizedName = "Расчетчик";

        public const string RolesUsers = "Users";
        public const string RolesUsersNormalizedName = "Пользователи";
    }
}