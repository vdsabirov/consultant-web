﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductServices
    {
        /// <summary>
        /// Список категорий продуктов
        /// </summary>
        /// <returns></returns>
        List<ProductCategory> GetProductCategories();
        /// <summary>
        /// Получить название категории продукта
        /// </summary>
        /// <param name="productCategoryId">Id категории продукта</param>
        /// <returns></returns>
        string GetProductCategoryName(int productCategoryId);
        /// <summary>
        /// Получить продукт заданной категории
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <param name="productCategoryId">Id продукта</param>
        /// <returns></returns>
        Product GetNewProduct(int orderId = -1, int productCategoryId = -1);
        /// <summary>
        /// Редактировать продукт
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        Product Update(Product product);
        /// <summary>
        /// Получить продукт
        /// </summary>
        /// <param name="productId">Id ghjlernf</param>
        /// <returns></returns>
        Product GetProduct(int productId);

        List<Product> GetProductsByOrderId(int id);
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }
        /// <summary>
        /// Удалить продукт
        /// </summary>
        /// <param name="productId">Id продукта</param>
        void Delete(int productId);
        List<Product> GetProducts();
        void Dispose(bool disposing);
    }
}