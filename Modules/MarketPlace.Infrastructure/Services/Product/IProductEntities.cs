﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductEntities
    {
        List<ProductCategory> GetProductCategories();
        string GetProductCategoryName(int productCategoryId);
        Product Update(Product product);
        IErrorService ErrorService { get; set; }
        Product GetProduct(int productId);
        void Delete(int productId);
        List<Product> GetProducts();
        void Dispose(bool disposing);

        List<Product> GetProductsByOrderId(int id);
    }
}