﻿using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductDoorServices
    {
        /// <summary>
        /// Получить продукт категории ДВЕРИ
        /// </summary>
        /// <param name="productDoorId">Id продукта категории ДВЕРИ</param>
        /// <returns></returns>
        ProductDoor GetProduct(int productDoorId);
        /// <summary>
        /// Получить продукт по Id продукта
        /// </summary>
        /// <param name="productId">Id продукта</param>
        /// <returns></returns>
        ProductDoor GetProductByProductId(int productId);
        /// <summary>
        /// Редактировать продукт
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        ProductDoor Update(ProductDoor product);
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }
    }
}