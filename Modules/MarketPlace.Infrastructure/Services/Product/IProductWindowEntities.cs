﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductWindowEntities
    {
        ProductWindow GetProduct(int productWindowId);
        ProductWindow GetProductByProductId(int productId);
        ProductWindow Update(ProductWindow product);
        IErrorService ErrorService { get; set; }
        void Delete(int productWindowId);
        List<ProductWindow> GetProducts();
        void Dispose(bool disposing);

        List<ProductWindowBrand> GetProductWindowBrands();
        List<ProductWindowFinding> GetProductWindowFindings();
        List<ProductWindowGlazingType> GetProductWindowGlazingTypes();
        List<ProductWindowLaminating> GetProductWindowLaminatings();
        List<ProductWindowsill> GetProductWindowills();
        List<ProductWindowSystem> GetProductWindowSystems();
        List<ProductWindowTinting> GetProductWindowTintings();
    }
}