﻿using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductDoorEntities
    {
        ProductDoor GetProduct(int productDoorId);
        ProductDoor GetProductByProductId(int productId);
        ProductDoor Update(ProductDoor product);
        IErrorService ErrorService { get; set; }
    }
}