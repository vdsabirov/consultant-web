﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IProductWindowServices
    {
        /// <summary>
        /// Получить продукт категории ОКНА
        /// </summary>
        /// <param name="productWindowId">Id продукта категории ОКНА</param>
        /// <returns></returns>
        ProductWindow GetProduct(int productWindowId);
        /// <summary>
        /// Получить продукт по Id продукта
        /// </summary>
        /// <param name="productId">Id ghjlernf</param>
        /// <returns></returns>
        ProductWindow GetProductByProductId(int productId);
        /// <summary>
        /// Редактировать продукт
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        ProductWindow Update(ProductWindow product);
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }
        /// <summary>
        /// Удаление продукта категории ОКНА
        /// </summary>
        /// <param name="productWindowId"></param>
        void Delete(int productWindowId);
        /// <summary>
        /// Список продуктов категории ОКНА
        /// </summary>
        /// <returns></returns>
        List<ProductWindow> GetProducts();
        void Dispose(bool disposing);

        List<ProductWindowBrand> GetProductWindowBrands();
        List<ProductWindowFinding> GetProductWindowFindings();
        List<ProductWindowGlazingType> GetProductWindowGlazingTypes();
        List<ProductWindowLaminating> GetProductWindowLaminatings();
        List<ProductWindowsill> GetProductWindowills();
        List<ProductWindowSystem> GetProductWindowSystems();
        List<ProductWindowTinting> GetProductWindowTintings();
    }
}