﻿using System;

namespace MarketPlace.Infrastructure.Services
{
    public interface IErrorService
    {
        /// <summary>
        /// Обработать ошибку
        /// </summary>
        /// <param name="exception">Исключение</param>
        void Handle(Exception exception);
        /// <summary>
        /// Обработать ошибку
        /// </summary>
        /// <param name="errorMessage">текст исключения</param>
        void Handle(string errorMessage = null);
        /// <summary>
        /// Обработать ситуацию с запретом доступа к запрашиваемому объекту
        /// </summary>
        void AccessDenied();
        /// <summary>
        /// Обработать ситуацию когда запрашиваемый объект не найден
        /// </summary>
        void ItemNotFound();
    }
}