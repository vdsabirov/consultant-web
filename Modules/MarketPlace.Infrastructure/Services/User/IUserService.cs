﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Получить данные текущего пользователя
        /// </summary>
        /// <returns></returns>
        User GetCurrentUser();
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }

        void Dispose(bool disposing);
        List<User> GetUsers();

        User GetUser(string id);

        Company GetCompany(int id);

        List<Company> GetCompanies();
    }
}