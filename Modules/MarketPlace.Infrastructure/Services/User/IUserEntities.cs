﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IUserEntities
    {
        User GetCurrentUser();
        IErrorService ErrorService { get; set; }
        void Dispose(bool disposing);
        List<User> GetUsers();
        User GetUser(string id);

        Company GetCompany(int id);

        List<Company> GetCompanies();
    }
}