﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IOrderEntities
    {
        Order GetOrder(int orderId = -1, bool loadProducts = false);
        List<Order> GetOrders();
        List<Order> GetOrdersByCompanyId(int companyId);
        List<Order> GetOrdersByUserId(string userId);
        Company GetCompanyByOrderId(int orderId);
        Company GetCompanyByUserId(string userId);
        bool IsOrderOwner(int orderId, string userId);
        List<Product> GetOrderProducts(int orderId);
        Order Update(Order order);
        void Delete(int orderId);
        string UserId { get; }
        IErrorService ErrorService { get; set; }
        List<OrderState> GetOrderStates();
        void Dispose(bool disposing);
    }
}