﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IOrderService
    {
        /// <summary>
        /// Список заказов
        /// </summary>
        /// <returns></returns>
        List<Order> GetOrders();
        /// <summary>
        /// Редактирование заказа
        /// </summary>
        /// <param name="order">заказ</param>
        /// <returns></returns>
        Order Update(Order order);
        /// <summary>
        /// Получить заказ для редактирования
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <param name="loadProducts">загружать продукты</param>
        /// <returns></returns>
        Order GetOrderForEdit(int orderId, bool loadProducts = false);
        /// <summary>
        /// Получить заказ для просмотра
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <param name="loadProducts">загружать продукты</param>
        /// <returns></returns>
        Order GetOrderForRead(int orderId, bool loadProducts = false);
        /// <summary>
        /// Удалить заказ
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        void Delete(int orderId);
        /// <summary>
        /// Список продуктов заказа
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <returns></returns>
        List<Product> GetOrderProducts(int orderId);
        /// <summary>
        /// Установить статус заказа
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <param name="orderState">статус заказа</param>
        void UpdateState(int orderId, OrdersStates orderState);
        /// <summary>
        /// Новый заказ
        /// </summary>
        /// <returns></returns>
        Order GetNewOrder();
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }
        /// <summary>
        /// Статусы заказа
        /// </summary>
        /// <returns></returns>
        List<OrderState> GetOrderStates();

        /// <summary>
        /// Освобождаем ресурсы
        /// </summary>
        void Dispose(bool disposing);
    }
}