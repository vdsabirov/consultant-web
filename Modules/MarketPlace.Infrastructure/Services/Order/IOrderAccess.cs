﻿using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IOrderAccess
    {
        bool AllowEdit(int orderId);
        bool AllowDelete(int orderId);
        bool AllowEditByState(int orderId);
        bool AllowRead(int orderId);
        bool AllowEditState(int orderId, OrdersStates orderState);
        IErrorService ErrorService { get; set; }

        bool AllowAuction(int orderId);
    }
}