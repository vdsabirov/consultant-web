﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IAuctionProposalEntities
    {
        AuctionProposal GetAuctionРroposal(int auctionProposalId);
        List<AuctionProposal> GetAuctionРroposals();
        AuctionProposal Update(AuctionProposal auctionProposal);
        void Delete(int auctionProposalId);
        IErrorService ErrorService { get; set; }
        void Dispose(bool disposing);
    }
}