﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IAuctionEntities
    {
        List<AuctionProposal> GetAuctionРroposalsByOrderId(int orderId);

        List<AuctionProposal> GetAuctionРroposalsByAuctionId(int auctionId);

        Auction Update(Auction auction);
        IErrorService ErrorService { get; set; }
        string UserId { get; }
        Auction GetAuctionByOrderId(int orderId);
        List<Auction> GetAuctions();
        void SetProposal(int auctionId, decimal price);
        void SetWinner(int auctionРroposalId);
        Auction GetAuction(int auctionId);
        void Delete(int auctionId);
        void Dispose(bool disposing);
    }
}