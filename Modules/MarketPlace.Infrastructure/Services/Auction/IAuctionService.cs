﻿using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    public interface IAuctionService
    {
        /// <summary>
        /// Список предложений аукциона
        /// </summary>
        /// <param name="orderId">Id заказа</param>
        /// <returns></returns>
        List<AuctionProposal> GetAuctionРroposalsByOrderId(int orderId);

        List<AuctionProposal> GetAuctionРroposalsByAuctionId(int auctionId);


        /// <summary>
        /// Редактирование аукциона
        /// </summary>
        /// <param name="auction"></param>
        /// <returns></returns>
        Auction Update(Auction auction);
        /// <summary>
        /// Служба обработки ошибок
        /// </summary>
        IErrorService ErrorService { get; set; }
        /// <summary>
        /// Создание аукциона для заказа
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Auction Create(int orderId);
        /// <summary>
        /// Получить аукцион по Id заказа
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Auction GetAuctionByOrderId(int orderId);
        /// <summary>
        /// Список открытых аукционов
        /// </summary>
        /// <returns></returns>
        List<Auction> GetAuctions();
        /// <summary>
        /// Сделать предложение по аукциону
        /// </summary>
        /// <param name="auctionId">Id аукциона</param>
        /// <param name="price">цена</param>
        void SetProposal(int auctionId, decimal price);
        /// <summary>
        /// Установить победителя аукциона
        /// </summary>
        /// <param name="auctionРroposalId">Id предложения участника аукциона</param>
        void SetWinner(int auctionРroposalId);

        Auction GetAuction(int auctionId);
        void Delete(int auctionId);
        void Dispose(bool disposing);
    }
}