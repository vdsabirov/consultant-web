﻿using MarketPlace.Infrastructure.Models;

namespace MarketPlace.Infrastructure.Services
{
    //TODO интерфейс для службы логов есть еще варианты...
    public interface ILogService
    {
        void Write(Log log);
    }
}