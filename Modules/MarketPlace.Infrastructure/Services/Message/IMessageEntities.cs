﻿using MarketPlace.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Infrastructure.Services
{
    public interface IMessageEntities
    {
        Message GetMessage(int messageId = -1);
        List<Message> GetMessages();
        List<Message> GetMessages(string senderId, string recievierId);
        bool IsMessageSender(int messageId, string userId);
        bool IsMessageReciever(int messageId, string userId);
        Message Update(Message message);
        void Delete(int messageId);
        IErrorService ErrorService { get; set; }
        void Dispose(bool disposing);
    }
}
