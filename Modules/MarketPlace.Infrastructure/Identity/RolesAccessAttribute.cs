﻿using System;
using System.Linq;
using MarketPlace.Infrastructure.Constants;
using Microsoft.AspNet.Mvc.Filters;

namespace MarketPlace.Infrastructure.Identity
{
    public class RolesAccessAttribute : ActionFilterAttribute
    {
        public string Roles { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated && Roles == Constants.Identity.RolesAdministrators)
            {
                return;
            }

            var roles = Roles.Split(',');
            if (context.HttpContext.User.Identity.IsAuthenticated && roles.Any(role => context.HttpContext.User.IsInRole(role)))
            {
                return;
            }

            //TODO В странице "Доступ запрещен" сделать перехват url страницы перенаправления и записать в лог
            context.HttpContext.Response.Redirect(
                Uri.EscapeUriString($"{Common.ErrorPage}{Messages.MessagesAccessDenied}"));

            base.OnActionExecuting(context);
        }
    }
}