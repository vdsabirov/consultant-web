﻿using System;
using System.Security.Claims;
using MarketPlace.Infrastructure.Constants;
using Microsoft.AspNet.Mvc.Filters;

namespace MarketPlace.Infrastructure.Identity
{
    public class ClaimsAccessAttribute : ActionFilterAttribute
    {
        public string Claim { get; set; }
        public string Value { get; set; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated &&
                context.HttpContext.User.IsInRole(Constants.Identity.RolesAdministrators))
                return;

            if (context.HttpContext.User.Identity.IsAuthenticated
                && context.HttpContext.User.Identity is ClaimsIdentity
                && ((ClaimsIdentity) context.HttpContext.User.Identity).HasClaim(x =>
                    x.Type == Claim && x.Value == Value))
            {
                return;
            }

            //TODO В странице "Доступ запрещен" сделать перехват url страницы перенаправления и записать в лог
            context.HttpContext.Response.Redirect(
                Uri.EscapeUriString($"{Common.ErrorPage}{Messages.MessagesAccessDenied}"));

            base.OnActionExecuting(context);
        }
    }
}