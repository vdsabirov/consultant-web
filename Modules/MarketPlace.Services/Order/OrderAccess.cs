﻿using System;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    public class OrderAccess : IOrderAccess
    {
        private readonly IHttpContextAccessor _actionContext;
        //private readonly IErrorService _errorService;
        private readonly IOrderEntities _orderEntities;
        private IErrorService _errorService;
        //private readonly IUserService _userService;

        public OrderAccess(
            IOrderEntities orderEntities,
            IHttpContextAccessor actionContext
            //IErrorService errorService,
            //IUserService userService
            )
        {
            if (orderEntities == null) throw new ArgumentNullException(nameof(orderEntities));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            //if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            //if (userService == null) throw new ArgumentNullException(nameof(userService));
            _orderEntities = orderEntities;
            _actionContext = actionContext;
            //_errorService = errorService;
            //_userService = userService;
        }

        public bool AllowEdit(int orderId)
        {
            if (orderId <= 0)
                return true;
            //Проверяем сначала по статусу
            if (!AllowEditByState(orderId)) return false;

            //Заказ может редактировать только владелец, менеджер и администратор
            if (_actionContext.HttpContext.User.IsInRole(Identity.RolesAdministrators)) return true;

            try
            {
                if (_orderEntities.IsOrderOwner(orderId, _orderEntities.UserId))
                    return true;

                var orderCompany = _orderEntities.GetCompanyByOrderId(orderId);
                var userCompany = _orderEntities.GetCompanyByUserId(_orderEntities.UserId);
                if (
                    _actionContext.HttpContext.User.IsInRole(Identity.RolesManagers) &&
                    ((orderCompany?.Id ?? -1) == (userCompany?.Id ?? 0))
                    )
                    return true;
            }
            catch (Exception ex)
            {
                //_errorService.Handle(ex);
            }
            return false;
        }

        public bool AllowDelete(int orderId)
        {
            //TODO Уточнить условие
            //Заказ может удалять только тот, кто может его редактировать
            return AllowEditByState(orderId) && AllowEdit(orderId);
        }

        public bool AllowEditByState(int orderId)
        {
            var order = _orderEntities.GetOrder(orderId);
            return order?.OrderStateId == (int) OrdersStates.Forming;
        }

        //TODO оптимизировать
        public bool AllowRead(int orderId)
        {
            //TODO Уточнить и реализовать требования
            //Просматривать данные заказа могут:
            //администраторы, менеджеры компании владельца заказа, владельцы заказа
            //поставщики, подрядчики
            //все, если у заказа статус "на тендере" или "на аукционе"
            if (_actionContext.HttpContext.User.IsInRole(Identity.RolesAdministrators))
                return true;

            var order = _orderEntities.GetOrder(orderId);
            if (order == null)
            {
                //_errorService.ItemNotFound();
                return false;
            }

            //Если заказ на тендере или аукционе - просматривать могут все
            if (order.OrderStateId == (int) OrdersStates.AtTheTender ||
                order.OrderStateId == (int) OrdersStates.OnTheAuction)
                return true;

            //if (
            //    _actionContext.HttpContext.User.IsInRole(Identity.RolesManagers) &&
            //    (_orderEntities.GetCompanyByOrderId(orderId).Id ==
            //     _orderEntities.GetCompanyByUserId(_orderEntities.UserId).Id)
            //    )
            //    return true;
            if (_actionContext.HttpContext.User.IsInRole(Identity.RolesManagers) )
                return true;

            return _orderEntities.IsOrderOwner(orderId, _orderEntities.UserId);
        }

        // Проверка возможности установки нового статуса заказа
        public bool AllowEditState(int orderId, OrdersStates orderState)
        {
            var order = _orderEntities.GetOrder(orderId);
            if (order == null)
            {
                //_errorService.ItemNotFound();
                return false;
            }
            var currentOrderState = (OrdersStates)order.OrderStateId;
            
            //TODO Реализовать проверку полностью
            return (currentOrderState == OrdersStates.Forming && orderState == OrdersStates.Formed) ||
                   (currentOrderState == OrdersStates.Formed && orderState == OrdersStates.AtTheTender) ||
                   (currentOrderState == OrdersStates.Formed && orderState == OrdersStates.OnTheAuction) ||
                   (currentOrderState == OrdersStates.Performing && orderState == OrdersStates.Done);
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _orderEntities.ErrorService); }
            set { _errorService = value; }
        }

        public bool AllowAuction(int orderId)
        {
            //TODO реализовать проверку
            return true;
        }
    }
}