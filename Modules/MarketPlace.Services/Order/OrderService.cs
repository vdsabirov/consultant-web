﻿                                                                                            using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    public class OrderServicesService : IOrderService
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly IOrderAccess _orderAccess;
        private readonly IOrderEntities _orderEntities;
        private IErrorService _errorService;

        public OrderServicesService(
            IOrderAccess orderAccess,
            IOrderEntities orderEntities,
            IHttpContextAccessor actionContext
            )
        {
            if (orderAccess == null) throw new ArgumentNullException(nameof(orderAccess));
            if (orderEntities == null) throw new ArgumentNullException(nameof(orderEntities));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _orderAccess = orderAccess;
            _orderEntities = orderEntities;
            _actionContext = actionContext;
        }

        public void UpdateState(int orderId, OrdersStates orderState)
        {
            if (!_orderAccess.AllowEditState(orderId, orderState) || !_orderEntities.IsOrderOwner(orderId, _orderEntities.UserId)) return;
            var order = _orderEntities.GetOrder(orderId);
            if (order == null) return;
            order.OrderStateId = (int) orderState;
            _orderEntities.Update(order);
        }

        public Order Update(Order order)
        {
            if (order == null) return _orderEntities.GetOrder();

            //Если заказ создается, то проверку доступа делать не нужно
            if (order.Id == 0 && order.UserId.Equals(_orderEntities.UserId))
            {
                //Устанавливаем статус
                order.OrderStateId = (int) OrdersStates.Forming;
            }
            else if (!_orderAccess.AllowEdit(order.Id))
            {
                ErrorService.AccessDenied();
                return order;
            }

            return _orderEntities.Update(order);
        }

        public Order GetOrderForRead(int orderId, bool loadProducts = false)
        {
            if (_orderAccess.AllowRead(orderId))
                return _orderEntities.GetOrder(orderId, loadProducts);
            ErrorService.AccessDenied();
            return _orderEntities.GetOrder();
        }

        public Order GetOrderForEdit(int orderId, bool loadProducts = false)
        {
            if (_orderAccess.AllowEdit(orderId)) return _orderEntities.GetOrder(orderId, loadProducts);
            ErrorService.AccessDenied();
            return _orderEntities.GetOrder();
        }

        public void Delete(int orderId)
        {
            if (_orderAccess.AllowDelete(orderId))
                _orderEntities.Delete(orderId);
        }

        public List<Product> GetOrderProducts(int orderId)
        {
            return _orderAccess.AllowRead(orderId) ? _orderEntities.GetOrderProducts(orderId) : new List<Product>();
        }

        public List<Order> GetOrders()
        {
            if (_actionContext.HttpContext.User.IsInRole(Identity.RolesAdministrators))
                return _orderEntities.GetOrders();
            if (_actionContext.HttpContext.User.IsInRole(Identity.RolesManagers))
                return _orderEntities.GetOrdersByCompanyId(_orderEntities.GetCompanyByUserId(_orderEntities.UserId).Id);
            return _orderEntities.GetOrdersByUserId(_orderEntities.UserId);
        }

        public Order GetNewOrder()
        {
            return _orderEntities.GetOrder();
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _orderEntities.ErrorService); }
            set { _errorService = _orderEntities.ErrorService = _orderAccess.ErrorService = value; }
        }

        public List<OrderState> GetOrderStates()
        {
            return _orderEntities.GetOrderStates();
        }

        public void Dispose(bool disposing)
        {
            _orderEntities.Dispose(disposing);
        }
    }
}