﻿using System;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;

namespace MarketPlace.Services
{
    public class LogService : ILogService
    {
        private readonly ILogEntities _logEntities;

        public LogService(ILogEntities logEntities)
        {
            if (logEntities == null) throw new ArgumentNullException(nameof(logEntities));
            _logEntities = logEntities;
        }

        public void Write(Log log)
        {
            _logEntities.Write(log);
        }
    }
}