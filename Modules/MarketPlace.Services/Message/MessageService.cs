﻿using MarketPlace.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Infrastructure.Models;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    public class MessageService : IMessageService
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly IMessageEntities _messageEntities;

        public MessageService(
            IMessageEntities messageEntities,
            IHttpContextAccessor actionContext
          )
        {
            if (messageEntities == null) throw new ArgumentNullException(nameof(messageEntities));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            _messageEntities = messageEntities;
            _actionContext = actionContext;
        }


        public void Delete(int messageId)
        {
            _messageEntities.Delete(messageId);
        }

        public void Dispose(bool disposing)
        {
            _messageEntities.Dispose(disposing);
        }

        public Message GetMessage(int messageId = -1)
        {
            return _messageEntities.GetMessage(messageId);
        }

        public List<Message> GetMessages()
        {
            return _messageEntities.GetMessages();
        }

        public List<Message> GetMessages(string senderId, string recievierId)
        {
            return _messageEntities.GetMessages(senderId, recievierId);
        }

        public bool IsMessageReciever(int messageId, string userId)
        {
            return _messageEntities.IsMessageReciever(messageId, userId);
        }

        public bool IsMessageSender(int messageId, string userId)
        {
            return _messageEntities.IsMessageReciever(messageId, userId);
        }

        public Message Update(Message message)
        {
            return _messageEntities.Update(message);
        }
    }
}
