﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;

namespace MarketPlace.Services
{
    public class UserService : IUserService
    {
        private readonly IUserEntities _userEntities;
        private IErrorService _errorService;

        public UserService(
            IUserEntities userEntities
            )
        {
            if (userEntities == null) throw new ArgumentNullException(nameof(userEntities));
            _userEntities = userEntities;
        }

        public User GetCurrentUser()
        {
            return _userEntities.GetCurrentUser();
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _userEntities.ErrorService); }
            set { _errorService = _userEntities.ErrorService = _userEntities.ErrorService = value; }
        }

        public void Dispose(bool disposing)
        {
            _userEntities.Dispose(disposing);
        }

        public List<User> GetUsers()
        {
            return _userEntities.GetUsers();
        }

        public User GetUser(string id)
        {
            return _userEntities.GetUser(id);
        }

        public Company GetCompany(int id)
        {
            return _userEntities.GetCompany(id);
        }

        public List<Company> GetCompanies()
        {
            return _userEntities.GetCompanies();
        }
    }
}