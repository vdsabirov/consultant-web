﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    public class AuctionProposalService : IAuctionProposalService
    {
        private readonly IAuctionProposalEntities _auctionProposalEntities;

        private IErrorService _errorService;

        public AuctionProposalService(IAuctionProposalEntities auctionProposalEntities)
        {
            if (auctionProposalEntities == null) throw new ArgumentNullException(nameof(auctionProposalEntities));
            _auctionProposalEntities = auctionProposalEntities;
        }

        public AuctionProposal GetAuctionРroposal(int auctionProposalId)
        {
            return _auctionProposalEntities.GetAuctionРroposal(auctionProposalId);
        }

        public List<AuctionProposal> GetAuctionРroposals()
        {
            return _auctionProposalEntities.GetAuctionРroposals();
        }

        public AuctionProposal Update(AuctionProposal auctionProposal)
        {
            return _auctionProposalEntities.Update(auctionProposal);
        }

        public void Delete(int auctionProposalId)
        {
            _auctionProposalEntities.Delete(auctionProposalId);
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _auctionProposalEntities.ErrorService); }
            set { _errorService = _auctionProposalEntities.ErrorService = _auctionProposalEntities.ErrorService = value; }
        }

        public void Dispose(bool disposing)
        {
            _auctionProposalEntities.Dispose(disposing);
        }
    }
}