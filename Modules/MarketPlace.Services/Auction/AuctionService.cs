﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    public class AuctionService : IAuctionService
    {
        private readonly IHttpContextAccessor _actionContext;
        private readonly IAuctionEntities _auctionEntities;
        private readonly IOrderAccess _orderAccess;
        private IErrorService _errorService;

        public AuctionService(
            IAuctionEntities auctionEntities,
            IHttpContextAccessor actionContext,
            IOrderAccess orderAccess
            )
        {
            if (auctionEntities == null) throw new ArgumentNullException(nameof(auctionEntities));
            if (actionContext == null) throw new ArgumentNullException(nameof(actionContext));
            if (orderAccess == null) throw new ArgumentNullException(nameof(orderAccess));
            _auctionEntities = auctionEntities;
            _actionContext = actionContext;
            _orderAccess = orderAccess;
        }

        public List<AuctionProposal> GetAuctionРroposalsByOrderId(int orderId)
        {
            return _auctionEntities.GetAuctionРroposalsByOrderId(orderId);
        }

        public List<AuctionProposal> GetAuctionРroposalsByAuctionId(int auctionId)
        {
            return _auctionEntities.GetAuctionРroposalsByAuctionId(auctionId);
        }

        public Auction Update(Auction auction)
        {
            return _auctionEntities.Update(auction);
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _auctionEntities.ErrorService); }
            set { _errorService = _auctionEntities.ErrorService = _auctionEntities.ErrorService = value; }
        }

        public Auction Create(int orderId)
        {
            if (_orderAccess.AllowAuction(orderId))
                //TODO Сделать форму создания аукциона
                return _auctionEntities.Update(new Auction
                {
                    ClosingTime = DateTime.Now.AddMinutes(20),
                    OpeningTime = DateTime.Now,
                    IsOpen = true,
                    OrderId = orderId
                });
            ErrorService.AccessDenied();
            return new Auction();
        }

        public Auction GetAuctionByOrderId(int orderId)
        {
            return _auctionEntities.GetAuctionByOrderId(orderId);
        }

        public List<Auction> GetAuctions()
        {
            return _auctionEntities.GetAuctions();
        }

        public void SetProposal(int auctionId, decimal price)
        {
            _auctionEntities.SetProposal(auctionId, price);
        }

        public void SetWinner(int auctionРroposalId)
        {
            _auctionEntities.SetWinner(auctionРroposalId);
        }

        public Auction GetAuction(int auctionId)
        {
            return _auctionEntities.GetAuction(auctionId);
        }

        public void Delete(int auctionId)
        {
            _auctionEntities.Delete(auctionId);
        }

        public void Dispose(bool disposing)
        {
            _auctionEntities.Dispose(disposing);
        }
    }
}