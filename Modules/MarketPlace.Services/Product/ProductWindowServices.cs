﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;

namespace MarketPlace.Services
{
    public class ProductWindowServices : IProductWindowServices
    {

        private IErrorService _errorService;
        private readonly IOrderAccess _orderAccess;
        private readonly IProductWindowEntities _productEntities;

        public ProductWindowServices(
            IProductWindowEntities productEntities,
            IOrderAccess orderAccess
            )
        {
            if (productEntities == null) throw new ArgumentNullException(nameof(productEntities));
            if (orderAccess == null) throw new ArgumentNullException(nameof(orderAccess));
            _productEntities = productEntities;
            _orderAccess = orderAccess;
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _productEntities.ErrorService); }
            set { _errorService = _productEntities.ErrorService = _orderAccess.ErrorService = value; }
        }

        public void Delete(int productWindowId)
        {
            _productEntities.Delete(productWindowId);
        }

        public List<ProductWindow> GetProducts()
        {
            return _productEntities.GetProducts();
        }

        public void Dispose(bool disposing)
        {
            _productEntities.Dispose(disposing);
        }

        public ProductWindow GetProduct(int productWindowId)
        {
            return _productEntities.GetProduct(productWindowId);
        }

        public ProductWindow GetProductByProductId(int productId)
        {
            return _productEntities.GetProductByProductId(productId);
        }

        public ProductWindow Update(ProductWindow product)
        {
            return _productEntities.Update(product);
        }

        public List<ProductWindowBrand> GetProductWindowBrands()
        {
            return _productEntities.GetProductWindowBrands();
        }

        public List<ProductWindowFinding> GetProductWindowFindings()
        {
            return _productEntities.GetProductWindowFindings();
        }
        public List<ProductWindowGlazingType> GetProductWindowGlazingTypes()
        {
            return _productEntities.GetProductWindowGlazingTypes();
        }
        public List<ProductWindowLaminating> GetProductWindowLaminatings()
        {
            return _productEntities.GetProductWindowLaminatings();
        }
        public List<ProductWindowsill> GetProductWindowills()
        {
            return _productEntities.GetProductWindowills();
        }
        public List<ProductWindowSystem> GetProductWindowSystems()
        {
            return _productEntities.GetProductWindowSystems();
        }
        public List<ProductWindowTinting> GetProductWindowTintings()
        {
            return _productEntities.GetProductWindowTintings();
        }
    }
}