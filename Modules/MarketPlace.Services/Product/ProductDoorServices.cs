﻿using System;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;

namespace MarketPlace.Services
{
    public class ProductDoorServices : IProductDoorServices
    {

        private IErrorService _errorService;
        private readonly IOrderAccess _orderAccess;
        private readonly IProductDoorEntities _productEntities;

        public ProductDoorServices(
            IProductDoorEntities productEntities,
            IOrderAccess orderAccess
            )
        {
            if (productEntities == null) throw new ArgumentNullException(nameof(productEntities));
            if (orderAccess == null) throw new ArgumentNullException(nameof(orderAccess));
            _productEntities = productEntities;
            _orderAccess = orderAccess;
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _productEntities.ErrorService); }
            set { _errorService = _productEntities.ErrorService = _orderAccess.ErrorService = value; }
        }

        public ProductDoor GetProduct(int productDoorId)
        {
            return _productEntities.GetProduct(productDoorId);
        }

        public ProductDoor GetProductByProductId(int productId)
        {
            return _productEntities.GetProductByProductId(productId);
        }

        public ProductDoor Update(ProductDoor product)
        {
            return _productEntities.Update(product);
        }
    }
}