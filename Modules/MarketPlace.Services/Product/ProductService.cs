﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;

namespace MarketPlace.Services
{
    public class ProductService : IProductServices
    {
        private IErrorService _errorService;
        private readonly IOrderAccess _orderAccess;
        private readonly IProductEntities _productEntities;

        public ProductService(
            IProductEntities productEntities,
            IOrderAccess orderAccess
            )
        {
            if (productEntities == null) throw new ArgumentNullException(nameof(productEntities));
            if (orderAccess == null) throw new ArgumentNullException(nameof(orderAccess));
            _productEntities = productEntities;
            _orderAccess = orderAccess;
        }

        public List<ProductCategory> GetProductCategories()
        {
            return _productEntities.GetProductCategories();
        }

        public string GetProductCategoryName(int productCategoryId)
        {
            return _productEntities.GetProductCategoryName(productCategoryId);
        }

        public Product GetNewProduct(int orderId = -1, int productCategoryId = -1)
        {
            return new Product {OrderId = orderId, ProductCategoryId = productCategoryId};
        }

        public Product Update(Product product)
        {
            if (product == null) return GetNewProduct();

            if (_orderAccess.AllowEdit(product.OrderId)) return _productEntities.Update(product);
            _errorService.AccessDenied();
            return product;
        }

        public Product GetProduct(int productId)
        {
            return _productEntities.GetProduct(productId);
        }

        public IErrorService ErrorService
        {
            get { return _errorService ?? (_errorService = _productEntities.ErrorService); }
            set { _errorService = _productEntities.ErrorService = _orderAccess.ErrorService = value; }
        }

        public void Delete(int productId)
        {
            _productEntities.Delete(productId);
        }

        public List<Product> GetProducts()
        {
            return _productEntities.GetProducts();
        }

        public List<Product> GetProductsByOrderId(int id)
        {
            return _productEntities.GetProductsByOrderId(id);
        }

        public void Dispose(bool disposing)
        {
            _productEntities.Dispose(disposing);
        }
    }
}