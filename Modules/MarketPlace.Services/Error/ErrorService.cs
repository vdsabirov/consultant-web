﻿using System;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;

namespace MarketPlace.Services
{
    //TODO Заменить строки на константы или ресурсы
    //public class ErrorService : IErrorService
    public class ErrorService : ErrorServiceDefault
    {
        public ErrorService(ILogService logsService, IHttpContextAccessor actionContext) : base(logsService, actionContext)
        {
        }
        //private readonly IHttpContextAccessor _actionContext;
        //private readonly ILogService _logsService;

        //public ErrorService(ILogService logsService, IHttpContextAccessor actionContext)
        //{
        //    _logsService = logsService;
        //    _actionContext = actionContext;
        //}

        //public void Handle(Exception exception)
        //{
        //    Handle(exception.InnerException?.Message ?? exception.Message);
        //}

        //public void Handle(string errorMessage = null)
        //{
        //    if (string.IsNullOrEmpty(errorMessage))
        //        errorMessage = Messages.MessagesErrorUnknown;

        //    _logsService.Write(new Log
        //    {
        //        UserName = _actionContext.HttpContext.User.Identity.Name,
        //        LogsType = LogsTypes.Error,
        //        Message = GetErrorLogMessage(errorMessage),
        //        Time = DateTime.Now,
        //        Url = _actionContext.HttpContext.Request.Path
        //    });

        //    _actionContext?.HttpContext.Response.Redirect(
        //        Uri.EscapeUriString($"{Common.ErrorPage}{errorMessage}"));
        //}

        //public void AccessDenied()
        //{
        //    _logsService.Write(new Log
        //    {
        //        UserName = _actionContext.HttpContext.User.Identity.Name,
        //        LogsType = LogsTypes.AccessDenied,
        //        Message =
        //            $"Доступ запрещен для пользователя {_actionContext.HttpContext.User.Identity.Name}",
        //        Time = DateTime.Now,
        //        Url = _actionContext.HttpContext.Request.Path
        //    }
        //        );
        //    _actionContext?.HttpContext.Response.Redirect(
        //        Uri.EscapeUriString($"{Common.ErrorPage}{Messages.MessagesAccessDenied}"));
        //}

        //public void ItemNotFound()
        //{
        //    _logsService.Write(new Log
        //    {
        //        UserName = _actionContext.HttpContext.User.Identity.Name,
        //        LogsType = LogsTypes.ItemNotFound,
        //        Message = "Не удалось обнаружить запрашиваемый объект.",
        //        Time = DateTime.Now,
        //        Url = _actionContext.HttpContext.Request.Path
        //    }
        //        );
        //    _actionContext?.HttpContext.Response.Redirect(
        //        Uri.EscapeUriString($"{Common.ErrorPage}{Messages.MessagesItemNotFound}"));
        //}

        //private string GetErrorLogMessage(string errorMessage)
        //{
        //    return string.Format("{1}{0}{0}Пользователь: {2}{0}{0}", Environment.NewLine, errorMessage,
        //        _actionContext.HttpContext.User.Identity.Name);
        //}
    }
}