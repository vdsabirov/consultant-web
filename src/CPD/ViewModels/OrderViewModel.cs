﻿using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class OrderViewModel
    {
        public const int HOURS_FOR_SET_WINNER = 48;
        public const int HOURS_FOR_TENDER = 24;

        private static OrderActionViewModel _deleteOrderAction = new OrderActionViewModel() { Title = "Удалить", Rest = "deleteOrder", Type = "danger" };
        private static OrderActionViewModel _orderToTenderAction = new OrderActionViewModel() { Title = "На тендер", Rest = "orderToTender", Type = "success" };
        private static OrderActionViewModel _cancelOrderAction = new OrderActionViewModel() { Title = "Отменить", Rest = "cancelOrder", Type = "warning" };
        private static OrderActionViewModel _doneOrderAction = new OrderActionViewModel() { Title = "Выполнен", Rest = "doneOrder", Type = "success" };
        private static OrderActionViewModel _performingOrderAction = new OrderActionViewModel() { Title = "Исполняется", Rest = "performingOrder", Type = "info" };

        public OrderViewModel()
        {
            CanModify = true;
            Proposals = new List<TenderProposalViewModel>();
            Products = new List<ProductViewModel>();
        }

        public int Id { get; set; }

        [MaxLength(200)]
        public string Notes { get; set; }

        public string Code { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(50)]
        public string StatusTitle { get; set; }

        public bool CanSetWinner { get; set; }

        [DefaultValue(true)]
        public bool CanModify { get; set; }

        //время закрытия тендера
        public DateTime? ClosingTime { get; set; }

        public IEnumerable<TenderProposalViewModel> Proposals { get; set; }

         public IEnumerable<ProductViewModel> Products { get; set; }
              
        public string UserId { get; set; }

        public string ConsultantId { get; set; }

        public IEnumerable<OrderActionViewModel> PossibleActions { get; set; }

        public void Map(MarketPlace.Entities.Models.Order order, bool isOwner = false, bool isRoleAccess = false)
        {
            var possibleAction = new List<OrderActionViewModel>();

            Id = order.Id;
            Notes = order.Notes;
            Code = order.Code;
            Date = order.OrderDate;
            if (order.OrderState != null)
                StatusTitle = order.OrderState.Name;
            CanSetWinner = order.OrderStateId == (int)OrdersStates.AtTheTender;
            CanModify = order.OrderStateId == (int)OrdersStates.Forming;
            UserId = order.UserId;
            if (order.ConsultantProfile != null)
                ConsultantId = order.ConsultantProfile.UserId;

            var products = new List<ProductViewModel>();
            if (order.Products != null)
                order.Products.ToList().ForEach(p => 
                {
                    var vm = new ProductViewModel();
                    vm.MapFrom(p);
                    products.Add(vm);                   
                });
            Products = products;

            if (order.Tender != null && order.Tender.TenderProposals != null)
            {
                if (order.Tender.ClosingTime > DateTime.Now)
                    ClosingTime = order.Tender.ClosingTime;
                var isTimeForProposals = order.Tender.ClosingTime > DateTime.Now;
                var isTimeToChooseWinner = !isTimeForProposals && order.Tender.ClosingTime.AddHours(HOURS_FOR_SET_WINNER) > DateTime.Now;
                var isWinnerSet = order.Tender.TenderProposals.Any(p => p.IsWinner);
                CanSetWinner = isTimeToChooseWinner && !isWinnerSet;

                if (!isTimeForProposals && !isWinnerSet && isTimeToChooseWinner)
                {
                    StatusTitle = "Ожидает выбора победителя тендера";
                    ClosingTime = order.Tender.ClosingTime.AddHours(HOURS_FOR_SET_WINNER);
                }
                   

                if (order.OrderStateId != (int)OrdersStates.Canceled && !isTimeForProposals && !isWinnerSet && !isTimeToChooseWinner)
                {
                    StatusTitle = "Заархивирован";
                    possibleAction.Add(_cancelOrderAction);
                }

                var proposals = new List<TenderProposalViewModel>();
                order.Tender.TenderProposals.ToList().ForEach(p =>
                {
                    var vm = new TenderProposalViewModel();
                    vm.Map(p);
                    proposals.Add(vm);
                });

                Proposals = proposals;
            }

            if (isRoleAccess && order.TenderId == null)
            {
                possibleAction.Add(_deleteOrderAction);
                if (order.Products.Count > 0)
                    possibleAction.Add(_orderToTenderAction);
            }

            if (isRoleAccess && order.OrderStateId == (int)OrdersStates.WattingPayment)
                possibleAction.Add(_performingOrderAction);


            if ((isOwner || isRoleAccess) && order.OrderStateId == (int)OrdersStates.Performing)
                possibleAction.Add(_doneOrderAction);

            PossibleActions = possibleAction;
        }

    }
}
