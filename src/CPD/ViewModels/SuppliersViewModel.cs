﻿
using CPD.Helpers;
using MarketPlace.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class SuppliersViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(128)]
        public string Title { get; set; }

        [MaxLength(50)]
        public string Term { get; set; }

        [MaxLength(50)]
        public string Guarantee { get; set; }

        [MaxLength(50)]
        public string Prepay { get; set; }

        public int Rating { get; set; }

        public void Map(Company company)
        {
            Id = company.Id;
            Title = company.Name;
            Term = company.SupplyTerm;
            Guarantee = company.GuaranteeTerm;
            Prepay = string.Format("{0} %", company.PrepaymentTermPercentage);
            Rating = company.Rating;
        }

        public void MapTo(Company company)
        {
            company.Id = Id;
            company.Name = Title;
            company.SupplyTerm  = Term;
            company.GuaranteeTerm = Guarantee;

            int value;
            if (int.TryParse(Regex.Match(Prepay, @"\d+").Value, out value) && value >=0 && value <= 100)
                company.PrepaymentTermPercentage = value;

            company.Rating = Rating;
        }

    }
}
