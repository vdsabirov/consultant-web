﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class CallBackVoronezhViewModel
    {
        public string ClientId { get; set; }

        [DataType(DataType.EmailAddress)]
        public string YourEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required]
        public string YourPhone { get; set; }

        [MaxLength(50)]
        public string YourName { get; set; }

        public DateTime? Date { get; set; }

        [MaxLength(200)]
        public string Target { get; set; }

        public string Time { get; set; }
    }
}
