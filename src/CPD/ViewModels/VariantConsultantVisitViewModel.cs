﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class VariantConsultantVisitViewModel
    {
        public string Date { get; set; }

        public IEnumerable<string> Times { get; set; }
    }
}
