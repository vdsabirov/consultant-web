﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class OrderWinnerViewModel
    {
        public string ClientId { get; set; }

        [Required]
        public int OrderId { get; set; }

        [Required]
        public int ProposalId { get; set; }
    }
}
