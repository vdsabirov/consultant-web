﻿using MarketPlace.Entities.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class TenderProposalViewModel
    {
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public int ShipperId { get; set; }

        public string UserTitle { get; set; }

        public int Rating { get; set; }

        [Required]
        public decimal Price { get; set; }

        [MaxLength(3500)]
        public string Note { get; set; }

        [MaxLength(50)]
        public string Term { get; set; }

        [MaxLength(50)]
        public string Guarantee { get; set; }

        [MaxLength(50)]
        public string Prepay { get; set; }

        public bool IsWinner { get; set; }

        public IDictionary<int, JObject> Products { get; set; }

        public void Map(TenderProposal p)
        {
            Id = p.Id;
            UserId = p.UserId;
            ShipperId = p.CompanyId;
            if (p.Company != null)
            {
                UserTitle = p.Company.Name;
                Rating = p.Company.Rating;
            }

            Price = p.Price;
            Note = p.Notes;
            Term = p.SupplyTerm;
            Guarantee = p.GuaranteeTerm;
            Prepay = string.Format("{0} %", p.PrepaymentTermPercentage);
            IsWinner = p.IsWinner;

            Products = new Dictionary<int, JObject>();

            if (p.ProductTenderProposals == null)
                return;

            foreach (var pp in p.ProductTenderProposals.Where(t => !string.IsNullOrWhiteSpace(t.SpecificationData)).ToList())
                Products.Add(pp.ProductId, JObject.Parse(pp.SpecificationData));
        }

        public void MapTo(TenderProposal p)
        {
            p.UserId = UserId;
            p.CompanyId = ShipperId;
            p.Price = Price;
            p.Notes = Note;
            p.SupplyTerm = Term;
            p.GuaranteeTerm = Guarantee;
            p.IsWinner = IsWinner;

            int value;
            if (int.TryParse(Regex.Match(Prepay, @"\d+").Value, out value) && value >= 0 && value <= 100)
                p.PrepaymentTermPercentage = value;
        }

    }
}
