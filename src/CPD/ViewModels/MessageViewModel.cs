﻿using MarketPlace.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class MessageViewModel
    {

        public MessageViewModel() { }

        public MessageViewModel(Message message)
        {
            Map(message);
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public string SenderId { get; set; }

        public string ReceiverId { get; set; }

        public string SenderName { get; set; }

        public string ReceiverName { get; set; }

        public DateTime? Created { get; set; }

        public void Map(Message message)
        {
            Id = message.Id;
            Title = message.Title;
            Text = message.Text;
            SenderId = message.SenderId;
            ReceiverId = message.RecieverId;
            if (message.Reciever != null)
                ReceiverName = string.Format("{0} ({1})", message.Reciever.FirstName, message.Reciever.Email);
            if (message.Sender != null)
                SenderName = string.Format("{0} ({1})", message.Sender.FirstName, message.Sender.Email);
            Created = message.CreatedAt;

        }

        //public void MapTo(Message message)
        //{
        //     message.Title = Title;
        //     message.Text = Text;
        //     message.RecieverId = ReceiverId;
        //}
    }
}
