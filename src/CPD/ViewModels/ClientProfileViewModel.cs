﻿

using MarketPlace.Entities.Models;
using System.ComponentModel.DataAnnotations;

namespace CPD.ViewModels
{
    public class ClientProfileViewModel
    {
        //Id пользователя, чей профиль
        public string Id { get; set; }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        public int Balance { get; set; }

        public ConsultantProfileViewModel Consultant { get; set; }

        public static ClientProfileViewModel Map(ClientProfile p)
        {
            var vm = new ClientProfileViewModel();
            if (p == null)
                return vm;
            vm.FirstName = p.FirstName;
            vm.MiddleName = p.MiddleName;
            vm.LastName = p.LastName;
            vm.Address = p.Address;
            vm.PhoneNumber = p.PhoneNumber;
            vm.Email = p.Email;
            vm.Balance = p.Balance;
            if (p.ConsultantProfile != null)
                vm.Consultant = ConsultantProfileViewModel.Map(p.ConsultantProfile);
            return vm;
        }

        public void MapTo(ClientProfile p)
        {
            if (p == null)
                return;
            p.FirstName = FirstName;
            p.MiddleName = MiddleName;
            p.LastName = LastName;
            p.Address = Address;
            p.PhoneNumber = PhoneNumber;
            p.Email = Email;
            p.Balance = Balance;
        }
    }
}
