﻿
namespace CPD.ViewModels
{
    public class RatingViewModel
    {
        public RatingServices Service { get; set; }

        public int Rating { get; set; }

        public int OrderId { get; set; }
    }

   
    public enum RatingServices
    {
        service,
        company,
        consultant
    }
}
