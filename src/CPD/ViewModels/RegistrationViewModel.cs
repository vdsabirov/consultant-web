﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class RegistrationViewModel
    {
        [Required(ErrorMessage = "Не указано имя пользователя")]
        [MinLength(3, ErrorMessage = "Имя пользователя должно содержать не менее 3 символов")]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        //public bool isCompany { get; set; }

    }
}
