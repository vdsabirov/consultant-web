﻿using MarketPlace.Entities.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            TypeId = 1;
        }

        public int Id { get; set; }

        [DefaultValue(1)]
        public int TypeId { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }

        public JToken ObjectData { get; set; }
        //public IEnumerable<FileViewModel> Files { get; set; }

        public void MapFrom(Product productToMapFrom)
        {
            Id = productToMapFrom.Id;
            TypeId = productToMapFrom.ProductCategoryId;
            Title = productToMapFrom.Name;
            try {
                ObjectData = JToken.Parse(productToMapFrom.SpecificationData);
            }
            catch { }
            //handling of the related files into the specification object
            //var filesInfo = ObjectData?.Value<JObject>("values")?.Value<JObject>("adds")?.Value<JArray>("files");
            //if (filesInfo != null)
            //{
            //    var files = new List<File>();
            //    for (int i=0; i< filesInfo.Count; i++)
            //    {
            //        var f = new File();
            //        f.FileName = filesInfo[i].Value<string>("name");
            //        f.FilePath = filesInfo[i].Value<string>("fileref");
            //        files.Add(f);
            //    }
            //}

            //handling of the related files via the joining entities
            //if (productToMapFrom.ProductFiles != null)
            //{
            //    var fvms = new List<FileViewModel>();
            //    productToMapFrom.ProductFiles.ToList().ForEach(pf =>
            //    {
            //        var fvm = new FileViewModel();
            //        fvm.MapFrom(pf.File);
            //        fvms.Add(fvm);
            //    });
            //    Files = fvms;
            //}
        }
        public void MapToProduct(Product productToMapTo)
        {
            productToMapTo.Id = Id;
            productToMapTo.ProductCategoryId = TypeId;
            productToMapTo.Name = Title;
            productToMapTo.SpecificationData = ObjectData.ToString();

            //handling of the related files from the specification object
            //var filesInfo = ObjectData?.Value<JObject>("values")?.Value<JObject>("adds")?.Value<JArray>("files");
            //if (filesInfo != null)
            //{
            //    var files = new List<File>();
            //    for (int i = 0; i < filesInfo.Count; i++)
            //    {
            //        var f = new File();
            //        f.FileName = filesInfo[i].Value<string>("name");
            //        f.FilePath = filesInfo[i].Value<string>("fileref");
            //        files.Add(f);
            //    }
            //    var productFiles = new List<ProductFile>();
            //    foreach (var f in files)
            //    {
            //        productFiles.Add(new ProductFile{
            //            Product = productToMapTo,
            //            File = f
            //        });
            //    }
            //    productToMapTo.ProductFiles = productFiles;
            //}

            //handling of the related files via the joining entities
            //if (Files != null)
            //{
            //    var pfs = new List<ProductFile>();
            //    Files.ToList().ForEach(fvm =>
            //    {
            //        var pf = new ProductFile();
            //        pf.Product = productToMapTo;
            //        fvm.MapTo(pf.File);
            //        pfs.Add(pf);
            //    });
            //    productToMapTo.ProductFiles = pfs;
            //}
        }
    }
}
