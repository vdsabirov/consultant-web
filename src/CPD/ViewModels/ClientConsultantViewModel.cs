﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class ClientConsultantViewModel
    {

        public string UserId { get; set; }

        public string ConsultantId { get; set; }
    }
}
