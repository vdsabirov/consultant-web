﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class CallBackViewModel
    {
        public string ClientId { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required]
        public string Phone { get; set; }

        public string Time { get; set; }

    }
}
