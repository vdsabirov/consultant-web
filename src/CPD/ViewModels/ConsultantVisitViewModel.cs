﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CPD.ViewModels
{
    public class ConsultantVisitViewModel
    {
        public string ClientId { get; set; }

        public int VisitRating { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public DateTime? Date { get; set; }

        public string Times { get; set; }

        public DateTime? Created { get; set; }
    }
}
