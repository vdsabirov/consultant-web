﻿using MarketPlace.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class ConsultantProfileViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public int Rating { get; set; }

        public string Email { get; set; }

        public static ConsultantProfileViewModel Map(ConsultantProfile p)
        {
            var vm = new ConsultantProfileViewModel();
            if (p == null)
                return vm;
            vm.Email = p.Email;
            vm.FullName = string.Format("{0} {1} {2}", p.FirstName, p.MiddleName, p.LastName);
            vm.Rating = p.Rating;
            vm.Id = p.Id;
            vm.PhoneNumber = p.PhoneNumber;

            return vm;
        }
    }
}
