﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Entities.Models;

namespace CPD.ViewModels
{
    public class FileViewModel //not used for now
    {
        public FileViewModel()
        {
            Products = new List<ProductViewModel>();
        }

        public int Id { get; set; }
        public string FileComment { get; set; }
        public string FileName { get; set; } //name of the file
        public string FilePath { get; set; } //relative path to the file (from root), including name of the file
        public IEnumerable<ProductViewModel> Products { get; set; }

        public void MapFrom(File fileToMapFrom)
        {
            Id = fileToMapFrom.Id;
            FileComment = fileToMapFrom.FileComment;
            FileName = fileToMapFrom.FileName;
            FilePath = fileToMapFrom.FilePath;
            //handling of the related products via the joining entities
            if (fileToMapFrom.ProductFiles != null)
            {
                var productViewModels = new List<ProductViewModel>();
                fileToMapFrom.ProductFiles.ToList().ForEach(pf =>
                {
                    var productViewModel = new ProductViewModel();
                    productViewModel.MapFrom(pf.Product);
                    productViewModels.Add(productViewModel);
                });
                Products = productViewModels;
            }
        }
        public void MapTo(File fileToMapTo)
        {
            fileToMapTo.Id = Id;
            fileToMapTo.FileComment = FileComment;
            fileToMapTo.FileName = FileName;
            fileToMapTo.FilePath = FilePath;
            //handling of the related products via the joining entities
            if (Products != null)
            {
                var pfs = new List<ProductFile>();
                Products.ToList().ForEach(pvm =>
                {
                    var pf = new ProductFile();
                    pf.File = fileToMapTo;
                    pvm.MapToProduct(pf.Product);
                    pfs.Add(pf);
                });
                fileToMapTo.ProductFiles = pfs;
            }
        }
    }
}
