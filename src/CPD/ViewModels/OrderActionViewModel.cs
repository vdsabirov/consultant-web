﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.ViewModels
{
    public class OrderActionViewModel
    {
        public string Title { get; set; } 

        public string Rest { get; set; }

        //primary,default,success,info,warninig,danger
        public string Type { get; set; }
    }
}
