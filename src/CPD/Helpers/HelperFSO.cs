﻿using Microsoft.AspNet.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace CPD.Helpers
{
    public class HelperFSO
    {
        //get absolute path to a folder or file from a relative path, which is represented as a number of folders
        public static string AbsPathFromRelative(IHostingEnvironment env, string strRootFolderPath, params string[] strPathParts)
        {
            var r = env.MapPath(strRootFolderPath);
            var n = strPathParts.Length;
            if (n > 0)
            {
                for (int i = 0; i < strPathParts.Length; i++)
                {
                    r = Path.Combine(r, strPathParts[i]);
                }
            }
            return r;
        }
    }
}
