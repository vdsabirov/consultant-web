﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Helpers
{
    public static class JsonParser
    {
        public static bool TryIntJsonParser(this string condition, string name, out int value)
        {
            value = 0;
            if (condition == null)
                return false;

            var jCondition = JToken.Parse(condition);
            if (jCondition == null)
                return false;

            var result = jCondition.Value<int?>(name);
            if (result == null)
                return false;

            value = (int)result;
            return true;
        }

        public static string StringJsonParser(this string condition, string name)
        {
            if (condition == null)
                return null;

            var jCondition = JToken.Parse(condition);
            if (jCondition == null)
                return null;

            return jCondition.Value<string>(name);
        }
    }
}
