﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Helpers
{
    public static class TitleNumbers
    {
        public static string NumberMonthsToString(int months)
        {
            var last = months % 100;
            if (last == 0 || (last > 4 && last < 21))
                return string.Format("{0} месяцев", months.ToString());

            last = last % 10;
            if (last == 1)
                return string.Format("{0} месяц", months.ToString());

            if (last < 5)
                return string.Format("{0} месяца", months.ToString());

            return string.Format("{0} месяцев", months.ToString());

        }
    }
}
