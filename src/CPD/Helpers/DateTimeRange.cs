﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Helpers
{
    public class DateTimeRange
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public DateTimeRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public DateTimeRange(DateTime start, double hoursRange)
        {
            Start = start;
            End =Start.AddHours(hoursRange);
        }

        public bool Intersects(DateTimeRange test)
        {
            if (this.Start > this.End || test.Start > test.End)
                return false;

            if (this.Start == this.End || test.Start == test.End)
                return false; // No actual date range

            if (this.Start == test.Start || this.End == test.End)
                return true; // If any set is the same time, then by default there must be some overlap. 

            if (this.Start < test.Start)
            {
                if (this.End > test.Start && this.End < test.End)
                    return true; // Condition 1

                if (this.End > test.End)
                    return true; // Condition 3
            }
            else
            {
                if (test.End > this.Start && test.End < this.End)
                    return true; // Condition 2

                if (test.End > this.End)
                    return true; // Condition 4
            }

            return false;
        }
    }
}
