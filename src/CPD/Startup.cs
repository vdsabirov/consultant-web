﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MarketPlace.Infrastructure.Services;
using MarketPlace.Services;
using MarketPlace.Entities.Services;
using MarketPlace.Entities.Models;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MarketPlace.Entities.InitDataStore;
using Microsoft.AspNet.Identity;
using MarketPlace.Infrastructure.Constants;
using System.IO;

using Microsoft.AspNet.SignalR;


namespace CPD
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

            services.AddIdentity<ApplicationUser, IdentityRole>(o=>
                {
                    o.Password.RequireDigit = false;
                    o.Password.RequireLowercase = false;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireNonLetterOrDigit = false;
                    o.Password.RequiredLength = 6;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddSingleton<IConfiguration>(sp => { return Configuration; });

            // Add framework services.
            services.AddMvc();

            //allow CORS from Voronezh site
            
            services.AddCors(options =>
            {
                options.AddPolicy("AllowVoronezhOrigin",
                    //builder => builder.WithOrigins(Configuration["CORS:AllowedOrigin"]));//("http://voronezhsheltr/"));
                    builder => builder.WithOrigins("http://voronezh.sheltr.ru/"));//("http://sheltrvoronezh/"));
            });

            AddTransients(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //if (!env.IsDevelopment())
            {

                // For more details on creating database during deployment see http://go.microsoft.com/fwlink/?LinkID=615859
                try
                {
                    using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                        .CreateScope())
                    {
                        serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                            .Database.Migrate();
                    }
                }
                catch
                {
                }
            }

            app.UseIISPlatformHandler();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseFileServer();

            app.UseIdentity();

            //allow CORS from Voronezh site
            app.UseCors("AllowVoronezhOrigin");

            //для refresh страницы из браузера
            app.Use(async (context, next) =>
            {
                var internalUrl = context.Request.Cookies["CPD.InternalUrl"].FirstOrDefault();
                var requestPath = context.Request.Path.ToString();
                await next();
                if (context.Response.StatusCode != 404)
                    return;
                if (internalUrl == null)
                    context.Response.Redirect(@"/");
                else if (requestPath == internalUrl)
                   context.Response.Redirect(@"/#" + context.Request.Path);
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute("accountLogin", "rest/login", new { controller = "Account", action = "Login" });
                routes.MapRoute("accountLogOff", "rest/logout", new { controller = "Account", action = "LogOff" });
                routes.MapRoute("accountRegister", "rest/registration", new { controller = "Account", action = "Register" });
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}");
            });

            //if (env.IsDevelopment())
                CreateDemoData(app.ApplicationServices, env).Wait();

            //Check if folders for files exist and if not, create temp and main folders - add to application settings later on
            try
            {
                var strFileFolderTemp = env.MapPath(Configuration["FilesStorage:FolderForFiles"] + "/"
                        + Configuration["FilesStorage:FolderTemp"]);
                if (!Directory.Exists(strFileFolderTemp))
                    Directory.CreateDirectory(strFileFolderTemp);
            }
            catch (Exception ex)
            {
                //TODO - add logging of the errors related to existing or directory for uploading of files

            }

        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);

        private void AddTransients(IServiceCollection services)
        {
            // Add application services.
            services.AddTransient<IErrorService, ErrorService>();

            services.AddTransient<ILogEntities, LogEntities>();
            services.AddTransient<ILogService, LogService>();

            services.AddTransient<IMessageEntities, MessageEntities>();
            services.AddTransient<IMessageService, MessageService>();

            services.AddTransient<IOrderEntities, OrderEntities>();
            services.AddTransient<IOrderAccess, OrderAccess>();
            services.AddTransient<IOrderService, OrderServicesService>();

            services.AddTransient<IProductServices, ProductService>();
            services.AddTransient<IProductEntities, ProductEntities>();

            services.AddTransient<IProductWindowServices, ProductWindowServices>();
            services.AddTransient<IProductWindowEntities, ProductWindowEntities>();

            services.AddTransient<IProductDoorServices, ProductDoorServices>();
            services.AddTransient<IProductDoorEntities, ProductDoorEntities>();

            services.AddTransient<IAuctionService, AuctionService>();
            services.AddTransient<IAuctionEntities, AuctionEntities>();

            services.AddTransient<IAuctionProposalService, AuctionProposalService>();
            services.AddTransient<IAuctionProposalEntities, AuctionProposalEntities>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserEntities, UserEntities>();
            
        }

        private static async Task CreateDemoData(IServiceProvider applicationServices, IHostingEnvironment env)
        {
            using (var context = applicationServices.GetService<ApplicationDbContext>())
            {
                if (context.Database == null)
                    return;

                //нельзя использовать при миграции
                //await context.Database.EnsureDeletedAsync();

                //var resultDbCreated = await context.Database.EnsureCreatedAsync();
                //if (!resultDbCreated)
                //    return;
                DataStoreInit.CreateVocablaryData(context);


                var userManager = applicationServices.GetService<UserManager<ApplicationUser>>();

                if (context.Roles.Count() == 0)
                {
                    context.Roles.AddRange(
                        new IdentityRole { Name = Identity.RolesAdministrators, NormalizedName = Identity.RolesAdministratorsNormalizedName },
                        new IdentityRole { Name = Identity.RolesManagers, NormalizedName = Identity.RolesManagersNormalizedName },
                        new IdentityRole { Name = Identity.RolesConsultants, NormalizedName = Identity.RolesConsultantsNormalizedName },
                        new IdentityRole { Name = Identity.RolesEstimators, NormalizedName = Identity.RolesEstimatorsNormalizedName },
                        new IdentityRole { Name = Identity.RolesUsers, NormalizedName = Identity.RolesUsersNormalizedName });
                    context.SaveChanges();
                }

                //if (!env.IsDevelopment())
                    //return;

                if (context.Users.Count() != 0)
                    return;

                DataStoreInit.CreateCompanies(context);

                var adminUser = new ApplicationUser
                {
                    FirstName = "Администратор",
                    UserName = DemoConstants.UserAdminEmail,
                    Email = DemoConstants.UserAdminEmail
                };
                var resultadminUser = await userManager.CreateAsync(adminUser, DemoConstants.UserPassword);

                var manager = new ApplicationUser
                {
                    FirstName = "Менеджер",
                    UserName = DemoConstants.UserManagerEmail,
                    Email = DemoConstants.UserManagerEmail,
                    CompanyId = 1
                };
                var resultcompanyUser = await userManager.CreateAsync(manager, DemoConstants.UserPassword);

                var estimator = new ApplicationUser
                {
                    FirstName = "Расчетчик",
                    UserName = DemoConstants.UserEstimatorEmail,
                    Email = DemoConstants.UserEstimatorEmail,
                    CompanyId = 1
                };
                await userManager.CreateAsync(estimator, DemoConstants.UserPassword);

                var consultantUser1 = new ApplicationUser
                {
                    FirstName = "Консультант1",
                    UserName = DemoConstants.UserConsultantEmail1,
                    Email = DemoConstants.UserConsultantEmail1,
                    CompanyId = 1
                };
                var resultConsultant1 = await userManager.CreateAsync(consultantUser1, DemoConstants.UserPassword);

                if (resultConsultant1.Succeeded)
                {
                    var profileConsultant1 = new ConsultantProfile
                    {
                        FirstName = "Иван",
                        LastName = "Иванов",
                        DateOfEmployment = new DateTime(2016, 01, 11),
                        UserId = consultantUser1.Id
                    };

                    context.ConsultantProfiles.Add(profileConsultant1);
                    context.SaveChanges();
                }

                var consultantUser2 = new ApplicationUser
                {
                    FirstName = "Консультант2",
                    UserName = DemoConstants.UserConsultantEmail2,
                    Email = DemoConstants.UserConsultantEmail2,
                    CompanyId = 1
                };
                var resultConsultant2 = await userManager.CreateAsync(consultantUser2, DemoConstants.UserPassword);

                if (resultConsultant2.Succeeded)
                {
                    var profileConsultant2 = new ConsultantProfile
                    {
                        FirstName = "Петр",
                        LastName = "Петров",
                        DateOfEmployment = new DateTime(2016, 01, 11),
                        UserId = consultantUser2.Id
                    };

                    context.ConsultantProfiles.Add(profileConsultant2);
                    context.SaveChanges();
                }


                var customerUser = new ApplicationUser
                {
                    FirstName = "Пользователь",
                    UserName = DemoConstants.UserEmail,
                    Email = DemoConstants.UserEmail
                };
                var resultNorthwindUser = await userManager.CreateAsync(customerUser, "123456");

                context.SaveChanges();

                await userManager.AddToRoleAsync(adminUser, Identity.RolesAdministrators);
                await userManager.AddToRoleAsync(manager, Identity.RolesManagers);
                await userManager.AddToRoleAsync(consultantUser1, Identity.RolesConsultants);
                await userManager.AddToRoleAsync(consultantUser2, Identity.RolesConsultants);
                await userManager.AddToRoleAsync(estimator, Identity.RolesEstimators);
                await userManager.AddToRoleAsync(customerUser, Identity.RolesUsers);

                //if (resultNorthwindUser.Succeeded)
                //    DataStoreInit.CreateSampleData(context, customerUser.Id);

            }
        }

        private class DemoConstants
        {
            public const string UserPassword = "123456";
            
            public const string UserAdminEmail = "a@m.ru";//"admin@marketplace.ru";
            public const string UserManagerEmail = "m@m.ru";// "manager@marketplace.ru";
            public const string UserEstimatorEmail = "e@m.ru"; //"estimator@marketplace.ru";
            public const string UserConsultantEmail1 = "c1@m.ru";//"consultant1@marketplace.ru";
            public const string UserConsultantEmail2 = "c2@m.ru";//"consultant2@marketplace.ru";
            public const string UserEmail = "u@m.ru";//"test@test.ru";
        }
    }

}
