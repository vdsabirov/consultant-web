﻿using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Controllers
{
    [Produces("application/json")]
    [Route("rest/ProductCategories")]
    public class ProductCategoriesController : Controller
    {
        private readonly IProductServices _productService;

        public ProductCategoriesController(IProductServices productService)
        {
            if (productService == null) throw new ArgumentNullException(nameof(productService));
            _productService = productService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_productService.GetProductCategories());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_productService.GetProductCategoryName(id));
        }
    }
}
