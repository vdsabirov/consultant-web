﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using System.Linq;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using MarketPlace.Entities.Models;
using System.Threading.Tasks;
using MarketPlace.Infrastructure.Constants;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Users")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IErrorService _errorService;
        private readonly IUserService _userService;

        public UsersController(
            UserManager<ApplicationUser> userManager,
            IErrorService errorService,
            IUserService userService
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (userService == null) throw new ArgumentNullException(nameof(userService));
            _errorService = errorService;
            _userService = userService;
            _userService.ErrorService = _errorService;
            _userManager = userManager;
        }



        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var proxyUser = _userService.GetCurrentUser();
            var currentUser = await _userManager.FindByEmailAsync(proxyUser.Email);
            if (currentUser == null)
                return HttpNotFound();

            var roles = await _userManager.GetRolesAsync(currentUser);
            if (roles.Contains(Identity.RolesAdministrators) || roles.Contains(Identity.RolesManagers) || roles.Contains(Identity.RolesConsultants))
                return Ok(_userManager.Users.Select(u => new { Name = u.FirstName, Login = u.Email, Email = u.Email, Id = u.Id }).ToList());

            var managers = await _userManager.GetUsersInRoleAsync(Identity.RolesManagers);
            var consultants = await _userManager.GetUsersInRoleAsync(Identity.RolesConsultants);
            var admins = managers.Concat(consultants);

            var users = _userService.GetUsers();
            admins.Where(a => users.All(u => u.Id != a.Id)).ToList().ForEach(f => users.Add(MarketPlace.Entities.Mappers.User.MapTo(f)));

            return Ok(users.OrderBy(u => u.FirstName).Select(u => new { Name = u.FirstName, Login = u.Email, Email = u.Email, Id = u.Id }));
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(string id)
        {
            if (id != null)
                return Ok(_userService.GetUser(id));
            return Ok();
        }

        [Route("~/rest/Companies")]
        public IActionResult GetCompanies()
        {
            return Ok(_userService.GetCompanies());
        }

        [Route("~/rest/Companies/{id}")]
        public IActionResult GetCompany(int id)
        {
            return Ok(_userService.GetCompany(id));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }
    }
}