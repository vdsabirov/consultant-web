﻿using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace CPD.Controllers
{
    [Authorize]
    public class ProfilesController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProfilesController(
            IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService,
            UserManager<ApplicationUser> userManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
            _userManager = userManager;
        }

        [Route("rest/clientprofiles")]
        public async Task<IActionResult> GetClientProfiles()
        {
            if (!_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesAdministrators))
                return HttpUnauthorized();

            
            var users = await _userManager.GetUsersInRoleAsync(Identity.RolesUsers);
            var profiles = users.Select(u =>
            {
                var p =  _dbContext.ClientProfiles.Include(c => c.ConsultantProfile).FirstOrDefault(c => c.UserId == u.Id);
                var vm = ClientProfileViewModel.Map(p); 
                if (string.IsNullOrWhiteSpace(vm.FirstName))
                    vm.FirstName = u.FirstName;
                if (string.IsNullOrWhiteSpace(vm.Email))
                    vm.Email = u.Email;
                vm.Id = u.Id;

                return vm;

            }).ToList();

            return Ok(profiles);
        }

        [HttpPost]
        [Route("rest/clientprofile/{userId}")]
        public IActionResult SetClientProfile(string userId, [FromBody] ClientProfileViewModel vm)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(userId))
                return HttpBadRequest();

            var user = _userManager.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesAdministrators))
                return HttpUnauthorized();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);
            var currentConsultant = _dbContext.ConsultantProfiles.FirstOrDefault(c => c.UserId == currentUserId);

            var profile = _dbContext.ClientProfiles.FirstOrDefault(p => p.UserId == userId);
            if (profile == null)
                profile = new ClientProfile() { UserId = userId };

            vm.MapTo(profile);
            if (currentConsultant != null)
                profile.ConsultantProfileId = currentConsultant.Id;

            if (profile.Id > 0)
                _dbContext.Update(profile);
            else
                _dbContext.ClientProfiles.Add(profile);

            _dbContext.SaveChanges();

            return Ok(vm);
        }


    }
}
