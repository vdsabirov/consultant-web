using System;
using System.Collections.Generic;
using System.Linq;
//using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using CPD.ViewModels;
using MarketPlace.Entities.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Data.Entity;
using Newtonsoft.Json.Linq;
using System.IO;
using Microsoft.Extensions.Configuration;
using CPD.Helpers;
using Microsoft.AspNet.Hosting;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Orders")]
    public class OrdersController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IOrderService _orderService;
        private readonly IAuctionService _auctionService;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private Random _random;

        public OrdersController(
            IHostingEnvironment hostingEnvironment,
            IOrderService orderService,
            IErrorService errorService,
            IAuctionService auctionService,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration
            )
        {
            if (orderService == null) throw new ArgumentNullException(nameof(orderService));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (auctionService == null) throw new ArgumentNullException(nameof(auctionService));
            _hostingEnvironment = hostingEnvironment;
            _orderService = orderService;
            _errorService = errorService;
            _auctionService = auctionService;
            //TODO ����������� � ������� "A circular dependency was detected for the service of type"
            _orderService.ErrorService = _errorService;
            _dbContext = dbContext;
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _random = new Random();
        }

        // GET: rest/Orders
        [HttpGet]
        public IEnumerable<Order> GetOrders()
        {
            return new List<Order>();
            //return _orderService.GetOrders().OrderByDescending(o => o.OrderDate).ThenByDescending(c => c.Code);
        }

        [HttpGet]
        [Route("~/rest/clientOrderList/{clientId}")]
        [Route("~/rest/clientOrders")]
        public IActionResult GetOrders(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
                return HttpBadRequest();

            var isOwner = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService) == clientId;

            var isRoleAccess = _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                    || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                    || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators);

            if (!isOwner && !isRoleAccess)
                return HttpUnauthorized();

            var result = new List<OrderViewModel>();
            _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .Where(o => o.UserId == clientId).ToList().ForEach(o =>
                {
                    var vm = new OrderViewModel();
                    vm.Map(o, isOwner, isRoleAccess);
                    result.Add(vm);                     
                });


            return Ok(result);
        }

        [HttpGet]
        [Route("~/rest/order")]
        public IActionResult GetOrder(int orderId)
        {
            var order = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == orderId);
            if (order == null)
                return HttpNotFound();

            var isOwner = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService) == order.UserId;

            var isRoleAccess = _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                    || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                    || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators);

            if (!isOwner && !isRoleAccess)
                return HttpUnauthorized();

            var vm = new OrderViewModel();
            vm.Map(order, isOwner, isRoleAccess);

            return Ok(vm);
        }

        [HttpPost]
        [Route("~/rest/newOrder")]
        public IActionResult NewOrder([FromBody]ClientConsultantViewModel vm)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest(ModelState);

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var consultantProfile = _dbContext.ConsultantProfiles.FirstOrDefault(c => c.UserId == vm.ConsultantId);
            var user = _userManager.Users.FirstOrDefault(c => c.Id == vm.UserId);
            if (consultantProfile == null || user == null)
                return HttpBadRequest();

            var order = new Order()
            {
                OrderStateId = 1,
                UserId = user.Id,
                ConsultantProfileId = consultantProfile.Id,
                Code = _random.Next(0, 9999).ToString("D4") + " " + _random.Next(0, 9999).ToString("D4"),
                OrderDate = DateTime.Now
            };

            _dbContext.Orders.Add(order);
            _dbContext.SaveChanges();

            var savedOrder = _dbContext.Orders.Include(o => o.User).Include(o => o.ConsultantProfile).Include(o => o.OrderState).FirstOrDefault(o => o.Id == order.Id);

            var orderVm = new OrderViewModel();
            orderVm.Map(savedOrder, false, true);

            return Ok(orderVm);
        }

        [HttpPost]
        [Route("~/rest/deleteOrder/{orderId}")]
        public IActionResult DeleteOrder(int orderId)
        {
            if (orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            //var order = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            var order = _dbContext.Orders
                .Include(o => o.Products)
                .ThenInclude(p => p.ProductFiles)
                .ThenInclude(pf => pf.File)
                .FirstOrDefault(o => o.Id == orderId);
            if (order == null)
                return HttpNotFound();

            if (order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.Forming)
                return HttpBadRequest();

            var products = order?.Products?.ToList();
            var filesToDeleteFromDB = new List<MarketPlace.Entities.Models.File>();
            products?.ForEach(p =>
            {
                var productFiles = p?.ProductFiles?.ToList();
                productFiles?.ForEach(pf =>
                {
                    if (!filesToDeleteFromDB.Contains(pf.File))
                        filesToDeleteFromDB.Add(pf.File);
                    _dbContext.ProductFiles.Remove(pf);
                });
                _dbContext.SaveChanges();
            });

            //delete files related to the product from DB and from file system
            var fileFolderPath = HelperFSO.AbsPathFromRelative(_hostingEnvironment,
                _configuration["FilesStorage:FolderForFiles"],
                _configuration["FilesStorage:FolderTemp"]
                );
            if (filesToDeleteFromDB?.Count() > 0)
            {
                foreach (var f in filesToDeleteFromDB)
                {
                    //removing file from the file system
                    var filePath = Path.Combine(fileFolderPath, f.FileName);
                    System.IO.File.Delete(filePath);
                    //removing file from the db
                    //_dbContext.Files.Remove(f);
                }
                //removing file from the db
                _dbContext.Files.RemoveRange(filesToDeleteFromDB);
                _dbContext.SaveChanges();
            }

            _dbContext.Remove(order);
            _dbContext.SaveChanges();

            return Ok();                            
        }

        [HttpPost]
        [Route("~/rest/saveOrderNote/{orderId}")]
        public IActionResult SetOrderNotes(int orderId, [FromBody]JToken jNotes)
        {
            if (jNotes == null || orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var notes = jNotes.Value<string>("Notes");

            var order = _dbContext.Orders.Include(o => o.OrderState).Include(o => o.ConsultantProfile).Include(o => o.Products).FirstOrDefault(o => o.Id == orderId);
            if (notes == null || notes.Length > 200 || order == null)
                return HttpBadRequest();

            order.Notes = notes;
            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var vm = new OrderViewModel();
            vm.Map(order, false, true);

            return Ok(vm);
        }

        [HttpPost]
        [Route("~/rest/orderToTender/{orderId}")]
        public IActionResult SetOrderToTender(int orderId)
        {
            if (orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var order = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null || order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.Forming)
                return HttpBadRequest();

            order.OrderStateId = (int)MarketPlace.Infrastructure.Models.OrdersStates.AtTheTender;

            order.Tender = new Tender()
            {
                IsOpen = true,
                OpeningTime = DateTime.Now,
                ClosingTime = DateTime.Now.AddHours(OrderViewModel.HOURS_FOR_TENDER)
            };
            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var updatedOrder = _dbContext.Orders.Include(o => o.OrderState).Include(o => o.ConsultantProfile).Include(o => o.Products).FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var vm = new OrderViewModel();
            vm.Map(updatedOrder, false, true);

            return Ok(vm);
        }

        [HttpPost]
        [Route("~/rest/cancelOrder/{orderId}")]
        public IActionResult SetOrderAsCanceled(int orderId)
        {
            if (orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var order = _dbContext.Orders.Include(o => o.Tender).FirstOrDefault(o => o.Id == orderId);
            if (order == null 
                || order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.AtTheTender
                || order.Tender == null
                || order.Tender.ClosingTime.AddHours(OrderViewModel.HOURS_FOR_SET_WINNER) < DateTime.Now)
                return HttpBadRequest();

            order.OrderStateId = (int)MarketPlace.Infrastructure.Models.OrdersStates.Canceled;

            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var updatedOrder = _dbContext.Orders.Include(o => o.OrderState).Include(o => o.ConsultantProfile).Include(o => o.Products).FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var vm = new OrderViewModel();
            vm.Map(updatedOrder, false, true);

            return Ok(vm);
        }

        [HttpPost]
        [Route("~/rest/performingOrder/{orderId}")]
        public IActionResult SetOrderAsPerfoming(int orderId)
        {
            if (orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var order = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null || order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.WattingPayment)
                return HttpBadRequest();

            order.OrderStateId = (int)MarketPlace.Infrastructure.Models.OrdersStates.Performing;

            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var updatedOrder = _dbContext.Orders.Include(o => o.OrderState).Include(o => o.ConsultantProfile).Include(o => o.Products).FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var vm = new OrderViewModel();
            vm.Map(updatedOrder, false, true);

            return Ok(vm);
        }

        [HttpPost]
        [Route("~/rest/doneOrder/{orderId}")]
        public IActionResult SetOrderAsDone(int orderId)
        {
            if (orderId < 1)
                return HttpBadRequest();

            var order = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId);

            if (order == null || order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.Performing)
                return HttpBadRequest();

            var isOwner = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService) == order.UserId;
            var isRoleAccess = _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators);

            if (!isOwner && !isRoleAccess)
                return HttpUnauthorized();

            order.OrderStateId = (int)MarketPlace.Infrastructure.Models.OrdersStates.Done;

            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var updatedOrder = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var vm = new OrderViewModel();
            vm.Map(updatedOrder, isOwner, isRoleAccess);

            return Ok(vm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _orderService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            var order = _orderService.GetOrderForRead(id);
            return order != null && order.Id > 0;
        }
    }
}