using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json.Linq;
using CPD.Helpers;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/AuctionProposals")]
    public class AuctionProposalsController : Controller
    {
        private readonly IAuctionProposalService _auctionProposalService;
        private readonly IAuctionService _auctionService;
        private readonly IErrorService _errorService;

        public AuctionProposalsController(
            IErrorService errorService,
            IAuctionProposalService auctionProposalService,
            IAuctionService auctionService
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (auctionProposalService == null) throw new ArgumentNullException(nameof(auctionProposalService));
            _errorService = errorService;
            _auctionProposalService = auctionProposalService;
            _auctionProposalService.ErrorService = _errorService;
            _auctionService = auctionService;
            _auctionService.ErrorService = _errorService;
        }

        [HttpGet]
        public IActionResult GetAuctionРroposals(string condition)
        {
            var auctionId = 0;
            if (!condition.TryIntJsonParser("AuctionId", out auctionId))
                return Ok(_auctionProposalService.GetAuctionРroposals());

            return Ok(_auctionService.GetAuctionРroposalsByAuctionId(auctionId));
        }

        [HttpGet("New")]
        public IActionResult GetAuctionРroposalNew()
        {
            var auctionProposal = _auctionProposalService.GetAuctionРroposal(-1);

            if (auctionProposal == null)
                return HttpNotFound();
            
            return Ok(auctionProposal);
        }

        // GET: api/AuctionProposals/5
        [HttpGet("GetAuctionProposal/{value}")]
        public IActionResult GetAuctionProposal([FromRoute] string value)
        {
            int id;
            if (!int.TryParse(value, out id) && value != "New")
            {
                return HttpBadRequest();
            }

            var auctionProposal = _auctionProposalService.GetAuctionРroposal(id);

            if (auctionProposal == null)
            {
                return HttpNotFound();
            }

            return Ok(auctionProposal);
        }

        // POST: api/AuctionProposals
        [HttpPost]
        public IActionResult PostAuctionProposal([FromBody] AuctionProposal auctionProposal)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
               var updatedAuctionProposal = _auctionProposalService.Update(auctionProposal);
                return Ok(updatedAuctionProposal);
            }
            catch (Exception)
            {
                if (AuctionProposalExists(auctionProposal.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }
        }

        // /rest/AuctionProposals/1/proposalSetWinner
        [HttpGet("{proposalId:int}/proposalSetWinner")]
        public IActionResult SetProposalWinner(int proposalId)
        {
            if (proposalId <= 0)
                return HttpBadRequest();

            _auctionService.SetWinner(proposalId);

            var proposal = _auctionProposalService.GetAuctionРroposal(proposalId);
            return Ok(proposal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _auctionProposalService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool AuctionProposalExists(int id)
        {
            var auctionProposal = _auctionProposalService.GetAuctionРroposal(id);
            return auctionProposal != null && auctionProposal.Id > 0;
        }

    }
}