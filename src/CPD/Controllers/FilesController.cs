using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.AspNet.Http;
using Microsoft.Net.Http.Headers;
using System.IO;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Authorization;
//using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using CPD.Helpers;
using CPD.ViewModels;
using MarketPlace.Entities.Models;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Files")]
    public class FilesController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly ApplicationDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public FilesController(IHostingEnvironment hostingEnvironment,
            IErrorService errorService,
            ApplicationDbContext dbContext,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _errorService = errorService;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        [Route("~/rest/uploadfile")]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            // Create a file to write to.
            var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                .FileName
                .ToLower()
                .Trim('"');// FileName returns "fileName.ext"(with double quotes) in beta 3 //check "name" just for the file name
            var fn = Path.GetFileName(fileName);

            // various checks to see if the file is corrent - file extension, size, etc.
            var folderRelativePathToSave = _configuration["FilesStorage:FolderForFiles"] + "/" + _configuration["FilesStorage:FolderTemp"];
            var folderFullPathToSave = _hostingEnvironment.MapPath(folderRelativePathToSave);
            var fileFullPathToSave = Path.Combine(folderFullPathToSave,fn);
            var bfileExists = System.IO.File.Exists(fileFullPathToSave);
            var intFileSizeMax = int.Parse(_configuration["UploadOfFiles:FileSizeMax"]);
            var bfileSizeErr = file.Length > intFileSizeMax;

            var allowedFileTypes = _configuration["UploadOfFiles:ValidExtensions"].Split(',');
            var fileType = Path.GetExtension(fileName).TrimStart('.');
            var bFileTypeErr = !allowedFileTypes.Contains(fileType);
            var result = new JObject();

            if (bfileExists || bfileSizeErr || bFileTypeErr)
            {
                var m = (bfileExists) ? "����� ���� ��� ����������!" : "";
                m += (bfileSizeErr) ? "������ ����� ��������� ��������! " : "";
                m += (bFileTypeErr) ? "�� ���������� ��� �����! " : "";
                result["err"] = m;
                return HttpBadRequest(result);
            }
            else
            {
                try
                {
                    await file.SaveAsAsync(fileFullPathToSave);
                    result["name"] = fn;
                    var filePath = folderRelativePathToSave + @"/" + fn;
                    result["fileref"] = filePath;
                    var f = new MarketPlace.Entities.Models.File
                    {
                        FileName = fn,
                        FilePath = filePath
                    };

                    _dbContext.Files.Add(f);
                    _dbContext.SaveChanges();
                    result["status"] = "���� ��������!";
                    return Ok(result);
                }
                catch (Exception ex)
                {
                    //TODO - add logging of errors
                    result["err"] = "Error saving file";
                    return HttpBadRequest(result);
                }
            }
        }
        [Route("~/rest/handlefile")]
        [HttpPost]
        public IActionResult HandleFile([FromBody]FileHandleViewModel fileHandleViewModel)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest(ModelState);
            var result = new JObject();

            if (!String.IsNullOrEmpty(fileHandleViewModel.deleteFile))
            {
                var fileToDelete = fileHandleViewModel.deleteFile;
                var filePathToDelete = _hostingEnvironment.MapPath(fileToDelete);
                try
                {
                    var productFileInDB = _dbContext.ProductFiles
                        .Include(pf => pf.File)
                        .FirstOrDefault(f => f.File.FilePath == fileToDelete);
                    var fileInDB = productFileInDB.File;
                    var productInDB = productFileInDB.Product;
                    if (fileInDB != null)
                    {
                        _dbContext.ProductFiles.Remove(productFileInDB);
                        _dbContext.SaveChanges();
                        _dbContext.Files.Remove(fileInDB);
                        _dbContext.SaveChanges();
                        System.IO.File.Delete(filePathToDelete);
                    }
                    result["status"] = "���� ������ �� ����� ��������!";
                }
                catch
                {
                    result["err"] = "������ ��������";
                    return HttpBadRequest(result);
                }
            }
            return Ok(result);
        }
    }
}