using CPD.Helpers;
using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Controllers
{
    [Produces("application/json")]
    public class CompaniesController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;
        private ApplicationDbContext _dbContext;

        public CompaniesController(IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService)
        {
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
        }
        
        [Authorize]
        [Route("rest/clientSuppliers")]
        [Route("rest/suppliers")]
        public IActionResult GetCompanies()
        {
            var suppliers = new List<SuppliersViewModel>();
            _dbContext.Companies.ToList().ForEach(c => 
            {
                var vm = new SuppliersViewModel();
                vm.Map(c);
                suppliers.Add(vm);
            });
            return Ok(suppliers);
        }

        [Authorize]
        [HttpPost]
        [Route("rest/suppliers/{supplierId}")]
        public IActionResult SetCompany(int supplierId, [FromBody] SuppliersViewModel vm)
        {
            if (supplierId < 1 && !ModelState.IsValid)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesEstimators)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var company = _dbContext.Companies.FirstOrDefault(c => c.Id == supplierId);
            if (company == null)
                return HttpNotFound();

            vm.MapTo(company);
            _dbContext.Update(company);
            _dbContext.SaveChanges();

            vm.Map(company);

            return Ok(vm);
        }

        [Authorize]
        [HttpPost]
        [Route("rest/newSupplier")]
        public IActionResult NewCompany()
        {
            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesEstimators)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var company = new Company() { Name = "New company" };
            _dbContext.Companies.Add(company);
            _dbContext.SaveChanges();

            var vm = new SuppliersViewModel();
            vm.Map(company);

            return Ok(vm);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(int id)
        {
            return _dbContext.Companies.Count(e => e.Id == id) > 0;
        }
    }
}