﻿using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using MarketPlace.Infrastructure.Services;
using System.Net;
using System.Net.Mail;
using Serilog;
using Microsoft.Extensions.Logging;

namespace CPD.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;

        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<AccountController> _logger;

        private const string CLIENT_ACCESS = "client";
        private const string ADMIN_ACCESS = "admin";
        private const string MANAGER_ACCESS = "manager";
        private const string CONSULTANT_ACCESS = "consultant";
        private const string ESTIMATOR_ACCESS = "estimator";

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService, ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginViewModel loginViewModel)
        {
            _logger.LogCritical("XXX Login controller entered!"); //.LogInformation("Login controller entered!");
            _logger.LogError("XXX Login controller entered!");
            _logger.LogWarning("XXX Login controller entered!");

            if (!ModelState.IsValid)
                return HttpBadRequest(ModelState);

            //var login = "user@marketplace.ru";
            //var pw = "!123Qwerty";

            //var result = SignInResult.Failed;
            //if (!ModelState.IsValid)
            //   result = await _signInManager.PasswordSignInAsync(login, pw, true, lockoutOnFailure: false);
            //else
            var result = await _signInManager.PasswordSignInAsync(loginViewModel.Login, loginViewModel.Password, loginViewModel.RememberMe, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.FindByNameAsync(loginViewModel.Login);
                if (appUser == null)
                {
                    ModelState.AddModelError(string.Empty, "Такая учетная запись не существует");
                    return HttpBadRequest(ModelState);
                }
                var access = await GetAccess(appUser);

                var data = GetJsonLoginData(appUser, access);
                return Ok(data);
            }

            ModelState.AddModelError(string.Empty, "Такая учетная запись не существует");
            return HttpBadRequest(ModelState);
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            if (_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                var user = await _userManager.FindByNameAsync(_httpContextAccessor.HttpContext.User.Identity.Name);
                if (user == null)
                    return Ok();

                var access = await GetAccess(user);

                var data = GetJsonLoginData(user, access);
                return Ok(data);

            }
            return Ok();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegistrationViewModel model)
        {

            if (ModelState.IsValid)
            {
                //проверка если такой пользователь уже существует
                ApplicationUser user = null;
                try
                {
                    user = await _userManager.FindByEmailAsync(model.Email);
                }
                catch (Exception e)
                {
                    var m = e.Message;
                }
                if (user != null)
                    return HttpBadRequest(); //согласовать коды ошибки для отображения на клиенте

                user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.Name };
                IdentityResult result = null;
                try
                {
                    result = await _userManager.CreateAsync(user, model.Password);
                }
                catch (Exception e)
                {
                    var s = e.Message;
                }
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                    // Send an email with this link
                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                    //    "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");

                    await _userManager.AddToRoleAsync(user, Identity.RolesUsers);

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    if (result.Succeeded)
                    {
                        

                        return Ok(GetJsonLoginData(user, CLIENT_ACCESS));
                    }
                }
            }
            return HttpBadRequest(ModelState);

            //sending of confirmation email
            //try
            //{
            //    //SMTP server settings/configuration
            //    string SMTPhost = "smtp.gmail.com";
            //    bool SMTPEnableSsl = true;
            //    int SMTPPort = 587; //;//465;
            //    string SMTPUser = "cpd.inforeqs@gmail.com", SMTPPassword = "jryf2016";
            //    System.Net.NetworkCredential SMTPCredentials = new System.Net.NetworkCredential(userName: SMTPUser, password: SMTPPassword);

            //    string Subject = "Test of email sending",
            //            Body = "test",
            //            ToEmail = "vitaly.belik@gmail.com";

            //    MailMessage email = new MailMessage(from: SMTPUser, to: ToEmail, subject: Subject, body: Body);
            //    //configuring email
            //    email.IsBodyHtml = true;
            //    email.Priority = MailPriority.Normal;
            //    //work with the SMTP client
            //    using (SmtpClient smtpClient = new SmtpClient(host: SMTPhost, port: SMTPPort))
            //    {
            //        //configuring the client according to the settings
            //        smtpClient.UseDefaultCredentials = false;
            //        smtpClient.Credentials = SMTPCredentials;
            //        smtpClient.EnableSsl = SMTPEnableSsl;
            //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            //        //sending email
            //        smtpClient.Send(email);
            //    }

            //}
            //catch (SmtpException ex)
            //{
            //    //TODO - add handling of SMTP errors - notification of the user and/or system managers, etc.
            //}
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        private async Task<string> GetAccess(ApplicationUser user)
        {
            var access = new List<string>();
            var roles = await _userManager.GetRolesAsync(user);
            if (roles.Contains(Identity.RolesAdministrators))
                access.Add(ADMIN_ACCESS);
            if (roles.Contains(Identity.RolesManagers))
                access.Add(MANAGER_ACCESS);
            if (roles.Contains(Identity.RolesConsultants))
                access.Add(CONSULTANT_ACCESS);
            if (roles.Contains(Identity.RolesEstimators))
                access.Add(ESTIMATOR_ACCESS);
            if (roles.Contains(Identity.RolesUsers))
                access.Add(CLIENT_ACCESS);

            return string.Join(",", access);
        }

        private JObject GetJsonLoginData(ApplicationUser appUser, string access)
        {
            var user = new JObject();
            user["Id"] = appUser.Id;
            user["Name"] = appUser.FirstName;

            var data = new JObject();
            data["token"] = appUser.SecurityStamp;
            data["user"] = user;
            data["access"] = access;

            return data;
        }

    }
}
