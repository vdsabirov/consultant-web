using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Cors;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    public class CallsController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;
        private readonly ApplicationDbContext _dbContext;
        public CallsController(
            IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService)
        {
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
        }

        // GET: api/Calls
        [Route("rest/callbacks")]
        public IActionResult GetAllCallBacks()
        {
            var isRoleAccess = _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                || _httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators);

            if (!isRoleAccess)
                return HttpUnauthorized();

            var cbs = _dbContext.CallBacks.Include(c => c.User).OrderByDescending(c => c.Created).ToList().Select(cb => new
            {
                Id = cb.Id,
                Created = cb.Created,
                Phone = cb.PhoneNumber,
                Time = cb.ConvenientTime,
                UserId = cb.UserId,
                UserName = GetUserName(cb)
            }).ToList();

            return Ok(cbs);
        }

        [AllowAnonymous]
        [Route("rest/VoronezhRequest")]
        [HttpPost]
        [EnableCors("AllowVoronezhOrigin")]
        public IActionResult VoronezhRequest(CallBackVoronezhViewModel vm)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest(ModelState);

            var updated = DateTime.Now;
            var cb = new CallBack
            {
                PhoneNumber = vm.YourPhone,
                Created = updated,
                Updated = updated,
                CallerName = (string.IsNullOrEmpty(vm.YourName)) ? Properties.Resource.DidNotProvideName : vm.YourName, //��� �� �������������
                Target = HttpContext.Request.Headers["Origin"] + " " + vm.Target,
                Email = vm.YourEmail,
                ConvenientTime = vm.Time
            };
            _dbContext.CallBacks.Add(cb);
            _dbContext.SaveChanges();

            var result = new JObject();
            result["result"] = Properties.Resource.CORS_result_OK;
            return Ok(result);

        }

        private string GetUserName(CallBack callBack)
        {
            if (callBack.User == null)
            {
                var name = callBack.CallerName;
                if (callBack.Email != null)
                    name = string.IsNullOrWhiteSpace(name) ? callBack.Email : string.Format("{0} {1}", name, callBack.Email); 
               
                return string.IsNullOrWhiteSpace(name) ? Properties.Resource.NotAuthorized : string.Format("{0} ({1})", name, Properties.Resource.NotAuthorized); //�� �����������
            }

            //var profile = _dbContext.ClientProfiles.FirstOrDefault(p => p.UserId == callBack.UserId);
            //if (profile == null)
                return string.Format("{0} {1}", callBack.User.FirstName, callBack.User.Email);

            //var names = new List<string>();
            //names.Add(profile.FirstName);
            //if (!string.IsNullOrWhiteSpace(profile.MiddleName))
            //    names.Add(profile.MiddleName);
            //if (!string.IsNullOrWhiteSpace(profile.LastName))
            //    names.Add(profile.LastName);

            //return string.Format("{0} ({1})",string.Join(" ", names), profile.Email);

        }
    }
}