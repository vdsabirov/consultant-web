﻿using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Rating")]
    public class OrderExecutionRatingsController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;

        public OrderExecutionRatingsController( 
            ApplicationDbContext dbContext, 
            IHttpContextAccessor httpContextAccessor,
            IErrorService errorService)
        {
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
            _errorService = errorService;
        }

        [HttpGet]
        public IActionResult GetRating(int orderId, RatingServices service)
        {
            if (orderId < 1)
                return HttpBadRequest();

            var order = _dbContext.Orders.Include(o => o.OrderExecutionRating).FirstOrDefault(o => o.Id == orderId);
            if (order == null)
                return HttpNotFound();

            if (order.OrderExecutionRating == null)
                return Ok(0);

            switch (service)
            {
                case RatingServices.service:
                    return Ok(new { Disable = false, Rate = order.OrderExecutionRating.ServiceRating });
                case RatingServices.company:
                    return Ok(new { Disable = false, Rate = order.OrderExecutionRating.CompanyWinnerRating });
                case RatingServices.consultant:
                    return Ok(new { Disable = false, Rate = order.OrderExecutionRating.ConsultantRating });
            }
            return Ok(0);
        }

        [HttpPost]
        public IActionResult PostRating([FromBody]RatingViewModel rating)
        {
            if (!ModelState.IsValid || rating.OrderId < 1)
                return HttpBadRequest();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);

            var order = _dbContext.Orders.Include(o => o.OrderExecutionRating).FirstOrDefault(o => o.Id == rating.OrderId);
            if (order == null)
                return HttpNotFound();

            if (order.UserId != currentUserId
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            if (order.OrderExecutionRating == null)
                order.OrderExecutionRating = new OrderExecutionRating();

            switch (rating.Service)
            {
                case RatingServices.service:
                    order.OrderExecutionRating.ServiceRating = rating.Rating;
                    break;
                case RatingServices.company:
                    order.OrderExecutionRating.CompanyWinnerRating = rating.Rating;
                    break;
                case RatingServices.consultant:
                    order.OrderExecutionRating.ConsultantRating = rating.Rating;
                    break;
                default:
                    break;
            }

            _dbContext.Update(order);
            _dbContext.SaveChanges();

            return Ok(new { Disable = false, Rate = rating.Rating });
        }
    }
}
