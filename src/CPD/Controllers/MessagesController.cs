using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using System;
using CPD.Helpers;
using CPD.ViewModels;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using MarketPlace.Infrastructure.Constants;
using System.Threading.Tasks;
using MarketPlace.Entities.Models;
using Microsoft.Data.Entity;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Messages")]
    public class MessagesController : Controller
    {
        private readonly IUserService _userService;
        private MarketPlace.Entities.Models.ApplicationDbContext _dbContext;
        private readonly IErrorService _errorService;
        private IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;

        public MessagesController(IUserService userService,
            ApplicationDbContext dbContext,
            IErrorService errorService,
            IHttpContextAccessor httpContextAccessor,
            UserManager<ApplicationUser> userManager)
        {
            _userService = userService;
            _dbContext = dbContext;
            _errorService = errorService;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        [HttpGet("~/rest/correspondents")]
        public async Task<IActionResult> GetCorrespondents()
        {
            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);

            var isRoleAccess = _httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesManagers)
                || _httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesConsultants)
                || _httpContextAccessor.HttpContext.User.IsInRole(Identity.RolesAdministrators);

            if (isRoleAccess)
                return Ok(_userManager.Users.Where(u => u.Id != currentUserId).Select(u => new { Name = u.FirstName, Login = u.Email, Email = u.Email, Id = u.Id }).ToList());

            var managers = await _userManager.GetUsersInRoleAsync(Identity.RolesManagers);
            var consultants = await _userManager.GetUsersInRoleAsync(Identity.RolesConsultants);
            var admins = managers.Concat(consultants);

            var users = _userService.GetUsers();
            admins.Where(a => users.All(u => u.Id != a.Id)).ToList().ForEach(f => users.Add(MarketPlace.Entities.Mappers.User.MapTo(f)));

            return Ok(users.OrderBy(u => u.FirstName).Select(u => new { Name = u.FirstName, Login = u.Email, Email = u.Email, Id = u.Id }));
        }

        // GET: api/Messages
        [HttpGet]
        public IActionResult GetMessages(string userId, string correspondentId)
        {

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);
            if (userId == null)
                userId = currentUserId;

            if (correspondentId != currentUserId && userId != currentUserId)
                return HttpBadRequest();

            var messages = new List<MessageViewModel>();

            if (correspondentId == null)
                _dbContext.Messages
                    .Include(m => m.Reciever)
                    .Include(m => m.Sender)
                    .Where(f => f.RecieverId.Equals(userId))
                    .OrderByDescending(m => m.CreatedAt)
                    .Take(5)
                    .ToList()
                    .ForEach(f => messages.Add(new MessageViewModel(f)));

            if (userId != null && correspondentId != null)
                _dbContext.Messages
                    .Include(m => m.Reciever)
                    .Include(m => m.Sender)
                    .Where(f => (f.SenderId.Equals(userId) && f.RecieverId.Equals(correspondentId)) || f.SenderId.Equals(correspondentId) && f.RecieverId.Equals(userId))
                    .ToList()
                    .ForEach(f => messages.Add(new MessageViewModel(f)));

            return Ok(messages);
        }

        //POST: api/Messages
        [HttpPost]
        public IActionResult PostMessage([FromBody] MessageViewModel vm)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest(ModelState);

            var reciever = _userManager.Users.FirstOrDefault(u => u.Id == vm.ReceiverId);
            if (reciever == null)
                return HttpNotFound();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);

            var message = new Message();
            message.Text = vm.Text;
            message.Title = vm.Title;
            message.RecieverId = vm.ReceiverId;
            message.SenderId = currentUserId;
            message.CreatedAt = DateTime.Now;

            _dbContext.Messages.Add(message);
            _dbContext.SaveChanges();

            vm.Map(message);

            return Ok(vm);
        }

    }
}