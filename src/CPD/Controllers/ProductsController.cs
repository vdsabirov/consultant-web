using System;
using System.IO;
using System.Collections.Generic;
//using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;
using CPD.Helpers;
using CPD.ViewModels;
using MarketPlace.Entities.Models;
using Microsoft.Data.Entity;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Products")]
    public class ProductsController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IProductServices _productService;
        private readonly ApplicationDbContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public ProductsController(
            IHostingEnvironment hostingEnvironment,
            IProductServices productService,
            IErrorService errorService,
            ApplicationDbContext dbContext,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration
            )
        {
            if (productService == null) throw new ArgumentNullException(nameof(productService));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _hostingEnvironment = hostingEnvironment;
            _productService = productService;
            _errorService = errorService;
            _productService.ErrorService = _errorService;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        [Route("~/rest/newProduct/{orderId}")]
        [HttpPost]
        public IActionResult NewProduct(int orderId, [FromBody] ProductViewModel vm)
        {
            if (!ModelState.IsValid || orderId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var order = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null)
                return HttpBadRequest();

            var product = new Product();
            product.OrderId = orderId;
            vm.MapToProduct(product);
            _dbContext.Products.Add(product);
            _dbContext.SaveChanges();

            //handling of the related files into the specification object
            var filesInfo = vm?.ObjectData?.Value<JObject>("values")?.Value<JObject>("adds")?.Value<JArray>("files");
            if (filesInfo != null)
            {
                var newFiles = new List<MarketPlace.Entities.Models.File>();
                var existingFiles = new List<MarketPlace.Entities.Models.File>();
                for (int i = 0; i < filesInfo.Count; i++)
                {
                    var fileName = filesInfo[i].Value<string>("name").ToLower();
                    var filePath = filesInfo[i].Value<string>("fileref").ToLower();
                    var newFile = _dbContext.Files.FirstOrDefault(f => f.FilePath == filePath); // new File();
                    if (newFile == null)
                        newFiles.Add(new MarketPlace.Entities.Models.File
                        {
                            FileName = fileName,
                            FilePath = filePath
                        });
                    else
                        existingFiles.Add(newFile);
                }
                _dbContext.Files.AddRange(newFiles);

                var productFiles = new List<ProductFile>();
                foreach (var f in newFiles)
                {
                    productFiles.Add(new ProductFile
                    {
                        Product = product,
                        File = f
                    });
                }
                foreach (var f in existingFiles)
                {
                    productFiles.Add(new ProductFile
                    {
                        Product = product,
                        File = f
                    });
                }
                _dbContext.ProductFiles.AddRange(productFiles);
                _dbContext.SaveChanges();
                
            }

            //��� ������� ���������� ��������� �������� ��������� �������� � ������� �� ����� ���������� � ��������� ����� 
            var updatedOrder = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var orderVM = new OrderViewModel();
            orderVM.Map(updatedOrder, false, true);

            return Ok(orderVM);
        }

        [Route("~/rest/saveProduct/{productId}")]
        [HttpPost]
        public IActionResult SaveProduct(int productId, [FromBody] ProductViewModel vm)
        {
            if (!ModelState.IsValid || productId < 1)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                    && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var product = _dbContext.Products.Include(p => p.ProductFiles).ThenInclude(f => f.File).FirstOrDefault(p => p.Id == productId);
            if (product == null)
                return HttpBadRequest();

            vm.MapToProduct(product);
            _dbContext.Update(product);
            _dbContext.SaveChanges();

            var productFilesList = product.ProductFiles?.ToList();

            //handling of the related files into the specification object
            var filesInfo = vm?.ObjectData?.Value<JObject>("values")?.Value<JObject>("adds")?.Value<JArray>("files");
            if (filesInfo == null)
            {
                if (productFilesList != null)
                {
                    productFilesList.ForEach(pf => product.ProductFiles.Remove(pf));
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                var vmFilesList = new List<MarketPlace.Entities.Models.File>();
                for (int i = 0; i < filesInfo.Count; i++)
                {
                    var fileName = filesInfo[i].Value<string>("name");
                    var filePath = filesInfo[i].Value<string>("fileref");
                    vmFilesList.Add(new MarketPlace.Entities.Models.File{
                        FileName = fileName,
                        FilePath = filePath
                    });
                }
                var addedFiles = vmFilesList.Where(f => !productFilesList.Any(pf => pf.File.FilePath == f.FilePath));
                var addedProductFiles = new List<ProductFile>();
                    addedFiles.ToList().ForEach(f => { addedProductFiles.Add(new ProductFile
                        {
                            File = f,
                            Product = product
                        });
                    });
                var deletedProductFiles = productFilesList.Where(f => !vmFilesList.Any(pf => pf.FilePath == f.File.FilePath)).ToList();
                try //need to check if the files exist in the db
                {
                    deletedProductFiles.ForEach(pf => {
                        product.ProductFiles.Remove(pf);

                    });
                    addedProductFiles.ForEach(pf => product.ProductFiles.Add(pf));
                    _dbContext.SaveChanges();
                }
                catch { }
            }

            //��� ������� ���������� ��������� �������� ��������� �������� � ������� �� ����� ���������� � ��������� ����� 
            var updatedOrder = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == product.OrderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var orderVM = new OrderViewModel();
            orderVM.Map(updatedOrder, false, true);

            return Ok(orderVM);
        }

        // GET: api/Products
        [HttpGet]
        public IActionResult GetProducts(string condition)
        {
            if(condition == null)
                return Ok(_productService.GetProducts());
            int orderId;
            if (!condition.TryIntJsonParser("OrderId", out orderId))
                return HttpBadRequest(); ;
            var products = _productService.GetProductsByOrderId(orderId);
            return Ok(products);
        }

        // GET: api/Products/5
        [HttpGet("{value}", Name = "GetProduct")]
        public IActionResult GetProduct([FromRoute] string value)
        {
            int id;
            if (!int.TryParse(value, out id) && value != "New")
            {
                return HttpBadRequest();
            }

            var product = _productService.GetProduct(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return Ok(product);
        }

        [HttpPost("~/rest/deleteProduct/{id}")]
        public IActionResult DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesManagers)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var product = _dbContext.Products
                .Include(p => p.Order)
                .Include(pf => pf.ProductFiles)
                .ThenInclude(f => f.File)
                .FirstOrDefault(p => p.Id == id);
            if (product == null || product.Order == null || product.Order.OrderStateId != (int)MarketPlace.Infrastructure.Models.OrdersStates.Forming)
                return HttpNotFound();

            var orderId = product.OrderId;
            var filesToDeleteFromDB = new List<MarketPlace.Entities.Models.File>();
            product.ProductFiles.ToList().ForEach(pf => {
                //add the file to the list if it is not there
                if (!filesToDeleteFromDB.Contains(pf.File))
                    filesToDeleteFromDB.Add(pf.File);
                //delete all relationships between the deleted product and various files
                _dbContext.ProductFiles.Remove(pf);
            });
            _dbContext.SaveChanges();

            //delete files related to the product from DB and from file system
            var fileFolderPath = HelperFSO.AbsPathFromRelative(_hostingEnvironment,
                _configuration["FilesStorage:FolderForFiles"],
                _configuration["FilesStorage:FolderTemp"]
                );
            if (filesToDeleteFromDB?.Count()>0)
            {
                foreach (var f in filesToDeleteFromDB)
                {
                    //removing file from the file system
                    var filePath = Path.Combine(fileFolderPath, f.FileName);
                    System.IO.File.Delete(filePath);
                    //removing file from the db
                    //_dbContext.Files.Remove(f);
                }
                //removing file from the db
                _dbContext.Files.RemoveRange(filesToDeleteFromDB);
                _dbContext.SaveChanges();
            }

            _dbContext.Products.Remove(product);
            _dbContext.SaveChanges();

            var updatedOrder = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == orderId);
            if (updatedOrder == null)
                return HttpNotFound();

            var orderVM = new OrderViewModel();
            orderVM.Map(updatedOrder, false, true);

            return Ok(orderVM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _productService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            var product = _productService.GetProduct(id);
            return product != null && product.Id > 0;
        }

    }
}