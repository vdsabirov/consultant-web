using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using MarketPlace.Entities.Models;
using Microsoft.AspNet.Authorization;
using MarketPlace.Infrastructure.Services;
using MarketPlace.Infrastructure.Models;
using Microsoft.Framework.Runtime;
using CPD.ViewModels;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Report")]
    public class ReportController : Controller
    {
        private readonly IErrorService _errorService;
        IApplicationEnvironment _hostingEnvironment;


        public ReportController(
            IErrorService errorService,
            IApplicationEnvironment hostingEnvironment
            )
        {
            _errorService = errorService;
            _hostingEnvironment = hostingEnvironment;

        }


        // POST: api/Report
        [HttpPost]
        public async Task<IActionResult> PostAuction([FromBody] ReportBugViewModel report)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }
            else
            {
                //try
                //{

                //}
                //catch (Exception)
                //{
                //    if (AuctionExists(auction.Id))
                //    {
                //        return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                //    }
                //    throw;
                //}
                var r = report.Comment;
                //return CreatedAtRoute("GetAuction", new { id = auction.Id }, auction);
                return new NoContentResult();
            }
        }
        
    }
}