﻿using CPD.Helpers;
using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Controllers
{
    [Produces("application/json")]
    public class ConsultantVisitController : Controller
    {
        private const double WORK_HOUR_START = 10.0;
        private const double WORK_HOUR_END = 18.0;
        private const double VISIT_PERIOD = 2.0;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;

        public ConsultantVisitController(
            IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService,
            UserManager<ApplicationUser> userManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
            _userManager = userManager;
        }

        [Route("rest/currentCallBack")]
        [Authorize]
        public IActionResult GetCallBack(string clientId)
        {
            //var callBack = _dbContext.CallBacks.FirstOrDefault(c => c.UserId == clientId && c.WhenCall == null);
            //var vm = new CallBackViewModel() { ClientId = clientId };
            //if (callBack != null)
            //{
            //    vm.Phone = callBack.PhoneNumber;
            //    vm.Time = callBack.ConvenientTime;
            //}
            //return Ok(vm);

            var vm = new CallBackViewModel() { ClientId = clientId };
            return Ok(vm);
        }

        [Route("rest/visits")]
        [Authorize]
        public IActionResult GetVisits(string userId)
        {
            var vs = _dbContext.ConsultantVisits.Include(v => v.User).OrderByDescending(c => c.Create).ToList();
            var cvs = vs.Select(v => new
            {
                Id = v.Id,
                Created = v.Create,
                Date = v.Date.Date,
                Times = v.Date.ToShortTimeString(),
                Address = v.Address,
                Phone = v.Phone,
                UserId = v.UserId,
                UserName = (v.User == null) ? string.Empty : string.Format("{0} ({1})", v.User.FirstName, v.User.Email),
                Cancelled = v.CancelledDateTime
            });
            return Ok(cvs);
        }

        [HttpPost]
        [Route("rest/orderCallBack")]
        public IActionResult OrderCallBack([FromBody] CallBackViewModel vm)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest();

            if (vm.ClientId == "False")
                vm.ClientId = null;

            var updated = DateTime.Now;

            //var lastUnmarkedCallBack = _dbContext.CallBacks.FirstOrDefault(c => c.UserId == vm.ClientId && c.PhoneNumber == vm.Phone && c.WhenCall == null);
            //if (lastUnmarkedCallBack == null)
            //    lastUnmarkedCallBack = new CallBack() { UserId = vm.ClientId, Created = updated };

            //пока записываем все
            var lastUnmarkedCallBack = new CallBack() { UserId = vm.ClientId, Created = updated };

            lastUnmarkedCallBack.PhoneNumber = vm.Phone;
            lastUnmarkedCallBack.ConvenientTime = vm.Time;
            lastUnmarkedCallBack.Updated = updated;


            //if (lastUnmarkedCallBack.Id > 0)
            //    _dbContext.Update(lastUnmarkedCallBack);
            //else
                _dbContext.CallBacks.Add(lastUnmarkedCallBack);

            _dbContext.SaveChanges();

            if (vm.ClientId != null)
            {
                var currentUser = _userManager.Users.FirstOrDefault(u => u.Id == vm.ClientId);
                SetClientProfile(currentUser, vm.Phone, string.Empty);
            }
               

            return Ok(vm);
        }

        [Route("rest/currentConsultantVisit")]
        [Authorize]
        public IActionResult CurrentConsultantVisit(string clientId)
        {
            if (clientId == null)
                clientId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);

            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var vm = new ConsultantVisitViewModel();

            var userProfile = _dbContext.ClientProfiles.FirstOrDefault(c => c.UserId == clientId);
            if (userProfile != null)
            {
                vm.Address = userProfile.Address;
                vm.Phone = userProfile.PhoneNumber;
            }

            var visit = _dbContext.ConsultantVisits.FirstOrDefault(v => v.CancelledDateTime == null && v.UserId == clientId && (v.Date >= DateTime.Now));

            if (visit != null)
            {
                vm.VisitRating = visit.VisitRating < 0 ? 0 : visit.VisitRating;
                vm.Address = visit.Address;
                vm.Phone = visit.Phone;
                vm.Date = new DateTime(visit.Date.Year, visit.Date.Month, visit.Date.Day);
                vm.Times = visit.Date.ToShortTimeString();
                vm.Created = visit.Create;
            }

            return Ok(vm);
        }

        [Route("rest/variantConsultantVisit")]
        public IActionResult GetVariantConsultantVisit()
        {
            var result = new List<VariantConsultantVisitViewModel>();

            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            var timeRange = new DateTimeRange(today.AddDays(1).AddHours(WORK_HOUR_START), today.AddDays(30).AddHours(WORK_HOUR_END));

            var busy = _dbContext.ConsultantBusyPeriods.Where(p => timeRange.Intersects(new DateTimeRange(p.From, p.To))).ToList();
            var consultants = _dbContext.ConsultantProfiles.Where(c => c.DateOfRemoval == null).Select(c => c.Id).OrderBy(c => c).ToList();

            var date = today;
            while (date < timeRange.End)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday)
                    continue;
                var times = new List<string>();
                var hour = WORK_HOUR_START;
                while (hour < WORK_HOUR_END)
                {
                    var range = new DateTimeRange(date.AddHours(hour), VISIT_PERIOD);
                    var busyCosultant = busy.Where(p => range.Intersects(new DateTimeRange(p.From, p.To))).Select(b => b.ConsultantId).Distinct().OrderBy(c => c);
                    if (!consultants.SequenceEqual(busyCosultant))
                        times.Add(TimeSpan.FromHours(hour).ToString(@"hh\:mm"));
                    hour = hour + VISIT_PERIOD;
                }

                if (times.Count > 0)
                    result.Add(new VariantConsultantVisitViewModel() { Date = date.ToString("yyyy-MM-dd"), Times = times });

            }
            return Ok(result);
        }

        [HttpPost]
        [Route("rest/orderConsultantVisit")]
        [Authorize]
        public IActionResult SetConsultantVisit([FromBody]  ConsultantVisitViewModel vm)
        {
            if (!ModelState.IsValid || vm.Date == null || vm.Date < DateTime.Now)
                return HttpBadRequest();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);
            if (vm.ClientId != currentUserId)
                return HttpBadRequest();

            var visit = _dbContext.ConsultantVisits.FirstOrDefault(v => v.UserId == vm.ClientId && v.Date >= DateTime.Now && v.CancelledDateTime == null);
            if (visit != null)
                return HttpBadRequest();

            TimeSpan timeSpan;
            var from = TimeSpan.TryParse(vm.Times, out timeSpan) ? ((DateTime)vm.Date).Add(timeSpan) : ((DateTime)vm.Date).AddHours(WORK_HOUR_START);
            var range = new DateTimeRange(from, VISIT_PERIOD);

            //var busy = _dbContext.ConsultantBusyPeriods.Where(p => range.Intersects(new DateTimeRange(p.From, p.To))).Select(b => b.ConsultantId).ToList();

            //var freeConsultants = _dbContext.ConsultantProfiles.Where(c => c.DateOfRemoval == null && busy.All(b => b != c.Id)).Select(c => c.Id).ToList();
            //if (freeConsultants.Count == 0)
            //    return HttpBadRequest();

            //var selectedConsultant = freeConsultants.Select(c => new { ConsultantId = c, VisitCount = _dbContext.ConsultantBusyPeriods.Where(p => p.ConsultantId == c).Count() }).OrderByDescending(c => c.VisitCount).FirstOrDefault();
            //if (selectedConsultant == null)
            //    return HttpBadRequest();

            //var busyPeriod = new ConsultantBusyPeriod()
            //{
            //    ConsultantId = selectedConsultant.ConsultantId,
            //    From = range.Start,
            //    To = range.End
            //};

            visit = new ConsultantVisit()
            {
                UserId = vm.ClientId,
                Address = vm.Address,
                Phone = vm.Phone,
                Date = range.Start,
                Create = DateTime.Now,
                //ConsultantBusyPeriod = busyPeriod
            };

            _dbContext.Add(visit);
            _dbContext.SaveChanges();

            var currentUser = _userManager.Users.FirstOrDefault(u => u.Id == currentUserId);
            SetClientProfile(currentUser, vm.Phone, vm.Address);

            return Ok(vm);
        }

        private void SetClientProfile(ApplicationUser user, string phone, string address)
        {
            if (user == null)
                return;
            var clientProfile = _dbContext.ClientProfiles.FirstOrDefault(p => p.UserId == user.Id);
            if (clientProfile == null)
                clientProfile = new ClientProfile() { UserId = user.Id, FirstName = user.FirstName, Email = user.Email };

            if (string.IsNullOrWhiteSpace(clientProfile.PhoneNumber))
                clientProfile.PhoneNumber = phone;

            if (string.IsNullOrWhiteSpace(clientProfile.Address))
                clientProfile.Address = address;

            if (clientProfile.Id > 0)
                _dbContext.Update(clientProfile);
            else
                _dbContext.ClientProfiles.Add(clientProfile);

            _dbContext.SaveChanges();

        }


        [HttpPost]
        [Authorize]
        [Route("rest/currentConsultantVisit/cancel")]
        public IActionResult CancelConsultantVisit()
        { 
            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);

            var visit = _dbContext.ConsultantVisits.Include(c => c.ConsultantBusyPeriod).FirstOrDefault(v => v.UserId == currentUserId && v.Date >= DateTime.Now && v.CancelledDateTime == null);
            if (visit == null)
                return HttpBadRequest();

            if (visit.ConsultantBusyPeriod != null)
                _dbContext.Remove(visit.ConsultantBusyPeriod);

            visit.CancelledDateTime = DateTime.Now;
            _dbContext.Update(visit);

            _dbContext.SaveChanges();

            return Ok();
        }

    }
}
