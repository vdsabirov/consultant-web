﻿using CPD.ViewModels;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPD.Controllers
{
    [Authorize]
    [Produces("application/json")]
    public class TendersController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IErrorService _errorService;
        private ApplicationDbContext _dbContext;

        public TendersController(IHttpContextAccessor httpContextAccessor,
            ApplicationDbContext dbContext,
            IErrorService errorService)
        {
            _httpContextAccessor = httpContextAccessor;
            _dbContext = dbContext;
            _errorService = errorService;
        }

        [Route("rest/Tenders")]
        public IActionResult GetTenders(string userId)
        {
            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesEstimators)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesConsultants))
                return HttpUnauthorized();

            if (userId == null)
                return Ok(GetAllTenders());

            return  Ok(GetTendersByUserId(userId));

        }

        [HttpPost]
        [Route("rest/AddProposal/{orderId}")]
        public IActionResult AddProposal(int orderId, [FromBody] TenderProposalViewModel vm)
        {
            if (orderId < 1 || !ModelState.IsValid)
                return HttpBadRequest();

            if (!_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesEstimators)
                && !_httpContextAccessor.HttpContext.User.IsInRole(MarketPlace.Infrastructure.Constants.Identity.RolesAdministrators))
                return HttpUnauthorized();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);
            if (currentUserId != vm.UserId)
                return HttpBadRequest();

            var order = _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .FirstOrDefault(o => o.Id == orderId);

            if (order == null || order.Tender == null)
                return HttpNotFound();

            if (!order.Tender.IsOpen || order.Tender.ClosingTime < DateTime.Now)
                return HttpBadRequest();

            //var proposal = order.Tender.TenderProposals.FirstOrDefault(p => p.CompanyId == vm.ShipperId);
            //if (proposal == null)
            var    proposal = new TenderProposal() { TenderId = (int)order.TenderId};
            vm.MapTo(proposal);

            if (proposal.ProductTenderProposals == null)
                proposal.ProductTenderProposals = new List<ProductTenderProposal>();

            var deleteModels = proposal.ProductTenderProposals.Where(p => vm.Products.All(m => m.Key != p.ProductId)).ToList();
            var updateModels = proposal.ProductTenderProposals.Where(p => vm.Products.Any(m => m.Key == p.ProductId)).ToList();
            var addViewModels = vm.Products.Where(p => order.Products.Any(o => o.Id == p.Key) && proposal.ProductTenderProposals.All(m => m.ProductId != p.Key))
                                           .Select(p => new ProductTenderProposal() { ProductId = p.Key, TenderProposalId = proposal.Id, SpecificationData = p.Value.ToString()})
                                           .ToList();

            deleteModels.ForEach(d => proposal.ProductTenderProposals.Remove(d));

            updateModels.ForEach(u => 
            {
                var p = vm.Products[u.ProductId];
                u.SpecificationData = p.ToString();
            });

            addViewModels.ForEach(p => proposal.ProductTenderProposals.Add(p));

            if (proposal.Id > 0)
                _dbContext.Update(proposal);
            else
                _dbContext.TenderProposals.Add(proposal);

            _dbContext.SaveChanges();

            var vms = new List<TenderProposalViewModel>();

            _dbContext.TenderProposals
                .Include(o => o.Company)
                .Include(o => o.ProductTenderProposals)
                .Where(o => o.TenderId == order.TenderId).ToList()
                .ForEach(o => 
                {
                    var p = new TenderProposalViewModel();
                    p.Map(o);
                    vms.Add(p);
                });

            return Ok(vms);
        }

        [HttpPost]
        [Route("rest/clientOrdersSetWinner")]
        public IActionResult SetTenderWinner([FromBody]OrderWinnerViewModel vm)
        {
            if (!ModelState.IsValid)
                return HttpBadRequest();

            var currentUserId = _dbContext.GetCurrentUserId(_httpContextAccessor, _errorService);
            if (currentUserId != vm.ClientId)
                return HttpBadRequest();

            var order = _dbContext.Orders.Include(o => o.Tender).ThenInclude(t => t.TenderProposals).FirstOrDefault(o => o.Id == vm.OrderId);
            if (order == null || order.Tender == null)
                return HttpBadRequest();

            var proposal = order.Tender.TenderProposals.FirstOrDefault(p => p.Id == vm.ProposalId);
            if (proposal == null)
                return HttpBadRequest();

            proposal.IsWinner = true;
            order.Tender.IsOpen = false;
            order.OrderStateId = (int)MarketPlace.Infrastructure.Models.OrdersStates.WattingPayment;

            _dbContext.Update(order);
            _dbContext.SaveChanges();

            var result = new List<OrderViewModel>();
            _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Where(o => o.UserId == vm.ClientId).ToList().ForEach(o =>
                {
                    var ovm = new OrderViewModel();
                    ovm.Map(o);
                    result.Add(ovm);
                });

            return Ok(result);
        }

        private IEnumerable<OrderViewModel> GetAllTenders()
        {
            var result = new List<OrderViewModel>();
            _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .Where(o => o.OrderStateId == (int)MarketPlace.Infrastructure.Models.OrdersStates.AtTheTender && o.Tender != null && o.Tender.IsOpen && o.Tender.ClosingTime > DateTime.Now)
                .ToList().ForEach(o =>
                {
                    var vm = new OrderViewModel();
                    vm.Map(o);
                    result.Add(vm);
                });

            return result;
        }

        private IEnumerable<OrderViewModel> GetTendersByUserId(string userId)
        {
            var result = new List<OrderViewModel>();
            _dbContext.Orders
                .Include(o => o.OrderState)
                .Include(o => o.ConsultantProfile)
                .Include(o => o.Products)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.Company)
                .Include(o => o.Tender).ThenInclude(o => o.TenderProposals).ThenInclude(o => o.ProductTenderProposals)
                .Where(o => o.UserId == userId && o.OrderStateId == (int)MarketPlace.Infrastructure.Models.OrdersStates.AtTheTender && o.Tender != null && o.Tender.IsOpen && o.Tender.ClosingTime > DateTime.Now)
                .ToList().ForEach(o =>
                {
                    var vm = new OrderViewModel();
                    vm.Map(o);
                    result.Add(vm);
                });

            return result;
        }
    }
}
