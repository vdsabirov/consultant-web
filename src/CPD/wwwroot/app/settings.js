

var SETTINGS = {
    Table: {
        Users: "Users",
        Tables: "Tables",
        Fields: "Fields"
    },
    Rest: {
        root:          "/rest/",
        login:          "/rest/login",
        logout:         "/rest/logout",
        registration:   "/rest/registration",
        server:         "/rest/:table/:id/:action/",
        //sendFile: "/mps/demo/save_file.php"
        sendFile: "/rest/uploadfile",
        handleFile: "/rest/handlefile"

    },

    Demo: {
        //store: "/mps/demo/store.php",
        store: false,
        REG: {
            full: /^\/rest\/*/,
            half: /^\/rest\/Tables|Fields|Types|OrderStates|Actions\/*/
        }
    },
    Type: {
        KEY: 1,
        INT: 2,
        STRING: 3,
        TEXT: 4,
        REAL: 5,
        BOOL: 6,
        LINK: 7,
        LIST: 8,
        DATE: 9,
        HREF: 10
    }
};
