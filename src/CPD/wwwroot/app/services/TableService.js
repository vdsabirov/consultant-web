function TableService(DataServices) {
    var table_info = {};
    var table_fields = {};
    var table_choice = {};
    var table_keys = {};
    var _lost_choice = [];
    var _callback_ok = false;

    var _TableIdByName = function(name) {
        for(var id in table_info) if(table_info[id].Name==name) return id;
        return 0;
    };

    var _addChoice = function(tid, data) {
        table_choice[tid] = data;
    };


    var _init_choice = function() {

    };

    var _getRecordTitle = function(tableName, record) {
        var result = table_info[_TableIdByName(tableName)].RecordTitle;
        for(var field in record) result = result.replace("["+field+"]", record[field]);
        return result;
    };


    var _getFieldNameById = function(fid, table_id) {
        for(var field in table_fields[table_id]) if(table_fields[table_id][field].Id==fid) return field;
        return false;
    };

    var _parse_tables = function(result) {
        for(var i=0;i<result.length;i++) {
            table_info[result[i].Id] = ToObject(result[i]);
            table_fields[result[i].Id] = {};
        }
        DataServices.query({table:"Fields"}).$promise.then(function(result) {
            _parse_fields(result)
        });
    };

    var _parse_choice = function(tid, result) {
        table_choice[tid] = {0: " -- "};
        table_keys[tid] = {0: "off"};
        for(var i=0;i<result.length;i++) {
            table_choice[tid][result[i].Id] = _getRecordTitle(table_info[tid].Name , result[i]);
            table_keys[tid][result[i].Id] = result[i][table_info[tid].KeyField];
        }
        _load_choice();

    };

    var _load_choice = function( ) {
        if(_lost_choice.length===0) {
            if( (_callback_ok!==false) ) _callback_ok.call();
            return true;
        }
        var tid = _lost_choice.shift();
        var tableName = table_info[tid].Name;
        DataServices.query( { table: tableName }).$promise.then( function(result) {
            _parse_choice(tid, result);
        });
    };


    var _parse_fields = function(result) {
        for(var i=0;i<result.length;i++) {
            var field = ToObject ( result[i] );

            table_fields[ field.TableId][field.Name] = field;
            //table_fields[ field.TableId][field.Name]["isSlaved"] = false;

            // slave watch
            //if(field.LinkedTableId>0 && (table_info[field.LinkedTableId].SlaveOfFieldId != field.Id)) { // link & не зависимое

            if(field.LinkedTableId>0 ) _lost_choice.push(field.LinkedTableId);
        }
        //console.log(table_fields);
        _load_choice();

    };



    var _refresh = function() {
        DataServices.query({table:"Tables"}).$promise.then(function(result) {_parse_tables(result); });
    };


    this.refresh = function(_call_ok) {
        _callback_ok = _call_ok;
        _refresh();
    };

    this.getTableInfo = function(tableName) {
        return table_info[_TableIdByName(tableName)];
    };

    this.getFieldsInfo = function(tableName) {
        var tid = _TableIdByName(tableName);
        return table_fields[tid];
    };


    this.getTableName = function(table_id) {
        return table_info[table_id].Name;
    };

    this.getFieldNameById = function(fid, table_id) {
        for(var field in table_fields[table_id]) if(table_fields[table_id][field].Id==fid) return field;
        return false;
    };


    this.getSlaveTables = function(tableName) {
        var tid = _TableIdByName(tableName);
        var tables = [];
        for(var slave_id in table_info) {
            var slave = table_info[slave_id];
            if(slave.SlaveOfTableId==tid) {
                tables.push({
                    tableId: slave.Id,
                    table:slave.Name,
                    fieldId: slave.SlaveOfFieldId,
                    field: this.getFieldNameById(slave.SlaveOfFieldId, slave.Id),
                    Title: slave.Title,
                    Titles: slave.Titles
                });
            }
        }
        return tables;
    };

    this.getRecordTitle = function(tableName, record) {
        return _getRecordTitle(tableName, record)
    };

    this.getTableChoices = function(tableName) {
        var result = {};
        var tid = _TableIdByName(tableName);
        var fields = table_fields[tid];
        for(var field in fields) {
            if(fields[field].LinkedTableId>0) {
                result[field] = table_choice[fields[field].LinkedTableId];
            }
        }
        return result;
    };

    this.getKeyValue = function(tableName, id) {
        var tid = _TableIdByName(tableName);
        return angular.isDefined(table_keys[tid]) ?  table_keys[tid][id] : {};
    };

    this.getTableId = function(tableName) {
        return _TableIdByName(tableName);
    };

    this.reload = function() {
        table_info = {};
        table_fields = {};
        table_choice = {};
        table_keys = {};
        _lost_choice = [];
        _callback_ok = false;
        _refresh();
    };



    return this;
}