function DBService(DataService) {
    var DB = {};



    var setData = function(table, id, record) {
        if(angular.isUndefined(DB[table])) DB[table] = {};
        DB[table][id] = record;
    };



    this.getData = function(table, id) {
        DataService.get({table:table,id:id},
            function(data){
                setData(table, id, data);
            }
        )
    };

    this.need = function( sendData ) {
        DataService.query({table:"Tables"}).$promise
            .then(function(data){
                alert("OK");
                var res;
                angular.copy(data,res);
                console.log(res);
                sendData(data);
            });
    }
    this.getDB = function(){
        return DB;
    }
    this.getOne = function() {
        if(angular.isDefined(DB["Tables"]) && angular.isDefined(DB["Tables"][1])) return DB["Tables"][1];
        return false;
    }


    this.setRec = function(table, id, distance) {
        DataService.get({table:table,id:id},
            function(data){
                setData(table, id, data);
                distance = DB[table][id];
                console.log("DB[table][id]");
                console.log(DB[table][id])
                console.log(distance)
            });

    }

    this.needRecord = function(table, id, callback) {
        DataService.get({table:table,id:id},
            function(data){
                setData(table, id, data);
                callback(DB[table][id]);
            });
    }

    this.needList = function(table, callback) {
        DataService.query({table:table},
            function(records){
                for(var i=0;i<records.length;i++) setData(table, records[i].Id);
                callback(DB[table]);
            });
    }

    var p = this.getData("Tables",1);
}
