function ActionService(DataServices) {

    var actions = DataServices.query({table:"Actions"});


    var checkCondition = function(record, condition_string) {

        var conditions = angular.fromJson(condition_string);
        if( !angular.isArray( conditions) ) conditions = [conditions];

        //console.log("ACT : RECORD");
        //console.log(record);

        for(var i=0;i<conditions.length;i++) {
            var condition = conditions[i];
            //console.log("condition " +condition + " ~ " +  record[field]);
            for(var field in condition) {
                //if(record[field]==null) record[field] = false;
                if(angular.isUndefined(record) || angular.isUndefined(record[field]) || record[field]!=condition[field]) return false;
            }
            //console.log("condition  ok" );
            return true;
        }
        return true;
    };

    this.refresh = function() {
        DataServices.query({table:"Actions"}, function(result){
            actions = result;
        });
    };

    this.getRecordActions = function(table_id, record) {
        var result = [];
        for(var i=0;i<actions.length;i++) {
            var action = actions[i];

            if(action.ObjectTableId==table_id && checkCondition(record, action["ConditionShow"])) {
                result.push( action);
            }
        }
        return result;
    };

    return this;
}