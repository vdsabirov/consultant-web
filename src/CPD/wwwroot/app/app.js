var app = angular
        .module('app', ["SignalR","ngRoute",  "ngResource","ngCookies","ui.bootstrap","ui.mask","ui.select"])
        .config(['$routeProvider','$locationProvider',ConfigRouter])
        //.service('TranslateService',['$http',TranslateService])

        .factory('DataServices',    ['$resource', 'UserService',ConfigResource])
        .service('TableService',    ['DataServices',TableService])
        .service('ActionService',   ['DataServices',ActionService])
        .service('Session',         [Session])
        .factory('UserService',     ['$http',   'Session', UserService])
        .controller('UserController',       ['$scope', '$rootScope', '$cookieStore','$http' , 'UserService', 'Session',UserController])
        .controller('MainController',       ['$scope', '$routeParams', '$location','$http', '$uibModal', 'UserService', 'DataServices','TableService','ActionService', 'Hub', MainController])
        .controller('DataRootController',   ['$scope', '$routeParams', '$location',  '$timeout','DataServices', 'TableService', 'ActionService', 'Hub',   DataRootController])
        .controller('DataListController',   ['$scope', '$routeParams', '$controller',  '$timeout','DataServices',  'TableService','ActionService', 'Hub', DataListController])
        .controller('DataRecordController', ['$scope', '$routeParams', '$controller',  '$timeout','DataServices', 'TableService', 'ActionService', 'Hub', DataRecordController])
        .controller('RouteController',      ['$scope', '$routeParams', RouteController])

        .controller('ProductWindowsMasterController',      ['$scope', '$http', '$routeParams', ProductWindowsMasterController])


        .controller('MicroServiceController',      ['$scope', '$http', MicroServiceController])

        .controller('CallBackController',               ['$scope', '$controller', CallBackController])
        .controller('ConsultantVisitController',        ['$scope', '$controller', ConsultantVisitController])
        .controller('ClientOrderController',            ['$scope', '$controller','$routeParams', ClientOrderController])
        .controller('SupplierController',                ['$scope', '$controller', SupplierController])

        .controller('ClientsController',                ['$scope', '$controller', '$routeParams', ClientsController])
        .controller('OrdersController',                ['$scope', '$controller', OrdersController])
        .controller('CallBacksController',                ['$scope', '$controller', '$interval', CallBacksController])
        .controller('VisitsController',                ['$scope', '$controller', '$interval', VisitsController])
        .controller('ClientTendersController',                ['$scope', '$controller', '$interval', '$routeParams', ClientTendersController])

        .controller('ShippersController',               ['$scope', '$controller', ShippersController])
        .controller('TendersController',                ['$scope', '$controller', '$interval', '$routeParams', TendersController])
        .controller('ClientConsultantVisitController',  ['$scope', '$controller', ClientConsultantVisitController])
        .controller('MessagesController',               ['$scope', '$controller','$routeParams', MessagesController])


        .filter('fields', function(){
            return function(fields, need_names) {
                if(angular.isUndefined(need_names)) return fields;
                need_names = need_names.split(",");
                var result = {};
                for(var field in fields) if( need_names.indexOf(field)!==-1) result[field] = fields[field];
                return result;
            }
        })
        .filter('max', function(){
            return function(items, count) {
                return items.slice(0, count);
            }
        })
        .filter('nofields', function(){
            return function(fields, need_names) {
                if(angular.isUndefined(need_names)) return fields;
                need_names = need_names.split(",");
                var result = {};
                for(var field in fields) if( need_names.indexOf(field)===-1) result[field] = fields[field];
                return result;
            }
        })
        .filter('notnull', function(){
            return function(fields) {
                var result = [];
                for(var field in fields) if( field!=0 && !!field ) result.push(fields[field]);
                return result;
            }
        })
        .filter("html", ['$sce', function($sce) {
            return function(htmlCode){
                return $sce.trustAsHtml(htmlCode);
            }
        }])

        .filter('view', function(record, field, $scope){
            if(angular.isString(field)) field = $scope.fields[field];
            var choiseOf = $scope.fieldsChoices[field.Name];
            var cId = record[field.Name];
            return angular.isDefined( choiseOf[cId] ) ? choiseOf[cId] : cId;
        })

        .filter('record_select', function(){//lalala
            return function(records, value, fields){

                if(!value) return [];
                if(value=="*") return records;
                value = value.toLowerCase();

                if(angular.isDefined(fields)) {
                    fields = fields.split(",");
                } else {
                    fields = false;
                }
                var result = [];
                for(var i=0;i<records.length;i++) {
                    var ok = false;
                    for(var f in records[i]) {
                        if( fields!==false && fields.indexOf(f)===-1 ) continue;
                        if( String(records[i][f]).toLowerCase().indexOf(value)!==-1 ) { ok = true; break }
                    }
                    if(ok) result.push(records[i]);
                }
                return result;
            }

        })

        .directive("clientRating", function(){
            return {
                scope: {},
                controller: function($scope, $controller) {
                    $controller('MicroServiceController', {$scope: $scope});
                    $scope.current = {};
                    $scope.title;
                    $scope.service = "service";
                    $scope.param = {UserId: $scope.userID};
                    $scope.set = function() {
                        angular.extend($scope.param, {Rating: $scope.current.Rate})
                        $scope.post("Rating","current", $scope.param);
                    };
                },
                link: function(scope, element, attrs) {
                    scope.$watch(attrs["params"], function(value){
                        angular.extend(scope.param, value);
                    });
                    scope.title = attrs["title"];
                    angular.extend(scope.param, attrs["params"]);
                    scope.param.Service = attrs["name"];
                    scope.get("Rating","current", scope.param);
                },
                template: '<div class="client_rate">{{title}}: <uib-rating ng-model="current.Rate" max="5" readonly="current.Disable" ng-click="set()" titles="[\'Плохо\',\'Удовлетворительно\',\'Хорошо\',\'Очень хорошо\',\'Отлично\',]" aria-labelledby="default-rating"></uib-rating></div>'
            }
        })

    .directive("backTimer", function($interval){
            return {
                link: function(scope, element, attrs) {
                    scope.$watch(attrs["end"], function(value) {
                        var winEnd = getDateFromMS(value);
                        if(winEnd) $interval(function(){
                            if(!value) {
                                element.html("");
                                return;
                            }
                            var sec = Math.floor( winEnd.valueOf()/1000- ((new Date()).valueOf()/1000));
                            var min = Math.floor(sec/60); sec = sec-min*60;
                            var hour = Math.floor(min/60); min = min-hour*60;
                            var days = Math.floor(hour/24); hour = hour-days*24;
                            var t = "";
                            if(days>0) t+=""+days+" д ";
                            if(hour>0) t+=""+hour+" ч ";
                            if(min>0) t+=""+min+" м ";
                            if(sec>0) t+=""+sec+" с ";
                            element.html(t);

                        },1000)

                    });

                }
            }
        })

        .filter("msdate", [function() {
            return function(ms){
                return MsViewDate( ms );
            }
        }])

        .filter('tel', function () {
            return function (tel) {
                if (!tel) { return ''; }

                var value = tel.toString().trim().replace(/^\+/, '');

                if (value.match(/[^0-9]/)) {
                    return tel;
                }

                var country, city, number;

                switch (value.length) {
                    case 10: // +1PPP####### -> C (PPP) ###-####
                        country = 1;
                        city = value.slice(0, 3);
                        number = value.slice(3);
                        break;

                    case 11: // +CPPP####### -> CCC (PP) ###-####
                        country = value[0];
                        city = value.slice(1, 4);
                        number = value.slice(4);
                        break;

                    case 12: // +CCCPP####### -> CCC (PP) ###-####
                        country = value.slice(0, 3);
                        city = value.slice(3, 5);
                        number = value.slice(5);
                        break;

                    default:
                        return tel;
                }

                if (country == 1) {
                    country = "";
                }

                number = number.slice(0, 3) + '-' + number.slice(3);

                return (country + " (" + city + ") " + number).trim();
            };
        })


;


app.run(function ($rootScope, $location, $cookies) {
  $rootScope.$on('$routeChangeSuccess', function (event, current) {
    $cookies.put("CPD.InternalUrl", $location.url());
    });
});


