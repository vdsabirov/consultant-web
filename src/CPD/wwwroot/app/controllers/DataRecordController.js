function DataRecordController ($scope, $routeParams, $controller, $timeout, DataServices, TableService, ActionService, Hub) {


    $scope.record = {};
    $scope.linkedTitles = {};
    $scope.linkedChoice = {};
    $scope.js_record = {};
    $scope.actions = {};
    $scope.isNew = false;
    $scope.isEdit = false;


    $controller('DataRootController', { $scope: $scope});

    $scope.initNew = function(table) {
        $scope.table = table;
        $scope.id = "New";
        $scope.loadTableInfo();
        console.log("table = "+$scope.table+ " id = "+$scope.id);
    };



    $scope.init  = function(table, id) {
        //return;

        if(angular.isDefined(table)) {
            if( getNomber(table)>0 ) table = TableService.getTableName(table);
            //$scope.mode = $scope.getCurrentPath();
            $scope.table = table;
            $scope.id = id;
            //console.log("table = "+$scope.table+ " id = "+$scope.id);
          //  $scope.loadTableInfo();
        } else {
            $scope.mode = $scope.getCurrentPath();
            $scope.table = $scope.getParam();
            $scope.id = $scope.getParam();
        }
        if(angular.isDefined($scope.initFields)) $scope.filteredFields = $scope.initFields;
        return true;
    };

    $scope.basePath = function() {
        var path = $scope.getParentPath();
        if(path.length>0) path+="/"
        path+= $scope.mode + "/" + $scope.table+"/"+$scope.id;
        return path;
    };




    $scope.$watchCollection('js_record',function(){
        for(var fieldName in $scope.js_record) {
            if($scope.fields[fieldName].TypeId==9) $scope.record[fieldName] = getMsDateFromJS($scope.js_record[fieldName]);
        }
    });


    $scope.$watchCollection('record',function() {
        $scope.loadActions();
    });

    var loadLinked = function() {
        $scope.ready = true;
        return;
        for(var field in $scope.fields) {
            if( $scope.fields[field].LinkedTableId>0) {
                var linkTable = $scope.fields[field].LinkedTableId;
                var field_value = $scope.record[field];
                $scope.linkedTitles[field]={0: " -- "};
                if(field_value>0) DataServices.get({table: TableService.getTableName(linkTable), id: field_value }, function(result){

                    $scope.ready = true;
                },$scope._error)
            }
        }
    };

    //$scope.recordTitle = function() {
    //    var title = $scope.tableInfo.RecordTitle;
    //    for(var field in $scope.record) title = title.replace("["+field+"]",  $scope.fieldView(field, $scope.record[field]));
    //    return title;
    //};
    //
    //$scope.getSelectForRecord = function(record, field) {
    //    if($scope.fields[field].LinkedTableId == $scope.tableInfo.SlaveOfTableId  && $scope.tableInfo.SlaveOfFieldId!=$scope.fields[field].Id );
    //};

    $scope.refresh = function () {

        $scope.ready = false;
        if(!$scope.table) return;
        DataServices.get({table:$scope.table, id: $scope.id}, function(result){
            console.log(result);
            console.log($scope.fieldsChoices);
            $scope.record = result;
            $scope.normalizeRecord();
            //console.log("$scope.record = ");
            //console.log($scope.record);
            //console.log($scope.fields);

            $scope.isNew = $scope.id=="New" || $scope.id==0;
            if($scope.isNew ) $scope.record = $scope.mergePreset($scope.record);
            if($scope.fields!==false) loadLinked(); else $scope.ready = true;
            $scope.loadActions();
            $scope._loading();




        },$scope._error);
    };

    $scope.remove   = function() {
        $scope.record.$remove({table: $scope.table, id: $scope.id}, function(){
            //$scope.goPath( $scope.getPathList({mode:"table"}) );
            $scope.goBack();
        }, $scope._error );


    };

    $scope.save     = function(path) {
        var data = {table: $scope.table};
        if(!$scope.isNew) data.id = $scope.id;
        $scope.record.$save(data,function(res) {
            TableService.refresh(false);
            $scope.changeListOf($scope.table);
            if((SETTINGS.Table.table+" "+SETTINGS.Table.field).indexOf($scope.table)!==-1) TableService.reload();
            if(angular.isFunction(path)) {
                path(res);
                return;
            }
            if(angular.isDefined(path) && path=="current") {
                $scope._proceed_refresh();
                return;
            }
            if(angular.isDefined(path) && path=="back") $scope.goBack();
            else $scope.goPath( angular.isDefined(path) ? path :   $scope.getPathList({mode:$scope.mode}) );
        }, $scope._error );
    };

    $scope.getPresetValues = function() {
        //if(!$scope.table) return {};
        var values = angular.isDefined($scope.$parent) && angular.isDefined($scope.$parent["getPresetValues"]) ? $scope.$parent.getPresetValues() : {};
        if(angular.isDefined($scope.tableInfo) && angular.isDefined($scope.tableInfo.Id) && $scope.thisIsKey($scope.id)) values[$scope.tableInfo.Id] = parseInt($scope.id, 10);
        return values;
    };

    $scope.loadActions = function() {
        if(angular.isDefined($scope.tableInfo) && angular.isDefined($scope.tableInfo["Id"])) $scope.actions[$scope.id] = ActionService.getRecordActions($scope.tableInfo["Id"], $scope.record);

    };
    
    $scope.$watch("id", function () {
        //$scope.refresh();
    });

    $scope.normalizeRecord = function() {
        if (angular.isDefined($scope.record)) {
            for (var fieldName in $scope.fields) {
                if ($scope.fields[fieldName].TypeId == SETTINGS.Type.DATE && angular.isDefined($scope.record[fieldName])) {
                    $scope.js_record[fieldName] = getDateFromMS($scope.record[fieldName]);
                } else if ($scope.fields[fieldName].TypeId == SETTINGS.Type.BOOL) $scope.record[fieldName] = !!$scope.record[fieldName];

            }
        }
    };


    $scope.$on("setRecordValue", function(event, data){
        var self = data.self;
        if( angular.isDefined($scope.Self)  && $scope.Self==data.self ) {
            for(var field in data.value) $scope.record[field] = data.value[field];
        }
    });


}