function RouteController($scope, $routeParams) {

    $scope.guest_pages = [
        "views/form/authorization.html",
        "views/form/registration.html"
    ];

    $scope.templateUrl = "views/form/authorization.html";
    $scope.inited = false;
    $scope.uri_request = angular.isDefined($scope.$parent.uri_request) ? $scope.$parent.uri_request : [];
    $scope.current_path = false;


    var parseRequest = function() {
        for(var i=1;i<20;i++) if(angular.isDefined($routeParams["p"+i]))  $scope.uri_request.push( $routeParams["p"+i]);
        return true;
    };

    var isTemplatePath = function(path) {
        return (path.toLowerCase() == path) && ("0123456789".indexOf(path.substr(0,1))===-1) && (path.indexOf("-")===-1);
    };

    $scope.nextTemplate = function() {
        var template = "views";
        while( $scope.uri_request.length>0 &&  isTemplatePath( $scope.uri_request[0])) {
            var path = $scope.uri_request.shift();
            $scope.current_path= !!$scope.current_path ? $scope.current_path+"/"+path : path;
            template+="/"+path;
        }
        template+=".html";
        return template.length > 11 ? template : false;
    };

    $scope.getParams = function() {
        var params = [];
        while( $scope.uri_request.length>0 &&  !isTemplatePath($scope.uri_request[0])) params.push($scope.uri_request.shift());
        return params;
    };

    $scope.getParam = function() {
        if( $scope.uri_request.length>0 && !isTemplatePath($scope.uri_request[0])) return $scope.uri_request.shift();
        return false;
    };

    $scope.getCurrentPath = function() {
        return $scope.current_path;
    };


    $scope.inited   = angular.isDefined($scope.$parent.uri_request) ? true : parseRequest() ;
    $scope.templateUrl =  $scope.nextTemplate();

   /////////////
   if((!$scope.userID) && $scope.guest_pages.indexOf($scope.templateUrl)===-1) $scope.templateUrl = $scope.guest_pages[0];

    $scope.isRoute = function() {
        var result =  $scope.uri_request.length>0 && isTemplatePath($scope.uri_request[0]);
        return result;
    }
}
