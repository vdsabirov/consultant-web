function ProductWindowsMasterController($scope, $http,  $routeParams) {
    $scope.stages       = _MASTER_WINDOW.stages;
    $scope.profiles     = _MASTER_DATA.profiles;
    $scope.glass        = _MASTER_DATA.glass;
    $scope.i_glass      = _MASTER_DATA.i_glass;
    $scope.furnitures   = _MASTER_DATA.furniture;
    $scope.windowsills  = _MASTER_DATA.windowsill;
    $scope.decors       = _MASTER_DATA.decor;
    $scope.escarpments  = _MASTER_DATA.escarpment;


    $scope.loadMaster = false;
    $scope.img_base = "img/products/windows/";
    $scope.class_param = {
        hot: "danger",
        silence: "success",
        light: "warning"
    };

    var _init_data = $scope.data = {
        stage: "sizes",
        values: {
            sizes:{
                type: "window",
                window: {
                    width: 165,
                    height: 200
                },
                door: {
                    width: 75,
                    height: 210
                },
                note: " "
            },
            casements:{
                window_leaf: 2,     // створок окна
                fanlight: false,     // Фромуга
                leaf1: 1,
                leaf2: 3,
                leaf3: 1,
                door: 2
            },
            pane:{
                profile: 0,
                glass: 1,
                option: false,
                note: " "
            },
            glass: {
                note: " "
            },
            furniture:{
                index: 0,
                note: " "
            },
            sill:{
                with: true,
                depth: 10,
                index: "vitrag",
                sill: 0
            },
            decoration:{
                tonic: 0,
                room: 0,
                facade: 0,
                handle: 0
            },
            assembling: {
                escarpment: 0,
                depth: 10,
                issealing: false
            },
            adds: {
                notes: " ",
                file: "",
                files: []
            }
        }
    };

    $scope.data = _init_data;

    $scope.goStage = function(stage) {
        $scope.data.stage = stage;
    };

    $scope.getNextStage = function() {
        var result = false, find=false;
        for(var stage in $scope.stages) {
            if(find) {  result = stage; break }
            if(stage==$scope.data.stage) find=true;
        }
        return result;
    };

    $scope.$on("setMasterWindowData", function(event, data){
        $scope.loadMaster = true;

        $scope.data = angular.copy(_init_data);

        angular.extend($scope.data, data);




        $scope.data.stage = "sizes";


        $scope.loadMaster = false;

    });

    $scope.Finish = function() {
        $scope.$emit("getMasterWindowData", $scope.data);
    };

    $scope.saveFile = function(file) {
        var fd = new FormData();
        var f = file.files[0];

        if(angular.isUndefined(f)) return;

        fd.append('file', f);

        $http.post(SETTINGS.Rest.sendFile, fd, {
            transformRequest: angular.identity ,
            headers: { 'Content-Type': undefined }
        })

            .success(function (result) {
                $scope.data.values.adds.files.push(result);

            })

            .error(function (error) {
                $scope.showError(data, status);
            });
    };


    $scope.deleteFile = function(index) {
        $scope.data.values.adds.files.splice(index, 1);

    }

}