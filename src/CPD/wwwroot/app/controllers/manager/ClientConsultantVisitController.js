function ClientConsultantVisitController($scope, $controller) {
    $controller('ClientsController', {$scope: $scope});

    $scope.VisitUserId = false;
    $scope.$watch("sClient.selected.Id", function(user_id) {
        $scope.VisitUserId = false;
        if(angular.isUndefined(user_id)) return;
        $scope.VisitUserId = user_id;
    });
}