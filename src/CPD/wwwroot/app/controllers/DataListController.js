function DataListController( $scope, $routeParams,  $controller, $timeout, DataServices, TableService, ActionService, Hub) {

    $controller('DataRootController', { $scope: $scope});

    $scope.condition = {};
    $scope.records = [];
    $scope.actions = {};
    $scope.js_records = {};
    $scope.actions = {};

    $scope.init = function(table, condition) {
        console.log("INIT=================");
        $scope.ready = false;
        if(angular.isDefined(table)) {
            if( angular.isNumber(table) ) table = TableService.getTableName(table);
            if(angular.isDefined(condition)) $scope.condition = condition;
            $scope.table = table;
            $scope.loadTableInfo();
            if(angular.isDefined(condition)) $scope.condition = condition;
        } else {
            $scope.mode = $scope.getCurrentPath();
            $scope.table = $scope.getParam();
        }
        if(angular.isDefined($scope.initFields)) $scope.filteredFields = $scope.initFields;

        console.log("condition:"+$scope.table);
        console.log($scope.condition);
        //$scope._proceed_refresh();


        return true;
    };





    $scope.basePath = function() {
        var path = $scope.getParentPath();
        if(path.length>0) path+="/"
        path+= $scope.mode + "/" + $scope.table;
        return path;
    };



    // Преобразуем Дату в Микрософт при изменении, если есть
    $scope.$watchCollection('js_records',function(){
        for(var i=0; i< $scope.records.length;i++) {
            var id = $scope.records[i].Id;
            for(var fieldName in $scope.fields) {
                if($scope.fields[fieldName].TypeId==9 && angular.isDefined($scope.js_records[id]) && angular.isDefined($scope.js_records[id][fieldName]) ) {
                    $scope.records[i][fieldName] = getMsDateFromJS( $scope.js_records[id][fieldName] );
                }
            }
        }
    });


    $scope.asSlave = angular.isDefined( $scope.$parent.table );

    $scope.$watch('condition', function() {
            if(angular.isString($scope.table) && $scope.table.length>3)$scope._proceed_refresh
        }
    );


    $scope.refresh = function () {

        if($scope.table=="Messages") {
            console.log("=================================== Messages");
            console.log({table:$scope.table , condition: $scope.condition });
        }


        if(!$scope.table) return;

        if($scope.asSlave) {
            var slaveFieldId = $scope.tableInfo["SlaveOfFieldId"];
            var slaveField = TableService.getFieldNameById(slaveFieldId, $scope.tableInfo.Id); //var slaveFieldValue = $routeParams.Id;
            var slaveFieldValue = $scope.$parent.id;

            // Если неопределено - посмтреть на устанвленные параметры
            if(angular.isUndefined($scope.condition[slaveField])) $scope.condition[slaveField] =  parseInt(slaveFieldValue, 10);
        }
        $scope.process = true;
        DataServices.query({table:$scope.table , condition: $scope.condition },function(result){

            $scope.records = result;
            $scope.normalizeRecord();
            $scope.loadActions();
            $scope.process = false;
        },$scope._error);
    };

    //$scope.remove = function(id) { DataServices.remove(id, $scope._loading,$scope._error); };
    $scope.saveRecord = function(id) { $scope.records[id].$save() };

    $scope.action = function(id, action) {
        DataServices.query({table:$scope.table,id:id,action: action},$scope._loading,$scope._error);
    };


    $scope.$watchCollection('records',function() {
        if($scope.records.length>0 && angular.isDefined($scope.tableInfo) && angular.isDefined($scope.tableInfo["Id"])) $scope.loadActions();
    });



    $scope.loadActions = function() {
        if(angular.isDefined($scope.tableInfo) && angular.isDefined($scope.tableInfo["Id"])) for(var i=0;i<$scope.records.length;i++) $scope.actions[$scope.records[i].Id] = ActionService.getRecordActions($scope.tableInfo["Id"], $scope.records[i]);

    };

    $scope.$on("listChanges", function(event, data){
        //console.log("listChanges:"+$scope.table+" / "+data);
        $scope.refresh();
    });


    $scope.normalizeRecord = function() {
        if(angular.isDefined($scope.records)) for(var i=0; i<$scope.records.length;i++) {
            $scope.js_records[$scope.records[i].Id] = {};
            for(var fieldName in $scope.fields) {
                if($scope.fields[fieldName].TypeId==SETTINGS.Type.DATE && angular.isDefined($scope.records[i][fieldName]))  {
                    //$scope.records[i][fieldName] = getDateFromMS($scope.records[i][fieldName]);
                    $scope.js_records[$scope.records[i].Id][fieldName] = getDateFromMS($scope.records[i][fieldName]);
                }
            }
        }
    }


    $scope.$on("setCondition", function(event, data){
        var self = data.self;
        console.log("setCondition:"+$scope.self);
        console.log(data);
        if( angular.isDefined($scope.Self)  && $scope.Self==data.self ) {
            console.log("SET");
            for(var field in data.value) $scope.record[field] = data.value[field];
        }
    });

}

