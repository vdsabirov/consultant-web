function ListController($scope, $controller) {
    $controller('DataRootController', { $scope: $scope});
    $controller('DataListController', { $scope: $scope});

    $scope.table = "Product";

    var init  = function() {
        if(angular.isDefined($scope.initFields)) $scope.filteredFields = $scope.initFields;
    };
    $scope.ready = init();
}
