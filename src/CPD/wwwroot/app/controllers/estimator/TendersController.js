function TendersController($scope, $controller, $interval, $routeParams) {
    $controller('ClientsController', {$scope: $scope});

    $scope.shipper_id = false;
    $scope.shipperIndex = false;
    $scope.proposal = {Price: 0, Note:  "", Products: {}};
    $scope.orderIndex = false;


    $scope.sShipper = {selected: undefined};

    $scope.Load = function(loading) {
        $scope.get("tenders","orders",{}, function(result){
            if($routeParams.order_id) {
                $scope.order_id = $routeParams.order_id;
                for(var i=0;i<$scope.orders.length;i++) if($scope.orders[i].Id==$scope.order_id) {
                    $scope.orderIndex = i;
                    break;
                }
            }
        }, loading);
    };

    $scope.get("suppliers","shippers");

    $scope.addProposal = function() {
        console.log($scope.proposal);
        $scope.post("addProposal/"+$scope.order_id,"newProposal",{
            UserId: $scope.userID,
            ShipperId:  $scope.shipper_id,
            Price:      $scope.proposal.Price,
            Note:       $scope.proposal.Note,
            Term:       $scope.proposal.Term,
            Guarantee:  $scope.proposal.Guarantee,
            Prepay: $scope.proposal.Prepay,
            Products: $scope.proposal.Products
        }, function (result) {
            $scope.orders[$scope.orderIndex].Proposals = result;
            //$scope.orders[$scope.orderIndex].Proposals.push(result);
            console.log(result);
            $scope.proposal = {};
        })
    };

    $scope.pathOrder = function(order_id) {
        $scope.goPath("estimator/tenders/"+order_id);
    };

    $scope.setOrder = function(index) {
        if($scope.orderIndex === index) {
            $scope.orderIndex = false;
            $scope.order_id = false;
        } else {
            $scope.orderIndex = index;
            console.log($scope.orders);
            $scope.order_id = $scope.orders[$scope.orderIndex].Id;
        }
    };


    $scope.goOrder = function(orderId) {
        $scope.goPath("estimator/tenders/"+orderId);
    };




    $scope.$watch("sShipper.selected.Id", function(newId){
        if(angular.isUndefined($scope.shippers)) return;
        for(var i=0;i<$scope.shippers.length;i++) if($scope.shippers[i].Id==newId) {
            $scope.shipper_id = newId;
            $scope.shipperIndex = i;
            $scope.proposal.Term = $scope.sShipper.selected.Term;
            $scope.proposal.Prepay = $scope.sShipper.selected.Prepay;
            $scope.proposal.Guarantee = $scope.sShipper.selected.Guarantee;
            return;
        }
        $scope.shipper_id = false;
        $scope.shipperIndex = false;
    });

    $scope.Load(true);
    $scope.order_id = $routeParams.order_id ? $routeParams.order_id  : false;



    $interval(function(){
        $scope.Load(false)
    }, 60*1000);


    $scope.ProductTitle = function(product_id) {
        for(var i=0;i<$scope.orders[$scope.orderIndex].Products.length;i++) if ($scope.orders[$scope.orderIndex].Products[i].Id==product_id) return $scope.orders[$scope.orderIndex].Products[i].Title;
        return "�� ������";
    }

}
