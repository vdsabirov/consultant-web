function ShippersController($scope, $controller) {
    $controller('MicroServiceController', {$scope: $scope});

    $scope.suppliers = false;
    $scope.get("suppliers","suppliers");
    $scope.currentIndex = false;
    $scope.currentId = false;

    $scope.setActive = function(index) {
        if($scope.currentIndex === index) {
            $scope.currentIndex = false;
            $scope.currentId = false;
        } else {
            $scope.currentIndex = index;
            $scope.currentId = $scope.suppliers[$scope.currentIndex].Id;
        }
    };

    $scope.addNew = function() {
        $scope.post("newSupplier","newSupplier",{}, function(result){
            $scope.suppliers.push(result);
            $scope.currentIndex = $scope.suppliers.length-1;
            $scope.currentId = $scope.suppliers[$scope.currentIndex].Id;
        })
    }

    $scope.SaveCurrent = function() {
        $scope.post("suppliers/"+$scope.currentId,"savesupplier", $scope.suppliers[$scope.currentIndex], function(result){
            $scope.suppliers[$scope.currentIndex] = result;
            $scope.currentId = false;
            $scope.currentIndex = false;
        })

    }


}