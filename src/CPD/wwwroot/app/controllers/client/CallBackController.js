function CallBackController($scope, $controller) {
    $controller('MicroServiceController', { $scope: $scope});

    $scope.current  = {};



    $scope.WaitCallback = function() {
        $scope.get("currentCallBack", "current",{ClientId: $scope.userID})
    };

    $scope.OrderCallback = function(phone, time) {
        $scope.post("orderCallBack", "current",{ClientId: $scope.userID, Phone: phone, Time: time},
            function(result) {
                console.log(result);
                $scope.current = result;
            });
    };
   $scope.WaitCallback();
}
