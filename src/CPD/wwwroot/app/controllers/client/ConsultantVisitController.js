function ConsultantVisitController($scope, $controller) {
    $controller('MicroServiceController', { $scope: $scope});

    $scope.choise  = {
        Date: '',
        Times: '',
        Variants: []
    };
    $scope.current  = {};
    $scope.variant  = [];
    $scope.minDate = new Date();
    //$scope.maxDate = (new Date).getMilliseconds()+ 60*60*24*31  ;




    $scope.getMarked = function(date, mode) {
        var ymd = YYYY_MM_DD(date);
        for(var i=0;i<$scope.variant.length;i++) if( $scope.variant[i].Date==ymd ) return 'calendar_mark';
        return '';
    };

    function getDayClass(data) {
        return 'bg-success';
    }

    $scope.getDayClass = function(d,m) {
        //console.log(d+ " : "+m);
        return 'calendar_mark';
    };


    $scope.LoadInfo = function() {
        $scope.get("currentConsultantVisit", "current",{ClientId: $scope.userID})
        $scope.get("variantConsultantVisit", "variant",{ClientId: $scope.userID})
    };

    $scope.OrderConsultantVisit = function(date, times, phone, address) {
        var user_id = angular.isDefined($scope.VisitUserId) ? $scope.VisitUserId : $scope.userID; // Есил за него кто то заказывает
        $scope.post("orderConsultantVisit", "current",{ClientId: user_id, Date: date, Times: times, Phone: phone, Address: address},
            function(result) {
                console.log(result);
                $scope.current = result;
            });
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };


    $scope.selectDay = function(d) {
        if(!angular.isDate($scope.choise.datestamp)) return;
        $scope.choise.Date = YYYY_MM_DD($scope.choise.datestamp);

        $scope.choise.Variants = [];
        $scope.choise.Times = '';
        for(var i=0;i<$scope.variant.length;i++) {
            if( $scope.variant[i].Date==$scope.choise.Date ) {
                $scope.choise.Variants = $scope.variant[i].Times;
            }
        }

    };

    $scope.CancelVisit = function() {
        $scope.post("currentConsultantVisit/cancel","current");
    }

   $scope.LoadInfo();
}
