function ClientOrderController($scope, $controller, $routeParams) {
    $controller('MicroServiceController', {$scope: $scope});

    $scope.orders = [];

    $scope.orderIndex   = false;
    $scope.productIndex = false;


    $scope.isReadonly = true;

    $scope.LoadInfo = function() {
        $scope.get("clientOrders", "orders",{ClientId: $scope.userID}, function(result){
            $scope.orders = result;
            if($routeParams.order_id) {
                for(var i=0;i<$scope.orders.length;i++) if($scope.orders[i].Id==$routeParams.order_id) {
                    $scope.setOrder(i);
                    break;
                }
            }
        })
    };

    $scope.setOrder = function(i) {
        $scope.orderIndex = i;
    };

    $scope.getOrderIndex = function(order_id) {
        for(var i=0;i<$scope.orders.length;i++) if($scope.orders[i].Id==order_id) return i;
        return 0;
    };

    $scope.goOrder = function(orderId) {
        $scope.goPath("client/orders/"+orderId);
    };

    $scope.setWinner = function (ProposalId) {
        $("#setWinnerConfirm").modal('hide');
        $scope.post("clientOrdersSetWinner","orders",{ClientId: $scope.userID, OrderId: $scope.orders[$scope.orderIndex].Id, ProposalId: ProposalId});
    };

    $scope.setProposalForConfirming = function (proposal)
    {
        $scope.proposalForConfirming = proposal;
    };



    $scope.LoadInfo();


    $scope.doAction = function (action, order_id) {
        $scope.post(action + "/" + order_id, "_tmp", {}, function (result) {
            if (angular.isUndefined(result.Id)) {
                $scope.orders.splice($scope.getOrderIndex(order_id), 1);
                $scope.orderIndex = false;
                $scope.order_id = false;
            } else {
                $scope.orders[$scope.getOrderIndex(order_id)] = result;
            }
        })
    };

    $scope.ProductTitle = function(product_id) {
        for(var i=0;i<$scope.orders[$scope.orderIndex].Products.length;i++) if ($scope.orders[$scope.orderIndex].Products[i].Id==product_id) return $scope.orders[$scope.orderIndex].Products[i].Title;
        return "Не найден";
    }

}