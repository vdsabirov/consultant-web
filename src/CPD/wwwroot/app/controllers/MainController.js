function MainController($scope, $routeParams, $location, $http, $uibModal, UserService,  DataServices, TableService, ActionService, Hub){
    $scope.title = "Тестироваие ресурсов";
    $scope.isLogged = UserService.isLogged;
    $scope.getUser = UserService.getUser;
    $scope.getToken = UserService.getToken;
    $scope.isLoaded = false;
    $scope.hrefRoute = $routeParams;
    $scope.pagePath = "tables";
    $scope.userRole = "guest";
    $scope.currentRole = "guest";
    $scope.userID = false;
    $scope.LANG = {};
    $scope.setLocale = function(locale) {
    $http.get("app/i18n/"+locale+".json").then(function(data) {
            $scope.LANG = data.data;
        });
    };

    $scope.setLocale("ru_RU");





    $scope.role_picto = {
        guest: "street-view",
        client: "user",
        admin: "support",
        estimator: "calculator",
        consultant: "question-circle",
        manager: "male"
    };

    $scope.homePages = {
        guest: "form/authorization",
        client: "client/callback",
        admin: "table/Tables",
        estimator: "estimator/shippers",
        consultant: "consultant/clients",
        manager: "consultant/clients"
    };

    $scope.logOut =  function() {
        $http.post(SETTINGS.Rest.logout).then(function(){
            UserService.clear();
            $scope.setRole();
            $scope.goPath("form/authorization");
        })

    };

    $scope.goHome = function() {

        var uri = $scope.homePages[$scope.currentRole];
        if(window.location.href.indexOf(uri)===-1) $location.path(uri);
    };


    $scope.$on("change_role", function(){
        $scope.setRole();

    });

    $scope.setRole = function(role) {
        var user_roles = UserService.getAccessRoles().split(",");
        console.log(user_roles);
        if(angular.isUndefined(role)) $scope.currentRole = user_roles[0];
        else {
            if(user_roles.indexOf(role)!==-1)$scope.currentRole = role;
            else $scope.currentRole = user_roles[0];
        }
        console.log("SET ROLE: "+$scope.currentRole);
        $scope.userID = $scope.getUser().Id;
        //if(!$scope.userID) $scope.userID==-1;
        $scope.goHome();



    };

    $scope.getRoles = function() {
        return UserService.getAccessRoles().split(",");
    };

    $scope.setTitle = function(title) {
        $scope.title = title;
    };



    $scope.isLoaded = true;

    UserService.tryLogin().then(function(){
        $scope.setRole();
    });


    $scope.goPath = function(path) {
        $location.path(path);
    };

    $scope.getPresetValues = function() {
        var values = {};
        if($scope.isLogged()) values[TableService.getTableId(SETTINGS.Table.Users)] = $scope.getUser().Id;
        return values;
    };




    $scope.changeListOf = function(tables) {
        $scope.$broadcast('listChanges', tables);
    };





    $scope.testSave = function() {
        var all_data = DemoData.getAllData().data;
        for(var table in all_data) {
            var tableData = all_data[table];
            for(var i=0;i<tableData.length;i++) {
                var record = tableData[i];
                var id = record.Id;
                $http.get("/rs/"+table+"/"+id, record);
                for(var tt=0;tt<100000000;tt++);
                console.log(record);return;
            }
            return;
        }
    };

    $scope.findScopeRecord = function(scope, table, id) {

        if(!angular.isString(table)) return false;

        if( angular.isDefined(scope) && angular.isDefined(scope.table) && scope.table == table ) {
            if( angular.isDefined(scope.id) && scope.id == id ) return scope.record;
            if( angular.isDefined(scope.records) && !!scope.records ) for(var i=0;i<scope.records.length; i++) if( scope.records[i].Id == id ) return scope.records[i];
        }

        return angular.isDefined(scope.$parent) && angular.isDefined(scope.$parent.table) && angular.isString(table)  ? $scope.findScopeRecord(scope.$parent, table, id) : false;

    };






    $scope.goAuth = function() {
        $location.path("form/authorization");
    };




    $scope.valueFor = function(name, value) {
        $scope.$broadcast("setRecordValue", {self: name, value: value});
    };

    $scope.conditionFor = function(name, value) {
        $scope.$broadcast("setCondition", {self: name, value: condition});
    };





    $scope.saveOrder = function($current_scope) {
        $current_scope.save(function(record) {
            $current_scope.goPath("orders/"+record.Id+"/details");
        });
    };


    $scope.saveProduct = function($current_scope) {
        $current_scope.save(function(record) {
            $current_scope.goPath("orders/"+$current_scope.params.OrderId+"/details/"+record.Id+"/view/objectedit");
        });
    };



    $scope.saveProductDetail = function($current_scope) {
        $current_scope.save(function(record) {
            $current_scope.goPath("orders/"+$current_scope.params.OrderId+"/details/"+$current_scope.params.ProductId+"/view");
        });
    };



    $scope.bug = {
        Image: false,
        Text: "Напишите сообщение...",
        window: null,
        cancel: function() {
            $scope.bug.window.close();
        },
        send: function() {

        }
    };

    $scope.bagReportScreens = function() {
        html2canvas($("body"),{
            onrendered: function(canvas) {
                var image64 = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
                $.post('demo/save_screen.php',{image64:image64}, function(image_url) {
                    $scope.bug.Image = image_url;
                    $scope.bug.window = $uibModal.open({
                        templateUrl: 'views/bug/send_report.html',
                        scope: $scope
                    });
                });
            }
        });

    };

    $scope.pChat = {
        userId: false,
        window: null,
        cancel: function() {
            $scope.pChat.window.close();
        },
        setScroll: function() {
            window.setTimeout('document.getElementById("correspondence-body").scrollTop = 19999;', 500);
        },
        send: function(scope) {
            scope.record.RecieverId = $scope.pChat.userId;
            scope.record.SenderId = $scope.userID;
            scope.record.CreatedAt = $Time();
            scope.save('current');
            $scope.pChat.setScroll();
            
        },
        open: function (userId) {
            $scope.pChat.userId = userId;
            $scope.pChat.window = $uibModal.open({
                templateUrl: 'views/messages/popup.html',
                scope: $scope
            });
            $scope.pChat.setScroll();
        }
    };


    $scope.Notification = {
        window: null,
        title: "Уведомление",
        text: "...",
        picto: "",
        callback : null,
        close: function () {
            $scope.Notification.window.close();
            if($scope.Notification.callback!=null) $scope.Notification.callback();
        },
        open: function (title, text, call_back) {
            $scope.Notification.title = title;
            $scope.Notification.text = text;
            $scope.Notification.callback = angular.isDefined(call_back) ? call_back : null;
            $scope.Notification.window = $uibModal.open({
                templateUrl: 'views/messages/notification.html',
                scope: $scope
            });
        }
    };

    $scope.productDetail = {
        window: null,
            title: "Уведомление",

            product: null,
            close: function () {
            $scope.productDetail.window.close();
        },
        open: function (product) {
            $scope.productDetail.product = product;
            $scope.productDetail.window = $uibModal.open({
                templateUrl: 'views/product/windows/summaryWindow.html',
                scope: $scope
            });
        }

    };

    $scope.showError = function(data, status) {
        var error = "";
        if(angular.isString(data)) error = data;
        if(angular.isObject(data) && angular.isDefined(data.error)) error = data.error;

        if(status==401) {
            $scope.Notification.open("Ошибка","Необходима авторизация,<br>Ваша сессия истекла либо Вы не имеете необходимых прав<br>"+error, function(){ $scope.goAuth() });
        } else {
            $scope.Notification.open("Ошибка передачи данных ","При связи с сервером возникла ошибка передачи данных "+status+"<br>"+error);
        }

    };


    $scope.videochat = {
        message: "",
        setVideoConnection: function(user_id) {
            navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
            navigator.getUserMedia({ audio: true, video: true }, gotStream, streamError);

            function gotStream(stream) {
                document.querySelector('video').src = URL.createObjectURL(stream);
            }

            function streamError(error) {
                console.log(error);
            }
        }
    };


    $scope.konvert = false;
    $scope.testTitle = "Не загружено";
    $scope.values = false;
    $scope.initHub = function() {

        $scope.konvert = !$scope.konvert;

        var hub = new Hub("hubtest", {listeners: {
            close: function() {
                $scope.konvert = false;
            },
            open: function() {
                $scope.konvert = true;
            },
            title: function(title) {
                $scope.testTitle = title;
            },
            values: function(values) {
                $scope.values = values;
            }
        }})
    }

    $scope.setLoading = function(value) {
        if(value) {
            $("#loadingIndicator").show();
        } else {
            $("#loadingIndicator").hide();
        }

    }

}
