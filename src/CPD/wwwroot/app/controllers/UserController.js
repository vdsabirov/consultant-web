function Session() {
    this.set = function(token, user, access) {
        this.token = token;
        this.user   = user;
        this.access = access;
    };

    this.clear = function() {
        this.token = false;
        this.user   = false;
        this.access = false;
    };
    return this;
}



function UserService($http, Session) {
    return {
        tryLogin: function() {
            return $http.get(SETTINGS.Rest.login).then(
                function(result) {
                    if(result.data.token) {
                        console.log(result.data.user);
                        Session.set(result.data.token, result.data.user, result.data.access);
                    }
                }
            )
        },
        logOut: function() {
            $http.post(SETTINGS.Rest.logout).then(function(){
                Session.clear();
                console.log(callback);
            });
        },
        isAccess: function(role) {
            return (!!Session.access) && ( Session.access.indexOf(role)!==-1 );
        },
        isLogged: function() {
            return !!Session.token;
        },

        clear: function() {
            Session.clear();
        },

        getAccessRoles: function() {
            return !!Session.token ? Session.access : "guest";
        },
        getUser: function() {
            return (!!Session.access) ? Session.user : { name: "Гость", Id: false }
        },
        getToken: function() {
            return Session.token;
        }


    }
}

function UserController($scope, $rootScope, $cookieStore, $http, UserService, Session) {


    //$scope.login = $cookieStore.get("login");
    //$scope.password = $cookieStore.get("password");
    //$scope.rememberme = $cookieStore.get("rememberme");

    $scope.message = "";



    $scope.doLogin = function(login, password, rememberme) {
        $scope.message = "";
        $scope.login = login;
        $scope.password = password;
        $scope.rememberme = rememberme;


        //UserService.doLogin($scope.login, $scope.password, $scope.rememberme).then(
        $http.post(SETTINGS.Rest.login, {login: $scope.login, password: $scope.password, rememberme: $scope.rememberme})
            .success(function(result, status){
                if(status==200 && result.token) {
                    Session.set(result.token, result.user, result.access);
                    $scope.login = "";
                    $scope.password = "";
                    $scope.rememberme = "";
                    $scope.setRole();



                    // if($scope.rememberme) {
                        ;
                        //$cookieStore.put("login", $scope.login);
                        //$cookieStore.put("password", $scope.password);
                        //$cookieStore.put("token", Session.token);
                    //}

                } else {
                    console.log(result);
                     $scope.message = joinAny(result) ;
                }
            })
            .error(function(result, status){
                $scope.message = "Ошибка авторизации "+status;
            });
    };

    $scope.tryLogin = function() {
        UserService.tryLogin().then(function(){
            $scope.setRole();
        });
    };

    //$scope.logOut = function() {
    //    $scope.message = "";
    //    UserService.logOut();
    //    $scope.setRole();
    //};


    $scope.registrationUser = function () {
        console.log($scope.user);

        $http.post(SETTINGS.Rest.registration, $scope.user)
                .success(
                    function (result, status) {
                        console.log("S:"+status);
                        console.log(result);
                        // Зарегистрированный сразу же будет залогиненым
                        if (status==200 && result.token) Session.set(result.token, result.user, result.access);
                        else {
                            Session.clear();
                            $scope.Notification.open("Регистрация", joinAny(result) );
                        }
                        $scope.setRole();
                        //if (UserService.isLogged()) $scope.goPath("form/authorization");
                    })
                .error(
                    function (result, status, headers, config) {
                        console.log("E:"+status);
                        console.log(result);
                        Session.clear();
                        $scope.Notification.open("Регистрация", "Ошибка выполнения <br /> запроса <br />Код: <b>" + status+"</b>");

                    });

        
    }



}
