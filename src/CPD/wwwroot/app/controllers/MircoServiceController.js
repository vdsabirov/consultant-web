function MicroServiceController($scope, $http) {
    $scope.error = false;



    $scope.post = function(serviceName, name, data, callback, loading) {
        if(angular.isUndefined(loading)) loading = true;
        if(loading) $scope.setLoading(true);
        console.log(":post:"+SETTINGS.Rest.root + serviceName);
        $http.post(SETTINGS.Rest.root + serviceName, data)
            .success( function(result) {
                $scope[name] = result;
                if(angular.isDefined(callback)) callback(result);
                $scope.setLoading(false);
            })
            .error( function(data, status) {
                // Сделать обработку
                $scope.error = data;
                console.log("ERROR:"+serviceName + ": "+name);
                console.log(data);
                $scope.setLoading(false);
                $scope.showError(data, status);
            })
    };
    $scope.get = function(serviceName, name, data, callback, loading) {
        if(angular.isUndefined(loading)) loading = true;
        if(loading) $scope.setLoading(true);
        $http.get(SETTINGS.Rest.root + serviceName, {params:data})
            .success( function(result) {
                $scope[name] = result;
                if(angular.isDefined(callback)) callback(result);
                $scope.setLoading(false);
            })
            .error( function(data, status) {

                // Сделать обработку
                $scope.error = data;
                console.log("ERROR:"+serviceName + ": "+name);
                console.log(data);
                $scope.setLoading(false);
                $scope.showError(data, status);
            })

    }

}