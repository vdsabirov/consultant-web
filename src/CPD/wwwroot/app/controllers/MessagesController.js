function MessagesController($scope, $controller, $routeParams) {
    $controller('MicroServiceController', {$scope: $scope});



    $scope.mode = false;
    $scope.correspondent_id = false;


    $scope.message = {Title: "", Text: ""};

    $scope.init = function(mode, correspondent_id) {
        $scope.mode = mode;
        if(angular.isDefined(correspondent_id)) $scope.correspondent_id = correspondent_id;
        if(!$scope.correspondent_id) $scope.correspondent_id = $routeParams.RecieverId;


        $scope.Load();
    };


    $scope.Load = function() {
        switch ($scope.mode) {
            case "last":
                $scope.get("messages", "messages", {UserId: $scope.userID});
                break;
            case "user":
                $scope.get("messages", "messages", {UserId: $scope.userID, CorrespondentId: $scope.correspondent_id});
                break;
            case "correspondents":
                $scope.get("correspondents", "correspondents", { UserId: $scope.userID});
                break;
        }
        console.log($scope.messages);
        console.log("mode:"+$scope.mode);
    };

    $scope.send = function() {
        console.log($scope.message);
        var data = angular.copy($scope.message);
        data.ReceiverId = $scope.correspondent_id;
        $scope.post("messages", "sendMessage",data, function(result){
            $scope.Load();
            $scope.message = {Title: "", Text: ""};
        })

    };



}