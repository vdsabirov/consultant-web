function DataRootController($scope, $routeParams, $location,  $timeout, DataServices, TableService, ActionService, Hub) {

    $scope.table;
    //$scope.table = $routeParams.tableId;
    $scope.tableInfo = {};
    $scope.fields = false;
    $scope.waitUpdate = false;
    $scope.slaveInfo = [];
    $scope.asSlave = false;
    $scope.selectOptions = {};
    $scope.ready = false;
    $scope.mode = "table";

    $scope.params = $routeParams;
    $scope.filteredFields = false;



    $scope.boolView = {
        false: "Нет",
        true: "Да"
    };

    $scope._proceed_refresh = function() {
        if(!$scope.table) return;
        if(angular.isUndefined($scope.tableInfo)) {
            console.log("angular.isUndefined:"+$scope.tableInfo+" ( "+$scope.table);
            //$scope.loadTableInfo();
            return;
        }
        $scope.process = true;
        if (!!$scope.waitUpdate) $timeout.cancel($scope.waitUpdate);
        $scope.waitUpdate = $timeout(function () {
            $scope.refresh();
        }, 100)

    };

    $scope.thisIsKey = function(id) {
        return (angular.isNumber(id) && id>0) || (angular.isString(id) && id!="New");
    };

    $scope._loading = function (d) {
        $scope.process = false;
    };

    $scope._error = function (d) {
        $scope.process = false;
        $scope.Notification.open("Ошибка", "Ошибка передачи данных: "+d);
        console.log(d);
    };

    $scope.getParentPath = function() {
        return angular.isDefined($scope.$parent.basePath) ? $scope.$parent.basePath() : "";
    };


    $scope.getPathList = function(param) {
        var path = $scope.getParentPath();
        if(path.length>0) path+="/";
        path+=(angular.isDefined(param) && angular.isDefined(param.mode) ? param.mode  : $scope.mode)
            + "/"+ (angular.isDefined(param) && angular.isDefined(param.table) ? param.table  : $scope.table);
        return path;
    };

    $scope.getPathRecord = function(param) {
        var path = $scope.getParentPath();
        if(path.length>0) path+="/";
        path+= (angular.isDefined(param) && angular.isDefined(param.mode) ? param.mode  : $scope.mode)
            + "/"+ (angular.isDefined(param) && angular.isDefined(param.table) ? param.table  : $scope.table)
            + "/"+ (angular.isDefined(param) && angular.isDefined(param.id) ? param.id : $scope.id);
        return path;
    };


    $scope.getPresetValues = function() {
        var values = angular.isDefined($scope.$parent.getPresetValues) ? $scope.$parent.getPresetValues() : {};
        //values[TableService.getTableId("Tables")] = $scope.tableInfo.Id;
        return values;
    };

    $scope.mergePreset = function(record) {
        var preset = $scope.getPresetValues();
        for(var fieldName  in $scope.fields) {
            var field = $scope.fields[fieldName];
            if( ( (field.LinkedTableId>0 && $scope.tableInfo.SlaveOfFieldId == field.Id) || ( field.LinkedTableId>0 && $scope.tableInfo.SlaveOfFieldId != field.Id ))
                && angular.isDefined( preset[field.LinkedTableId] )) {
                record[fieldName] = preset[field.LinkedTableId];
            }
        }
        return record;
    };



    $scope.$watch('table', function() {
        if(angular.isString($scope.table) && $scope.table.length>3) $scope.loadTableInfo();
    });

    $scope.loadTableInfo = function() {
        console.log("loadTableInfo:"+$scope.table);
        //$scope.fields = false;
        $scope.fields           = TableService.getFieldsInfo($scope.table);
        $scope.tableInfo        = TableService.getTableInfo($scope.table);
        $scope.slaveInfo        = TableService.getSlaveTables($scope.table);
        $scope.fieldsChoices    = TableService.getTableChoices($scope.table);
        //console.log($scope.fieldsChoices);

        var preset = $scope.getPresetValues();
        for(var fieldName in $scope.fields) {
            var field = $scope.fields[fieldName];
            $scope.fields[fieldName].isPreset   = field.LinkedTableId>0
            && $scope.tableInfo.SlaveOfTableId == field.LinkedTableId
            && $scope.tableInfo.SlaveOfFieldId == field.Id
            && angular.isDefined(preset[field.LinkedTableId]);
        }




        for(var field in $scope.fieldsChoices) {
            $scope.selectOptions[field] = [];
            for(var Id in $scope.fieldsChoices[field]) $scope.selectOptions[field].push( {Id:   (""+Id).length>12 ? Id : parseInt(Id,10) , Title:  $scope.fieldsChoices[field][Id]} ); //parseInt(Id, 10)
        }
        $scope._proceed_refresh();

    };

    $scope.filterSelection = function(fieldName) {
        return $scope.fieldsChoices[fieldName];
    };

    //$scope.$watch('fields', $scope._proceed_refresh );

    $scope.$on('changeStructure', $scope._proceed_refresh);

    $scope.homeRoot = function() {
        return $scope.asSlave ? "./table/"+$routeParams.tableId + "/" + $routeParams.Id : "./table";
    };

    $scope.isCalcField = function(field) {
        return field.substr(0,1)=="$";
    };

    $scope.choiceView = function( field, value ) {
        //value = parseInt(value,10);
        return angular.isDefined( $scope.fieldsChoices[field]) && angular.isDefined( $scope.fieldsChoices[field][value] ) ? $scope.fieldsChoices[field][value] : value;//" -?- ";
    };

    $scope.fieldView = function(field, value) {

        var result = "?"+value;
        if( angular.isDefined($scope.fields[field]) && angular.isDefined($scope.fields[field]["TypeId"]) ) switch ($scope.fields[field].TypeId) {
            case SETTINGS.Type.KEY: result = "#"+value; break;
            case SETTINGS.Type.BOOL: result = $scope.boolView[!!value]; break;
            case SETTINGS.Type.LINK: result = $scope.choiceView(field, value); break;
            case SETTINGS.Type.DATE: result = !!value ?  msDareToView(value) : "--";
                break;
            default : result = value;
        }
        return result;
    };

    $scope.getFieldKey = function(field, value) {
        return TableService.getKeyValue($scope.fields[field].LinkedTableId,value);
    };

    $scope.getTypeKey = function(field) {
       return TableService.getKeyValue("Types",field.TypeId);
    };

    $scope.getEditTemplate = function(mode,field) {
        if(angular.isString(field)) field = $scope.fields[field];
        var type = $scope.getTypeKey(field);
        var types = {
            key: "key",
            int: "string",
            string: "string",
            text: "text",
            real: "string",
            bool: "bool",
            link: "select",
            list: "list",
            date: "datetime",
            href: "string",
            image: "image"
        };


        return "views/types/"+mode+"/"+types[type]+".html";
    };


    $scope.getViewTemplate = function(mode,field) {
        if(angular.isString(field)) field = $scope.fields[field];
        var type = $scope.getTypeKey(field);
        var types = {
            key: "key",
            int: "string",
            string: "string",
            text: "text",
            real: "string",
            bool: "bool",
            link: "select",
            list: "list",
            date: "datetime",
            href: "string",
            image: "image"
        };


        return "views/types/"+mode+"/"+types[type]+".html";
    };

    $scope.getSelected = function (field) {
        var result = [];
        for(var Id in $scope.fieldsChoices[field]) result.push( {Id: Id, Title:  $scope.fieldsChoices[field][Id]} );
        return result;
    };

    $scope.goPath = function(path) {
        $location.path(path);
    };

    $scope.goBack = function() {
        window.history.back();
    };

    $scope.isCurrentPath = function(path) {
        if(path.substr(0,1)!="/") path="/"+path;
        return $location.path() == path;
    };

    $scope.doAction = function(action, record) {
        DataServices.get({table:$scope.table, id:record.Id,  action: action.Name}).$promise.then(
            function(result) {
                $scope._proceed_refresh();
            },
            function(error) {
                $scope.Notification.open("Ошибка "+action.Title + "("+error.status+")", joinAny(error) )
            }
        );
   };


    $scope.recordTitle = function(record) {
        var rec = angular.isUndefined(record) ? $scope.record : record;
        if(angular.isUndefined($scope.tableInfo.RecordTitle)) return "";
        var title = $scope.tableInfo.RecordTitle;
        for(var field in rec) title = title.replace("["+field+"]",  $scope.fieldView(field, rec[field]));
        return title;
    };

    $scope.isChecked = function(id, field) {
        console.log("isChecked");
        console.log($scope.record[field]);
        return $scope.record[field].indexOf(id)!==-1;
    }

    $scope.toggleChecked = function(id, field) {
        console.log("toggleChecked:"+id);
        console.log($scope.record[field]);
        if( $scope.record[field].indexOf(0+id)!==-1) $scope.record[field].push(id);
        else $scope.record[field].slice( $scope.record[field].indexOf(id), 1 );
    }



}