function VisitsController($scope, $controller, $interval) {
    $controller('MicroServiceController', { $scope: $scope });

    $scope.load = function (loading) {
        $scope.get("visits", "visits", { UserId: $scope.userID },undefined, loading);
        console.log("Load");
    };

    $scope.load(true);

    $interval(function () { $scope.load(false) }, 30000);
    

    $scope.isPast = function(visit) {
        return getDateFromMS(visit.Date+"T00:00:00").valueOf() <(new Date()).valueOf();
    }

}