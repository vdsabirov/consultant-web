function ClientTendersController($scope, $controller, $interval, $routeParams) {
    $controller('ClientsController', {$scope: $scope});

    $scope.orderIndex = false;
    $scope.client_id = false;

    $scope.$watch("clientIndex", function (value) {
        $scope.client_id = value === false ? false :$scope.clients[value].Id;
        if($scope.client_id!==false) $scope.Load(true);
    });


    $scope.Load = function(loading) {

        $scope.get("tenders","orders",{UserId: $scope.client_id}, function(result){
        }, loading);
    };


    $scope.goOrder = function(orderId) {
        $scope.goPath("consultant/tenders/"+$scope.clientIndex+"/"+orderId);
    };

    $scope.goClient = function(client_index) {
        var uri = "consultant/tenders";
        if(client_index!==false) {
            uri+="/"+client_index;
            if($routeParams.tender_id) uri+="/"+$routeParams.tender_id;
        }
        $scope.goPath(uri);
    };


    if($routeParams.tender_id) {
        $scope.order_id = $routeParams.tender_id;
        console.log("order_id = "+$scope.order_id);

        $scope.get("order","order", {OrderId: $scope.order_id});

    } else {
        $scope.order_id = false;
    }



    $scope.$watch("sClient.selected.Id", function(newId){
        if(angular.isUndefined($scope.clients)) return;
        for(var i=0;i<$scope.clients.length;i++) if($scope.clients[i].Id==newId) {
            $scope.clientIndex = i;
            $scope.clientCopy = angular.copy($scope.clients);
            if(angular.isDefined($scope.form_profile)) $scope.form_profile.$setPristine();
            $scope.goClient($scope.clientIndex);
            return;
        }
        $scope.clientIndex = false;
        $scope.goClient(false);

    });


    $scope.ProductTitle = function (product_id) {
        for (var i = 0; i < $scope.order.Products.length; i++)
            if ($scope.order.Products[i].Id == product_id)
                return $scope.order.Products[i].Title;
        return "�� ������";
    }


}



