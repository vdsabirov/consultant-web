function ClientsController($scope, $controller, $routeParams) {
    $controller('MicroServiceController', {$scope: $scope});
    $scope.clientIndex = false;
    $scope.sClient = {selected: undefined};
    $scope.createClient = false;

    $scope.clientCopy = false;

    $scope.ReLoad = function() {
        $scope.get("clientprofiles", "clients", {ConsultantId: $scope.userID}, function(result){

            if($routeParams.client_idx) {
                $scope.clientIndex = $routeParams.client_idx;
                $scope.sClient.selected = $scope.clients[$scope.clientIndex];
            } else {
                $scope.clientIndex = false;
            }
        });
        //$scope.clientIndex = false;
    };

    $scope.ReLoad();

    $scope.setClient = function(idx) {
        $scope.clientIndex = idx;
    };

    $scope.SaveProfile = function(profile) {
        if($scope.createClient==true) {
            $scope.post("clientprofile/create", "currentProfile", $scope.clients[$scope.clientIndex], function(result){
                $scope.clients[$scope.clientIndex] = result;
                profile.$setPristine();
            });
        } else {
            $scope.post("clientprofile/"+$scope.clients[$scope.clientIndex].Id, "currentProfile", $scope.clients[$scope.clientIndex], function(result){
                $scope.clients[$scope.clientIndex] = result;
                profile.$setPristine();
            });
        }
    };



    $scope.$watch("sClient.selected.Id", function(newId){
        if(angular.isUndefined($scope.clients)) return;
        for(var i=0;i<$scope.clients.length;i++) if($scope.clients[i].Id==newId) {
            $scope.clientIndex = i;
            $scope.clientCopy = angular.copy($scope.clients);
            if(angular.isDefined($scope.form_profile)) $scope.form_profile.$setPristine();
            return;
        }
        $scope.clientIndex = false;
    });

    $scope.getClientId = function() {
        return $scope.clientIndex == false ? false : $scope.clients[$scope.clientIndex].Id;
    }

    $scope.addClient = function() {
        $scope.createClient = true;
        $scope.clients.push({});
        $scope.clientIndex = $scope.clients.length-1;
    }

    $scope.Cancel = function(profile) {
        $scope.clients = angular.copy($scope.clientCopy);
        profile.$setPristine();
    }

    $scope.setProfile = function(profile) {
        $scope.form_profile = profile;
        console.log("PROFILE");
        console.log($scope.form_profile);
    }
}

