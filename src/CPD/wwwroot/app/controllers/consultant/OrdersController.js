function OrdersController($scope, $controller) {
    $controller('ClientsController', {$scope: $scope});

    $scope.client_id = false;
    $scope.order_id = false;
    $scope.product_id = false;
    $scope.orderIndex = false;
    $scope.productIndex = false;

    $scope.paramsData = _MASTER_DATA;
    $scope.productNew = false;


    $scope.orders;


    $scope.Load = function() {
        if($scope.client_id) $scope.get("clientOrderList/"+$scope.client_id,"orders" );
    };

    $scope.$on("getMasterWindowData", function(event, windowData){

        if($scope.productNew == false) {
            $scope.orders[$scope.orderIndex].Products[$scope.productIndex].ObjectData  = angular.copy(windowData);
            $scope.orders[$scope.orderIndex].Products[$scope.productIndex].Title = $scope.getProductWindowTitle(windowData);
            $scope.post("saveProduct/"+$scope.product_id,"savedProduct", $scope.orders[$scope.orderIndex].Products[$scope.productIndex], function(result){
                $scope.orders[$scope.orderIndex] = result; //.Products[$scope.productIndex] = result;
            })
        } else {
            $scope.productNew.Title =  $scope.getProductWindowTitle(windowData);
            $scope.productNew.ObjectData = windowData;

            $scope.post("newProduct/"+$scope.order_id,"productNew", $scope.productNew, function(result){
                $scope.orders[$scope.orderIndex] = result;//.Products.push(result);
                $scope.productNew = false;
            });

            //$scope.orders[$scope.orderIndex].Products.push($scope.productNew);
            //$scope.productNew = false;
        }

        $scope.product_id = false;
        $scope.productIndex = false;
        $scope.productNew = false;


    });

    $scope.$watch("sClient.selected.Id", function(client_id) {
        $scope.client_id = false;
        $scope.order_id = false;
        $scope.product_id = false;
        $scope.orderIndex = false;
        $scope.productIndex = false;
        $scope.productNew = false;

        if(angular.isUndefined(client_id)) return;
        $scope.client_id = client_id;
        $scope.Load();
    });

    $scope.setOrder = function(order_id) {
        $scope.order_id = $scope.order_id === order_id ? false : order_id;
        $scope.product_id = false;
        $scope.orderIndex = false;
        $scope.productIndex = false;
        $scope.productNew = false;

        if(order_id)  $scope.orderIndex = $scope.getOrderIndex(order_id)
    };

    $scope.getOrderIndex = function(order_id) {
        for(var i=0;i<$scope.orders.length;i++) if($scope.orders[i].Id==order_id) return i;
        return 0;
    };


    $scope.setProduct = function(product_id) {
        $scope.product_id = $scope.product_id == product_id ? false : product_id;
        $scope.productIndex = false;
        $scope.productNew = false;
        if($scope.product_id) for(var i=0;i<$scope.orders[$scope.orderIndex].Products.length;i++) {
            if($scope.orders[$scope.orderIndex].Products[i].Id==product_id) {
                $scope.productIndex = i;
                $scope.$broadcast("setMasterWindowData", $scope.orders[$scope.orderIndex].Products[$scope.productIndex].ObjectData);
                break;
            }
        }

    };

    $scope.getProductWindowTitle = function(data) {

        var title = "";
        var title_w = {
            2: "Двухстворчатое",
            3: "Трехстворчатое",
            4: "Нестандартное"
        };
        if (data.values.sizes.type == 'window') {
            title = title_w[data.values.casements.window_leaf];
            title += " окно";
        } else {
            title = "Балконный блок";
        };
        return title;
    };

    $scope.newProduct = function() {
        $scope.productNew = { Id: 0, Title: "Новый продукт", TypeId: 1, ObjectData: {} };
        $scope.product_id = 0;
        $scope.$broadcast("setMasterWindowData", {});
    };

    $scope.deleteProduct = function() {
        $scope.post("deleteProduct/"+$scope.product_id,"newProducts", {}, function(result){
            $scope.orders[$scope.orderIndex] = result;//.Products = result;
            $scope.product_id = false;
            $scope.productIndex = false;
            $scope.productNew = false;

        })
    };

    $scope.orderToTender = function() {
        $scope.post("orderToTender/"+$scope.order_id,"orderModify", {}, function(result){
            $scope.orders[$scope.orderIndex] = result;
            $scope.product_id = false;
            $scope.productIndex = false;
            $scope.productNew = false;

        })
    }

    $scope.saveOrderNote = function() {
        $scope.post("saveOrderNote/"+$scope.order_id,"orderModify", {Notes: $scope.orders[$scope.orderIndex].Notes}, function(result){
            $scope.orders[$scope.orderIndex] = result;
        })
    };

    $scope.newOrder = function() {
        $scope.post("newOrder","_newOrder", { UserId: $scope.client_id, ConsultantId: $scope.userID }, function(result){
            $scope.orders.push(result);
            $scope.order_id = result.Id;
            $scope.product_id = false;
            $scope.orderIndex = $scope.orders.length-1;
            $scope.productIndex = false;
            $scope.productNew = false;
            $scope.newProduct();
        })
    };

    $scope.deleteCurrentOrder = function() {
        $scope.post("deleteOrder/"+$scope.order_id,"_tmp",  {}, function(result){
            $scope.orders.splice($scope.orderIndex, 1);
            $scope.order_id = false;
            $scope.product_id = false;
            $scope.orderIndex = false;
            $scope.productIndex = false;
            $scope.productNew = false;
        })
    };

    $scope.canToTender = function() {
        return $scope.orders[$scope.orderIndex].Products.length>0;
    };

    $scope.doAction = function (action, order_id) {
        $scope.post(action + "/" + order_id, "_tmp", {}, function (result) {
            if (angular.isUndefined(result.Id)) {
                $scope.orders.splice($scope.getOrderIndex(order_id), 1);
                $scope.orderIndex = false;
                $scope.order_id = false;
                $scope.product_id = false;
                $scope.productIndex = false;
                $scope.productNew = false;
            } else {
                $scope.orders[$scope.getOrderIndex(order_id)] = result;
            }
        })
    }







}