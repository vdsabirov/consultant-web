function ConfigResource($resource, UserService) {
    var resource =  $resource( SETTINGS.Rest.server, {},  {
        get:    {method:'GET',  headers: { Token: UserService.getToken } },
        save:   {method:'POST',  headers: { Token: UserService.getToken }},
        query:  {method:'GET', isArray:true,  headers: { Token: UserService.getToken }},
        remove: {method:'DELETE',  headers: { Token: UserService.getToken }},
        put:    {method:'PUT',  headers: { Token: UserService.getToken }},
        delete: {method:'DELETE',  headers: { Token: UserService.getToken }}
    });
    return resource;

}