function ConfigRouter ($routeProvider, $locationProvider, $translateProvider) {
    $routeProvider.when("/orders",                                          {templateUrl: "views/orders/list.html", controller: DataListController });
    $routeProvider.when("/orders/add",                                      {templateUrl: "views/orders/add.html", controller: DataRecordController });
    $routeProvider.when("/orders/type",                                      {templateUrl: "views/orders/list.html", controller: DataListController });
    $routeProvider.when("/orders/type/:orderType",                           {templateUrl: "views/orders/list.html", controller: DataListController });
    $routeProvider.when("/orders/:OrderId/edit",                            {templateUrl: "views/orders/edit.html", controller: DataRecordController });
    $routeProvider.when("/orders/:OrderId/proposals",                       {templateUrl: "views/orders/proposals.html", controller: DataListController });
    $routeProvider.when("/orders/:OrderId/details",                         {templateUrl: "views/orders/view.html", controller: DataRecordController });
    $routeProvider.when("/orders/:OrderId/details/:ProductId/:mode",         {templateUrl: "views/orders/view.html", controller: DataRecordController });
    $routeProvider.when("/orders/:OrderId/details/:ProductId/:mode/:objectmode",{templateUrl: "views/orders/view.html", controller: DataRecordController });


    $routeProvider.when("/tenders",                                                {templateUrl: "views/tenders/list.html", controller: DataListController });
    $routeProvider.when("/tenders/:TenderId",                                       {templateUrl: "views/tenders/view.html", controller: DataRecordController });
    $routeProvider.when("/tenders/:TenderId/product/:ProductId",                            {templateUrl: "views/tenders/view.html", controller: DataRecordController });


    $routeProvider.when("/messages/",                                                   {templateUrl: "views/roles/all/messages.chat.html", controller: MessagesController });
    $routeProvider.when("/messages/:RecieverId",                                               {templateUrl: "views/roles/all/messages.chat.html", controller: MessagesController });
    $routeProvider.when("/videochat",                                                       {templateUrl: "views/video/chat.html"});
    $routeProvider.when("/videochat/:UserId",                                                   {templateUrl: "views/video/chat.html"});

    $routeProvider.when("/product/windows",                                                 {templateUrl: "views/product/windows/master.html", controller: ProductWindowsMasterController});

    $routeProvider.when("/client/callback",                                                 {templateUrl: "views/roles/client/callback.html", controller: CallBackController});
    $routeProvider.when("/client/consultant_visit",                                         {templateUrl: "views/roles/client/consultant_visit.html", controller: ConsultantVisitController});
    $routeProvider.when("/client/orders",                                                   {templateUrl: "views/roles/client/orders.html", controller: ClientOrderController});
    $routeProvider.when("/client/orders/:order_id",                                         {templateUrl: "views/roles/client/orders.html", controller: ClientOrderController});
    $routeProvider.when("/client/suppliers",                                                   {templateUrl: "views/roles/client/suppliers.html", controller: SupplierController});

    $routeProvider.when("/consultant/clients",                                                   {templateUrl: "views/roles/consultant/clients.html", controller: ClientsController});
    $routeProvider.when("/consultant/orders",                                                   {templateUrl: "views/roles/consultant/orders.html", controller: OrdersController});

    $routeProvider.when("/consultant/callbacks",                                                   {templateUrl: "views/roles/consultant/callbacks.html", controller: CallBacksController});
    $routeProvider.when("/consultant/visits",                                                   {templateUrl: "views/roles/consultant/visits.html", controller: VisitsController});
    $routeProvider.when("/consultant/tenders",                                                   {templateUrl: "views/roles/consultant/tenders.html", controller: ClientTendersController});
    $routeProvider.when("/consultant/tenders/:client_idx",                                         {templateUrl: "views/roles/consultant/tenders.html", controller: ClientTendersController});
    $routeProvider.when("/consultant/tenders/:client_idx/:tender_id",                              {templateUrl: "views/roles/consultant/tenders.html", controller: ClientTendersController});

    $routeProvider.when("/estimator/shippers",                                                   {templateUrl: "views/roles/estimator/shippers.html", controller: ShippersController});
    $routeProvider.when("/estimator/tenders",                                                   {templateUrl: "views/roles/estimator/tenders.html", controller: TendersController});
    $routeProvider.when("/estimator/tenders/:order_id",                                          {templateUrl: "views/roles/estimator/tenders.html", controller: TendersController});


    $routeProvider.when("/manager/consultant_visit",                                                   {templateUrl: "views/roles/manager/consultant_visit.html", controller: ClientConsultantVisitController});


    $routeProvider.when('/', {templateUrl : "views/main.html"},{} );
    for(var p= 1, par = ""; p<20; p++) $routeProvider.when(par+="/:p"+p,{templateUrl : "views/route.html", controller: RouteController},{} );
    $routeProvider.otherwise({redirectTo: '/'});
    $locationProvider.html5Mode(true);
}

