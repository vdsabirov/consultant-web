var _MASTER_DATA = {};

_MASTER_DATA["decor"] =
{
    tonic: [
        {
            img: "7da569fe520d234507e412cdfdbb468d",
            title: "Без тонировки",
            name: false,
            type: null
        },
        {
            img: "a2b313a3d84d01ea4fdb451b6f5f0c34",
            title: "Matelux Clear",
            name: "Matelux Clear",
            description: "Элегантное матовое полупрозрачное стекло",
            type: null
        },
        {
            img: "95d0564608f3f4d042786a8581566c73",
            title: "Planibel Bronze",
            name: "Planibel Bronze",
            description: "Теплый золотистый цвет бронзы сочетается с коричневыми, зелеными и желтыми цветами в интерьере",
            type: null
        },
        {
            img: "5220f9b4347ac72927ee7911a0c5bf5d",
            title: "Planibel Grey",
            name: "Planibel Grey",
            description: "Стекло имеет стильный светло-серый оттенок, позволяющий создавать интересные варианты светового настроения в помещении",
            type: null
        },
        {
            img: "074ffe264d9264c68438497f95f1b5e5",
            title: "Planibel Green",
            name: "Planibel Green",
            description: "Помогает создать интерьер в легких, светлых, теплых тонах",
            type: null
        },
        {
            img: "f14e1a23d9784752b3ff1bf5d05e0726",
            title: "Stopsol Classic Clear",
            name: "Stopsol Classic Clear",
            description: "Обладает повышенной защитой от солнечного света",
            type: null
        },
        {
            img: "e70ec5c5c9235362e57ef064cdc17f82",
            title: "Stopsol Classic Bronze",
            name: "Stopsol Classic Bronze",
            description: "Светоотражающее стекло с насыщенным бронзовым оттенком",
            type: null
        },
        {
            img: "41b137a59d6256ea0bbe3ce26bb2379b",
            title: "Stopsol Classic Grey",
            name: "Stopsol Classic Grey",
            description: "Серое рефлекторное стекло, идеально подходит для коммерческих зданий",
            type: null
        },
        {
            img: "0c2cb3617ed70166e06ef8598023f377",
            title: "Stopsol Classic Green",
            name: "Stopsol Classic Green",
            description: "Стекло болотно-зеленого оттенка с легким отражающим эффектом",
            type: null
        },
        {
            img: "772b5d7170d6626681dec01efda103e8",
            title: "Sun-Guard Royal Blue",
            name: "Sun-Guard Royal Blue",
            description: "Солнцезащитное стекло с повышенной стойкостью к механическим воздействиям",
            type: null
        }
    ],
    room: [
        {
            title: "Без цвета",
            name: false,
            type: "white"
        },
        {
            img: "a3169019fd41a3a64494a35a2df5c2ae=c1h65.jpg",
            title: "Ламинация Черная вишня",
            name: "Черная вишня",
            type: "laminat"
        },
        {
            img: "7ef24d885effc73fb5ef73f609e74b40=c1h65.jpg",
            title: "Ламинация Орех",
            name: "Орех",
            type: "laminat"
        },
        {
            img: "79e2db53807ff52987fb248a1488b084=c1h65.jpg",
            title: "Ламинация Светло-серый",
            name: "Светло-серый",
            type: "laminat"
        },
        {
            img: "f800c8d577ee443eba8952122d8f3ae0=c1h65.jpg",
            title: "Ламинация Орегон",
            name: "Орегон",
            type: "laminat"
        },
        {
            img: "886c4f1cde314f0202d7ec8b769518fd=c1h65.jpg",
            title: "Ламинация Темно-коричневый",
            name: "Темно-коричневый",
            type: "laminat"
        },
        {
            img: "1b6596586f1db20cf3e8e940ed2b285f=c1h65.jpg",
            title: "Ламинация Винно-красный",
            name: "Винно-красный",
            type: "laminat"
        },
        {
            img: "87a3382063751ef61383f990ce643340=c1h65.jpg",
            title: "Ламинация Бриллиантово-синий",
            name: "Бриллиантово-синий",
            type: "laminat"
        },
        {
            img: "1e1fa285d9e2bb38e457560756347565=c1h65.jpg",
            title: "Ламинация Мореный дуб",
            name: "Мореный дуб",
            type: "laminat"
        },
        {
            img: "abff56f90cc22d15d56215e183030bfb=c1h65.jpg",
            title: "Ламинация Коричневый каштан",
            name: "Коричневый каштан",
            type: "laminat"
        },
        {
            img: "ccd084f72f7dc399461175e629f8caa4=c1h65.jpg",
            title: "Ламинация Темно-зеленый",
            name: "Темно-зеленый",
            type: "laminat"
        },
        {
            img: "8d197ca2a4d6501ec87bfeaafa740be0=c1h65.jpg",
            title: "Ламинация Дуб",
            name: "Дуб",
            type: "laminat"
        },
        {
            img: "ab77ab2abbe941d113c74eee836e9334=c1h65.jpg",
            title: "Ламинация Кремовый",
            name: "Кремовый",
            type: "laminat"
        },
        {
            img: "ea7689a2c7d18747fc459acde63bb5da=c1h65.jpg",
            title: "Ламинация Шоколадно-коричневый",
            name: "Шоколадно-коричневый",
            type: "laminat"
        },
        {
            img: "5234ce659f5e423cda0560401dfd88a9=c1h65.jpg",
            title: "Ламинация Светлый дуб",
            name: "Светлый дуб",
            type: "laminat"
        },
        {
            img: "43d532cee433480b74474c30670db198=c1h65.jpg",
            title: "Ламинация Красный",
            name: "Красный",
            type: "laminat"
        },
        {
            img: "b356eefd6f0c163653adae5c73f7c70e=c1h65.jpg",
            title: "Ламинация Золотой дуб",
            name: "Золотой дуб",
            type: "laminat"
        },
        {
            img: "282877c9ac00259e540a5066b497e695=c1h65.jpg",
            title: "Ламинация Темно-красный",
            name: "Темно-красный",
            type: "laminat"
        },
        {
            img: "89ba5ae8477be4109ff588397759bfac=c1h65.jpg",
            title: "Ламинация Золотой орех",
            name: "Золотой орех",
            type: "laminat"
        },
        {
            img: "8ed41c3c39f362b884fc41604c15c0c4=c1h65.jpg",
            title: "Ламинация Зеленый",
            name: "Зеленый",
            type: "laminat"
        },
        {
            img: "8c8e2474565f2df644a04198a2bf8a0e=c1h65.jpg",
            title: "Ламинация Мореный дуб",
            name: "Мореный дуб",
            type: "laminat"
        },
        {
            img: "e327532abb72c76bf18ac74c58464f42=c1h65.jpg",
            title: "Ламинация Мореный дуб2",
            name: "Мореный дуб 2",
            type: "laminat"
        },
        {
            img: "01b3f077243a05c6af666db96c56d7d1=c1h65.jpg",
            title: "Ламинация Антрацитово-серый",
            name: "Антрацитово-серый",
            type: "laminat"
        },
        {
            img: "266e9cef9f280fc59ef7f1a7d5ff9651=c1h65.jpg",
            title: "Ламинация Зимний дуглас",
            name: "Зимний дуглас",
            type: "laminat"
        },
        {
            img: "30910380d2e4100233daa6f01897b6f7=c1h65.jpg",
            title: "Ламинация Махагон",
            name: "Махагон",
            type: "laminat"
        },
        {
            img: "fc1ca3e42c455fb36535d5784ec90c80=c1h65.jpg",
            title: "Ламинация Дуглас",
            name: "Дуглас",
            type: "laminat"
        },
        {
            img: "1aec05db5afe21886e584b72ed7a8c6a=c1h65.jpg",
            title: "Ламинация Металлик серый",
            name: "Металлик серый",
            type: "laminat"
        },
        {
            img: "969ebc4f058a28ed16582cf20b590d44=c1h65.jpg",
            title: "Ламинация Горная сосна",
            name: "Горная сосна",
            type: "laminat"
        },
        {
            img: "ae8be2abd0db44488e300ef98ffb64ac=c1h65.jpg",
            title: "Ламинация Темно-коричневый",
            name: "Темно-коричневый",
            type: "laminat"
        },
        {
            img: "df70951ddd7e24ac1117fa79d9863301=c1h65.jpg",
            title: "Ламинация Агатовый серый",
            name: "Агатовый серый",
            type: "laminat"
        }
    ],
    facade: [
        {
            title: "Без цвета",
            name: false,
            type: "white"
        },
        {
            title: "Краска RAL 1000",
            name: "RAL 1000",
            type: "color",
            bg: "#BEBD7F"
        },
        {
            title: "Краска RAL 1001",
            name: "RAL 1001",
            type: "color",
            bg: "#C2B078"
        },
        {
            title: "Краска RAL 1002",
            name: "RAL 1002",
            type: "color",
            bg: "#C6A664"
        },
        {
            title: "Краска RAL 1003",
            name: "RAL 1003",
            type: "color",
            bg: "#E5BE01"
        },
        {
            title: "Краска RAL 1004",
            name: "RAL 1004",
            type: "color",
            bg: "#CDA434"
        },
        {
            title: "Краска RAL 1005",
            name: "RAL 1005",
            type: "color",
            bg: "#A98307"
        },
        {
            title: "Краска RAL 1006",
            name: "RAL 1006",
            type: "color",
            bg: "#E4A010"
        },
        {
            title: "Краска RAL 1011",
            name: "RAL 1011",
            type: "color",
            bg: "#8A6642"
        },
        {
            title: "Краска RAL 1012",
            name: "RAL 1012",
            type: "color",
            bg: "#C7B446"
        },
        {
            title: "Краска RAL 1013",
            name: "RAL 1013",
            type: "color",
            bg: "#C7B446"
        },
        {
            title: "Краска RAL 1015",
            name: "RAL 1015",
            type: "color",
            bg: "#E6D690"
        },
        {
            title: "Краска RAL 1017",
            name: "RAL 1017",
            type: "color",
            bg: "#F5D033"
        },
        {
            title: "Краска RAL 1018",
            name: "RAL 1018",
            type: "color",
            bg: "#F8F32B"
        },
        {
            title: "Краска RAL 1019",
            name: "RAL 1019",
            type: "color",
            bg: "#9E9764"
        },
        {
            title: "Краска RAL 1020",
            name: "RAL 1020",
            type: "color",
            bg: "#999950"
        },
        {
            title: "Краска RAL 1023",
            name: "RAL 1023",
            type: "color",
            bg: "#FAD201"
        },
        {
            title: "Краска RAL 1024",
            name: "RAL 1024",
            type: "color",
            bg: "#AEA04B"
        },
        {
            title: "Краска RAL 1026",
            name: "RAL 1026",
            type: "color",
            bg: "#FFFF00"
        },
        {
            title: "Краска RAL 1027",
            name: "RAL 1027",
            type: "color",
            bg: "#9D9101"
        },
        {
            title: "Краска RAL 1032",
            name: "RAL 1032",
            type: "color",
            bg: "#D6AE01"
        },
        {
            title: "Краска RAL 1033",
            name: "RAL 1033",
            type: "color",
            bg: "#F3A505"
        },
        {
            title: "Краска RAL 1034",
            name: "RAL 1034",
            type: "color",
            bg: "#EFA94A"
        },
        {
            title: "Краска RAL 1037",
            name: "RAL 1037",
            type: "color",
            bg: "#F39F18"
        },
        {
            title: "Краска RAL 2001",
            name: "RAL 2001",
            type: "color",
            bg: "#C93C20"
        },
        {
            title: "Краска RAL 2002",
            name: "RAL 2002",
            type: "color",
            bg: "#CB2821"
        },
        {
            title: "Краска RAL 2003",
            name: "RAL 2003",
            type: "color",
            bg: "#FF7514"
        },
        {
            title: "Краска RAL 2004",
            name: "RAL 2004",
            type: "color",
            bg: "#F44611"
        },
        {
            title: "Краска RAL 2005",
            name: "RAL 2005",
            type: "color",
            bg: "#FF2301"
        },
        {
            title: "Краска RAL 2007",
            name: "RAL 2007",
            type: "color",
            bg: "#FFA420"
        },
        {
            title: "Краска RAL 2009",
            name: "RAL 2009",
            type: "color",
            bg: "#F54021"
        },
        {
            title: "Краска RAL 2010",
            name: "RAL 2010",
            type: "color",
            bg: "#D84B20"
        },
        {
            title: "Краска RAL 2011",
            name: "RAL 2011",
            type: "color",
            bg: "#EC7C26"
        },
        {
            title: "Краска RAL 2012",
            name: "RAL 2012",
            type: "color",
            bg: "#E55137"
        },
        {
            title: "Краска RAL 2013",
            name: "RAL 2013",
            type: "color",
            bg: "#C35831"
        },
        {
            title: "Краска RAL 3000",
            name: "RAL 3000",
            type: "color",
            bg: "#AF2B1E"
        },
        {
            title: "Краска RAL 3004",
            name: "RAL 3004",
            type: "color",
            bg: "#75151E"
        },
        {
            title: "Краска RAL 3005",
            name: "RAL 3005",
            type: "color",
            bg: "#5E2129"
        },
        {
            title: "Краска RAL 3007",
            name: "RAL 3007",
            type: "color",
            bg: "#412227"
        },
        {
            title: "Краска RAL 3011",
            name: "RAL 3011",
            type: "color",
            bg: "#781F19"
        },
        {
            title: "Краска RAL 3012",
            name: "RAL 3012",
            type: "color",
            bg: "#C1876B"
        },
        {
            title: "Краска RAL 3015",
            name: "RAL 3015",
            type: "color",
            bg: "#EA899A"
        },
        {
            title: "Краска RAL 3016",
            name: "RAL 3016",
            type: "color",
            bg: "#B32821"
        },
        {
            title: "Краска RAL 3017",
            name: "RAL 3017",
            type: "color",
            bg: "#E63244"
        },
        {
            title: "Краска RAL 3020",
            name: "RAL 3020",
            type: "color",
            bg: "#CC0605"
        },
        {
            title: "Краска RAL 3022",
            name: "RAL 3022",
            type: "color",
            bg: "#D95030"
        },
        {
            title: "Краска RAL 3024",
            name: "RAL 3024",
            type: "color",
            bg: "#F80000"
        },
        {
            title: "Краска RAL 3027",
            name: "RAL 3027",
            type: "color",
            bg: "#C51D34"
        },
        {
            title: "Краска RAL 3033",
            name: "RAL 3033",
            type: "color",
            bg: "#B44C43"
        },
        {
            title: "Краска RAL 4001",
            name: "RAL 4001",
            type: "color",
            bg: "#6D3F5B"
        },
        {
            title: "Краска RAL 4002",
            name: "RAL 4002",
            type: "color",
            bg: "#922B3E"
        },
        {
            title: "Краска RAL 4003",
            name: "RAL 4003",
            type: "color",
            bg: "#DE4C8A"
        },
        {
            title: "Краска RAL 4004",
            name: "RAL 4004",
            type: "color",
            bg: "#641C34"
        },
        {
            title: "Краска RAL 4005",
            name: "RAL 4005",
            type: "color",
            bg: "#6C4675"
        },
        {
            title: "Краска RAL 4006",
            name: "RAL 4006",
            type: "color",
            bg: "#A03472"
        },
        {
            title: "Краска RAL 4007",
            name: "RAL 4007",
            type: "color",
            bg: "#4A192C"
        },
        {
            title: "Краска RAL 4008",
            name: "RAL 4008",
            type: "color",
            bg: "#924E7D"
        },
        {
            title: "Краска RAL 4009",
            name: "RAL 4009",
            type: "color",
            bg: "#A18594"
        },
        {
            title: "Краска RAL 4010",
            name: "RAL 4010",
            type: "color",
            bg: "#CF3476"
        },
        {
            title: "Краска RAL 4011",
            name: "RAL 4011",
            type: "color",
            bg: "#8673A1"
        },
        {
            title: "Краска RAL 4012",
            name: "RAL 4012",
            type: "color",
            bg: "#6C6874"
        },
        {
            title: "Краска RAL 5000",
            name: "RAL 5000",
            type: "color",
            bg: "#354D73"
        },
        {
            title: "Краска RAL 5002",
            name: "RAL 5002",
            type: "color",
            bg: "#20214F"
        },
        {
            title: "Краска RAL 5004",
            name: "RAL 5004",
            type: "color",
            bg: "#18171C"
        },
        {
            title: "Краска RAL 5007",
            name: "RAL 5007",
            type: "color",
            bg: "#3E5F8A"
        },
        {
            title: "Краска RAL 5009",
            name: "RAL 5009",
            type: "color",
            bg: "#025669"
        },
        {
            title: "Краска RAL 5010",
            name: "RAL 5010",
            type: "color",
            bg: "#0E294B"
        },
        {
            title: "Краска RAL 5012",
            name: "RAL 5012",
            type: "color",
            bg: "#3B83BD"
        },
        {
            title: "Краска RAL 5014",
            name: "RAL 5014",
            type: "color",
            bg: "#606E8C"
        },
        {
            title: "Краска RAL 5015",
            name: "RAL 5015",
            type: "color",
            bg: "#2271B3"
        },
        {
            title: "Краска RAL 5017",
            name: "RAL 5017",
            type: "color",
            bg: "#063971"
        },
        {
            title: "Краска RAL 5018",
            name: "RAL 5018",
            type: "color",
            bg: "#3F888F"
        },
        {
            title: "Краска RAL 5019",
            name: "RAL 5019",
            type: "color",
            bg: "#1B5583"
        },
        {
            title: "Краска RAL 5020",
            name: "RAL 5020",
            type: "color",
            bg: "#1D334A"
        },
        {
            title: "Краска RAL 5021",
            name: "RAL 5021",
            type: "color",
            bg: "#256D7B"
        },
        {
            title: "Краска RAL 5023",
            name: "RAL 5023",
            type: "color",
            bg: "#49678D"
        },
        {
            title: "Краска RAL 5026",
            name: "RAL 5026",
            type: "color",
            bg: "#102C54"
        },
        {
            title: "Краска RAL 6000",
            name: "RAL 6000",
            type: "color",
            bg: "#316650"
        },
        {
            title: "Краска RAL 6001",
            name: "RAL 6001",
            type: "color",
            bg: "#287233"
        },
        {
            title: "Краска RAL 6002",
            name: "RAL 6002",
            type: "color",
            bg: "#2D572C"
        },
        {
            title: "Краска RAL 6003",
            name: "RAL 6003",
            type: "color",
            bg: "#424632"
        },
        {
            title: "Краска RAL 6004",
            name: "RAL 6004",
            type: "color",
            bg: "#1F3A3D"
        },
        {
            title: "Краска RAL 6005",
            name: "RAL 6005",
            type: "color",
            bg: "#2F4538"
        },
        {
            title: "Краска RAL 6006",
            name: "RAL 6006",
            type: "color",
            bg: "#3E3B32"
        },
        {
            title: "Краска RAL 6007",
            name: "RAL 6007",
            type: "color",
            bg: "#343B29"
        },
        {
            title: "Краска RAL 6008",
            name: "RAL 6008",
            type: "color",
            bg: "#39352A"
        },
        {
            title: "Краска RAL 6009",
            name: "RAL 6009",
            type: "color",
            bg: "#31372B"
        },
        {
            title: "Краска RAL 6010",
            name: "RAL 6010",
            type: "color",
            bg: "#35682D"
        },
        {
            title: "Краска RAL 6011",
            name: "RAL 6011",
            type: "color",
            bg: "#587246"
        },
        {
            title: "Краска RAL 6012",
            name: "RAL 6012",
            type: "color",
            bg: "#343E40"
        },
        {
            title: "Краска RAL 6013",
            name: "RAL 6013",
            type: "color",
            bg: "#6C7156"
        },
        {
            title: "Краска RAL 6014",
            name: "RAL 6014",
            type: "color",
            bg: "#47402E"
        },
        {
            title: "Краска RAL 6015",
            name: "RAL 6015",
            type: "color",
            bg: "#3B3C36"
        },
        {
            title: "Краска RAL 6016",
            name: "RAL 6016",
            type: "color",
            bg: "#1E5945"
        },
        {
            title: "Краска RAL 6017",
            name: "RAL 6017",
            type: "color",
            bg: "#4C9141 "
        },
        {
            title: "Краска RAL 6018",
            name: "RAL 6018",
            type: "color",
            bg: "#57A639"
        },
        {
            title: "Краска RAL 6019",
            name: "RAL 6019",
            type: "color",
            bg: "#BDECB6"
        },
        {
            title: "Краска RAL 6020",
            name: "RAL 6020",
            type: "color",
            bg: "#2E3A23"
        },
        {
            title: "Краска RAL 6021",
            name: "RAL 6021",
            type: "color",
            bg: "#89AC76"
        },
        {
            title: "Краска RAL 6022",
            name: "RAL 6022",
            type: "color",
            bg: "#25221B"
        },
        {
            title: "Краска RAL 6024",
            name: "RAL 6024",
            type: "color",
            bg: "#308446"
        },
        {
            title: "Краска RAL 6025",
            name: "RAL 6025",
            type: "color",
            bg: "#3D642D"
        },
        {
            title: "Краска RAL 6026",
            name: "RAL 6026",
            type: "color",
            bg: "#015D52"
        },
        {
            title: "Краска RAL 6027",
            name: "RAL 6027",
            type: "color",
            bg: "#84C3BE"
        },
        {
            title: "Краска RAL 6028",
            name: "RAL 6028",
            type: "color",
            bg: "#2C5545"
        },
        {
            title: "Краска RAL 6029",
            name: "RAL 6029",
            type: "color",
            bg: "#20603D"
        },
        {
            title: "Краска RAL 6032",
            name: "RAL 6032",
            type: "color",
            bg: "#317F43"
        },
        {
            title: "Краска RAL 6033",
            name: "RAL 6033",
            type: "color",
            bg: "#497E76"
        },
        {
            title: "Краска RAL 6034",
            name: "RAL 6034",
            type: "color",
            bg: "#7FB5B5"
        },
        {
            title: "Краска RAL 6035",
            name: "RAL 6035",
            type: "color",
            bg: "#1C542D"
        },
        {
            title: "Краска RAL 6036",
            name: "RAL 6036",
            type: "color",
            bg: "#193737"
        },
        {
            title: "Краска RAL 6037",
            name: "RAL 6037",
            type: "color",
            bg: "#008F39"
        },
        {
            title: "Краска RAL 6038",
            name: "RAL 6038",
            type: "color",
            bg: "#00BB2D"
        },
        {
            title: "Краска RAL 7000",
            name: "RAL 7000",
            type: "color",
            bg: "#78858B"
        },
        {
            title: "Краска RAL 7001",
            name: "RAL 7001",
            type: "color",
            bg: "#8A9597"
        },
        {
            title: "Краска RAL 7002",
            name: "RAL 7002",
            type: "color",
            bg: "#7E7B52"
        },
        {
            title: "Краска RAL 7003",
            name: "RAL 7003",
            type: "color",
            bg: "#6C7059"
        },
        {
            title: "Краска RAL 7004",
            name: "RAL 7004",
            type: "color",
            bg: "#969992"
        },
        {
            title: "Краска RAL 7005",
            name: "RAL 7005",
            type: "color",
            bg: "#646B63"
        },
        {
            title: "Краска RAL 7006",
            name: "RAL 7006",
            type: "color",
            bg: "#6D6552"
        },
        {
            img: "a3169019fd41a3a64494a35a2df5c2ae=c1h65.jpg",
            title: "Ламинация Черная вишня",
            name: "Черная вишня",
            type: "laminat"
        },
        {
            img: "7ef24d885effc73fb5ef73f609e74b40=c1h65.jpg",
            title: "Ламинация Орех",
            name: "Орех",
            type: "laminat"
        },
        {
            img: "79e2db53807ff52987fb248a1488b084=c1h65.jpg",
            title: "Ламинация Светло-серый",
            name: "Светло-серый",
            type: "laminat"
        },
        {
            img: "f800c8d577ee443eba8952122d8f3ae0=c1h65.jpg",
            title: "Ламинация Орегон",
            name: "Орегон",
            type: "laminat"
        },
        {
            img: "886c4f1cde314f0202d7ec8b769518fd=c1h65.jpg",
            title: "Ламинация Темно-коричневый",
            name: "Темно-коричневый",
            type: "laminat"
        },
        {
            img: "1b6596586f1db20cf3e8e940ed2b285f=c1h65.jpg",
            title: "Ламинация Винно-красный",
            name: "Винно-красный",
            type: "laminat"
        },
        {
            img: "87a3382063751ef61383f990ce643340=c1h65.jpg",
            title: "Ламинация Бриллиантово-синий",
            name: "Бриллиантово-синий",
            type: "laminat"
        },
        {
            img: "1e1fa285d9e2bb38e457560756347565=c1h65.jpg",
            title: "Ламинация Мореный дуб",
            name: "Мореный дуб",
            type: "laminat"
        },
        {
            img: "abff56f90cc22d15d56215e183030bfb=c1h65.jpg",
            title: "Ламинация Коричневый каштан",
            name: "Коричневый каштан",
            type: "laminat"
        },
        {
            img: "ccd084f72f7dc399461175e629f8caa4=c1h65.jpg",
            title: "Ламинация Темно-зеленый",
            name: "Темно-зеленый",
            type: "laminat"
        },
        {
            img: "8d197ca2a4d6501ec87bfeaafa740be0=c1h65.jpg",
            title: "Ламинация Дуб",
            name: "Дуб",
            type: "laminat"
        },
        {
            img: "ab77ab2abbe941d113c74eee836e9334=c1h65.jpg",
            title: "Ламинация Кремовый",
            name: "Кремовый",
            type: "laminat"
        },
        {
            img: "ea7689a2c7d18747fc459acde63bb5da=c1h65.jpg",
            title: "Ламинация Шоколадно-коричневый",
            name: "Шоколадно-коричневый",
            type: "laminat"
        },
        {
            img: "5234ce659f5e423cda0560401dfd88a9=c1h65.jpg",
            title: "Ламинация Светлый дуб",
            name: "Светлый дуб",
            type: "laminat"
        },
        {
            img: "43d532cee433480b74474c30670db198=c1h65.jpg",
            title: "Ламинация Красный",
            name: "Красный",
            type: "laminat"
        },
        {
            img: "b356eefd6f0c163653adae5c73f7c70e=c1h65.jpg",
            title: "Ламинация Золотой дуб",
            name: "Золотой дуб",
            type: "laminat"
        },
        {
            img: "282877c9ac00259e540a5066b497e695=c1h65.jpg",
            title: "Ламинация Темно-красный",
            name: "Темно-красный",
            type: "laminat"
        },
        {
            img: "89ba5ae8477be4109ff588397759bfac=c1h65.jpg",
            title: "Ламинация Золотой орех",
            name: "Золотой орех",
            type: "laminat"
        },
        {
            img: "8ed41c3c39f362b884fc41604c15c0c4=c1h65.jpg",
            title: "Ламинация Зеленый",
            name: "Зеленый",
            type: "laminat"
        },
        {
            img: "8c8e2474565f2df644a04198a2bf8a0e=c1h65.jpg",
            title: "Ламинация Мореный дуб",
            name: "Мореный дуб",
            type: "laminat"
        },
        {
            img: "e327532abb72c76bf18ac74c58464f42=c1h65.jpg",
            title: "Ламинация Мореный дуб2",
            name: "Мореный дуб 2",
            type: "laminat"
        },
        {
            img: "01b3f077243a05c6af666db96c56d7d1=c1h65.jpg",
            title: "Ламинация Антрацитово-серый",
            name: "Антрацитово-серый",
            type: "laminat"
        },
        {
            img: "266e9cef9f280fc59ef7f1a7d5ff9651=c1h65.jpg",
            title: "Ламинация Зимний дуглас",
            name: "Зимний дуглас",
            type: "laminat"
        },
        {
            img: "30910380d2e4100233daa6f01897b6f7=c1h65.jpg",
            title: "Ламинация Махагон",
            name: "Махагон",
            type: "laminat"
        },
        {
            img: "fc1ca3e42c455fb36535d5784ec90c80=c1h65.jpg",
            title: "Ламинация Дуглас",
            name: "Дуглас",
            type: "laminat"
        },
        {
            img: "1aec05db5afe21886e584b72ed7a8c6a=c1h65.jpg",
            title: "Ламинация Металлик серый",
            name: "Металлик серый",
            type: "laminat"
        },
        {
            img: "969ebc4f058a28ed16582cf20b590d44=c1h65.jpg",
            title: "Ламинация Горная сосна",
            name: "Горная сосна",
            type: "laminat"
        },
        {
            img: "ae8be2abd0db44488e300ef98ffb64ac=c1h65.jpg",
            title: "Ламинация Темно-коричневый",
            name: "Темно-коричневый",
            type: "laminat"
        },
        {
            img: "df70951ddd7e24ac1117fa79d9863301=c1h65.jpg",
            title: "Ламинация Агатовый серый",
            name: "Агатовый серый",
            type: "laminat"
        }
    ],
    handle: [
        {
            img: "2034115482ba633e63cf633faa8329e5",
            title: "«Рапсодия», цвет — белый",
            name: "«Рапсодия», цвет — белый",
            type: null
        },
        {
            img: "c201c0e71b82a78550ead9b1ed0c07d8",
            title: "«Гармония», цвет — белый",
            name: "«Гармония», цвет — белый",
            type: null
        },
        {
            img: "8677f9b801297d041ae44bc80585bdf0",
            title: "«Рапсодия», цвет — шампань",
            name: "«Рапсодия», цвет — шампань",
            type: null
        },
        {
            img: "49f9bb533f80a5ca2b9ea64e490bf56b",
            title: "«Гармония», цвет — шампань",
            name: "«Гармония», цвет — шампань",
            type: null
        },
        {
            img: "ed91a106f8c5ba959d45a9c7ad2d3c80",
            title: "«Рапсодия», цвет — серебро",
            name: "«Рапсодия», цвет — серебро",
            type: null
        },
        {
            img: "aabc32cccff694e8670ad9b0e79cc184",
            title: "«Гармония», цвет — серебро",
            name: "«Гармония», цвет — серебро",
            type: null
        },
        {
            img: "8a2ef0d67898a5f935c80287943076f9",
            title: "«Рапсодия», цвет — титан",
            name: "«Рапсодия», цвет — титан",
            type: null
        },
        {
            img: "cd8c1807593ecc6c3c304864a5abec13",
            title: "«Гармония», цвет — титан",
            name: "«Гармония», цвет — титан",
            type: null
        },
        {
            img: "3a194fa9f3b46949386868b561d13c5f",
            title: "«Рапсодия», цвет — бронза",
            name: "«Рапсодия», цвет — бронза",
            type: null
        },
        {
            img: "a01a273910d3035d9346d4d15f720a71",
            title: "«Гармония», цвет — бронза",
            name: "«Гармония», цвет — бронза",
            type: null
        },
        {
            img: "5a0b2b900f88c05379867ded37b3886a",
            title: "«Гармония», цвет — коричневый",
            name: "«Гармония», цвет — коричневый",
            type: null
        }
    ]
};

_MASTER_DATA["windowsill"] =
{
    vitrag: {
        title: "Витраж",
        material: "Пластик",
        description: ["Практичный вариант с приятной на ощупь фактурой и защитой от бытовых воздействий.","Качественные подоконники с хорошими эксплуатационными свойствами по приемлемой цене"],
        img: "j/fbc7c2b64327e61d931b6748498f66d0/vitrag.png",
        items: [
            {
                title: "Цвет подоконника Витраж: Белый",
                name: "Белый",
                img: "326852281e5b4c293fca7cbe56981975=c1h65.jpg"
            }
        ]
    },

    cristalite: {
        title: "Crystalit",
        material: "Пластик",
        description: ["Основными преимуществами подоконников серии Crystalit являются высокая прочность за счет более толстых стенок, стойкость к воздействию бытовой химии и к истиранию, повышенная термостойкость.","Отличается большей ударопрочностью и долговечностью. Это вариант для тех, кто не любит компромиссы."],
        img: "j/861da569c44c5673570a66595da0d532/Cristalit.png",
        items: [
            {
                title: "Цвет подоконника Кристалит: Орех",
                name: "Орех",
                img: "4471428cb02b9d86436ace0eed5cc47e=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Кристалит: Махагон",
                name: "Махагон",
                img: "8a87c1017e12cb28608469fcc709a4ba=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Кристалит: Орех",
                name: "Орех",
                img: "f5f0291194b09aed673a37cc2ad3e29b=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Кристалит: Мрамор",
                name: "Мрамор",
                img: "12c35e924a9d00f8e87bccb61ab2b0a7=c1h65.jpg"
            }
        ]
    },
    fsf: {
        title: "Ламиформ",
        material: "Влагостойкая фанера",
        description: ["Обладает самыми высокими показателями по износостойкости, прочности и влагоустойчивости.","Отличительной особенностью является большой выбор цветов"],
        img: "j/cb6b87ea8c1e41e441227a51e7c72b67/Lamiform.png",
        items: [
            {
                title: "Цвет подоконника Ламиформ: Цвет № 294",
                name: "Цвет № 294",
                img: "80ba470e6a4a5d596182717b0ce7be06=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 435-01",
                name: "Цвет № 435-01",
                img: "ac1b3c62ef39ab8c748b20b08c46dc9c=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 435",
                name: "Цвет № 435",
                img: "d1cb54a614b3e9fdc87211a839c51fbc=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 982",
                name: "Цвет № 982",
                img: "05c73cdf28f893183f97cefcf6fe13be=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1365",
                name: "Цвет № 1365",
                img: "14cb8755d06ff603d19358d9e2473b39=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1570",
                name: "Цвет № 1570",
                img: "2618f778eccaf4ab8fa6602f0818cfc6=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1595",
                name: "Цвет № 1595",
                img: "03e9512af1b2e6dfb17908ce879a0d3a=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1661",
                name: "Цвет № 1661",
                img: "3493bb3d335a1a7c1d26e3604e2a1e6e=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1741",
                name: "Цвет № 1741",
                img: "480e31c542d489c02eb47f8b1a2cb92e=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1741-01",
                name: "Цвет № 1741-01",
                img: "0d3e63ae107642c09bdfe2befdc467fe=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1753",
                name: "Цвет № 1753",
                img: "61ad6d05f199a28efa0475d53f021fce=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 1755",
                name: "Цвет № 1755",
                img: "fdd689b8579e5c7363ee259690e1462a=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 3242",
                name: "Цвет № 3242",
                img: "d7c7ef65dec3805b90537387d2c3a471=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 3252",
                name: "Цвет № 3252",
                img: "1f80b74f0d49b2a6728891e39e619e3a=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4142",
                name: "Цвет № 4142",
                img: "09f3e9d670380921a87a1da87867d5e8=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4143",
                name: "Цвет № 4143",
                img: "d8f583580da80b2bdbbb808b3ae5e588=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4207-01",
                name: "Цвет № 4207-01",
                img: "267563207149c8ab46e12e996b85ac3e=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4207",
                name: "Цвет № 4207",
                img: "3e5d6a678216c980e7e861dba31aa7af=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4207-02",
                name: "Цвет № 4207-02",
                img: "30435fcb3fc558b12a1c5048c6f7afe1=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4388",
                name: "Цвет № 4388",
                img: "91a137c003dcf3fd706d6d23fc1b8213=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4551",
                name: "Цвет № 4551",
                img: "719fc4c6a19407df4e0210f84ace16ed=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4552",
                name: "Цвет № 4552",
                img: "d8dd56d6c6d2e6fbf5ebcc385ef354a6=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4621",
                name: "Цвет № 4621",
                img: "614cc8e7bd4669161d354727c6e6b22a=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4622",
                name: "Цвет № 4622",
                img: "eb932f00bf7153a30bd7c7207eb6e83a=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4623",
                name: "Цвет № 4623",
                img: "2f5a033f43cdaec5da16dc4c17304289=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4829",
                name: "Цвет № 4829",
                img: "360833260d21ef1673f930906d88f544=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4838",
                name: "Цвет № 4838",
                img: "3c8245701d6f253f9f972e05235069af=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 4899",
                name: "Цвет № 4899",
                img: "236e2479dc1d778733363476c469cc02=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7039",
                name: "Цвет № 7039",
                img: "b171c7712be278e40f1f02fc27e0ebc3=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7040",
                name: "Цвет № 7040",
                img: "ae08508ec5d2fdb501895e991f9dafaf=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7054",
                name: "Цвет № 7054",
                img: "467fb0231486727b48c9e86a9536db68=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7054-01",
                name: "Цвет № 7054-01",
                img: "97d5005342b80e16f2affa8c6aa0f7a4=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7061",
                name: "Цвет № 7061",
                img: "b00e244a48b6c05e209d23501b11bbe5=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7110",
                name: "Цвет № 7110",
                img: "28b5d49b4c5f1947e0cd9be4097823dc=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7816",
                name: "Цвет № 7816",
                img: "3d78f9d620bb16fe4aaded826a793608=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7911",
                name: "Цвет № 7911",
                img: "10be0416125526bf895c53509f094bbc=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7922",
                name: "Цвет № 7922",
                img: "085c8f21d69e0bf53dedb51fc2250895=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7923",
                name: "Цвет № 7923",
                img: "9575fed78ca3915174d8b232bc835c44=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7924",
                name: "Цвет № 7924",
                img: "4183b5807f83a3c9cdb903361d353148=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7952",
                name: "Цвет № 7952",
                img: "c73c1fef41bffc25763f85dfb4ec83de=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № 7952-02",
                name: "Цвет № 7952-02",
                img: "1ca84d83970b575678ab84aa828313bb=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № WB1",
                name: "Цвет № WB1",
                img: "3b084a231d0c8bebed87478eff49cd16=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № Д14",
                name: "Цвет № Д14",
                img: "1253e2f47992d9184032b28bf136e29c=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № Д379",
                name: "Цвет № Д379",
                img: "86c16a689df793cec3bf06784685a882=c1h65.jpg"
            },
            {
                title: "Цвет подоконника Ламиформ: Цвет № Д381",
                name: "Цвет № Д381",
                img: "9ccd150d19095b6d52eb225d560a2c7d=c1h65.jpg"
            }
        ]
    }
};

_MASTER_DATA["furniture"] = [
    {
        name: "Базовая",
        title: "MACO Multi Trend",
        description: "Классический вариант фурнитуры",
        img: "j/1ce78a91613c2d0605fa07e8ceb9831d/base.png"
    },
    {
        name: "Защитная",
        title: "MACO Multi Trend",
        description: "Повышенная защита от взлома",
        img: "j/f955b4e20625131faa19b41557cbe48f/protect.png"
    },
    {
        name: "Скрытая",
        title: "MACO Invisible",
            description: "Не видно ничего, кроме ручки",
        img: "j/3abf84226f71895169680878f99dfd59/hidden.png"
    }

];


_MASTER_DATA["profiles"] =
[
    {
        name: "Серия 3",
        title: "Rehau Euro Design",
        description: "Ничего лишнего: сохранение тепла, защита от шума, низкая стоимость.",
        glass: [0,1,3],
        params: {
            hot: {
                title: "Тепло",
                description: "Ничего лишнего, отлично выполняет свою функцию, сохраняет тепло и изолирует от шума",
                value: 36
            },
            silence: {
                title: "Тишина",
                description: "до 25 дБ Идеально для городских условий",
                value: 36
            },
            light: {
                title: "Свет",
                description: "Площадь светового проема на 288 см2 больше, чем у обычного пластикового окна",
                value: 36
            }
        }
    },
    {
        name: "Серия 4",
        title: "Rehau Delight Design",
        description: "Уникальный дизайн и изящные линии. Сохранение тепла и защита от шума.",
        glass: [1,3],
        params: {
            hot: {
                title: "Тепло",
                description: "Ничего лишнего, отлично выполняет свою функцию, сохраняет тепло и изолирует от шума",
                value: 70
            },
            silence: {
                title: "Тишина",
                description: "до 30 дБ Идеально для городских условий",
                value: 70
            },
            light: {
                title: "Свет",
                description: "Окно с дополнительной площадью светового проема — на 992 см2 больше обычного — идеально для комнат на теневой стороне",
                value: 70
            }
        }
    },
    {
        name: "Серия 5",
        title: "Rehau Delight Design New",
        description: "Максимальное шумоподавление и теплосбережение (подходит для экстремальных условий эксплуатации).",
        glass: [1,2,3],
        params: {
            hot: {
                title: "Тепло",
                description: "Серия подойдет даже для условий Крайнего Севера",
                value: 55
            },
            silence: {
                title: "Тишина",
                description: "до 50 дБ Самые высокие показатели шумоизоляции",
                value: 55
            },
            light: {
                title: "Свет",
                description: "Световой проем этого окна больше обычного на 697 см2, что обеспечивает отличную освещенность даже в пасмурный день",
                value: 55
            }
        }
    },
    {
        name: "Серия 6",
        title: "Rehau Brillant Design",
        description: "Воплощение всех достоинств. Максимальное светопропускание. Мягкие линии и элегантный дизайн.",
        glass: [1,2,3],
        params: {
            hot: {
                title: "Тепло",
                description: "Шестикамерное строение рам и створки шириной 70 мм. Идеально для загородных домов",
                value: 76
            },
            silence: {
                title: "Тишина",
                description: "до 40 дБ Тишина даже в комнатах с окнами, выходящими на оживленную улицу",
                value: 76
            },
            light: {
                title: "Свет",
                description: "Окно с увеличенной на 1364 см2 площадью светового проема пропускает в помещение абсолютный максимум света",
                value: 76
            }
        }
    }

];


_MASTER_DATA["glass"] =
[
    {
        title: "Однокамерный 32 мм",
        description: "Самый простой стеклопакет, подходит для помещений без повышенных требований к тепло- и звукоизоляции",
        img: "g/1k32.png",
        option: false
    },
    {
        title: "Двухкамерный 32 мм",
        description: "Оптимальное решение для остекления жилых помещений",
        img: "g/2k32.png",
        option: false
    },
    {
        title: "Двухкамерный 40 мм",
        description: "Максимальная шумоизоляция",
        img: "g/2k40.png",
        option: false
    },
    {
        title: "Теплопакет 32 мм",
        description: "Максимальное сохранение тепла",
        img: "g/hot32.png",
        option: true
    },
];

_MASTER_DATA["i_glass"] =
{
    default: {
        title: "i-стекло уже добавлено",
        description: "Это современное теплосберегающее стекло позволяет снизить затраты на отопление и кондиционирование квартиры."
    },
    option: {
        title: "Многофункциональное стекло",
        description: ["Теплопакет является самым современным и эффективным решением для сохранения тепла","Теплопакет — это стеклопакет со специальными свойствами: морозостойкий, энергосберегающий (с серебряным низкоэмиссионным покрытием), с дистанционной рамкой с терморазрывом (TGI). Возможно заполнение аргоном."]
    }
};


_MASTER_DATA["escarpment"] =
[
    {
        title: "Монтаж без откосов",
        description: "Этот вариант целесообразен в том случае, если окна устанавливаются до начала ремонта в помещении. Установленные пластиковые окна не должны находиться без откосов дольше одного месяца — это вредно для внутренних материалов, которые обязательно должны быть защищены откосами.",
        img: "o/without.png"
    },
    {
        title: "Монтаж с откосами из пластиковых сэндвич-панелей",
        description: "Сэндвич-панели представляют собой трехслойную структуру, внутри которой находится вспененный пенополистирол, а с внешних сторон — пластиковые пластины. Панели обладают малым весом и отлично сохраняют тепло. Внешние пластины выдерживают различные механические воздействия. Откосы из сэндвич-панелей служат долго, сохраняя неизменный вид.",
        img: "o/plastic.png"
    },
    {
        title: "Монтаж с откосами ГВЛ для последующей шпатлевки и покраски",
        description: "Экологически чистые и прочные гипсоволокнистые листы (ГВЛ), используемые в качестве материала для откосов, отлично поглощают шум и не подвержены возгоранию.",
        img: "o/gvl.png"
    },
    {
        title: "Без монтажа",
        description: "Если вы заинтересованы в закупке окон и самостоятельной установке. Не рекомендуется, людям без опыта установки, т.к. все преимущества окон не будут реализованы при некачественном монтаже.",
        img: "o/none.png"
    }

];

_MASTER_WINDOW = {
    stages: {
        sizes: {
            name: "Замеры",
            title: "Тип и замеры",
            description: false,
            no: 1,
            mnemo: "sizes"
        },
        casements: {
            name: "Створки",
            title: "Выбор количества створок",
            description: "Чем больше створок, тем тише будет в вашей комнате. То же касается и фрамуги: выполняя проветривательную функцию, она также повышает шумоизоляцию квартиры",
            no: 2,
            mnemo: "casements"
        },
        pane: {
            name: "Профиль и стеклопакет",
            title: "Выбор профиля и стеклопакета",
            description: "Все профильные системы отвечают требованиям сохранения тепла и защиты от шума. Они отличаются дизайном и широтой возможностей.",
            no: 3,
            mnemo: "pane"
        },
        furniture: {
            name: "Фурнитура",
            title: "Выбор фурнитуры",
            description: "От качества фурнитуры зависят герметичность окна, устойчивость к взломам, удобство использования, а также срок его службы. Выбор фурнитуры обуславливается индивидуальными требованиями.",
            no: 4,
            mnemo: "furniture"
        },
        sill: {
            name: "Подоконник",
            title: false,
            description: "Большой подоконник — дополнительная площадь для любимых вещей и питомцев. Стоимость подоконника зависит от глубины подоконника и материала, из которого он будет изготовлен.",
            no: 5,
            mnemo: "sill"
        },
        decoration: {
            name: "Декорирование",
            title: false,
            description: false,
            no: 6,
            mnemo: "decoration"
        },
        assembling: {
            name: "Монтаж и отделка",
            title: false,
            description: "Вы можете рассчитать стоимости окна с монтажом без откосов или же с полной отделкой «под ключ». Учтите, что от качества монтажа откосов в значительной степени зависит климат в квартире и внешний вид окна.",
            no: 7,
            mnemo: "assembling",
            option: {
                title: "Заделка монтажного шва c внешней стороны окна"
            }
        }
    }
};