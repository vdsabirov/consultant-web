<?php




define("_FILE_JSON", "base.json");
$data = file_get_contents('php://input');

$is_save = strlen($data)>100;

if($is_save) {
    file_put_contents(_FILE_JSON, $data);
    $result = array("status"=>"ok");
} else {
    if(is_readable(_FILE_JSON)) $result = array("status"=>"ok","data"=>json_decode(file_get_contents(_FILE_JSON)));
    else $result = array("status"=>"error");
}

if($result==false) $result = array();
header("Content-Type: application/json");
echo json_encode($result);