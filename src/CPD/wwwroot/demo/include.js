
var DemoData = (function () {
    var data = {};
    var actions =   {};
    var isTable = function(table) {
        return angular.isDefined(data[table]);
    };

    var isAction = function(action) {
        if( angular.isDefined(actions[action]) ) return true;
        for(var id in data.Actions) if(data.Actions[id].Name==action) return true;
        return false;
    };

    var parseParam = function(param) {
        param = decodeURIComponent(param);
        //console.log("param = " + param);
        param = param.replace("/rest/", "");
        var url_param = param.split("?")[0].split("/");
        var get_param = param.split("?").length>1 ? param.split("?")[1].split("&") : {};
        var request = {};
        //console.log("get_param");
        //console.log(get_param);

        if( !angular.isDefined(url_param[0])) return false;
        var table_or_action = url_param[0];
        var ident = angular.isDefined(url_param[1]) ? url_param[1] : false;

        var table_or_action_2 = url_param[2];
        var ident_2 = angular.isDefined(url_param[3]) ? url_param[3] : false;

        request.table = isTable(table_or_action) ? table_or_action : false;
        request.action = isAction(table_or_action) ? table_or_action : false;
        request.id = ident;
        request.table2 = isTable(table_or_action_2) ? table_or_action_2 : false;
        request.action2 = isAction(table_or_action_2) ? table_or_action_2 : false;
        request.id2 = ident_2;

        var condition = {};
        for(var i=0;i<get_param.length;i++) {
            var variable = get_param[i].split("=")[0];
            var value = get_param[i].split("=")[1];
            if((value.substr(0,1)=="{") || (value.substr(0,1)=="[")) value =  angular.fromJson(value);
            //console.log("["+variable+"] = "+get_param[i]);
            //console.log(value);
            if(angular.isUndefined(request[variable])) {
                request[variable] = value;
            } else {
                if(angular.isArray(request[variable])) request[variable].push(value);
                else {
                    request[variable] = [ request[variable], value ];
                }
            }

        }

        //console.log(request);

        return request;
    };


    /* Базовые функции по работе с данными */
    var keyField = function (table) {
        return data[table].KeyField;
    };

    var nextId = function(table) {
        var maxId = 0;
        if(angular.isDefined(data[table])) for(var i=0;i<data[table].length;i++) if(data[table][i]["Id"]>maxId) maxId = data[table][i]["Id"];
        return maxId+1;
    };

    var fieldType = function(table, field) {
        for(var i=0;i<data.Fields.length;i++) if(data.Fields[i].TableId == table && data.Fields[i].Id==field) return data.Fields[i].TypeId;
    };

    var getTableId = function (table) {
        for(var i=0;data.Tables.length; i++) if(data.Tables[i].Name==table) return data.Tables[i].Id;
        return 0;
    };

    var tableFields = function(table) {
        var fields = [];
        for(var i=0;i<data.Fields.length;i++) if(data.Fields[i].TableId == getTableId(table)) fields.push(data.Fields[i]);
        return fields;
    };

    // Данная функция должна иметь очень богатый функционал, но для демо достаточно однйо функции - $GenId
    var getDefaultsValue = function(table) {
        if(table=="TenderProposals") return {"Id":0,"TenderId":0,"Price":0.0,"ProposalTime":"0001-01-01T00:00:00","Accepted":"false","Tender":null,"UserId":null};
        //return {Id:1,	TableId:5,	Name:'Id',	        Title:'Ид',	            TypeId:1,	    Default:'$GenId',	            LinkedTableId:0};
        var result = {};
        var table_id = getTableId(table);
        for(var i=0;i<data.Fields.length;i++) if(data.Fields[i].TableId == table_id )  {
            var field_name = data.Fields[i].Name;
            var def = data.Fields[i].Default;
            if(def=="false") def = false;
            if(def=="true") def = true;
            result[field_name] = def;
            if(result[field_name]       == "$GenId")    result[field_name] = nextId(table);
            else if(result[field_name]  == "$GenCode")  result[field_name] = $GenCode();
            else if(result[field_name]  == "$Now")      result[field_name] = $Time();
            else if(result[field_name]  == "$Time")      result[field_name] = $Time();
        }
        console.log("DEF:");
        console.log(result);
        return result;
    };


    /* Функции для работы с базой данных (в памяти) */
    var getRecord = function(table, id) {
        if(id=="New") return getDefaultsValue(table);
        for(var i=0;i<data[table].length;i++) if(data[table][i]["Id"]==id) return data[table][i];
        return false;
    };


    var checkCondition = function(record, condition) {
        if( angular.isArray( condition ) ) {
            for(var i=0;i<condition.length;i++) if(checkCondition(record, condition[i])) return true;
            return false;
        }
        for(var field in condition) if( angular.isUndefined(record[field])  ||  record[field] !=condition[field]) return false;
        return true;
    };

    /// Выборка по условию включение подстроки, если строка/текст и  равно, если остальное
    var selectRecords = function (table, conditions) {
        //console.log("selectRecords:"+table);
        //console.log(conditions);
        var result = [];
        for(var i=0;i<data[table].length;i++) {
            if(checkCondition(data[table][i], conditions)) result.push(data[table][i]);
        }
        return result;
    };


    var triggersCreate = function(table, record) {
        if(table=="Products") {
            var subtable = {
                1: "ProductWindows",
                2: "ProductDoors"
            };
            if(angular.isDefined(subtable[record.ProductCategoryId])) {
                var sub_table = subtable[record.ProductCategoryId];
                var sub_record = getDefaultsValue(sub_table);
                sub_record.ProductId = record.Id
                var new_sub_id = createNew(sub_table, sub_record);
                record.ObjectId = new_sub_id;
                saveRecord(table, record.Id, record);
            }
        }
    };


    /// Доюавление
    var createNew = function (table, values) {
        console.log("createNew:"+table);
        console.log(values);
        var fields = tableFields(table);
        //console.log("F:");
        //console.log(fields);
        var defaults = getDefaultsValue(table);
        //console.log("defaults:");
        //console.log(defaults);
        for(var i=0;i<fields.length;i++) {
            var field = fields[i].Name;
            if(field=="Id" || !angular.isDefined(values[field])) values[field] = defaults[field];
        }
        if(angular.isUndefined(data[table])) data[table] = [];
        if(table==SETTINGS.Table.Tables)  DemoData.add(values.Name, []);
        data[table].push(values);
        triggersCreate(table, values);
        return values.Id;
    };

    /// Удаление
    var deleteRecord = function (table, id) {
        id = parseInt(id);
        for(var i=0;i<data[table].length;i++) {
            if (data[table][i].Id == id) {
                data[table].splice(i,1);
                return true;
            }

        }
        return false;
    };

    /// Сохранение
    var saveRecord = function (table, id, values) {
        //console.log("saveRecord");
        //console.log(values);
        for(var i=0;i<data[table].length;i++) {
            if( data[table][i].Id==id) {
                for(var k in values) data[table][i][k] = values[k];
                //console.log(data[table][i]);
                return true;
            }
        }
        return false;
    };

    var getUser = function(request) {
        var user = false;
        //if(angular.isDefined(request.token) && (request.token.length>10)) {
        if(TOKEN>10) {

            var users = selectRecords(SETTINGS.Table.Users, {Token: request.token});
            if(users.length ==1) user = users[0];
        }
        return user;
    };

    var tryLogin = function() {
        return getAutorization({Id:3});
    };

    var getAutorization = function(condition) {
        var result = {status: "ok", message:[]};
        var user = selectRecords(SETTINGS.Table.Users, condition);
        if(user.length==1) {
            var token = $GenToken();
            user = user[0];
            var access = user.Access;
            user.Token = token;
            saveRecord(SETTINGS.Table.Users, user.Id, user);
            //delete  user.Password;
            //delete  user.Token;

            result.user = {
                Name: user.Name,
                Email:user.Email,
                Id:user.Id,
                ConsultantId: user.ConsultantId
            };
            result.token = token;
            result.access = access;
        } else {
            result.status = "error";
            result.message.push("Ошибка авторизации");
        }
        return result;
    };

    var actionRecord = function(request) {
        console.log("actionRecord:");
        console.log(request);
        var record_action =  angular.isDefined( actions[  request.action ] ) ? actions[  request.action ] : actions[  request.action2 ];
        if( request.table == "Orders" ) {
            var record = getRecord("Orders", request.id);
            switch (request.action2) {
                case "orderDelete":         deleteRecord("Orders", record.Id); return true; break;
                case "orderFormed":         record.OrderStateId = 2; break;
                case "orderToTender":       record.OrderStateId = 4; break;
                case "orderCancel":         record.OrderStateId = 1; break;
                case "orderToTender":
                    var Tenders = selectRecords("Tenders", {OrderId: request.id});
                    /// добавляем новый аукцион
                    if(Tenders.length==0) {
                        var Tender_id = createNew("Tenders", {
                            OrderId: request.id,
                            IsOpen: true,
                            OpeningTime: $Time(),
                            ClosingTime: $NowPlusDays(7)
                        });
                    } else {
                        for(var i=0;i<Tenders.length;i++) {
                            Tenders[i].IsOpen = true;
                            Tenders[i].OpeningTime = $Time();
                            Tenders[i].ClosingTime = $NowPlusDays(7);
                            saveRecord("Tenders", Tenders[i]);
                        }
                    }
                    // Устанавливаем статус "На аукционе"
                    record.OrderStateId = 5;
                    break;
                case "orderTenderCancel":
                    // Находим аукционы и устанавливаем неактивным
                    var Tenders = selectRecords("Tenders", {OrderId: request.id});
                    for(var i=0;i<Tenders.length;i++) {
                        Tenders[i].IsOpen = false;
                        saveRecord("Tenders", Tenders[i]);
                    }
                    record.OrderStateId = 2;
                    break;
                case "orderTenderCancel":   record.OrderStateId = 2; break;
                case "orderReady":
                    record.OrderStateId = 7; break;
                    break;
                case "clientOrdersSetWinner":
                    break;
            }
            saveRecord("Orders", request.id, record);
            return getRecord("Orders", request.id);
        } else if( request.table=="TenderProposals" ) {
            switch (request.action2) {
                case "proposalSetWinner":
                    var record = getRecord("TenderProposals", request.id);
                    record.Accepted  = true;
                    var Tender_id = record.TenderId;
                    saveRecord("TenderProposals", request.id, record);
                    var proposals = selectRecords("TenderProposals", {TenderId:Tender_id});
                    for(var i=0;i<proposals.length;i++) {
                        if( proposals[i].Id!=request.id) {
                            proposals[i].Accepted = false;
                            saveRecord("TenderProposals", proposals[i].Id, proposals[i] );
                        }
                    }

                    var Tender = getRecord("Tenders", Tender_id);
                    var order_id = Tender.OrderId;
                    var order = getRecord("Orders", order_id);
                    order.OrderStateId = 6; // Исполняется
                    saveRecord("Orders", order_id, order);

                    break;

            }
            return getRecord("TenderProposals", request.id);
        }
    };

    var callback = false;
    var consultant_visit = false;
    var clientOrders = [
        {
            Id: 1001,
            Notes: "Заказ бронедвери",
            Code: "001",
            Date: "2016-02-28T12:00:00",
            StatusTitle: "Создание",
            ClosingTime: false,
            //ClosingTime: "2016-03-15T18:30:00",
            CanSetWinner: true,
            CanModify: true,
            PossibleActions: [{Title: "На тендер",Rest: "orderToTender",Type:"success"}, {Title: "Удалить",Rest: "deleteOrder",Type:"danger"}],
                Proposals: [
                {
                    Id: 101,
                    Date: "2016-02-28T12:30:00",
                    Price: 50000,
                    Prepay: 10000,
                    UserId: "user_code_giid-kak-v-visual-ci-sharp-12-65-65",
                    UserTitle: "Вася из компании Рога и копыта",
                    IsWinner: false,
                    Rating: 3,
                    Term: "2 месяца",
                    Guarantee: "3 года"

                },
                {
                    Id: 102,
                    Date: "2016-02-28T12:30:00",
                    Price: 55000,
                    Prepay: 9000,
                    UserId: "user_code_giid-kak-v-visual-ci-sharp-12-70-65",
                    UserTitle: "Петя из компании Новые техенологии",
                    IsWinner: false,
                    Rating: 4,
                    Term: "3 месяца",
                    Guarantee: "2 года"

                }
            ],
            Products: [
                {
                    Id: 303,
                    TypeId: 1,
                    Title: "Большое окно",
                    ObjectData: {}
                },
                {
                    Id: 305,
                    TypeId: 1,
                    Title: "Маленькое окно",
                    ObjectData: {}
                }

            ],
            UserId: 3,
            ConsultantId: 1
        },
        {
            Id: 1050,
            Notes: "Заказ Бронекна",
            Code: "001",
            Date: "2016-02-29T16:30:00",
            ClosingTime: "2016-03-15T16:30:00",
            StatusTitle: "На тендере",
            CanModify: false,
            CanSetWinner: true,
            PossibleActions: [{Title: "Заказ выполнен",Rest: "orderComplite",Type:"primary"}],
            Proposals: [
                {
                    Id: 104,
                    Date: "2016-02-28T12:30:00",
                    Price: 150000,
                    Prepay: 12000,
                    UserId: "user_code_giid-kak-v-visual-ci-sharp-12-65-65",
                    UserTitle: "Вася из компании Рога и копыта",
                    IsWinner: false,
                    Rating: 3,
                    Term: "2 месяца",
                    Guarantee: "3 года"

                },
                {
                    Id: 106,
                    Date: "2016-02-28T12:30:00",
                    Price: 155000,
                    Prepay: 29000,
                    UserId: "user_code_giid-kak-v-visual-ci-sharp-12-70-65",
                    UserTitle: "Петя из компании Новые техенологии",
                    IsWinner: false,
                    Rating: 4,
                    Term: "3 месяца",
                    Guarantee: "2 года"

                }
            ],
            Products: [
                {
                    Id: 504,
                    TypeId: 1,
                    Title: "Окно в комнату",
                    ObjectData: {}
                },
                {
                    Id: 505,
                    TypeId: 1,
                    Title: "Окно на веранду",
                    ObjectData: {}
                }

            ],
            UserId: "guid-4534-656-456456-54645645-6456546-45645",
            ConsultantId: 1

        }

    ];
    var rating = {
        service: {
            Disable: false
        },
        consultant: {
            Disable: false
        },
        company: {
            Disable: false
        }
    }
    var client_profiles = [
        {
            Id: "guid-4534-656-456456-54645645-6456546-45645",
            Email: "email@email.ru",
            FirstName: "Василий",
            MiddleName: "Николаевич",
            LastName: "Иванов",
            Address: "г. Москва, Столетовское ш., д. 20, кв. 31",
            PhoneNumber: "+7 495 205-08-16",
            Balance: 90000,
            Consultant: {
                Id: 456,
                FullName: "Константин Игоревич",
                PhoneNumber: "+7 495 908-18-06",
                Rating: 4,
                Email: "consultant@compny.ru"
            }
        },
        {
            Id: "guid-4534-656-456456-54645645-6456546-45000",
            Email: "email@email.ru",
            FirstName: "Серей",
            MiddleName: "Васильевич",
            LastName: "Иванов",
            Address: "г. Москва, Столетовское ш., д. 20, кв. 32",
            PhoneNumber: "+7 495 205-08-15",
            Balance: 50000,
            Consultant: {
                Id: 456,
                FullName: "Игорь Григорьевич",
                PhoneNumber: "+7 495 910-20-02",
                Rating: 5,
                Email: "consultant2@compny.ru"
            }
        },
        {
            Id: 3,
            Email: "79787246978@yandex.ru",
            FirstName: "Андрей",
            MiddleName: "Вячеславович",
            LastName: "Кутяев",
            Address: "г. Севастополь, Гагарина, д.1 кв. 1",
            PhoneNumber: "+7 978 724 69 78",
            Balance: 80000,
            Consultant: {
                Id: 1,
                FullName: "Игорь Григорьевич",
                PhoneNumber: "+7 495 910-20-02",
                Rating: 5,
                Email: "consultant2@compny.ru"
            }
        }

    ];

    var messages = [
        {
            Id: 111,
            Title: "Заголовок сообщения",
            Text: "Сообщение просто так написано, чтобы проверить работет ли сообщение",
            SenderId: 1,
            ReceiverId: 3,
            SenderName: "Василий Петрович",
            ReceiverName: "Пётр Васильевич",
            Created: "2016-03-10T15-45-18"
        },
        {
            Id: 112,
            Title: "Заголовок сообщения 2",
            Text: "Сообщение просто так написано, чтобы проверить работет ли сообщение 2",
            SenderId: 3,
            ReceiverId: 1,
            SenderName: "Пётр Васильевич",
            ReceiverName: "Василий Петрович",
            Created: "2016-03-10T14-37-15"
        }

    ];
    // Поставщики
    var suppliers = [
        {
            Id: 3001,
            Title: "Компания 'Рога и Копыта'",
            Term: "2 месяца",
            Guarantee: "3 года",
            Prepay: "50%",
            Rating: 5
        },
        {
            Id: 3002,
            Title: "Иванов и Компания",
            Term: "3 месяца",
            Guarantee: "2 года",
            Prepay: "25%",
            Rating: 4
        }
    ];

    var getSupplier = function(id) {
        for(var i=0;i<suppliers.length;i++) if(suppliers[i].Id==id) return suppliers[i];
        return {};
    };

    return  {
        //data:  {},

        add: function (table, records) {
            data[table] = records;
        },
        addAction: function (action, param) {
            actions[action] = param;
        },

        get: function (table) {
            return this.data[table];
        },

        requestPost: function(param, data) {
            console.log(param);
            data = angular.fromJson(data);
            var request = parseParam(param);
            console.log("requestPost");
            console.log(request);
            if(!request) return false;
            if( (!!request.table) && (!request.action2) && (!request.table2) ) {
                if(!!request.id) {
                    saveRecord(request.table, request.id, data);
                    return getRecord(request.table, request.id);
                } else {
                    var new_id = createNew(request.table,data);
                    return getRecord(request.table, new_id);
                }
            }
            //console.log("request");
            //console.log(request);
            //console.log(data);
            if(request.action) switch (request.action) {
                case "login":
                    var result = {status: "ok", message: []};
                    if( angular.isUndefined(data.login) || data.login.length<3) {
                        result.status = "error";
                        result.message.push("Не введен логин");
                    }
                    if( angular.isUndefined(data.password)   || data.password.length<3) {
                        result.status = "error";
                        result.message.push("Отсутствует пароль");
                    }
                    if(result.status!="ok") return result;
                    result = getAutorization({Email: data.login, Password: data.password});
                    return result;
                    break;
                case "logout":
                    var result = {status: "ok", message: []};
                    var user = getUser(request);
                    if(user!==false) saveRecord(SETTINGS.Table.Users, user.Id, {Token: ""});
                    return result;
                    break;
                case "check_session":
                    var user = getUser(request);
                    return user;
                    break;
                case "registration":
                    var result = {status: "ok", message: []};
                    var password    = angular.isDefined( data.password ) ? data.password : "";
                    var email       = angular.isDefined( data.email ) ? data.email : "";
                    var name       = angular.isDefined( data.name ) ? data.name : "";
                    var isCompany   = angular.isDefined( data.isCompany ) ? data.isCompany : false;
                    //var login       = angular.isDefined( data.login ) ? data.login : "";
                    var login       = email;

                   // delete  data.password2;
                    var access      = 1;
                    if(login.length<5) {
                        result.message.push("Слишком короткий логин");
                    } else {
                        if(selectRecords(SETTINGS.Table.Users,{Login:login}).length>0) result.message.push("Такой логин уже существует");
                    }
                    if(password.length<4) {
                        result.message.push("Слишком короткий пароль");
                    }
                    if(name.length<2) {
                        result.message.push("Имя должно быть не менее двух символов");
                    }
                    if(email.length<5) {
                        result.message.push("Емейл введен некорректно");
                    } else {
                        if(selectRecords(SETTINGS.Table.Users,{Email:email}).length>0) result.message.push("Вы уже зарегистрированы");
                    }
                    if(result.message.length>0) {
                        result.status = "error";
                        return result;
                    }
                    var user_id = createNew(SETTINGS.Table.Users,{
                        Login: login,
                        Password: password,
                        Name: name,
                        Email: email,
                        Access: "client,estimator,consultant,manager",
                        Token: ""
                    });
                    //console.log("user_id = "+user_id);
                    return getAutorization({Id:user_id});
                    break;

                case "orderCallBack":
                    var clientID    = angular.isDefined( data.ClientId ) ? data.ClientId : 0;
                    var user = getRecord("Users", clientID);
                    var phone    = angular.isDefined( data.Phone ) ? data.Phone : "";
                    var time       = angular.isDefined( data.Time ) ? data.Time : "";
                    callback = {Phone: phone, Time: time, Created: $Time(), Id: getRandom(10,100), UserId: clientID, UserName: user.Name };
                    return callback;
                    break;
                case "orderConsultantVisit":
                    console.log(request);

                    var clientID    = angular.isDefined( data.ClientId ) ? data.ClientId : 0;
                    var user = getRecord("Users", clientID);
                    var date    = angular.isDefined( data.Date ) ? data.Date : "";
                    var times       = angular.isDefined( data.Times ) ? data.Times : "";
                    var phone    = angular.isDefined( data.Phone ) ? data.Phone : "";
                    var address    = angular.isDefined( data.Address ) ? data.Address : "";

                    consultant_visit = {Date: date, Times: times, Phone: phone, Address: address, Id: getRandom(100,999), Created: $Time(), UserId: clientID, UserName: user.Name };
                    return consultant_visit;
                    break;
                case "clientOrdersSetWinner":
                    var OrderId = angular.isDefined( data.OrderId ) ? data.OrderId : 0;
                    var ProposalId = angular.isDefined( data.ProposalId ) ? data.ProposalId : 0;
                    for(var o=0;o<clientOrders.length;o++) {
                        for(var p=0;p<clientOrders[o].Proposals.length;p++) if(clientOrders[o].Proposals[p].Id==ProposalId) {
                            clientOrders[o].Proposals[p].IsWinner = true;
                            clientOrders[o].CanSetWinner = false;
                            clientOrders[o].StatusTitle = "Назначен победитель";
                        }
                    }
                    return clientOrders;
                    break;
                case "Rating":
                    console.log("Rating:");
                    console.log(data);
                    var service = data["Service"];
                    var rate = data["Rating"];
                    rating[service]["Rate"] = rate;
                    rating[service]["Disable"] = true;
                    return rating[service];
                    break;
                case "clientprofile":

                    var client_id = request["id"];
                    if(client_id=="create") {
                        data.Id = $GenCode();
                        //data.FirstName += "SAVES";
                        client_profiles.push(data);
                        return data;
                    } else {
                        for(var i=0;i<client_profiles.length;i++) {
                            if(client_profiles[i].Id==client_id) {
                                angular.extend(client_profiles[i], data );
                                return client_profiles[i];
                            }
                        }
                    }
                    return false;
                break;
                case "saveProduct":

                    var product_id = request.id;
                    console.log("saveProduct:"+product_id);
                    for(var i=0;i<clientOrders.length;i++) for(var j=0;j<clientOrders[i].Products.length;j++) if(clientOrders[i].Products[j].Id==product_id) {
                        clientOrders[i].Products[j] = data;
                        console.log(data);
                        return clientOrders[i];
                    }
                    break;
                case "newProduct":
                    var order_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        var id = getRandom(1000,9999);
                        var product = data;
                        product.Id = id;
                        clientOrders[i].Products.push(product);
                        return product;
                    }
                    break;
                case "deleteProduct":
                    var product_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) for(var j=0;j<clientOrders[i].Products.length;j++) if(clientOrders[i].Products[j].Id==product_id) {
                        clientOrders[i].Products[j] = data;
                        clientOrders[i].Products.splice(j,1);
                        return clientOrders[i].Products;
                    }
                    break;
                case "orderToTender":
                    var order_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        clientOrders[i].StatusTitle = "На тендере";
                        clientOrders[i].CanModify =  false;
                        clientOrders[i].CanSetWinner = true;
                        return clientOrders[i];
                    }
                    return false;
                    break;
                case "orderComplite":
                    var order_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        clientOrders[i].StatusTitle = "Выполнен";
                        clientOrders[i].CanModify =  false;
                        clientOrders[i].CanSetWinner = false;
                        return clientOrders[i];
                    }
                    return false;
                    break;
                case "saveOrderNote":
                    var order_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        clientOrders[i].Notes = data.Notes;
                        return clientOrders[i];
                    }
                    return false;
                    break;
                case "newOrder":
                    var ConsultantId = data["ConsultantId"];
                    var UserId = data["UserId"];
                    var id = getRandom(10000,99999);
                    var time = $Time();
                    var order = {
                        Id: id,
                        Notes: "",
                        Code: "code-"+id,
                        Date: time,
                        StatusTitle: "Создание",
                        CanSetWinner: false,
                        CanModify: true,
                        Proposals: [],
                        Products: [],
                        UserId: UserId,
                        ConsultantId: ConsultantId

                    };
                    clientOrders.push(order);
                    return order
                    break;
                case "deleteOrder":
                    var order_id = request.id;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        clientOrders.splice(i, 1);
                        return {};
                    }
                    return false;
                    break;
                case "suppliers":
                    var id = request.id;
                    for(var i=0;i<suppliers.length;i++) if(suppliers[i].Id==id) {
                        suppliers[i] = data;
                        return suppliers[i];
                    }
                    break;
                case "newSupplier":
                    var id = getRandom(100,999);
                    var supplier = {
                        Id: id,
                        Title: "Новый поставщик",
                        Term: "0 месяцев",
                        Guarantee: "1 год",
                        Prepay: "0%",
                        Rating: 0
                    };
                    suppliers.push(supplier);
                    return supplier;
                    break;
                case "addProposal": /// todo
                    var order_id = request.id;
                    var supplier = getSupplier(data.ShipperId);
                    var Note = data.Note;
                    for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) {
                        var proposal = {
                            Id: getRandom(100,999),
                            Date: $Time(),
                            Price: data.Price,
                            Products: data.Products,
                            Prepay: data.Prepay ? data.Prepay : supplier.Prepay,
                            UserId: data.UserId,
                            UserTitle: supplier.Title,
                            IsWinner: false,
                            Rating: supplier.Rating,
                            Term: data.Term ? data.Term : supplier.Term,
                            Guarantee: data.Guarantee ? data.Guarantee : supplier.Guarantee,
                            Note:   Note
                        };
                        clientOrders[i].Proposals.push(proposal);
                        return proposal;
                    }
                    break;
                case "currentConsultantVisit":
                    if(request.id=="cancel") {
                        consultant_visit = false;
                        return {};
                    }
                    break;
                case "messages":
                    var UserId = 3;

                    var message = {
                        Id: getRandom(1000,10000),
                        Title: data.Title,
                        Text: data.Text,
                        SenderId: UserId,
                        ReceiverId: data.CorrespondentId,
                        SenderName: "Имя отправителя",
                        ReceiverName: "Имя получателя",
                        Created: $Time()
                    };
                    messages.push(message);

                    console.log(messages);

                    return message;

                    break;


            } else {
                return actionRecord(request);
            }

        },




        requestGet: function(param, post_data) {
            var request = parseParam(param);

            if(!request) return false;

            if( (!!request.table) && (!request.action2) && (!request.table2) ) {
                return (!!request.id) ? getRecord(request.table, request.id) : angular.isDefined(request.condition) ? selectRecords(request.table, request.condition) :  data[request.table];
            } else {

                if(request.action) {
                    switch (request.action) {       // Авторизация по кукису - tryLogin()  выдает будто это userId = 3
                        case "login": return tryLogin(); break;
                        case "currentCallBack": return callback ? callback : {}; break;
                        case "variantConsultantVisit":
                            return [
                                {
                                    Date: '2016-03-28',
                                    Times: ['11:15..11:45','13:00..13:45']
                                },
                                {
                                    Date: '2016-03-01',
                                    Times: ['10:15..10:45','12:00..12:30']
                                },
                                {
                                    Date: '2016-03-02',
                                    Times: ['10:15..10:45','12:00..12:30','16:00..16:30']
                                }
                            ];

                            break;
                        case "currentConsultantVisit":
                            return consultant_visit ? consultant_visit : {};
                            break
                        case "clientOrders":
                            return clientOrders;
                            break;
                        case "Rating":
                            var service = request["Service"];
                            return rating[service];
                            break;
                        case "clientSuppliers":
                            return suppliers;
                            break
                        case "clientprofiles":
                            return client_profiles;
                            break;
                        case "clientprofile":
                            var client_id = request["ClientId"];
                            return client_profiles[client_id];
                            break;
                        case "clientOrderList":
                            // не выбирая, т.к. это вместо сервера
                            return clientOrders;
                            break;
                        case "visits":
                            return consultant_visit ? [consultant_visit] : [];
                            break;
                        case "callbacks":
                            return callback ? [callback] : [];
                            break;
                        case "suppliers": // для расчетчика, вот и отличается
                            return suppliers;
                            break;
                        case "tenders":


                            var result = [];
                            var tenderUserId = angular.isDefined(request.UserId) ? request.UserId : false;
                            for(var i=0;i<clientOrders.length;i++) if(!clientOrders[i].CanModify && clientOrders[i].CanSetWinner) {
                                clientOrders[i].Notes += ".";
                                if(tenderUserId!==false && tenderUserId!=clientOrders[i].UserId) continue;
                                result.push(clientOrders[i]);
                            }
                            return result;
                            break;
                        case "order":
                            var order_id = request.OrderId;
                            console.log("order_id = " + request.OrderId);
                            for(var i=0;i<clientOrders.length;i++) if(clientOrders[i].Id==order_id) return clientOrders[i];
                            return false;
                            break;
                        case "messages":
                            if(angular.isDefined(request.correspondents)) {
                                var users = {};
                                for(var i=0;i<messages.length;i++) {
                                    var m = messages[i];
                                    users[m.ReceiverId] = {Id: m.ReceiverId, Name: m.ReceiverName, Unread: 3};
                                    users[m.SenderId] =   {Id: m.SenderId, Name: m.SenderName, Unread: 1};
                                }
                                var result = [];
                                for(var id in users) result.push(users[id]);
                                return result;
                            } else {
                                return messages;
                            }
                            break;

                    }
                }


                return actionRecord(request);

            }
        },
        requestRemove: function(param, data) {
            var request = parseParam(param);
            //console.log(request);
            deleteRecord(request.table, request.id);
            return getRecord(request.table, request.id);
        },

        getAllData: function() {
            return {data: data, actions: actions};
        },

        putAllData: function(store) {
            data = store.data;
            //actions = store.actions;
            for(var table in data) {
                if(angular.isUndefined(data[table.Name]))  data[table.Name] = [];
            }
            }

    };


})();




var _Id = 1;
DemoData.add(SETTINGS.Table.Tables, [
    {Id:_Id++,	Name:SETTINGS.Table.Tables, Title:'Таблица',	    Titles:'Таблицы',	            RecordTitle:'[Title]',           KeyField:'Name',	        SlaveOfTableId:0,   SlaveOfFieldId:0,   IsVocab:false},
    {Id:_Id++,	Name:'Fields',	            Title:'Поле',	        Titles:'Поля',	                RecordTitle:'[Title] / [Name]',  KeyField:'Name',	        SlaveOfTableId:1,	SlaveOfFieldId:11,  IsVocab:false},
    {Id:_Id++,	Name:'Types',	            Title:'Тип значения',	Titles:'Типы значений',	        RecordTitle:'[Title] ([Name])',  KeyField:'Name',	        SlaveOfTableId:0,	SlaveOfFieldId:0,   IsVocab:true},
    {Id:_Id++,	Name:'OrderStates',	        Title:'Статус',	        Titles:'Статусы заказов',	    RecordTitle:'[Title]',           KeyField:'Id',	            SlaveOfTableId:0,	SlaveOfFieldId:0,   IsVocab:true},
    {Id:_Id++,	Name:'ProductCategories',	Title:'Категория',	    Titles:'Категории продуктов',	RecordTitle:'[Title]',           KeyField:'Id',	            SlaveOfTableId:0,	SlaveOfFieldId:0,  IsVocab:true},
    {Id:_Id++,	Name:'Products',            Title:'Продукт',	    Titles:'Продукты',	            RecordTitle:'[Name] - [Code]',   KeyField:'Id',	            SlaveOfTableId:9,	SlaveOfFieldId:28,  IsVocab:false},
    {Id:_Id++,	Name:'ProductWindow',	    Title:'Окно',	        Titles:'Окна',	                RecordTitle:'Окно [Width] x [Height]',KeyField:'Id',	    SlaveOfTableId:6,	SlaveOfFieldId:36,  IsVocab:false},
    {Id:_Id++,	Name:'ProductDoors',	    Title:'Дверь',	        Titles:'Двери',	                RecordTitle:'Дверь [Width] x [Height]',KeyField:'Id',	    SlaveOfTableId:6,	SlaveOfFieldId:41,  IsVocab:false},
    {Id:_Id++,	Name:'Orders',	            Title:'Заказ',	        Titles:'Заказы',	            RecordTitle:'[Name] - [Code]',           KeyField:'Id',	    SlaveOfTableId:10,	SlaveOfFieldId:47,  IsVocab:false},
    {Id:_Id++,	Name:SETTINGS.Table.Users,  Title:'Пользователь',	Titles:'Пользователи',	        RecordTitle:'[Name] ([Login])',           KeyField:'Id',	SlaveOfTableId:0,	SlaveOfFieldId:0,   IsVocab:false}
]);

var _Id = 1;
DemoData.add("Fields", [

    /// Tables [1] || 1 ..9
    {Id:_Id++,	TableId:1,	Name:'Id',	                Title:'Ид',	                    TypeId:1,   Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'Name',	            Title:'Обозначение',	        TypeId:3,	Default:'NewTable',	            LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'Title',	            Title:'Название',	            TypeId:3,	Default:'Новый объект',	        LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'Titles',	            Title:'Название многих',	    TypeId:3,	Default:'Список новых объектов',LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'RecordTitle',         Title:'Маска записи',	        TypeId:3,	Default:'[Title]',              LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'KeyField',	        Title:'Ключ',	                TypeId:3,	Default:'Id',	                LinkedTableId:0},
    {Id:_Id++,	TableId:1,	Name:'SlaveOfTableId',	    Title:'Подчиненная таблицы',    TypeId:7,	Default:0,	                    LinkedTableId:1},
    {Id:_Id++,	TableId:1,	Name:'SlaveOfFieldId',	    Title:'Подчиненная по полю',    TypeId:7,	Default:0,	                    LinkedTableId:2},
    {Id:_Id++,	TableId:1,	Name:'IsVocab',	            Title:'Словарная',	            TypeId:6,	Default:false,	            LinkedTableId:0},

    /// Fields [2] || 10..16
    {Id:_Id++,	TableId:2,	Name:'Id',	        Title:'Ид',	                    TypeId:1,	Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:2,	Name:'TableId',	    Title:'Таблица',	            TypeId:7,	Default:0,	                    LinkedTableId:1},
    {Id:_Id++,	TableId:2,	Name:'Name',	    Title:'Обозначение',	        TypeId:3,	Default:'new_field',	       LinkedTableId:0},
    {Id:_Id++,	TableId:2,	Name:'Title',	    Title:'Наименование',	        TypeId:3,	Default:'Новое поле',	       LinkedTableId:0},
    {Id:_Id++,	TableId:2,	Name:'TypeId',	    Title:'Тип данных',     	    TypeId:7,	Default:2,	                    LinkedTableId:3},
    {Id:_Id++,	TableId:2,	Name:'Default', 	Title:'Значение по-умолчанию',	TypeId:3,	Default:'',	                    LinkedTableId:0},
    {Id:_Id++,	TableId:2,	Name:'LinkedTableId', Title:'Элемент таблицы',	    TypeId:7,	Default:0,	                     LinkedTableId:1},

    /// Types [3] || 17..19
    {Id:_Id++,	TableId:3,	Name:'Id',	        Title:'Ид',	                    TypeId:1,	Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:3,	Name:'Name',	    Title:'Обозначение',	        TypeId:3,	Default:'new_type',	            LinkedTableId:0},
    {Id:_Id++,	TableId:3,	Name:'Title',	    Title:'Название',	            TypeId:3,	Default:'Новый тип',	        LinkedTableId:0},

    /// OrderState [4] || 20..21
    {Id:_Id++,	TableId:4,	Name:'Id',	        Title:'Ид',	                    TypeId:1, Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:4,	Name:'Title',	    Title:'Название',	            TypeId:3,	Default:'Новое состояние',      LinkedTableId:0},

    /// ProductCategories [5] || 22..24
    {Id:_Id++,	TableId:5,	Name:'Id',	                    Title:'Ид',	                    TypeId:1,	    Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:5,	Name:'Title',	                Title:'Наименование',           TypeId:3,	    Default:'Новая категория',      LinkedTableId:0},
    {Id:_Id++,	TableId:5,	Name:'TableId',	                Title:'Таблица объектов',       TypeId:7,	    Default:7,      LinkedTableId:1},

    /// Product  [6]  || 25..32
    {Id:_Id++,	TableId:6,	Name:'Id',	                    Title:'Ид',	                    TypeId:1,	    Default:'$GenId',	            LinkedTableId:0},
    {Id:_Id++,	TableId:6,	Name:'Code',	                Title:'Код',                    TypeId:3,	    Default:'$GenCode',             LinkedTableId:0},
    {Id:_Id++,	TableId:6,	Name:'Name',	                Title:'Наименование',           TypeId:3,	    Default:'Новый продукт',        LinkedTableId:0},
    {Id:_Id++,	TableId:6,	Name:'OrderId',	                Title:'Заказ',                  TypeId:7,	    Default:0,                      LinkedTableId:9},
    {Id:_Id++,	TableId:6,	Name:'ProductCategoryId',	    Title:'Категория',              TypeId:7,	    Default:0,                      LinkedTableId:5},
    {Id:_Id++,	TableId:6,	Name:'Quantity',                Title:'Количество',             TypeId:2,	    Default:1,                      LinkedTableId:0},
    {Id:_Id++,	TableId:6,	Name:'SpecificationLink',       Title:'Ссылка на спецификацию', TypeId:10,	    Default:'http://',              LinkedTableId:0},
    {Id:_Id++,	TableId:6,	Name:'ViewOrder',	            Title:'Порядок просмотра',      TypeId:2,	    Default:0,                      LinkedTableId:0},

    /// ProductWindow [7]    || 33..36
    {Id:_Id++,	TableId:7,	Name:'Id',	        Title:'Ид',	            TypeId:1,	    Default:'$GenId', LinkedTableId:0},
    {Id:_Id++,	TableId:7,	Name:'Width',	    Title:'Ширина',         TypeId:2,	    Default:100,      LinkedTableId:0},
    {Id:_Id++,	TableId:7,	Name:'Height',	    Title:'Высота',         TypeId:2,	    Default:150,      LinkedTableId:0},
    {Id:_Id++,	TableId:7,	Name:'ProductId',	Title:'Продукт',        TypeId:7,	    Default:0,        LinkedTableId:6},

    /// ProductDoors [8]    || 37..41
    {Id:_Id++,	TableId:8,	Name:'Id',	        Title:'Ид',	            TypeId:1,	    Default:'$GenId', LinkedTableId:0},
    {Id:_Id++,	TableId:8,	Name:'Width',	    Title:'Ширина',         TypeId:2,	    Default:100,      LinkedTableId:0},
    {Id:_Id++,	TableId:8,	Name:'Height',	    Title:'Высота',         TypeId:2,	    Default:150,      LinkedTableId:0},
    {Id:_Id++,	TableId:8,	Name:'Peephole',	Title:'Дверной глазок', TypeId:6,	    Default:false,    LinkedTableId:0},
    {Id:_Id++,	TableId:8,	Name:'ProductId',	Title:'Продукт',        TypeId:7,	    Default:0,        LinkedTableId:6},

    /// Orders [9]    || 42..47
    {Id:_Id++,	TableId:9,	Name:'Id',	        Title:'Ид',	            TypeId:1,	    Default:'$GenId',       LinkedTableId:0},
    {Id:_Id++,	TableId:9,	Name:'Name',	    Title:'Наименование',	TypeId:3,	    Default:'Новый заказ',  LinkedTableId:0},
    {Id:_Id++,	TableId:9,	Name:'Code',	    Title:'Код',	        TypeId:3,	    Default:'$GenCode',     LinkedTableId:0},
    {Id:_Id++,	TableId:9,	Name:'OrderDate',	Title:'Дата',	        TypeId:9,	    Default:'$Now',         LinkedTableId:0},
    {Id:_Id++,	TableId:9,	Name:'OrderStateId',Title:'Статус',         TypeId:7,	    Default: 1,             LinkedTableId:4},
    {Id:_Id++,	TableId:9,	Name:'UserId',	    Title:'Пользователь',   TypeId:7,	    Default:0,              LinkedTableId:10},

    /// Users [10]    || 48..52
    {Id:_Id++,	TableId:10,	Name:'Id',	        Title:'Ид',	                TypeId:1,	    Default:'$GenId',               LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Name',	    Title:'Имя пользователя',	TypeId:3,	    Default:'Новый пользователь',   LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Login',	    Title:'Логин',	            TypeId:3,	    Default:'login',                LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Password',	Title:'Пароль',	            TypeId:3,	    Default:'password',             LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Email',	    Title:'Емейл',	            TypeId:3,	    Default:'',                     LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Access',	    Title:'Права',	            TypeId:3,	    Default:'company,customer',     LinkedTableId:0},
    {Id:_Id++,	TableId:10,	Name:'Token',	    Title:'Токен сессии',	    TypeId:3,	    Default:'$GenCode',             LinkedTableId:0},

]);

var _Id = 1;
DemoData.add("Types", [
    {Id:_Id++,	Name:'key',	    Title:'Ид'},
    {Id:_Id++,	Name:'int',	    Title:'Целое'},
    {Id:_Id++,	Name:'string',	Title:'Строка'},
    {Id:_Id++,	Name:'text',	Title:'Текст'},
    {Id:_Id++,	Name:'real',	Title:'Десятичное'}, // 5
    {Id:_Id++,	Name:'bool',	Title:'Логическое'},
    {Id:_Id++,	Name:'link',	Title:'Ссылка'},
    {Id:_Id++,	Name:'list',	Title:'Список ссылок'},
    {Id:_Id++,	Name:'date',	Title:'Дата/время'},
    {Id:_Id++,	Name:'href',	Title:'Ссылка'}      // 10

]);

var _Id = 1;
DemoData.add("OrderStates", [
    /// OrderState
    {Id:_Id++,	Title:'Формируется'},
    {Id:_Id++,	Title:'Сформирован'},
    {Id:_Id++,	Title:'Отменен'},
    {Id:_Id++,	Title:'На тендере'},
    {Id:_Id++,	Title:'На аукционе'},
    {Id:_Id++,	Title:'Исполняется'},
    {Id:_Id++,	Title:'Выполнен'},
    {Id:_Id++,	Title:'Заархивирован'},

]);

var _Id = 1;
DemoData.add("ProductCategories", [
    /// ProductCategories
    {Id:_Id++,	Title:'Окна', TableId:7},
    {Id:_Id++,	Title:'Двери', TableId:8},

]);

var _Id = 1;
DemoData.add("Products", [
    /// Product
    //{Id:_Id++,	Name:'Окно фасад', Code: '201602594014-0001',   OrderId: 1, ProductCategoryId: 1, Quantity: 2, SpecificationLink: "http://www.vashdom.ru/gost/11214-86/", ViewOrder: 0},
    //{Id:_Id++,	Name:'Окно комната', Code: '201602594500-0002', OrderId: 1, ProductCategoryId: 1, Quantity: 4, SpecificationLink: "http://www.vashdom.ru/gost/11214-86/", ViewOrder: 1},
    //{Id:_Id++,	Name:'Дверь парадная', Code: '20160259900-0003',OrderId: 1, ProductCategoryId: 2, Quantity: 1, SpecificationLink: "http://www.vashdom.ru/gost/11214-86/", ViewOrder: 2},
    //{Id:_Id++,	Name:'Дверь тыльная', Code: '20160279900-0007', OrderId: 1, ProductCategoryId: 2, Quantity: 1, SpecificationLink: "http://www.vashdom.ru/gost/11214-86/", ViewOrder: 2},

]);


var _Id = 1;
DemoData.add("ProductWindow", [
    /// ProductWindow
    //{Id:_Id++,	Width:200, Height:150,ProductId:1},
    //{Id:_Id++,	Width:120, Height:150,ProductId:2},
]);


var _Id = 1;
DemoData.add("ProductDoors", [
    /// ProductDoors
    //{Id:_Id++,	Width:100, Height:220, Peephole: false, ProductId:3},
    //{Id:_Id++,	Width:80, Height:190,  Peephole: true, ProductId:4},
]);


var _Id = 1;
DemoData.add("Orders", [
    /// Orders
    //{Id:_Id++,	Name:'Заказ окно-двери', Code: '201602594014-9564',OrderDate: 1453700000, OrderStateId: 2,  UserId: 3}
]);


var _Id = 1;
DemoData.add(SETTINGS.Table.Users, [
    /// Users
    {Id:_Id++,	Name:'Виталий Белик',	    Login:'Vitaliy',    Password: "Belik",  Email:"belik@mail.ru",  Access: "client,estimator,consultant,manager,guest", Token: "", ConsultantId: 2},
    {Id:_Id++,	Name:'Оксана Белик',	    Login:'Oxana',      Password: "Belik",  Email:"oxana@mail.ru",  Access: "client,estimator,consultant,manager,guest", Token: "", ConsultantId: 1},
    {Id:_Id++,	Name:'Андрей Кутяев ',	    Login:'Andrey',     Password: "123456", Email:"a@rt.net.ua",  Access: "client,estimator,consultant,manager,guest", Token: "", ConsultantId: 1},

]);




DemoData.addAction("login",{name: "Авторизация"});
DemoData.addAction("logout",{name: "Выход"});
DemoData.addAction("registration",{name: "Регистрация"});
DemoData.addAction("check_session",{name: "Проверка сессии"});




DemoData.addAction("orderCallBack",{name: "Заказать звонок"});
DemoData.addAction("currentCallBack",{name: "Текущий заказ звонка"});

DemoData.addAction("orderConsultantVisit",{name: "orderConsultantVisit"});
DemoData.addAction("variantConsultantVisit",{name: "variantConsultantVisit"});
DemoData.addAction("currentConsultantVisit",{name: "currentConsultantVisit"});

DemoData.addAction("clientOrders",{name: "clientOrders"});
DemoData.addAction("clientOrdersSetWinner",{name: "clientOrdersSetWinner"});

DemoData.addAction("Rating",{name: "Rating"});
DemoData.addAction("clientSuppliers",{name: "clientSuppliers"}); /// поставщики клиента

DemoData.addAction("clientprofiles",{name: "clientprofiles"}); ///  профили клиентов консультанта
DemoData.addAction("clientprofile",{name: "clientprofile"}); /// профиль клиента для консультанта

DemoData.addAction("clientOrderList",{name: "clientprofile"}); /// Для менеджера или консультанта список заказов клиента
DemoData.addAction("saveProduct",{name: "saveProduct"}); /// Сохранение отредактированного продукта
DemoData.addAction("newProduct",{name: "saveProduct"}); /// Создание нового продукта (добавление к заказу)
DemoData.addAction("deleteProduct",{name: "deleteProduct"}); /// Создание нового продукта (добавление к заказу)
DemoData.addAction("orderToTender",{name: "orderToTender"}); /// Отправить заказ на тендер

DemoData.addAction("visits",{name: "visits"}); /// Список заказов посещений клиентов
DemoData.addAction("callbacks",{name: "callbacks"}); /// Список заказов обратных зконов клиентов
DemoData.addAction("saveOrderNote",{name: "saveOrderNote"}); /// Список заказов обратных зконов клиентов
DemoData.addAction("newOrder",{name: "newOrder"}); /// Добавить новый заказ
DemoData.addAction("suppliers",{name: "suppliers"}); /// get/post - получить. сохранить
DemoData.addAction("newSupplier",{name: "newSupplier"}); /// post - создать проставщика

DemoData.addAction("tenders",{name: "tenders"}); /// Запрос списка тендеров клиента /tenders
DemoData.addAction("addProposal",{name: "addProposal"}); /// Добавить предлжение по тендеру /addProposal/order_id, post: todo
DemoData.addAction("deleteOrder",{name: "deleteOrder"}); //  Удаление заказа
DemoData.addAction("orderComplite",{name: "orderComplite"}); //  Заказ выполнен

DemoData.addAction("messages",{name: "messages"}); //  Сообщения UserId - для пользователя, CorrespondentId - собеседник. Если нет - последние сообщения
DemoData.addAction("order",{name: "order"}); //  Запрашивает конкретный ордер (тендер)





const _HALF_REAL_MODE = true;

var TOKEN = "NONE";
function checkHeaderAuth (headers) {
    TOKEN = angular.isDefined(headers.Token) ? headers.Token : "";
    //console.log(headers);
    return true;
}



angular.module('MockE2E_Module', ['ngMockE2E'])
    .run(function ($httpBackend) {

        var REG_EXP_URL = _HALF_REAL_MODE ? SETTINGS.Demo.REG.half : SETTINGS.Demo.REG.full ;
        var _ERROR = [404,  {} ];

        $httpBackend.whenGET(REG_EXP_URL, checkHeaderAuth).respond(function (method, url, data) {          // Get, Query
            var result =  DemoData.requestGet(url, data);
            return !!result ? [200,  result  ] : _ERROR;
        });

        $httpBackend.whenDELETE(REG_EXP_URL, checkHeaderAuth).respond(function (method, url, data) {       // Remove
            var result =  DemoData.requestRemove(url, data);
            return !!result ? [200,  result  ] : _ERROR;
        });

        $httpBackend.whenPOST(REG_EXP_URL, checkHeaderAuth).respond(function (method, url, data) {         // Save
            var result =  DemoData.requestPost(url, data);
            console.log(result);
            if(angular.isObject(result) && angular.isDefined(result.status)&& result.status=="error") return [201, result.message];
            return !!result ? [200,  result  ] : _ERROR;
        });

        $httpBackend.whenPOST(SETTINGS.Rest.sendFile).passThrough();
        $httpBackend.whenPOST(SETTINGS.Rest.handleFile).passThrough();
        $httpBackend.whenPOST(SETTINGS.Demo.store).passThrough();
        $httpBackend.whenGET(SETTINGS.Demo.store).passThrough();


        if(_HALF_REAL_MODE) {
            $httpBackend.
                whenGET(/^\/rest\/.*/).
                passThrough();
            $httpBackend.
                whenPOST(/^\/rest\/.*/).
                passThrough();
        }

            //$httpBackend.
            //    whenPOST(/^\/rs\/.*/).
            //    passThrough();

        $httpBackend.
            whenGET(/.*/).
            passThrough();


    });




app.requires.push('MockE2E_Module');

