function YYYYMMDD() {
    var d = new Date();
    var r = ""+ d.getFullYear() + d.getMonth() + d.getDate();
    return r;
}

function YYYYMMDDHHMMSS() {
    var d = new Date();
    var r = ""+ d.getFullYear() + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds();
    return r;
}


function getRandom(min, max) {
    var r = min + Math.random() * (max - min)
    return Math.round(r);
}


function ToObject(r) {
    if(angular.isString(r) || angular.isNumber(r) || typeof (r)=="boolean") return r;
    if(angular.isArray(r)) {
        var rr = [];
        for(var i=0;i< r.length;i++) rr[i] = ToObject(r[i]);
        return rr;
    }
    if(angular.isObject(r) || angular.isFunction(r)) {
        var rr = {};
        for(var k in r)  if( "_$".indexOf(k.substr(0,1)) + "toJSON".indexOf(k)==-2 ) rr[k] = ToObject(r[k]);
        return rr;
    }
    return r;
}

function $GenCode() {
    return YYYYMMDDHHMMSS() + "-"+getRandom(1000,9999);
}

function $GenToken() {
    var s = "qwertyuiopasdfghjklzxcvbnm123456789";
    var r = "";
    for(var i=0;i<30;i++) r+= s.substr(getRandom(0, s.length-1),1);
    return r;
}

function S00(d){
    d = "00"+d;
    return d.substr(d.length-2,2);
}

// 2016-01-25T00:00:00

function $Time() {
    var d = new Date();
    return ""
    +d.getFullYear()+"-"
    + S00(1+d.getMonth()) + "-"
    + S00(d.getDate())+"T"
    + S00(d.getHours())+":"
    + S00(d.getMinutes())+":"
    + S00(d.getSeconds());
}
// 0123 56 89 12 45 78
// 2016-01-25T00:00:00 - > new Date
function getDateFromMS(ms_date) {
    if(!angular.isString(ms_date) || ms_date.length<16) return ms_date;
    return new Date(ms_date.substr(0,4), ms_date.substr(5,2)-1, ms_date.substr(8,2), ms_date.substr(11,2), ms_date.substr(14,2), ms_date.substr(17,2),0 );
}

function getMsDateFromJS(d) {
    return ""
    +d.getFullYear()+"-"
    + S00(1+d.getMonth()) + "-"
    + S00(d.getDate())+"T"
    + S00(d.getHours())+":"
    + S00(d.getMinutes())+":"
    + S00(d.getSeconds());
}

function dateToView(d) {
    if( !angular.isObject(d) ) return d;
    return ""
    + S00(d.getDate())+"."
    + S00(1+d.getMonth()) + "."
    +d.getFullYear()+" "
    + S00(d.getHours())+":"
    + S00(d.getMinutes());
}

function msDareToView(d) {
    return dateToView( getDateFromMS(d) );
}

function getNomber(str) {
    var r = Number(str);
    return isNaN(r) ? 0 : r;
}

function $NowPlusDays(days) {
    var d = new Date();
    d.setDate(d.getDate()+days);
    return getMsDateFromJS(d);
}


function joinAny(any) {
    var result = "";
    if( angular.isString(any) ) return any;
    if( angular.isArray(any) ) return any.join("<br />");
    if(angular.isObject(any)) for(var k in any) result+=any[k] + "<br />";
    return result;
}


function YYYY_MM_DD(date) {
    return date.getFullYear() +"-" + S00(1+date.getMonth()) +"-" + S00(date.getDate());
}


function MsViewDate(ms_date) {
    if(!angular.isString(ms_date) || ms_date.length<16) return ms_date;
    return ""+ms_date.substr(8,2)+"." + ms_date.substr(5,2)+"."+ms_date.substr(0,4) + " "+ ms_date.substr(11,2) + ":" + ms_date.substr(14,2);

}