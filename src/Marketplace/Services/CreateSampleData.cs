﻿using System;
using System.Threading.Tasks;
using MarketPlace.Development;
using MarketPlace.Entities.InitDataStore;
using MarketPlace.Entities.Models;
using MarketPlace.Infrastructure.Constants;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Extensions.DependencyInjection;

// ReSharper disable once CheckNamespace
namespace MarketPlace
{
    public partial class Startup
    {
        private static async Task CreateSampleData(IServiceProvider applicationServices)
        {
            using (var context = applicationServices.GetService<ApplicationDbContext>())
            {
                if (context.Database != null)
                {
                    if (await context.Database.EnsureCreatedAsync())
                    {

                        DataStoreInit.CreateCompanies(context);

                        var userManager = applicationServices.GetService<UserManager<ApplicationUser>>();

                        context.Roles.AddRange
                            (new IdentityRole
                            {
                                Name = Identity.RolesAdministrators,
                                NormalizedName = Identity.RolesAdministratorsNormalizedName
                            },
                                new IdentityRole
                                {
                                    Name = Identity.RolesManagers,
                                    NormalizedName = Identity.RolesManagersNormalizedName
                                },
                                new IdentityRole
                                {
                                    Name = Identity.RolesUsers,
                                    NormalizedName = Identity.RolesUsersNormalizedName
                                });


                        #region USERS MarketPlace
                        var userMarketPlaceAdmin = new ApplicationUser
                        {
                            UserName = Constants.UserMarketPlaceAdminEmail,
                            Email = Constants.UserMarketPlaceAdminEmail,
                            CompanyId = 1,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultMarketPlaceAdmin = await userManager.CreateAsync(userMarketPlaceAdmin, Constants.UserPassword);

                        var userMarketPlaceManager = new ApplicationUser
                        {
                            UserName = Constants.UserMarketPlaceManagerEmail,
                            Email = Constants.UserMarketPlaceManagerEmail,
                            //CompanyId = 1,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultMarketPlaceManager = await userManager.CreateAsync(userMarketPlaceManager, Constants.UserPassword);

                        var userMarketPlaceUser = new ApplicationUser
                        {
                            UserName = Constants.UserMarketPlaceUserEmail,
                            Email = Constants.UserMarketPlaceUserEmail,
                            CompanyId = 1,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultMarketPlaceUser = await userManager.CreateAsync(userMarketPlaceUser, Constants.UserPassword);

                        context.SaveChanges();

                        await userManager.AddToRoleAsync(userMarketPlaceAdmin, Identity.RolesAdministrators);
                        await userManager.AddToRoleAsync(userMarketPlaceManager, Identity.RolesManagers);
                        await userManager.AddToRoleAsync(userMarketPlaceUser, Identity.RolesUsers);
                        #endregion

                        #region USERS Northwind
                        var userNorthwindAdmin = new ApplicationUser
                        {
                            UserName = Constants.UserNorthwindAdminEmail,
                            Email = Constants.UserNorthwindAdminEmail,
                            CompanyId = 2,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultNorthwindAdmin = await userManager.CreateAsync(userNorthwindAdmin, Constants.UserPassword);

                        var userNorthwindManager = new ApplicationUser
                        {
                            UserName = Constants.UserNorthwindManagerEmail,
                            Email = Constants.UserNorthwindManagerEmail,
                            CompanyId = 2,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultNorthwindManager = await userManager.CreateAsync(userNorthwindManager, Constants.UserPassword);

                        var userNorthwindUser = new ApplicationUser
                        {
                            UserName = Constants.UserNorthwindUserEmail,
                            Email = Constants.UserNorthwindUserEmail,
                            CompanyId = 2,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultNorthwindUser = await userManager.CreateAsync(userNorthwindUser, Constants.UserPassword);

                        context.SaveChanges();

                        await userManager.AddToRoleAsync(userNorthwindAdmin, Identity.RolesAdministrators);
                        await userManager.AddToRoleAsync(userNorthwindManager, Identity.RolesManagers);
                        await userManager.AddToRoleAsync(userNorthwindUser, Identity.RolesUsers);
                        #endregion

                        #region USERS AdventureWorks
                        var userAdventureWorksAdmin = new ApplicationUser
                        {
                            UserName = Constants.UserAdventureWorksAdminEmail,
                            Email = Constants.UserAdventureWorksAdminEmail,
                            //CompanyId = 3,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultAdventureWorksAdmin = await userManager.CreateAsync(userAdventureWorksAdmin, Constants.UserPassword);

                        var userAdventureWorksManager = new ApplicationUser
                        {
                            UserName = Constants.UserAdventureWorksManagerEmail,
                            Email = Constants.UserAdventureWorksManagerEmail,
                            //CompanyId = 3,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultAdventureWorksManager = await userManager.CreateAsync(userAdventureWorksManager, Constants.UserPassword);

                        var userAdventureWorksUser = new ApplicationUser
                        {
                            UserName = Constants.UserAdventureWorksUserEmail,
                            Email = Constants.UserAdventureWorksUserEmail,
                            //CompanyId = 3,
                            //FirstName = Guid.NewGuid().ToString(),
                            //MiddleName = Guid.NewGuid().ToString(),
                            //LastName = Guid.NewGuid().ToString()
                        };
                        var resultAdventureWorksUser = await userManager.CreateAsync(userAdventureWorksUser, Constants.UserPassword);

                        context.SaveChanges();

                        await userManager.AddToRoleAsync(userAdventureWorksAdmin, Identity.RolesAdministrators);
                        await userManager.AddToRoleAsync(userAdventureWorksManager, Identity.RolesManagers);
                        await userManager.AddToRoleAsync(userAdventureWorksUser, Identity.RolesUsers);
                        #endregion

                        //var orderViewListClime = new Claim("Order", "ViewList");
                        //await userManager.AddClaimAsync(userNorthwind, orderViewListClime);

                        //if (resultMarketPlaceAdmin.Succeeded)
                        //{
                        //    DataStoreInit.CreateSampleData
                        //        (
                        //        context, 

                        //        userMarketPlaceAdmin.Id,
                        //        userMarketPlaceManager.Id,
                        //        userMarketPlaceUser.Id,

                        //        userNorthwindAdmin.Id,
                        //        userNorthwindManager.Id,
                        //        userNorthwindUser.Id,

                        //        userAdventureWorksAdmin.Id,
                        //        userAdventureWorksManager.Id,
                        //        userAdventureWorksUser.Id

                        //        );
                        //}
                    }
                }
            }
        }
    }
}