﻿ // ReSharper disable once CheckNamespace
namespace MarketPlace.Development
{
    public class Constants
    {
        public const string UserPassword = "P@ssw0rd";

        public const string UserMarketPlaceAdminEmail = "admin@marketplace.ru";
        public const string UserMarketPlaceManagerEmail = "manager@marketplace.ru";
        public const string UserMarketPlaceUserEmail = "user@marketplace.ru";

        public const string UserNorthwindAdminEmail = "admin@northwind.ru";
        public const string UserNorthwindManagerEmail = "manager@northwind.ru";
        public const string UserNorthwindUserEmail = "user@northwind.ru";

        public const string UserAdventureWorksAdminEmail = "admin@adventureworks.ru";
        public const string UserAdventureWorksManagerEmail = "manager@adventureworks.ru";
        public const string UserAdventureWorksUserEmail = "user@adventureworks.ru";

        public const string ProductWindowSpecificationLisk = "https://drive.google.com/file/d/0B0oJBoisMsn2dThMcTZQYzVvbVU/view?usp=sharing";
    }
}
