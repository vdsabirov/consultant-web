﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceService', ['ngResource'])
        .factory('ProductList', ProductList)
        .factory('Order', Order)
        .factory('Product', Product)
        .factory('Auction', Auction)
        .factory('Offer', Offer)
        .factory('ProductDoors', ProductDoors)
        .factory('ProductWindows', ProductWindows);

    Order.$inject = ['$resource'];
    Product.$inject = ['$resource'];
    ProductList.$inject = ['$http', '$q'];
    ProductDoors.$inject = ['$resource'];
    ProductWindows.$inject = ['$resource'];
    Auction.$inject = ['$resource'];
    Offer.$inject = ['$resource'];
    
    function Auction($resource) {
        return $resource('/api/auctions/:id');
    }

    function Offer($resource) {
        return $resource('/api/offers/:id');
    }

    function Order($resource) {
        return $resource('/api/orders/:id');
    }

    function Product($resource) {
        return $resource('/api/products/:id');
    }

    function ProductDoors($resource) {
        return $resource('/api/products/doors/:id');
    }

    function ProductWindows($resource) {
        return $resource('/api/products/windows/:id');
    }

    function ProductList($http, $q) {
        return ({ products: products });

        function products(id) {
            var request = $http({
                method: "get",
                url: "/api/products/getail/" + id,
                params: {
                    
                },
                data: {
                    
                }
            });
            return (request.then(function (response) {
                var data = response.data;
                return data
            }));
        }
    }

})();