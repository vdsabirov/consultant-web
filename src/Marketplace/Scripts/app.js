﻿(function () {
    'use strict'; 

    config.$inject = ['$routeProvider', '$locationProvider']; 

    angular.module('MarketPlaceApp', [
        'ngRoute', 'ui.bootstrap', 'MarketPlaceService'
    ]).config(config);

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/Views/Index.html',
                controller: 'HomeController'
            })
            .when('', {
                templateUrl: '/Views/Index.html',
                controller: 'HomeController'
            })
            .when('/orders/list', {
                templateUrl: 'Views/Orders/List.html',
                controller: 'OrdersListController'
            })
            .when('/orders/add', {
                templateUrl: 'Views/Orders/Add.html',
                controller: 'OrdersAddController'
            })
            .when('/orders/edit/:id', {
                templateUrl: 'Views/Orders/Edit.html',
                controller: 'OrdersEditController'
            })
            .when('/orders/delete/:id', {
                templateUrl: 'Views/Orders/Delete.html',
                controller: 'OrdersDeleteController'
            })
            .when('/orders/details/:id', {
                templateUrl: 'Views/Orders/Details.html',
                controller: 'OrdersDetailsController'
            })
            .when('/products/list', {
                templateUrl: '/Views/Products/List.html',
                controller: 'ProductsListController'
            })
            .when('/products/add/:idOrder', {
                templateUrl: '/Views/Products/Add.html',
                controller: 'ProductsAddController'
            })
            .when('/products/delete/:id', {
                templateUrl: '/Views/Products/Delete.html',
                controller: 'ProductsDeleteController'
            })
            .when('/products/edit/:id', {
                templateUrl: '/Views/Products/Edit.html',
                controller: 'ProductsEditController'
            })
            .when('/products/details/:id', {
                templateUrl: '/Views/Products/Details.html',
                controller: 'ProductsDetailsController'
            })
            .when('/products/doors/add/:idProduct', {
                templateUrl: '/Views/Products/Doors/Add.html',
                controller: 'ProductDoorsAddController'
            })
            .when('/products/doors/edit/:id', {
                templateUrl: '/Views/Products/Doors/Edit.html',
                controller: 'ProductDoorsEditController'
            })
            .when('/products/windows/add/:idProduct', {
                templateUrl: '/Views/Products/Windows/Add.html',
                controller: 'ProductWindowsAddController'
            })
            .when('/products/windows/edit/:id', {
                templateUrl: '/Views/Products/Windows/Edit.html',
                controller: 'ProductWindowsEditController'
            })
            .when('/auctions/add/:id', {
                templateUrl: '/Views/Auctions/Add.html',
                controller: 'AuctionAddController'
            })
            .when('/offers/list', {
                templateUrl: '/Views/Offers/List.html',
                controller: 'OffersListController'
            })
            .when('/Manage', {
                redirectTo: '/Manage'
            })
        ;

        $locationProvider.html5Mode(true); 
    }

})();