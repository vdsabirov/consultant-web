﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('OrdersListController', OrdersListController)
        .controller('OrdersAddController', OrdersAddController)
        .controller('OrdersEditController', OrdersEditController)
        .controller('OrdersDetailsController', OrdersDetailsController)
        .controller('OrdersEditStateController', OrdersEditStateController)
        .controller('OrdersDeleteController', OrdersDeleteController);

    /* Список всех заказов  */
    OrdersListController.$inject = ['$scope', 'Order'];

    function OrdersListController($scope, Order) {
        $scope.orders = Order.query();
    }

    /* Подробное описание заказа  */
    OrdersDetailsController.$inject = ['$scope', '$routeParams', '$location', 'Order', 'Product'];

    function OrdersDetailsController($scope, $routeParams, $location, Order, Product) {
        $scope.order = Order.get({ id: $routeParams.id });
        $scope.products = Product.query();
    }

    /* Добавление нового заказа */
    OrdersAddController.$inject = ['$scope', '$location', 'Order'];

    function OrdersAddController($scope, $location, Order) {
        $scope.order = new Order();
        $scope.add = function () {
            $scope.order.$save(
				// success
				function () {
				    $location.path('/orders/list/');
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактирование заказа */
    OrdersEditController.$inject = ['$scope', '$routeParams', '$location', 'Order', 'Product'];

    function OrdersEditController($scope, $routeParams, $location, Order, Product) {
        $scope.order = Order.get({ id: $routeParams.id });

        $scope.products = Product.query();

        $scope.edit = function () {
            $scope.order.$save(
				// success
				function () {
				    $location.path('/orders/list');
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактирование статуса заказа */
    OrdersEditStateController.$inject = ['$scope', '$routeParams', '$location', 'Order', 'Product'];

    function OrdersEditStateController($scope, $routeParams, $location, Order, Product) {
        $scope.order = Order.get({ id: $routeParams.id });

        $scope.products = Product.query();

        $scope.order.OrderStateId = 2;

        $scope.editState = function () {
            $scope.order.$save(
				// success
				function () {
				    $location.path('/orders/list');
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Удаление заказа  */
    OrdersDeleteController.$inject = ['$scope', '$routeParams', '$location', 'Order'];

    function OrdersDeleteController($scope, $routeParams, $location, Order) {
        $scope.order = Order.get({ id: $routeParams.id });

        $scope.remove = function () {
            $scope.order.$remove({ id: $scope.order.Id }, function () {
                $location.path('/orders/list');
            });
        };
    }

    /* Валидатор (отображение ошибок) */

    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();