﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('DatePickerController', DatePickerController)
        .controller('HomeController', HomeController)
    ;

    /* Home Controller  */
    HomeController.$inject = ['$scope'];

    function HomeController($scope) {
        
    }

    /* DatePicker Delete Controller  */
    DatePickerController.$inject = ['$scope'];

    function DatePickerController($scope) {
    	$scope.open = function ($event) {
    		$event.preventDefault();
    		$event.stopPropagation();

    		$scope.opened = true;
    	};
    }


    
	/* Utility Functions */

    function _showValidationErrors($scope, error) {
    	$scope.validationErrors = [];
    	if (error.data && angular.isObject(error.data)) {
    		for (var key in error.data) {
    			$scope.validationErrors.push(error.data[key][0]);
    		}
    	} else {
    		$scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
    	};
    }

})();