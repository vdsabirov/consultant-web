﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')

        .controller('ProductsListController', ProductsListController)
        .controller('ProductsAddController', ProductsAddController)
        .controller('ProductsEditController', ProductsEditController)
        .controller('ProductsDeleteController', ProductsDeleteController)
        .controller('ProductsDetailsController', ProductsDetailsController);

    /* Список продуктов */
    ProductsListController.$inject = ['$scope', '$routeParams', '$location', 'Product'];

    function ProductsListController($scope, $routeParams, $location, Product) {
        $scope.products = Product.query();
    }

    /* Подробное описание продукта  */
    ProductsDetailsController.$inject = ['$scope', '$routeParams', '$location', 'Product', 'ProductDoors', 'ProductWindows'];

    function ProductsDetailsController($scope, $routeParams, $location, Product, ProductDoors, ProductWindows) {
        $scope.product = Product.get({ id: $routeParams.id });
        $scope.productDoors = ProductDoors.query();
        $scope.productWindows = ProductWindows.query();
    }

    /* Добавить новый продукт */
    ProductsAddController.$inject = ['$scope', '$routeParams', '$location', 'Product'];

    function ProductsAddController($scope, $routeParams, $location, Product) {
        $scope.product = new Product();
        $scope.product.OrderId = $routeParams.idOrder;
        $scope.add = function () {
            $scope.product.$save(
				// успешное создание продукта
				function () {
				    $location.path('/orders/edit/' + $routeParams.idOrder);
				},
				// ошибка
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактировать продукт */
    ProductsEditController.$inject = ['$scope', '$routeParams', '$location', 'Product', 'ProductDoors', 'ProductWindows'];

    function ProductsEditController($scope, $routeParams, $location, Product, ProductDoors, ProductWindows) {
        $scope.product = Product.get({ id: $routeParams.id });

        $scope.productDoors = ProductDoors.query();
        $scope.productWindows = ProductWindows.query();

        $scope.edit = function () {
            $scope.product.$save(
				// success
				function () {
				    $location.path('/orders/edit/' + $scope.product.OrderId);
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Удалить продукт */
    ProductsDeleteController.$inject = ['$scope', '$routeParams', '$location', 'Product'];

    function ProductsDeleteController($scope, $routeParams, $location, Product) {
        $scope.product = Product.get({ id: $routeParams.id });
        $scope.remove = function () {
            $scope.product.$remove({ id: $scope.product.Id }, function () {
                $location.path('/orders/edit/' + $scope.product.OrderId);
            });
        };
    }
    
    /* Отображение ошибок */
    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();