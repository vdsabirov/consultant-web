﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('ProductWindowsAddController', ProductWindowsAddController)
        .controller('ProductWindowsEditController', ProductWindowsEditController);

    /* Добавление нового продукта типа "Окна" */
    ProductWindowsAddController.$inject = ['$scope', '$location', '$routeParams', 'ProductWindows'];

    function ProductWindowsAddController($scope, $location, $routeParams, ProductWindows) {
        $scope.productWindow = new ProductWindows();
        $scope.productWindow.ProductId = $routeParams.idProduct;
        $scope.add = function () {
            $scope.productWindow.$save(
				// success
				function () {
				    $location.path('/products/edit/' + $routeParams.idProduct);
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактирование продукта типа "Окна" */
    ProductWindowsEditController.$inject = ['$scope', '$routeParams', '$location', 'ProductWindows'];

    function ProductWindowsEditController($scope, $routeParams, $location, ProductWindows) {
        $scope.productWindow = ProductWindows.get({ id: $routeParams.id });
        $scope.edit = function () {
            $scope.productWindow.$save(
				// success
				function () {
				    $location.path('/products/edit/' + $scope.productWindow.ProductId);
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Валидатор (отображение ошибок) */
    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();