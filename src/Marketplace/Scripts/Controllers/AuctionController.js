﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('AuctionListController', AuctionListController)
        .controller('AuctionAddController', AuctionAddController)
        .controller('AuctionEditController', AuctionEditController)
        .controller('AuctionDeleteController', AuctionDeleteController);

    /* Список всех заказов  */
    AuctionListController.$inject = ['$scope', 'Auction'];

    function AuctionListController($scope, Auction) {
        $scope.auctions = Auction.query();
    }

    /* Добавление нового аукциона */
    AuctionAddController.$inject = ['$scope', '$location', '$routeParams', 'Auction'];

    function AuctionAddController($scope, $location, $routeParams, Auction) {
        $scope.auction = new Auction();

        $scope.auction.IsOpen = true;
        $scope.auction.OrderId = $routeParams.id;

        $scope.add = function () {
            $scope.auction.$save(
				// success
				function () {
				    $location.path('/orders/list/');
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактирование аукциона */
    AuctionEditController.$inject = ['$scope', '$routeParams', '$location', 'Auction'];

    function AuctionEditController($scope, $routeParams, $location, Auction) {
        $scope.auction = Auction.get({ id: $routeParams.id });

        $scope.edit = function () {
            $scope.auction.$save(
				// success
				function () {
				    $location.path('/orders/list');
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Удаление аукциона  */
    AuctionDeleteController.$inject = ['$scope', '$routeParams', '$location', 'Auction'];

    function AuctionDeleteController($scope, $routeParams, $location, Auction) {
        $scope.auction = Auction.get({ id: $routeParams.id });

        $scope.remove = function () {
            $scope.auction.$remove({ id: $scope.auction.Id }, function () {
                $location.path('/orders/list');
            });
        };
    }

    /* Валидатор (отображение ошибок) */
    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();