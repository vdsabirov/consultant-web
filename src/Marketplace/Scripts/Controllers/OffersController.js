﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('OffersListController', OffersListController);

    /* Список всех предложений  */
    OffersListController.$inject = ['$scope', 'Offer'];

    function OffersListController($scope, Offer) {
        $scope.offers = Offer.query();
    }

    /* Валидатор (отображение ошибок) */
    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();