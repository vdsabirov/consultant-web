﻿(function () {
    'use strict';

    angular
        .module('MarketPlaceApp')
        .controller('ProductDoorsAddController', ProductDoorsAddController)
        .controller('ProductDoorsEditController', ProductDoorsEditController);

    /* Добавление нового продукта типа "Двери" */
    ProductDoorsAddController.$inject = ['$scope', '$location', '$routeParams', 'ProductDoors'];

    function ProductDoorsAddController($scope, $location, $routeParams, ProductDoors) {
        $scope.productDoor = new ProductDoors();
        $scope.productDoor.ProductId = $routeParams.idProduct;
        $scope.add = function () {
            $scope.productDoor.$save(
				// success
				function () {
				    $location.path('/products/edit/' + $routeParams.idProduct);
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Редактирование продукта типа "Двери" */
    ProductDoorsEditController.$inject = ['$scope', '$routeParams', '$location', 'ProductDoors'];

    function ProductDoorsEditController($scope, $routeParams, $location, ProductDoors) {
        $scope.productDoor = ProductDoors.get({ id: $routeParams.id });
        $scope.edit = function () {
            $scope.productDoor.$save(
				// success
				function () {
				    $location.path('/products/edit/' + $scope.productDoor.ProductId);
				},
				// error
				function (error) {
				    _showValidationErrors($scope, error);
				}
			);
        };
    }

    /* Валидатор (отображение ошибок) */
    function _showValidationErrors($scope, error) {
        $scope.validationErrors = [];
        if (error.data && angular.isObject(error.data)) {
            for (var key in error.data) {
                $scope.validationErrors.push(error.data[key][0]);
            }
        } else {
            $scope.validationErrors.push('Возникла ошибка во время выполнения операции.');
        };
    }

})();