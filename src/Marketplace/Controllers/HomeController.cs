﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace MarketPlace.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //if (User.Identity.IsAuthenticated)
            //    return View();
            //else
            //    return Redirect("~/Account/Login");
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
