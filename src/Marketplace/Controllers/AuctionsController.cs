using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Produces("application/json")]
    [Route("rest/Auctions")]
    public class AuctionsController : Controller
    {
        private readonly IAuctionService _auctionService;
        private readonly IErrorService _errorService;

        public AuctionsController(
            IErrorService errorService,
            IAuctionService auctionService
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (auctionService == null) throw new ArgumentNullException(nameof(auctionService));
            _errorService = errorService;
            _auctionService = auctionService;
            _auctionService.ErrorService = _errorService;
        }

        // GET: api/Auctions
        [HttpGet]
        public IEnumerable<Auction> GetAuctions()
        {
            return _auctionService.GetAuctions();
        }

        // GET: api/Auctions/5
        [HttpGet("{id}", Name = "GetAuction")]
        public IActionResult GetAuction([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var auction = _auctionService.GetAuction(id);

            if (auction == null)
            {
                return HttpNotFound();
            }

            return Ok(auction);
        }

        // PUT: api/Auctions/5
        [HttpPut("{id}")]
        public IActionResult PutAuction(int id, [FromBody] Auction auction)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != auction.Id)
            {
                return HttpBadRequest();
            }

            try
            {
                _auctionService.Update(auction);
            }
            catch (Exception)
            {
                if (!AuctionExists(id))
                {
                    return HttpNotFound();
                }
                throw;
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Auctions
        [HttpPost]
        public IActionResult PostAuction([FromBody] Auction auction)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
                _auctionService.Update(auction);
            }
            catch (Exception)
            {
                if (AuctionExists(auction.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }

            return CreatedAtRoute("GetAuction", new {id = auction.Id}, auction);
        }

        // DELETE: api/Auctions/5
        [HttpDelete("{id}")]
        public IActionResult DeleteAuction(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var auction = _auctionService.GetAuction(id);
            if (auction == null)
            {
                return HttpNotFound();
            }

            _auctionService.Delete(id);

            return Ok(auction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _auctionService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool AuctionExists(int id)
        {
            var auction = _auctionService.GetAuction(id);
            return auction != null && auction.Id > 0;
        }
    }
}