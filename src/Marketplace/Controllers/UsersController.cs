﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Users")]
    public class UsersController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IUserService _userService;

        public UsersController(
            IErrorService errorService,
            IUserService userService
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (userService == null) throw new ArgumentNullException(nameof(userService));
            _errorService = errorService;
            _userService = userService;
            _userService.ErrorService = _errorService;
        }

        [HttpGet]
        public List<User> GetUsers()
        {
            return _userService.GetUsers();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }
    }
}