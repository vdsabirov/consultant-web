﻿using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/OrderStates")]
    public class OrderStatesController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IOrderService _orderService;

        public OrderStatesController(
            IOrderService orderService,
            IErrorService errorService
            )
        {
            if (orderService == null) throw new ArgumentNullException(nameof(orderService));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _orderService = orderService;
            _errorService = errorService;
            _orderService.ErrorService = _errorService;
        }

        [HttpGet]
        public List<OrderState> GetOrderStates()
        {
            return _orderService.GetOrderStates();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _orderService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }
    }
}