using System;
using System.Collections.Generic;
using System.Linq;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Orders")]
    public class OrdersController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IOrderService _orderService;

        public OrdersController(
            IOrderService orderService,
            IErrorService errorService
            )
        {
            if (orderService == null) throw new ArgumentNullException(nameof(orderService));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _orderService = orderService;
            _errorService = errorService;
            //TODO ����������� � ������� "A circular dependency was detected for the service of type"
            _orderService.ErrorService = _errorService;
        }

        // GET: rest/Orders
        [HttpGet]
        public IEnumerable<Order> GetOrders()
        {
            return _orderService.GetOrders().OrderByDescending(o => o.OrderDate).ThenByDescending(c => c.Code);
        }

        // GET: api/Orders/5
        [HttpGet("{id}", Name = "GetOrder")]
        public IActionResult GetOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var order = _orderService.GetOrderForEdit(id);

            if (order == null)
            {
                return HttpNotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [HttpPut("{id}")]
        public IActionResult PutOrder(int id, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != order.Id)
            {
                return HttpBadRequest();
            }

            try
            {
                _orderService.Update(order);
            }
            catch (Exception)
            {
                if (!OrderExists(id))
                {
                    return HttpNotFound();
                }
                throw;
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Orders
        [HttpPost]
        public IActionResult PostOrder([FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
                _orderService.Update(order);
            }
            catch (Exception)
            {
                if (OrderExists(order.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }

            return CreatedAtRoute("GetOrder", new {id = order.Id}, order);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public IActionResult DeleteOrder(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var order = _orderService.GetOrderForEdit(id);
            if (order == null || order.Id <= 0)
            {
                return HttpNotFound();
            }

            _orderService.Delete(order.Id);

            return Ok(order);
        }

        // /rest/Orders/SetOrderState/1/2
        [Route("~/rest/Orders/SetOrderState/{orderId:int}/{orderStateId:int}")]
        public IActionResult SetOrderState(int orderId, int orderStateId)
        {
            _orderService.UpdateState(orderId, (OrdersStates) orderStateId);
            var order = _orderService.GetOrderForRead(orderId);
            return Ok(order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _orderService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(int id)
        {
            var order = _orderService.GetOrderForRead(id);
            return order != null && order.Id > 0;
        }
    }
}