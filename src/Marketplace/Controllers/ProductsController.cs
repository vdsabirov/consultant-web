using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("rest/Products")]
    public class ProductsController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IProductServices _productService;

        public ProductsController(
            IProductServices productService,
            IErrorService errorService
            )
        {
            if (productService == null) throw new ArgumentNullException(nameof(productService));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _productService = productService;
            _errorService = errorService;
            _productService.ErrorService = _errorService;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            return _productService.GetProducts();
        }

        // GET: api/Products/5
        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var product = _productService.GetProduct(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public IActionResult PutProduct(int id, [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return HttpBadRequest();
            }

            try
            {
                _productService.Update(product);
            }
            catch (Exception)
            {
                if (!ProductExists(id))
                {
                    return HttpNotFound();
                }
                throw;
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Products
        [HttpPost]
        public IActionResult PostProduct([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
                _productService.Update(product);
            }
            catch (Exception)
            {
                if (ProductExists(product.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }

            return CreatedAtRoute("GetProduct", new {id = product.Id}, product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var product = _productService.GetProduct(id);
            if (product == null || product.Id <= 0)
            {
                return HttpNotFound();
            }

            _productService.Delete(product.Id);

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _productService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            var product = _productService.GetProduct(id);
            return product != null && product.Id > 0;
        }
    }
}