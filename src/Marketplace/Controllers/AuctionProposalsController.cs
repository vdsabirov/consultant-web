using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Produces("application/json")]
    [Route("rest/AuctionProposals")]
    public class AuctionProposalsController : Controller
    {
        private readonly IAuctionProposalService _auctionProposalService;

        private readonly IErrorService _errorService;

        public AuctionProposalsController(
            IErrorService errorService,
            IAuctionProposalService auctionProposalService
            )
        {
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            if (auctionProposalService == null) throw new ArgumentNullException(nameof(auctionProposalService));
            _errorService = errorService;
            _auctionProposalService = auctionProposalService;
            _auctionProposalService.ErrorService = _errorService;
        }

        // GET: api/AuctionProposals
        [HttpGet]
        public IEnumerable<AuctionProposal> GetAuctionРroposals()
        {
            return _auctionProposalService.GetAuctionРroposals();
        }

        // GET: api/AuctionProposals/5
        [HttpGet("{id}", Name = "GetAuctionProposal")]
        public IActionResult GetAuctionProposal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var auctionProposal = _auctionProposalService.GetAuctionРroposal(id);

            if (auctionProposal == null || auctionProposal.Id <= 0)
            {
                return HttpNotFound();
            }

            return Ok(auctionProposal);
        }

        // PUT: api/AuctionProposals/5
        [HttpPut("{id}")]
        public IActionResult PutAuctionProposal(int id, [FromBody] AuctionProposal auctionProposal)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != auctionProposal.Id)
            {
                return HttpBadRequest();
            }

            try
            {
                _auctionProposalService.Update(auctionProposal);
            }
            catch (Exception)
            {
                if (!AuctionProposalExists(id))
                {
                    return HttpNotFound();
                }
                throw;
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/AuctionProposals
        [HttpPost]
        public IActionResult PostAuctionProposal([FromBody] AuctionProposal auctionProposal)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
                _auctionProposalService.Update(auctionProposal);
            }
            catch (Exception)
            {
                if (AuctionProposalExists(auctionProposal.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }

            return CreatedAtRoute("GetAuctionProposal", new {id = auctionProposal.Id}, auctionProposal);
        }

        // DELETE: api/AuctionProposals/5
        [HttpDelete("{id}")]
        public IActionResult DeleteAuctionProposal(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var auctionProposal = _auctionProposalService.GetAuctionРroposal(id);
            if (auctionProposal == null || auctionProposal.Id <= 0)
            {
                return HttpNotFound();
            }

            _auctionProposalService.Delete(auctionProposal.Id);

            return Ok(auctionProposal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _auctionProposalService.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool AuctionProposalExists(int id)
        {
            var auctionProposal = _auctionProposalService.GetAuctionРroposal(id);
            return auctionProposal != null && auctionProposal.Id > 0;
        }
    }
}