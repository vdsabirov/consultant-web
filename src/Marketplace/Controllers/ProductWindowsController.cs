using System;
using System.Collections.Generic;
using MarketPlace.Infrastructure.Models;
using MarketPlace.Infrastructure.Services;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;

namespace MarketPlace.Controllers
{
    [Produces("application/json")]
    [Route("rest/ProductWindows")]
    public class ProductWindowsController : Controller
    {
        private readonly IErrorService _errorService;
        private readonly IProductWindowServices _productWindowServices;

        public ProductWindowsController(
            IProductWindowServices productWindowServices,
            IErrorService errorService
            )
        {
            if (productWindowServices == null) throw new ArgumentNullException(nameof(productWindowServices));
            if (errorService == null) throw new ArgumentNullException(nameof(errorService));
            _productWindowServices = productWindowServices;
            _errorService = errorService;
            _productWindowServices.ErrorService = _errorService;
        }

        // GET: api/ProductWindows
        [HttpGet]
        public IEnumerable<ProductWindow> GetProductWindows()
        {
            return _productWindowServices.GetProducts();
        }

        [Route("~/rest/ProductWindowBrands")]
        public IEnumerable<ProductWindowBrand> GetProductWindowBrands()
        {
            return _productWindowServices.GetProductWindowBrands();
        }

        [Route("~/rest/ProductWindowFindings")]
        public IEnumerable<ProductWindowFinding> GetProductWindowFindings()
        {
            return _productWindowServices.GetProductWindowFindings();
        }

        [Route("~/rest/ProductWindowGlazingTypes")]
        public IEnumerable<ProductWindowGlazingType> GetProductWindowGlazingTypes()
        {
            return _productWindowServices.GetProductWindowGlazingTypes();
        }

        [Route("~/rest/ProductWindowLaminatings")]
        public IEnumerable<ProductWindowLaminating> GetProductWindowLaminatings()
        {
            return _productWindowServices.GetProductWindowLaminatings();
        }

        [Route("~/rest/ProductWindowSystems")]
        public IEnumerable<ProductWindowSystem> GetProductWindowSystems()
        {
            return _productWindowServices.GetProductWindowSystems();
        }

        [Route("~/rest/ProductWindowTintings")]
        public IEnumerable<ProductWindowTinting> GetProductWindowTintings()
        {
            return _productWindowServices.GetProductWindowTintings();
        }

        [Route("~/rest/ProductWindowsills")]
        public IEnumerable<ProductWindowsill> GetProductWindowsills()
        {
            return _productWindowServices.GetProductWindowills();
        }

        // GET: api/ProductWindows/5
        [HttpGet("{id}", Name = "GetProductWindow")]
        public IActionResult GetProductWindow([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var productWindow = _productWindowServices.GetProduct(id);

            if (productWindow == null || productWindow.Id <= 0)
            {
                return HttpNotFound();
            }

            return Ok(productWindow);
        }

        // PUT: api/ProductWindows/5
        [HttpPut("{id}")]
        public IActionResult PutProductWindow(int id, [FromBody] ProductWindow productWindow)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != productWindow.Id)
            {
                return HttpBadRequest();
            }

            try
            {
                _productWindowServices.Update(productWindow);
            }
            catch (Exception)
            {
                if (!ProductWindowExists(id))
                {
                    return HttpNotFound();
                }
                throw;
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/ProductWindows
        [HttpPost]
        public IActionResult PostProductWindow([FromBody] ProductWindow productWindow)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            try
            {
                _productWindowServices.Update(productWindow);
            }
            catch (Exception)
            {
                if (ProductWindowExists(productWindow.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                throw;
            }

            return CreatedAtRoute("GetProductWindow", new {id = productWindow.Id}, productWindow);
        }

        // DELETE: api/ProductWindows/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProductWindow(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            var productWindow = _productWindowServices.GetProduct(id);
            if (productWindow == null || productWindow.Id <= 0)
            {
                return HttpNotFound();
            }

            _productWindowServices.Delete(productWindow.Id);

            return Ok(productWindow);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _productWindowServices.Dispose(disposing);
            }
            base.Dispose(disposing);
        }

        private bool ProductWindowExists(int id)
        {
            var productWindow = _productWindowServices.GetProduct(id);
            return productWindow != null && productWindow.Id > 0;
        }
    }
}